#ifndef EIGENVALUECALCULATORSOLVER_CFACTORIZATIONSIZETORESTARTATOPTIONHANDLER_H
#define EIGENVALUECALCULATORSOLVER_CFACTORIZATIONSIZETORESTARTATOPTIONHANDLER_H

#include "./COptionHandler.h"

class CFactorizationSizeToRestartAtOptionHandler : public COptionHandler {
public:
  explicit CFactorizationSizeToRestartAtOptionHandler(std::optional<size_t> &factorizationSizeToRestartAt);

  // argumentsCnt must be >= 1
  void Handle(int &argumentsCnt, char **&argumentsVector) override;

  static constexpr const char OptionShortName[] = "-m";
  static constexpr const char OptionFullName[] = "--factorization-size-to-restart-at";
  static constexpr const char OptionVerboseName[] = "Factorization size to restart at";
  static constexpr const char NumberFormat[] = "%lu";
protected:
  std::optional<size_t> &m_FactorizationSizeToRestartAt;
  size_t m_Number;
};

#endif //EIGENVALUECALCULATORSOLVER_CFACTORIZATIONSIZETORESTARTATOPTIONHANDLER_H
