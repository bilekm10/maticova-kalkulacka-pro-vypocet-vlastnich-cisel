#ifndef EIGENVALUECALCULATORSOLVER_PRINTFUNCTIONS_H
#define EIGENVALUECALCULATORSOLVER_PRINTFUNCTIONS_H

#include <iostream>
#include <iomanip>
#include <complex>
#include <string>

// from https://stackoverflow.com/questions/9158150/colored-output-in-c
//the following are UBUNTU/LINUX, and MacOS ONLY terminal color codes.
#define RESET   "\033[0m"
#define BLACK   "\033[30m"      /* Black */
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */
#define BLUE    "\033[34m"      /* Blue */
#define MAGENTA "\033[35m"      /* Magenta */
#define CYAN    "\033[36m"      /* Cyan */
#define WHITE   "\033[37m"      /* White */
#define BOLDBLACK   "\033[1m\033[30m"      /* Bold Black */
#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define BOLDYELLOW  "\033[1m\033[33m"      /* Bold Yellow */
#define BOLDBLUE    "\033[1m\033[34m"      /* Bold Blue */
#define BOLDMAGENTA "\033[1m\033[35m"      /* Bold Magenta */
#define BOLDCYAN    "\033[1m\033[36m"      /* Bold Cyan */
#define BOLDWHITE   "\033[1m\033[37m"      /* Bold White */

#define DIAGONAL_COLOR BOLDGREEN
#define SUBDIAGONAL_COLOR BOLDRED

#define ALMOSTZERO_COLOR BOLDCYAN
#define ZERO_COLOR CYAN

#define TRUE_COLOR GREEN
#define FALSE_COLOR RED

template<typename MatrixType>
inline void printMatrix(const MatrixType &matrix,
                        const std::string &name = "",
                        const int manipWidth = 10,
                        const int manipPrecision = 2,
                        const double almostZero = 1e-15,
                        const bool printSize = true) {
  if (!name.empty()) {
    std::cout << name << " ";
  }
  if (printSize) {
    std::cout << "size:" << matrix.GetRowsCnt() << " x " << matrix.GetColumnsCnt() << "\n";
  }
  for (int row = 0; row < matrix.GetRowsCnt(); ++row) {
    for (int column = 0; column < matrix.GetColumnsCnt(); ++column) {
      if (column != 0) {
        //std::cout << "  ";
      }
      double element = matrix.At(row, column);
      if (std::abs(element) == 0) {
        std::cout << ZERO_COLOR << std::setw(manipWidth) << std::scientific << std::setprecision(manipPrecision)
                  << element << RESET;
      } else if (std::abs(element) < almostZero) {
        std::cout << ALMOSTZERO_COLOR << std::setw(manipWidth) << std::scientific << std::setprecision(manipPrecision)
                  << element << RESET;
      } else if (row == column) {
        std::cout << DIAGONAL_COLOR << std::setw(manipWidth) << std::scientific << std::setprecision(manipPrecision)
                  << element << RESET;
      } else if (column + 1 == row) {
        std::cout << SUBDIAGONAL_COLOR << std::setw(manipWidth) << std::scientific << std::setprecision(manipPrecision)
                  << element << RESET;
      } else {
        std::cout << std::setw(manipWidth) << std::scientific << std::setprecision(manipPrecision) << element;
      }
    }
    std::cout << "\n";
  }
  std::cout.flush();
}

/*inline void printMatrixTransposed(const std::vector<vector<double>> & M,
                           const std::string & name = "",
                           const int manipWidth = 10,
                           const int manipPrecision = 2,
                           const double almostZero = 1e-15,
                           const bool printSize = true)
{
  vector<vector<double>> newM(M[0].size(), std::vector<double>(M.size()));
  for(int i = 0; i < newM.size(); ++i)
  {
    for(int j = 0; j < newM[0].size(); ++j)
    {
      newM[i][j] = M[j][i];
    }
  }
  return printMatrix(newM, name, manipWidth, manipPrecision, almostZero, printSize);
}*/

/*inline void printRealVector(const std::vector<double> & vec,
                     bool inRow = false,
                     const std::string & name = "",
                     const int manipWidth = 10,
                     const int manipPrecision = 2,
                     const double almostZero = 1e-15,
                     const bool printSize = true)
{
  if(inRow)
  {
    printMatrix({{vec}}, name, manipWidth, manipPrecision, almostZero, false);
  }
  else
  {
    printMatrixTransposed({{vec}}, name, manipWidth, manipPrecision, almostZero, false);
  }
}*/

template<typename Vector>
//void printComplexVector(const Vector & vec,
inline void printVector(const Vector &vec,
                        const std::string &separator = "\n",
                        const std::string &name = "",
                        const int manipWidth = 10,
                        const int manipPrecision = 17,
                        double almostZero = 1e-15,
                        bool printSize = true,
                        std::ostream &os = std::cout) {
  if (!name.empty()) {
    os << name << "  "; // separator
  }
  if (printSize) {
    os << "size: " << vec.GetSize() << "\n";
  }
  for (size_t i = 0; i < vec.GetSize(); ++i) {
    if (i != 0) {
      //os << "  ";
    }
    if (std::abs(vec.At(i)) == 0) {
      os << ZERO_COLOR << std::setw(manipWidth) << std::scientific << std::setprecision(manipPrecision) << vec.At(i)
         << RESET;
    } else if (std::abs(vec.At(i)) < almostZero) {
      os << ALMOSTZERO_COLOR << std::setw(manipWidth) << std::scientific << std::setprecision(manipPrecision)
         << vec.At(i) << RESET;
    } else {
      os << std::setw(manipWidth) << std::scientific << std::setprecision(manipPrecision) << vec.At(i);
    }
    os << separator;
  }
  //os << "\n";
  os.flush();
}

#endif //EIGENVALUECALCULATORSOLVER_PRINTFUNCTIONS_H
