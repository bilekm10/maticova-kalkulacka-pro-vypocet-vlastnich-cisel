#ifndef EIGENVALUECALCULATORGUI_CCOMPUTATIONSTATEPAGE_H
#define EIGENVALUECALCULATORGUI_CCOMPUTATIONSTATEPAGE_H

#include "./CQStringPage.h"
#include "../../../../solver/arnoldi/restarted/EComputationState.h"
#include "../../../../solver/arnoldi/restarted/CImplicitlyRestartedArnoldi.h"
#include "../../../../solver/orthogonalization/basic/CClassicalGramSchmidtOrthogonalization.h"
#include "../../../../solver/orthogonalization/refined/CIterativeOrthogonalization.h"

class CComputationStatePage : public CQStringPage {
  using Orthogonalization = CIterativeOrthogonalization<CClassicalGramSchmidtOrthogonalization>;
  using RestartedArnoldi = CImplicitlyRestartedArnoldi <CMatrixVectorMultiplication<CCompressedSparseRowMatrix>, Orthogonalization>;
public:
  static constexpr const char *NotKnown = "Unknown computation state";
  static constexpr const char *NotInitialized = "Not initialized";;
  static constexpr const char *Computing = "Computing";
  // if some new end state is added and this list is not updated
  static constexpr const char *ComputationEnded = "Computation ended";
  static constexpr const char *Computed = "Computed";
  static constexpr const char *ExceptionallyConverged = "Exceptionally converged";
  static constexpr const char *MaximalNumberOfIterationsReached =
          "Maximal number of iterations reached";
  static constexpr const char *MaximalNumberOfIterationsReachedAndRestarted =
          "Maximal number of iterations reached and restarted";

  explicit CComputationStatePage(EComputationState computationState);

  [[nodiscard]]
  QString Render() const override;

protected:
  static
  bool IsEnd(const char *stateText);

  EComputationState m_ComputationState;
};

#endif //EIGENVALUECALCULATORGUI_CCOMPUTATIONSTATEPAGE_H
