#ifndef EIGENVALUECALCULATORSOLVER_CORTHOGONALITYCHECKER_H
#define EIGENVALUECALCULATORSOLVER_CORTHOGONALITYCHECKER_H

#include <stdexcept>

#include "../../../solver/model/matrix/dense/column/CColumnMatrix.h"
#include "../../../solver/operator/matrixmatrix/CTransposedMatrixMatrixMultiplication.h"
#include "../../print/printFunctions.h"
#include "../equality/matrix/CAreMatricesSame.h"

class COrthogonalityChecker {
public:
  explicit COrthogonalityChecker(double treatAsZero,
                                 bool throwOnError = false,
                                 bool visualOnError = true,
                                 bool visual = false,
                                 const char *name = "");

  void CheckOrthogonality(const CColumnMatrixConst &matrix);

protected:
  double m_TreatAsZero;
  bool m_ThrowOnError;
  bool m_VisualOnError;
  bool m_Visual;
  const char *m_Name;
};

inline COrthogonalityChecker::COrthogonalityChecker(double treatAsZero,
                                                    bool throwOnError,
                                                    bool visualOnError,
                                                    bool visual,
                                                    const char *name)
        : m_TreatAsZero(treatAsZero),
          m_ThrowOnError(throwOnError),
          m_VisualOnError(visualOnError),
          m_Visual(visual),
          m_Name(name) {
}

inline void
COrthogonalityChecker::CheckOrthogonality(const CColumnMatrixConst &matrix) {
  CTransposedMatrixMatrixMultiplication<CColumnMatrix, CColumnMatrix, CColumnMatrix> multiplication{matrix};
  CColumnMatrixDynamicDimensionsHolding result(matrix.GetColumnsCnt(), false);
  multiplication.Multiply(matrix, result);
  if (m_Visual)
    printMatrix(result, m_Name);
  CColumnMatrixDynamicDimensionsHolding identity(result.GetRowsCnt());
  identity.ToIdentity();
  CAreMatricesSame<CColumnMatrix, CColumnMatrix> areMatricesSame{m_TreatAsZero};
  bool areSame = areMatricesSame.AreSame(result, identity);
  if (areSame)
    return;
  if (m_VisualOnError) {
    printf("CheckOrthogonality error:\n");
    printMatrix(result, m_Name);
  }
  if (m_ThrowOnError)
    throw std::invalid_argument(
            std::string("CheckOrthogonality error for ") +
            m_Name +
            " m_TreatAsZero = " +
            std::to_string(m_TreatAsZero * 1e+15) +
            "e-15\n" +
            "MaxAbsElementError = " +
            std::to_string(areMatricesSame.GetMaxAbsElementError() * 1e+15) +
            "e-15");
}

#endif //EIGENVALUECALCULATORSOLVER_CORTHOGONALITYCHECKER_H
