#ifndef EIGENVALUECALCULATORSOLVER_COPTIONHANDLER_H
#define EIGENVALUECALCULATORSOLVER_COPTIONHANDLER_H

#include <iostream>
#include <string>
#include <cstring>
#include <stdexcept>
#include <optional>

class COptionHandler {
public:
  explicit COptionHandler();

  // argumentsCnt must be >= 1
  virtual
  void Handle(int &argumentsCnt, char **&argumentsVector) = 0;

  void SetNextOptionHandler(COptionHandler *optionHandler) {
    m_NextOptionHandler = optionHandler;
  }

protected:
  void MoveToNextOptionHandler(int &argumentsCnt, char **&argumentsVector);

  template<typename NumberType,
          const auto &NumberFormat,
          const auto &OptionShortName>
  bool ScanNumberArgumentShortName(int &argumentsCnt,
                                   char **&argumentsVector,
                                   NumberType &number);

  template<typename NumberType,
          const auto &NumberFormat,
          const auto &OptionFullName>
  bool ScanNumberArgumentFullName(int &argumentsCnt,
                                  char **&argumentsVector,
                                  NumberType &number);

  template<typename NumberType,
          const auto &NumberFormat,
          const auto &OptionFullName,
          const auto &OptionShortName>
  bool ScanNumberArgumentBothNames(int &argumentsCnt,
                                   char **&argumentsVector,
                                   NumberType &number);

  // there must be a space between the short name and the text
  template<const auto &OptionShortName>
  bool ScanTextArgumentShortName(int &argumentsCnt,
                                 char **&argumentsVector,
                                 std::string &str);

  template<const auto &OptionFullName>
  bool ScanTextArgumentFullName(int &argumentsCnt,
                                char **&argumentsVector,
                                std::string &str);

  template<const auto &OptionFullName,
          const auto &OptionShortName>
  bool ScanTextArgumentBothNames(int &argumentsCnt,
                                 char **&argumentsVector,
                                 std::string &str);


  template<const auto &OptionName>
  bool ScanName(int &argumentsCnt,
                char **&argumentsVector);

  template<const auto &OptionShortName,
          const auto &OptionFullName>
  bool ScanBothNames(int &argumentsCnt,
                     char **&argumentsVector);

  static void ThrowOptionDuplicationError(const char optionVerboseName[]);

  static void ThrowRedundantCharactersAfterNumber(const char optionName[]);

  // don't delete
  COptionHandler *m_NextOptionHandler;
};

// https://stackoverflow.com/questions/25890784/computing-length-of-a-c-std::string-at-compile-time-is-this-really-a-constexpr
template<size_t N>
constexpr size_t length(char const (&)[N]) { return N - 1; }


/*
template<size_t S>
using size=std::integral_constant<size_t, S>;

template<class T, size_t N>
constexpr size<N> length( T const(&)[N] ) { return {}; }
template<class T, size_t N>
constexpr size<N> length( std::array<T, N> const& ) { return {}; }

template<class T>
using length_t = decltype(length(std::declval<T>()));
constexpr size_t sum_string_sizes() { return 0; }
template<class...Ts>
constexpr size_t sum_string_sizes( size_t i, Ts... ts ) {
  return (i?i-1:0) + sum_sizes(ts...);
}
template
template<unsigned N1, unsigned N2, class... Us>
constexpr auto
concat(const char(&a1)[N1], const char(&a2)[N2], const Us&... xs)
-> std::array<char, sum_string_sizes( N1, N2, length_t<Us>::value... )+1 >
{
  return concat(a1, concat(a2, xs...));
}
// https://stackoverflow.com/questions/39199564/constexpr-c-string-concatenation-parameters-used-in-a-constexpr-context
*/
// C++17 answer for constexpr c_str concatenation https://stackoverflow.com/questions/28708497/constexpr-to-concatenate-two-or-more-char-strings
// we cannot return a char array from a function, therefore we need a wrapper
template<unsigned N>
struct String {
  char c[N];
};

template<unsigned ...Len>
constexpr auto cat(const char (&...strings)[Len]) {
  constexpr unsigned N = (... + Len) - sizeof...(Len);
  String<N + 1> result = {};
  result.c[N] = '\0';

  char *dst = result.c;
  for (const char *src : {strings...}) {
    for (; *src != '\0'; src++, dst++) {
      *dst = *src;
    }
  }
  return result;
}

template<typename NumberType,
        const auto &NumberFormat,
        const auto &OptionShortName>
bool COptionHandler::ScanNumberArgumentShortName(int &argumentsCnt,
                                                 char **&argumentsVector,
                                                 NumberType &number) {
  constexpr size_t OptionShortNameLen = length(OptionShortName);
  //printf(cat("\\", NumberFormat).c);
  if (strncmp(argumentsVector[0], OptionShortName, OptionShortNameLen) == 0) {
    // todo: throw if some characters remaining https://stackoverflow.com/questions/13503135/get-number-of-characters-read-by-sscanf
    // todo: rather https://stackoverflow.com/questions/8888748/how-to-check-if-given-c-string-or-char-contains-only-digits %*c == 1 or rather store the result and compare
    // todo: try it in main
    int pos;
    if (sscanf(argumentsVector[0] + OptionShortNameLen,
               cat(NumberFormat, "%n").c,
               &number,
               &pos) == 1) {
      if (pos == static_cast<long>(strlen(argumentsVector[0] + OptionShortNameLen))) {
        --argumentsCnt;
        ++argumentsVector;
        return true;
      } else
        ThrowRedundantCharactersAfterNumber(OptionShortName);
    }
    if (argumentsCnt >= 2
        && strlen(argumentsVector[0]) == OptionShortNameLen) {
      if (sscanf(argumentsVector[1],
                 cat(NumberFormat, "%n").c,
                 &number,
                 &pos) == 1) {
        if (pos == static_cast<long>(strlen(argumentsVector[1]))) {
          argumentsCnt -= 2;
          argumentsVector += 2;
          return true;
        } else
          ThrowRedundantCharactersAfterNumber(OptionShortName);
      }
    }
  }
  return false;
}

template<typename NumberType,
        const auto &NumberFormat,
        const auto &OptionFullName>
bool COptionHandler::ScanNumberArgumentFullName(int &argumentsCnt,
                                                char **&argumentsVector,
                                                NumberType &number) {
  constexpr size_t OptionFullNameLen = length(OptionFullName);
  if (strncmp(argumentsVector[0], OptionFullName, OptionFullNameLen) == 0
      && strncmp(argumentsVector[0] + OptionFullNameLen, "=", 1) == 0) {
    int pos;
    if (sscanf(argumentsVector[0] + OptionFullNameLen + 1,
               cat(NumberFormat, "%n").c,
               &number,
               &pos) == 1) {
      if (pos == static_cast<long>(strlen(argumentsVector[0] + OptionFullNameLen + 1))) {
        --argumentsCnt;
        ++argumentsVector;
        return true;
      } else
        ThrowRedundantCharactersAfterNumber(OptionFullName);
    }
  }
  return false;
}

template<typename NumberType,
        const auto &NumberFormat,
        const auto &OptionFullName,
        const auto &OptionShortName>
bool COptionHandler::ScanNumberArgumentBothNames(int &argumentsCnt,
                                                 char **&argumentsVector,
                                                 NumberType &number) {
  return ScanNumberArgumentShortName<
          NumberType,
          NumberFormat,
          OptionShortName>(argumentsCnt,
                           argumentsVector,
                           number)
         || ScanNumberArgumentFullName<
          NumberType,
          NumberFormat,
          OptionFullName>(argumentsCnt,
                          argumentsVector,
                          number);
}

// there must be a space between the short name and the text
template<const auto &OptionShortName>
bool COptionHandler::ScanTextArgumentShortName(int &argumentsCnt,
                                               char **&argumentsVector,
                                               std::string &str) {
  constexpr size_t OptionShortNameLen = length(OptionShortName);
  if (strncmp(argumentsVector[0], OptionShortName, OptionShortNameLen) == 0) {
    if (strlen(argumentsVector[0] + OptionShortNameLen) > 0) {
      /*str = argumentsVector[0] + OptionShortNameLen;
      --argumentsCnt;
      ++argumentsVector;
      return true;*/
      return false;
    }
    if (argumentsCnt >= 2
        && strlen(argumentsVector[0]) == OptionShortNameLen) {
      str = argumentsVector[1];
      argumentsCnt -= 2;
      argumentsVector += 2;
      return true;
    }
  }
  return false;
}

template<const auto &OptionFullName>
bool COptionHandler::ScanTextArgumentFullName(int &argumentsCnt,
                                              char **&argumentsVector,
                                              std::string &str) {
  constexpr size_t OptionFullNameLen = length(OptionFullName);
  if (strncmp(argumentsVector[0], OptionFullName, OptionFullNameLen) == 0
      && strncmp(argumentsVector[0] + OptionFullNameLen, "=", 1) == 0
      && strlen(argumentsVector[0] + OptionFullNameLen + 1) > 0) {
    str = argumentsVector[0] + OptionFullNameLen + 1;
    --argumentsCnt;
    ++argumentsVector;
    return true;
  }
  return false;
}

template<const auto &OptionFullName,
        const auto &OptionShortName>
bool COptionHandler::ScanTextArgumentBothNames(int &argumentsCnt,
                                               char **&argumentsVector,
                                               std::string &str) {
  return ScanTextArgumentShortName<
          OptionShortName>(argumentsCnt,
                           argumentsVector,
                           str)
         || ScanTextArgumentFullName<
          OptionFullName>(argumentsCnt,
                          argumentsVector,
                          str);
}

template<const auto &OptionName>
bool COptionHandler::ScanName(int &argumentsCnt,
                              char **&argumentsVector) {
  if (strcmp(argumentsVector[0], OptionName) == 0) {
    --argumentsCnt;
    ++argumentsVector;
    return true;
  }
  return false;
}

template<const auto &OptionShortName,
        const auto &OptionFullName>
bool COptionHandler::ScanBothNames(int &argumentsCnt,
                                   char **&argumentsVector) {
  return ScanName<
          OptionShortName>(argumentsCnt,
                           argumentsVector)
         || ScanName<
          OptionFullName>(argumentsCnt,
                          argumentsVector);
}

#endif //EIGENVALUECALCULATORSOLVER_COPTIONHANDLER_H
