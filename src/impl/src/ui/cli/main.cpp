#include "../../solver/loader/matrix/column/CLaplacian1DColumnMatrixLoader.h"
#include "./view/CCommandLineView.h"
#include "../../solver/exporter/CStandardOutputTextExporter.h"

//#define DBG_PRINT_DEALLOCATION_TILDE
#ifdef DBG_PRINT_DEALLOCATION_TILDE

class DbgPrintDeallocationTildesEndPrintNewLine {
public:
  ~DbgPrintDeallocationTildesEndPrintNewLine() {
    printf("\n");
  }
};

#endif

int main(int argc, char *argv[]) {
  #ifdef DBG_PRINT_DEALLOCATION_TILDE
  DbgPrintDeallocationTildesEndPrintNewLine nl;
  #endif

  CStandardOutputTextExporter standardOutputTextExporter{};
  CCommandLineController commandLineController{standardOutputTextExporter};
  CCommandLineView commandLineView{standardOutputTextExporter,
                                   commandLineController};
  commandLineView.Run(argc, argv);
  return 0;
}
