#ifndef EIGENVALUECALCULATORGUI_CNUMBEROFITERATIONSSENDEROBSERVER_H
#define EIGENVALUECALCULATORGUI_CNUMBEROFITERATIONSSENDEROBSERVER_H

#include "../../../../solver/observer/CPropertyChangeObserver.h"
#include "../../controller/CComputationSignals.h"

class CNumberOfIterationsSenderObserver : public CPropertyChangeObserver<size_t> {
public:
  explicit CNumberOfIterationsSenderObserver(
          CComputationSignals &computationSignals);

  void PropertyChange(const size_t &newValue) override;

protected:
  CComputationSignals &m_ComputationSignals;
};

#endif //EIGENVALUECALCULATORGUI_CNUMBEROFITERATIONSSENDEROBSERVER_H
