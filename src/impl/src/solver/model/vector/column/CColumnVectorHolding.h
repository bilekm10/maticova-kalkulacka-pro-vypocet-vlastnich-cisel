#ifndef EIGENVALUECALCULATORSOLVER_CCOLUMNVECTORHOLDING_H
#define EIGENVALUECALCULATORSOLVER_CCOLUMNVECTORHOLDING_H

#include <memory>

#include "./CColumnVector.h"

//#define DBG_PRINT_DEALLOCATION_TILDE

/// Providing methods for and storing a column vector
template<typename Scalar>
class CColumnVectorHolding : public CColumnVector<Scalar> {
public:
  CColumnVectorHolding();

  CColumnVectorHolding(Scalar *data, size_t dataSize);

  /// doesn't copy the data, refers to the same array, the data must be allocated by the caller, and will be deleted in this class destructor
  CColumnVectorHolding(Scalar **data, size_t dataSize);

  /// always deepCopy when const Scalar *
  CColumnVectorHolding(const Scalar *data, size_t dataSize);

  explicit CColumnVectorHolding(size_t dataSize);

  explicit CColumnVectorHolding(size_t dataSize, Scalar value);

  CColumnVectorHolding(const CColumnVectorHolding<Scalar> &other);

  CColumnVectorHolding(CColumnVectorHolding<Scalar> &&other) noexcept;

  CColumnVectorHolding<Scalar> &operator=(CColumnVectorHolding<Scalar> &&other) noexcept;

  void Resize(size_t newSize) {
    if (newSize > m_MaximalSize)
      throw std::invalid_argument("CColumnVectorHolding->Resize(): "
                                  "newSize > m_MaximalSize");
    this->m_View.SetSize(newSize);
  }

  void IncreaseSize(size_t cnt = 1) {
    Resize(this->m_View.GetSize() + cnt);
  }

  void Append(Scalar scalar) {
    IncreaseSize();
    this->AtRef(this->m_View.GetSize() - 1) = scalar;
  }

  virtual
  ~CColumnVectorHolding();

protected:
  size_t m_MaximalSize;
};

template<typename Scalar>
CColumnVectorHolding<Scalar>::CColumnVectorHolding(Scalar *data, size_t dataSize)
        : CColumnVector<Scalar>(new Scalar[dataSize],
                                dataSize),
          m_MaximalSize(dataSize) {
  copy(data, data + dataSize, this->m_View.GetData());
}

/// doesn't copy the data, refers to the same array, the data must be allocated by the caller, and will be deleted in this class destructor
template<typename Scalar>
CColumnVectorHolding<Scalar>::CColumnVectorHolding(Scalar **data, size_t dataSize)
        : CColumnVector<Scalar>(*data,
                                dataSize),
          m_MaximalSize(dataSize) {
}

/// always deepCopy when const Scalar *
template<typename Scalar>
CColumnVectorHolding<Scalar>::CColumnVectorHolding(const Scalar *data, size_t dataSize)
        : CColumnVector<Scalar>(new Scalar[dataSize],
                                dataSize),
          m_MaximalSize(dataSize) {
  //throw std::invalid_argument("DBG RM TODO REMOVE COUT CColumnVectorHolding const Scalar * copy");
  copy(data, data + dataSize, this->m_View.GetData());
}

template<typename Scalar>
CColumnVectorHolding<Scalar>::CColumnVectorHolding(size_t dataSize)
        : CColumnVector<Scalar>(new Scalar[dataSize],
                                dataSize),
          m_MaximalSize(dataSize) {
}

template<typename Scalar>
CColumnVectorHolding<Scalar>::CColumnVectorHolding(size_t dataSize, Scalar value)
        : CColumnVector<Scalar>(new Scalar[dataSize],
                                dataSize),
          m_MaximalSize(dataSize) {
  this->SetTo(value);
}

template<typename Scalar>
CColumnVectorHolding<Scalar>::CColumnVectorHolding(const CColumnVectorHolding<Scalar> &other)
        : CColumnVector<Scalar>(new Scalar[other.m_View.GetSize()],
                                other.m_View.GetSize()),
          m_MaximalSize(other.m_View.GetSize()) {
  this->Assign(other);
}

template<typename Scalar>
CColumnVectorHolding<Scalar>::CColumnVectorHolding(CColumnVectorHolding<Scalar> &&other) noexcept
        : CColumnVector<Scalar>(std::move(other.m_View)),
          m_MaximalSize(other.m_MaximalSize) {
}

template<typename Scalar>
CColumnVectorHolding<Scalar> &CColumnVectorHolding<Scalar>::operator=(CColumnVectorHolding<Scalar> &&other) noexcept {
  this->m_View = std::move(other.m_View);
  m_MaximalSize = other.m_MaximalSize;
  return *this;
}

template<typename Scalar>
CColumnVectorHolding<Scalar>::~CColumnVectorHolding() {
  if (this->m_View.GetData()) {
    #ifdef DBG_PRINT_DEALLOCATION_TILDE
    printf("~");
    #endif
  }
  this->m_View.DeleteData();
  //printf("~CColumnVectorHolding\n");
}

template<typename Scalar>
CColumnVectorHolding<Scalar>::CColumnVectorHolding()
        : CColumnVectorHolding(1) {
}

#endif //EIGENVALUECALCULATORSOLVER_CCOLUMNVECTORHOLDING_H
