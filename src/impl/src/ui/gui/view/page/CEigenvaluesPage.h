#ifndef EIGENVALUECALCULATORGUI_CEIGENVALUESPAGE_H
#define EIGENVALUECALCULATORGUI_CEIGENVALUESPAGE_H

#include "./CQStringPage.h"

#include <string>
#include <iomanip>
#include <complex>
#include <type_traits>
#include "../../../../solver/model/vector/column/CColumnVectorConst.h"

class CEigenvaluesPage : public CQStringPage {
  using Scalar = double;
public:
  explicit CEigenvaluesPage(
          const CColumnVectorConst <std::complex<Scalar>> &eigenvalues,
          uint8_t precisionManipulator = 10,
          const std::string &separator = "\n",
          bool omitZeroImaginaryPart = true);

  explicit CEigenvaluesPage(
          const CColumnVectorConst <std::complex<Scalar>> &eigenvalues,
          double precision,
          const std::string &separator = "\n",
          bool omitZeroImaginaryPart = true);

  [[nodiscard]]
  QString Render() const override;

protected:
  static uint8_t CalculatePrecisionManipulator(double precision);

  static uint8_t CalculatePrecisionManipulatorWithoutLowerBound(double precision);

  const CColumnVectorConst <std::complex<Scalar>> &m_Eigenvalues;
  const uint8_t m_PrecisionManipulator;
  const std::string &m_Separator;
  const bool m_OmitZeroImaginaryPart;
};

#endif //EIGENVALUECALCULATORGUI_CEIGENVALUESPAGE_H
