#ifndef EIGENVALUECALCULATORSOLVER_CROWMATRIX_H
#define EIGENVALUECALCULATORSOLVER_CROWMATRIX_H

#include <memory>

#include "./CRowMatrixConst.h"
#include "../../../view/matrix/CDense2DBlockView.h"
#include "../../../vector/row/CRowVector.h"

/// Providing methods for a row-major matrix
class CRowMatrix : public CRowMatrixConst {
  using Scalar = double;
public:
  using CRowMatrixConst::At;
  using CRowMatrixConst::GetRowsCnt;
  using CRowMatrixConst::GetColumnsCnt;
  using CRowMatrixConst::IsSquare;
  using CRowMatrixConst::AtRef;
  using CRowMatrixConst::ToIdentity;
  using CRowMatrixConst::Assign;
  using CRowMatrixConst::GetRowConstRefView;
  using CRowMatrixConst::GetSubMatrixConstRefView;
  using CRowMatrixConst::Multiply;

  explicit CRowMatrix(CDense2DBlockView &&blockView);

  /*CRowMatrix(
          Scalar * data,
          size_t columnsCnt,
          size_t rowsCnt,
          size_t nextRowShift)
          : m_BlockView(data,
                        // continuousSize
                        columnsCnt,
                        // discontinuousSize
                        rowsCnt,
                        // discontinuousShift
                        nextRowShift)
  {
  }*/

  // copy from the other block view into *this block view
  void Assign(const CRowMatrix &other) {
    m_BlockView.Assign(other.m_BlockView);
  }

  [[nodiscard]]
  CRowVector<Scalar> GetRowRefView(
          size_t row,
          size_t firstElementColumn,
          size_t lastElementColumn) {
    return CRowVector<Scalar>(m_BlockView.GetContinuousRefView(
            row,
            firstElementColumn,
            lastElementColumn));
  }

  [[nodiscard]]
  CRowVector<Scalar> GetRowToDiagonalRefView(size_t row) {
    return CRowVector<Scalar>(m_BlockView.GetContinuousToDiagonalRefView(
            row));
  }

  /*CRowVector<Scalar> GetLastRowRefView()
  {
    return GetRowRefView(
            GetRowsCnt() - 1,
            0,
            GetColumnsCnt() - 1);
  }*/

  [[nodiscard]]
  CRowMatrix GetSubMatrixRefView(
          size_t firstRow,
          size_t lastRow,
          size_t firstColumn,
          size_t lastColumn) {
    return CRowMatrix{m_BlockView.GetSubMatrixRefView(
            firstColumn,
            lastColumn,
            firstRow,
            lastRow)};
  }

  /*[[nodiscard]] // circular dependency would occur
  CColumnMatrix TransposedConst() const
  {
    return CColumnMatrix{CDense2DBlockView{m_BlockView}};
  }*/

  virtual
  ~CRowMatrix();

protected:
};

inline
CRowMatrix::CRowMatrix(CDense2DBlockView &&blockView)
        : CRowMatrixConst(std::move(blockView)) {
}

inline
CRowMatrix::~CRowMatrix() = default;

#endif //EIGENVALUECALCULATORSOLVER_CROWMATRIX_H
