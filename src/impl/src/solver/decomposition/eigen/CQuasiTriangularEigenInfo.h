#ifndef EIGENVALUECALCULATORSOLVER_CQUASITRIANGULAREIGENINFO_H
#define EIGENVALUECALCULATORSOLVER_CQUASITRIANGULAREIGENINFO_H

#include "../../model/vector/column/CColumnVector.h"
#include "../../model/vector/column/CColumnVectorHolding.h"
#include "../../model/matrix/dense/column/CColumnMatrix.h"

#include <vector>
#include <stdexcept>

/// Read the eigenvalues from a quasi-triangular matrix diagonal blocks
class CQuasiTriangularEigenInfo {
public:
  CQuasiTriangularEigenInfo(const CColumnMatrixConst &quasiTriangularMatrix,
                            double treatAsZero);

  explicit CQuasiTriangularEigenInfo(double treatAsZero);

  void SetQuasiTriangularMatrix(const CColumnMatrixConst &quasiTriangularMatrix) {
    m_QuasiTriangularMatrix = &quasiTriangularMatrix;
  }

  void ComputeEigenvalues(CColumnVector<std::complex<double>> &eigenvalues);

  void ComputeEigenvalues(
          const CColumnMatrixConst &quasiTriangularMatrix,
          CColumnVector<std::complex<double>> &eigenvalues);

protected:
  const CColumnMatrixConst *m_QuasiTriangularMatrix;
  const double m_TreatAsZero; ///< this constant must be >= the deflation constant used for quasi-triangularization
};

inline
CQuasiTriangularEigenInfo::CQuasiTriangularEigenInfo(const CColumnMatrixConst &quasiTriangularMatrix,
                                                     double treatAsZero)
        : m_QuasiTriangularMatrix(&quasiTriangularMatrix),
          m_TreatAsZero(treatAsZero) {
}

inline
CQuasiTriangularEigenInfo::CQuasiTriangularEigenInfo(double treatAsZero)
        : m_QuasiTriangularMatrix(nullptr),
          m_TreatAsZero(treatAsZero) {
}

inline void
CQuasiTriangularEigenInfo::ComputeEigenvalues(CColumnVector<std::complex<double>> &eigenvalues) {
  if (!m_QuasiTriangularMatrix)
    throw std::invalid_argument("CQuasiTriangularEigenInfo->ComputeEigenvalues(): "
                                "quasiTriangularMatrix was not set. Cannot dereference nullptr.");
  const CColumnMatrixConst &matrix = *m_QuasiTriangularMatrix;
  if (matrix.GetRowsCnt() != eigenvalues.GetSize())
    throw std::invalid_argument("CQuasiTriangularEigenInfo->ComputeEigenvalues(): "
                                "matrix.GetRowsCnt() != eigenvalues.GetSize()");
  size_t i = 0;
  for (; i <= matrix.GetRowsCnt() - 2; ++i) {
    const double treatAsZeroMultiplier = 1; // abs(matrix.At(i, i)) + abs(matrix.At(i + 1, i + 1)) + abs(matrix.At(i + 1, i));
    if (abs(matrix.At(i + 1, i)) <= m_TreatAsZero * treatAsZeroMultiplier) {
      eigenvalues.AtRef(i) = {matrix.At(i, i), 0};
      continue;
    }
    if (i > 0 && abs(matrix.At(i, i - 1)) > m_TreatAsZero * treatAsZeroMultiplier) {
      throw std::invalid_argument("CQuasiTriangularEigenInfo->ComputeEigenvalues: "
                                  "Provided quasiTriangularMatrix is not quasi-triangular "
                                  "according to provided treatAsZero: "
                                  "two adjacent nonZeros in the subdiagonal");
    }
    size_t j = i + 1;
    const double trace = matrix.At(i, i) + matrix.At(j, j);
    const double blockMatrixDeterminant = matrix.At(i, i) * matrix.At(j, j) - matrix.At(j, i) * matrix.At(i, j);
    double determinant = trace * trace - 4 * blockMatrixDeterminant;
    if (abs(determinant) < m_TreatAsZero * treatAsZeroMultiplier) {
      determinant = 0;
    }
    if (determinant >= 0) {
      const double sqrtDeterminant = sqrt(determinant);
      eigenvalues.AtRef(i) = {(trace + sqrtDeterminant) / 2, 0}; // nthColumn(ritzVectors, i) // ritzVectors[i]
      eigenvalues.AtRef(i + 1) = {(trace - sqrtDeterminant) / 2, 0}; // nthColumn(ritzVectors, j) // ritzVectors[j]
    } else {
      const double sqrtDeterminant = sqrt(-determinant);
      eigenvalues.AtRef(i) = {trace / 2, sqrtDeterminant / 2};
      eigenvalues.AtRef(i + 1) = {trace / 2, -sqrtDeterminant / 2};
    }
    ++i; // move by 2, not only one - added 2 eigenvalues
  }
  if (i == matrix.GetRowsCnt() - 1) { // remaining one real eigenvalue at the bottom
    eigenvalues.AtRef(i) = {matrix.At(i, i), 0};
    ++i;
  }
}

inline void
CQuasiTriangularEigenInfo::ComputeEigenvalues(
        const CColumnMatrixConst &quasiTriangularMatrix,
        CColumnVector<std::complex<double>> &eigenvalues) {
  SetQuasiTriangularMatrix(quasiTriangularMatrix);
  return ComputeEigenvalues(eigenvalues);
}

#endif //EIGENVALUECALCULATORSOLVER_CQUASITRIANGULAREIGENINFO_H
