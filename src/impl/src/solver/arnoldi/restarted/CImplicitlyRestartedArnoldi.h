#ifndef EIGENVALUECALCULATORSOLVER_CIMPLICITLYRESTARTEDARNOLDI_H
#define EIGENVALUECALCULATORSOLVER_CIMPLICITLYRESTARTEDARNOLDI_H

#include <stdexcept>
#include <complex>
#include <cmath>
#include <limits>
#include <memory>

#include "../basic/CArnoldiFactorization.h"
#include "../../model/vector/column/CColumnVector.h"
#include "../../model/vector/column/CColumnVectorHolding.h"
#include "../../model/matrix/dense/column/CColumnMatrix.h"
#include "../../operator/matrixvector/CMatrixVectorMultiplication.h"
#include "../../operator/matrixmatrix/CMatrixKthSubdiagonalMatrixMultiplication.h"
#include "../../decomposition/qr/CHessenbergQrShifts.h"
#include "../../decomposition/qr/CHessenbergQrSingleShiftStep.h"
#include "../../decomposition/qr/CHessenbergQrDoubleShiftStep.h"
#include "../../decomposition/qr/CHessenbergQrQuasiTriangularization.h"
#include "../../decomposition/eigen/CQuasiTriangularEigenInfo.h"
#include "../../decomposition/eigen/CHessenbergEigen.h"
#include "../../loader/vector/column/CRandomColumnVectorLoader.h"
#include "../../../debug/eig/eig.h"
#include "./EComputationState.h"

//#define DBG_PRINT

//#define DBG_PRINT_ITERATION_SHORT_INFO
#ifdef DBG_PRINT_ITERATION_SHORT_INFO
#define DBG_PRINT_ITERATION_SHORT_INFO_OSTREAM std::cerr
#endif

// friend declaration
template<typename T>
class CArnoldiRestartArchiver;

/// Implicitly restarting the Arnoldi factorization until wanted eigenvalues converge
template<typename InputLargeMatrixVectorMultiplication,
        // provide derivatives that don't require some of the last templates
        typename Orthogonalization>
class CImplicitlyRestartedArnoldi {
public:
  static bool IsNotInitialized(EComputationState computationState) {
    return computationState >= EComputationState::NotInitialized
           && computationState <= EComputationState::NotInitializedCompressedFactorization;
  }

  static bool IsComputing(EComputationState computationState) {
    return computationState >= EComputationState::FactorizationExpanded
           && computationState <= EComputationState::Restarted;
  }

  static bool IsEnd(EComputationState computationState) {
    return computationState >= EComputationState::AlreadyComputed
           && computationState <= EComputationState::MaximalNumberOfIterationsReachedAndRestarted;
  }

  static size_t DefaultFactorizationSizeToRestartAt(size_t numberOfWantedEigenvalues) {
    if (numberOfWantedEigenvalues == 1)
      return 3;
    size_t factorizationSizeToRestartAt = 2 * numberOfWantedEigenvalues + numberOfWantedEigenvalues / 2;
    if (factorizationSizeToRestartAt % 2 == 0)
      return factorizationSizeToRestartAt;
    return factorizationSizeToRestartAt + 1;
  }

  static const constexpr size_t DefaultMaximalNumberOfIterations = 1000;
  static const constexpr double DefaultTreatAsZero = 1e-10;
  static const constexpr ESortingCriterion DefaultSortingCriterion = ESortingCriterion::LargestMagnitude;

  CImplicitlyRestartedArnoldi(InputLargeMatrixVectorMultiplication &inputLargeMatrixVectorMultiplication,
                              size_t inputLargeMatrixRowsCnt,
                              CColumnVector<double> &initialVector,
                              size_t numberOfWantedEigenvalues,
                              size_t factorizationSizeToRestartAtAndCompress,
                              ESortingCriterion sortingCriterion,
                              size_t maximalNumberOfIterations,
                              double treatAsZero,
                              double eigenvaluePrecision,
          // some derivatives don't require this argument and provide their own orthogonalization?
                              Orthogonalization orthogonalization);

  size_t Initialize();

  size_t PerformOneIteration();

  size_t ExpandFactorizationAndPrepareForRestart();

  size_t PrepareForRestart();

  size_t TreatExceptionalConvergence();

  [[nodiscard]]
  size_t GetNumberOfIterations() const { return m_NumberOfIterations; }

  [[nodiscard]]
  EComputationState GetComputationState() const { return m_ComputationState; }

  [[nodiscard]]
  size_t GetArnoldiFactorizationSize() const { return m_ArnoldiFactorization.GetFactorizationSize(); }

  [[nodiscard]]
  const CColumnVectorConst<std::complex<double>> &GetConvergedEigenvalues() const { return m_ConvergedEigenvalues; }

  [[nodiscard]]
  const CColumnVectorConst<std::complex<double>> &GetCurrentHessenbergEigenvalues() const { return m_Eigenvalues; }

  [[nodiscard]]
  const CColumnVectorConst<double> &
  GetCurrentHessenbergEigenvalueErrorEstimates() const { return m_EigenvalueErrorEstimates; }

  [[nodiscard]]
  const CColumnVectorConst<std::complex<double>> &GetShiftsForRestart() const { return m_ShiftsForRestart; }

  [[nodiscard]]
  const CColumnMatrixConst &
  GetOrthonormalVectorsMatrix() const { return m_ArnoldiFactorization.GetOrthonormalVectorsMatrix(); }

  [[nodiscard]]
  const CColumnMatrixConst &GetHessenbergMatrix() const { return m_ArnoldiFactorization.GetHessenbergMatrix(); }

  [[nodiscard]]
  const CColumnVectorConst<double> &GetResidualVector() const { return m_ArnoldiFactorization.GetResidualVector(); }

  void SetShiftsForRestart(const CColumnVectorConst<std::complex<double>> &shiftsForRestart) {
    if (shiftsForRestart.GetSize() >= m_Eigenvalues.GetSize())
      throw std::invalid_argument("CImplicitlyRestartedArnoldi->SetShiftsForRestart: "
                                  "shiftsForRestart.GetSize() >= m_Eigenvalues.GetSize()");
    m_ShiftsForRestart.Assign(shiftsForRestart);
  }

  void Restart();

  // after maximalNumberOfIterations is reached
  void LastRestart();

  size_t Compute();

protected:
  void SelectShiftsFromComputedEigenvalues();

  void LockConvergedEigenvalues();

  bool IsEigenvalueConvergedAccordingToErrorEstimate(double errorEstimate, std::complex<double> eigenvalue) {
    double multiplier = std::max(std::abs(eigenvalue), 1.0);
    return std::abs(errorEstimate) <= m_EigenvaluePrecision * multiplier;
  }

  EComputationState m_ComputationState;

  size_t m_NumberOfWantedEigenvalues; ///< note: new factorization not always starts from this size - maybe some shift was not applied, perhaps because of complex conjugate
  size_t m_FactorizationSizeAtRestart;
  ESortingCriterion m_SortingCriterion;
  CColumnVectorHolding<std::complex<double>> m_ShiftsForRestart; ///< note: complex conjugates must be adjacent
  CColumnVectorHolding<std::complex<double>> m_Eigenvalues;
  CColumnVectorHolding<double> m_EigenvalueErrorEstimates;
  CColumnVectorHolding<std::complex<double>> m_ConvergedEigenvalues;
  /// when the expansion of the Arnoldi factorization didn't reach the wanted size
  CColumnVectorHolding<std::complex<double>> m_ExceptionallyConvergedEigenvalues;

  size_t m_NumberOfIterations;
  size_t m_MaximalNumberOfIterations;
  double m_TreatAsZero;
  double m_EigenvaluePrecision;

  CHessenbergQrShifts m_QrShifts;

  CHessenbergEigen m_HessenbergEigen;

  CArnoldiFactorization<InputLargeMatrixVectorMultiplication, Orthogonalization> m_ArnoldiFactorization;

  template<typename T>
  friend
  class CArnoldiRestartArchiver;
};

template<typename InputLargeMatrixVectorMultiplication,
        typename Orthogonalization>
CImplicitlyRestartedArnoldi<InputLargeMatrixVectorMultiplication, Orthogonalization>
::CImplicitlyRestartedArnoldi(InputLargeMatrixVectorMultiplication &inputLargeMatrixVectorMultiplication,
                              size_t inputLargeMatrixRowsCnt,
                              CColumnVector<double> &initialVector,
                              size_t numberOfWantedEigenvalues,
                              size_t factorizationSizeToRestartAtAndCompress,
                              ESortingCriterion sortingCriterion,
                              size_t maximalNumberOfIterations,
                              double treatAsZero,
                              double eigenvaluePrecision,
        // some derivatives don't require this argument and provide their own orthogonalization?
                              Orthogonalization orthogonalization)
        : m_ComputationState(EComputationState::NotInitialized),
          m_NumberOfWantedEigenvalues(numberOfWantedEigenvalues),
          m_FactorizationSizeAtRestart(factorizationSizeToRestartAtAndCompress),
          m_SortingCriterion(sortingCriterion),
          m_ShiftsForRestart(factorizationSizeToRestartAtAndCompress, {0, 0}),
          m_Eigenvalues(factorizationSizeToRestartAtAndCompress, {0, 0}),
          m_EigenvalueErrorEstimates(factorizationSizeToRestartAtAndCompress, 0),
          m_ConvergedEigenvalues(factorizationSizeToRestartAtAndCompress, {0, 0}),
          m_ExceptionallyConvergedEigenvalues(factorizationSizeToRestartAtAndCompress, {0, 0}),
          m_NumberOfIterations(0),
          m_MaximalNumberOfIterations(maximalNumberOfIterations),
          m_TreatAsZero(treatAsZero),
          m_EigenvaluePrecision(eigenvaluePrecision),
        //m_RestartAndStopComputing(false),
          m_QrShifts(factorizationSizeToRestartAtAndCompress),
          m_HessenbergEigen(factorizationSizeToRestartAtAndCompress, treatAsZero),
          m_ArnoldiFactorization(inputLargeMatrixVectorMultiplication,
                                 inputLargeMatrixRowsCnt,
                                 initialVector,
                                 numberOfWantedEigenvalues,
                                 factorizationSizeToRestartAtAndCompress,
                                 std::move(orthogonalization),
                                 treatAsZero) {
  if (treatAsZero < 0)
    throw std::invalid_argument("CImplicitlyRestartedArnoldi constructor: "
                                "double treatAsZero < 0, but must be at least 0");
  if (initialVector.GetSize() != inputLargeMatrixRowsCnt)
    throw std::invalid_argument("CImplicitlyRestartedArnoldi constructor: "
                                "initialVector.GetSize() != inputLargeMatrixRowsCnt, "
                                "i.e. cannot multiply inputLargeMatrix with initial vector from the right");
  if (numberOfWantedEigenvalues <= 0)
    throw std::invalid_argument("CImplicitlyRestartedArnoldi constructor: "
                                "numberOfWantedEigenvalues <= 0, "
                                "if no eigenvalues are wanted then initializing this class is redundant");
  if (factorizationSizeToRestartAtAndCompress <= numberOfWantedEigenvalues + 1)
    throw std::invalid_argument("CImplicitlyRestartedArnoldi constructor: "
                                "factorizationSizeToRestartAtAndCompress <= numberOfWantedEigenvalues + 1, "
                                "but must be bigger");
  // todo: + 10
  if (factorizationSizeToRestartAtAndCompress + 10 >= inputLargeMatrixRowsCnt)
    throw std::invalid_argument("CImplicitlyRestartedArnoldi constructor: "
                                "factorizationSizeToRestartAtAndCompress + 10 >= inputLargeMatrixRowsCnt, "
                                "so some other algorithm should be used instead - "
                                "CImplicitlyRestartedArnoldi is primarily for the cases when "
                                "factorizationSizeToRestartAtAndCompress is much smaller than inputLargeMatrixRowsCnt");
  m_ConvergedEigenvalues.Resize(0);
  m_ExceptionallyConvergedEigenvalues.Resize(0);
}

template<typename InputLargeMatrixVectorMultiplication,
        typename Orthogonalization>
size_t CImplicitlyRestartedArnoldi<InputLargeMatrixVectorMultiplication, Orthogonalization>
::CImplicitlyRestartedArnoldi::Initialize() {
  if (!IsNotInitialized(m_ComputationState))
    throw std::invalid_argument("CImplicitlyRestartedArnoldi->Initialize(): "
                                "This method was called after initialization");
  ++m_NumberOfIterations;
  size_t newSize;
  if (m_ComputationState == EComputationState::NotInitialized) // m_ArnoldiFactorization.GetFactorizationSize() == 0)
  { // initialize
    newSize = m_ArnoldiFactorization.InitializeArnoldiFactorization(m_FactorizationSizeAtRestart);
  } else if (m_ComputationState == EComputationState::NotInitializedCompressedFactorization) {
    newSize = m_ArnoldiFactorization.ExpandArnoldiFactorization(
            GetArnoldiFactorizationSize(),
            m_FactorizationSizeAtRestart);
  }
  m_ComputationState = EComputationState::FactorizationExpanded;
  if (newSize < m_FactorizationSizeAtRestart) {
    return TreatExceptionalConvergence();
  }
  return PrepareForRestart();
}

template<typename InputLargeMatrixVectorMultiplication,
        typename Orthogonalization>
size_t CImplicitlyRestartedArnoldi<InputLargeMatrixVectorMultiplication, Orthogonalization>
::PerformOneIteration() { // EComputationState::ShiftsSelected
  if (m_ComputationState != EComputationState::ShiftsSelected)
    throw std::invalid_argument("CImplicitlyRestartedArnoldi->PerformOneIteration(): "
                                "This method wasn't called in ShiftsSelected state, "
                                "but in a state with id " +
                                std::to_string(static_cast<int>(m_ComputationState)));
  //++m_NumberOfIterations;
  // m-k shifts from stored eigenvalues of hm were already selected in the end of previous iteration or caller provided his own
  // call: perform shifted qr with the selected shifts
  // call: perform implicit restart, compression of the factorization to size k
  Restart();
  // expand the factorization
  // calculate and store eigenvalues of the hessenbergMatrix and their error estimates
  // select and store the shifts, so the caller can provide his own perhaps based on those shifts before next iteration
  // return number of converged eigenvalues
  return ExpandFactorizationAndPrepareForRestart();
}

template<typename InputLargeMatrixVectorMultiplication,
        typename Orthogonalization>
size_t CImplicitlyRestartedArnoldi<InputLargeMatrixVectorMultiplication, Orthogonalization>
::ExpandFactorizationAndPrepareForRestart() {
  ++m_NumberOfIterations;
  if (m_ComputationState != EComputationState::Restarted)
    throw std::invalid_argument("CImplicitlyRestartedArnoldi->ExpandFactorizationAndPrepareForRestart(): "
                                "this method was called when the Arnoldi factorization "
                                "wasn't restarted i.e. compressed");
  size_t newSize = m_ArnoldiFactorization.ExpandArnoldiFactorization(
          GetArnoldiFactorizationSize(),
          m_FactorizationSizeAtRestart);
  m_ComputationState = EComputationState::FactorizationExpanded;
  if (newSize < m_FactorizationSizeAtRestart) {
    return TreatExceptionalConvergence();
  }
  return PrepareForRestart();
}

template<typename InputLargeMatrixVectorMultiplication,
        typename Orthogonalization>
size_t CImplicitlyRestartedArnoldi<InputLargeMatrixVectorMultiplication, Orthogonalization>
::PrepareForRestart() {
  if (GetArnoldiFactorizationSize() < m_FactorizationSizeAtRestart)
    throw std::invalid_argument("CImplicitlyRestartedArnoldi->PrepareForRestart(): "
                                "GetArnoldiFactorizationSize() < m_FactorizationSizeAtRestart, "
                                "this method doesn't treat with this exceptional case");
  if (m_ComputationState != EComputationState::FactorizationExpanded)
    throw std::invalid_argument("CImplicitlyRestartedArnoldi->PrepareForRestart(): "
                                "this method was called when the Arnoldi factorization "
                                "wasn't expanded, or the computation phase ended, "
                                "or some error occurred");
  // call: calculate spectrum hm
  m_HessenbergEigen.SetHessenbergMatrix(m_ArnoldiFactorization.GetHessenbergMatrixRef());
  // store spectrum hm
  // calculate and store eigenvalue error estimates
  m_HessenbergEigen.GetEigenvaluesWithErrorEstimates(
          m_Eigenvalues,
          m_EigenvalueErrorEstimates,
          m_ArnoldiFactorization.GetResidualVectorNorm(),
          m_SortingCriterion);
  #ifdef DBG_PRINT_ITERATION_SHORT_INFO
  if(m_NumberOfIterations == 80)
    std::cout <<"here"<<std::endl;
  DBG_PRINT_ITERATION_SHORT_INFO_OSTREAM << "DBG: num_it=" << m_NumberOfIterations
  << " res1=" << m_EigenvalueErrorEstimates.At(0)
  << " res" << m_NumberOfWantedEigenvalues << "=" << m_EigenvalueErrorEstimates.At(m_NumberOfWantedEigenvalues - 1)
  << std::endl;
  #endif
  // choose and store the shifts, so user can provide his own perhaps based on those shifts before next iteration
  SelectShiftsFromComputedEigenvalues();
  LockConvergedEigenvalues();
  if (m_ConvergedEigenvalues.GetSize() >= m_NumberOfWantedEigenvalues)
    m_ComputationState = EComputationState::AlreadyComputed;
  else if (m_NumberOfIterations >= m_MaximalNumberOfIterations)
    m_ComputationState = EComputationState::MaximalNumberOfIterationsReached;
  // return number of converged eigenvalues
  return m_ConvergedEigenvalues.GetSize();
}

template<typename InputLargeMatrixVectorMultiplication,
        typename Orthogonalization>
size_t CImplicitlyRestartedArnoldi<InputLargeMatrixVectorMultiplication, Orthogonalization>
::TreatExceptionalConvergence() {
  // store a state of the computation, throw when already computed or in this, ExceptionallyConverged, state and another iteration is called
  m_ComputationState = EComputationState::ExceptionallyConverged;
  // rewrite this

  // decrease columnsCnt, and rowsCnt, of the hessenberg matrix, colC of ortVM,

  // compute its eigenvalues and store them as some other vector
  //m_HessenbergEigen.SetHessenbergMatrix(m_ArnoldiFactorization.GetHessenbergMatrixRef());
  //m_HessenbergEigen.GetEigenvalues(m_ExceptionallyConvergedEigenvalues, m_SortingCriterion);

  // provide getter for this eigenvalues vector;

  // don't call this: LockConvergedEigenvalues();
  return m_ConvergedEigenvalues.GetSize();
}

template<typename InputLargeMatrixVectorMultiplication,
        typename Orthogonalization>
void CImplicitlyRestartedArnoldi<InputLargeMatrixVectorMultiplication, Orthogonalization>
::Restart() {
  #ifdef DBG_PRINT
  std::cout << "converged\n" << m_ConvergedEigenvalues << "\n";
    std::cout << "eigenvalues\n" << m_Eigenvalues << "\n";
    std::cout << "errorEstimates\n" << m_EigenvalueErrorEstimates << "\n";
    std::cout << "shifts\n" << m_ShiftsForRestart << "\n";
  #endif
  if (m_ComputationState != EComputationState::ShiftsSelected)
    throw std::invalid_argument("CImplicitlyRestartedArnoldi->Restart(): "
                                "This method wasn't called in ShiftsSelected state, "
                                "but in a state with id " +
                                std::to_string(static_cast<int>(m_ComputationState)));
  const size_t oldSize = m_Eigenvalues.GetSize();
  const size_t shiftsSize = m_ShiftsForRestart.GetSize();
  const size_t newSize = oldSize - shiftsSize;
  if (shiftsSize <= 0)
    throw std::invalid_argument("CImplicitlyRestartedArnoldi->Restart: "
                                "The size of shifts for restart <= 0,"
                                " but must be at least 1");
  if (newSize <= 0)
    throw std::invalid_argument("CImplicitlyRestartedArnoldi->Restart: "
                                "The size of the factorization after performing restart would be <= 0, "
                                "but must be at least 1");
  m_QrShifts.PerformQrWithShifts(
          m_ArnoldiFactorization.GetHessenbergMatrixRef(),
          m_ShiftsForRestart);
  // hessenberg matrix was shifted, but not resized - next Arnoldi factorization must be started from the right size
  // m_OrthogonalMatrixFromQrShifts now has 0 entries below the shiftsSize-th subdiagonal
  // compress orthonormalVectorsMatrix
  // maybe should get inspired for matrix-matrix multiplication in spectra, but don't overcomplicate, choose something simple
  // should check the nonzero structure of the qr shift matrix after making it work
  // should provide only the needed rectangular part of the orthogonal matrix from performing shifts
  const auto &qrShiftsMatrix = m_QrShifts.GetOrthogonalMatrix();
  #ifdef DBG_PRINT
  std::cout << "qrShiftsMatrix\n" << qrShiftsMatrix << "\n";
  #endif
  m_ArnoldiFactorization.CompressVectorsWithKthSubdiagonalMatrix(
          qrShiftsMatrix.GetSubMatrixConstRefView(
                  0,
                  qrShiftsMatrix.GetRowsCnt() - 1,
                  0,
                  newSize),
          shiftsSize);
  m_ComputationState = EComputationState::Restarted;
}

// after maximalNumberOfIterations is reached
template<typename InputLargeMatrixVectorMultiplication,
        typename Orthogonalization>
void CImplicitlyRestartedArnoldi<InputLargeMatrixVectorMultiplication, Orthogonalization>
::LastRestart() {
  if (m_ComputationState != EComputationState::MaximalNumberOfIterationsReached)
    throw std::invalid_argument("CImplicitlyRestartedArnoldi->LastRestart: "
                                "To perform the last restart, the computation must be "
                                "in the state MaximalNumberOfIterationsReached, but isn't");
  m_ComputationState = EComputationState::ShiftsSelected;
  Restart();
  m_ComputationState = EComputationState::MaximalNumberOfIterationsReachedAndRestarted;
}

template<typename InputLargeMatrixVectorMultiplication,
        typename Orthogonalization>
size_t CImplicitlyRestartedArnoldi<InputLargeMatrixVectorMultiplication, Orthogonalization>
::Compute() {
  if (IsNotInitialized(m_ComputationState))
    Initialize();
  while (IsComputing(m_ComputationState))
    PerformOneIteration();
  return m_ConvergedEigenvalues.GetSize();
}

template<typename InputLargeMatrixVectorMultiplication,
        typename Orthogonalization>
void CImplicitlyRestartedArnoldi<InputLargeMatrixVectorMultiplication, Orthogonalization>
::SelectShiftsFromComputedEigenvalues() {
  if (m_NumberOfWantedEigenvalues > m_Eigenvalues.GetSize())
    throw std::invalid_argument("CImplicitlyRestartedArnoldi->SelectShiftsFromComputedEigenvalues: "
                                "No shifts can be selected for implicit restart when "
                                "m_NumberOfWantedEigenvalues > m_Eigenvalues.GetSize(). "
                                "m_Eigenvalues.GetSize() = " + m_Eigenvalues.GetSize());
  // don't include a shift which is equal to some of the wanted eigenvalues or its complex conjugate; assuming the eigenvalues are sorted so that only the previous eigenvalue can be checked and that complex conjugates are adjacent and first is the one with positive imaginary part
  size_t eigenvaluesShiftsStartIndex = m_NumberOfWantedEigenvalues;
  double almostZero = std::abs(std::numeric_limits<double>::min()) * 100;
  for (;
          eigenvaluesShiftsStartIndex < m_Eigenvalues.GetSize()
          && std::abs(m_EigenvalueErrorEstimates.At(eigenvaluesShiftsStartIndex)) < almostZero;
          ++eigenvaluesShiftsStartIndex) {
  }
  // ARPACK dnaup2.f adjustment
  eigenvaluesShiftsStartIndex += std::min(m_ConvergedEigenvalues.GetSize(),
                                          (m_Eigenvalues.GetSize() - eigenvaluesShiftsStartIndex) / 2);
  if (eigenvaluesShiftsStartIndex == 1 && m_Eigenvalues.GetSize() >= 6) {
    eigenvaluesShiftsStartIndex = m_Eigenvalues.GetSize() / 2;
  } else if (eigenvaluesShiftsStartIndex == 1 && m_Eigenvalues.GetSize() > 3) {
    eigenvaluesShiftsStartIndex = 2;
  }
  if (eigenvaluesShiftsStartIndex + 2 > m_Eigenvalues.GetSize()) {
    if (m_Eigenvalues.GetSize() <= 2)
      throw std::logic_error("CImplicitlyRestartedArnoldi->SelectShiftsFromComputedEigenvalues: "
                             "FactorizationSizeToRestartAt is too small: <= 2: "
                             "but must be able to select at least two shifts for restart");
    eigenvaluesShiftsStartIndex = m_Eigenvalues.GetSize() - 2;
  }
  for (;
          eigenvaluesShiftsStartIndex < m_Eigenvalues.GetSize()
          &&
          (m_Eigenvalues.At(eigenvaluesShiftsStartIndex) == std::conj(m_Eigenvalues.At(eigenvaluesShiftsStartIndex - 1))
           || m_Eigenvalues.At(eigenvaluesShiftsStartIndex) == m_Eigenvalues.At(eigenvaluesShiftsStartIndex - 1));
          ++eigenvaluesShiftsStartIndex) {
  }
  m_ShiftsForRestart.Resize(m_Eigenvalues.GetSize() - eigenvaluesShiftsStartIndex);
  size_t i = 0;
  for (size_t eigenvalueShiftIndex = eigenvaluesShiftsStartIndex;
       eigenvalueShiftIndex < m_Eigenvalues.GetSize();
       ++eigenvalueShiftIndex,
               ++i) {
    m_ShiftsForRestart.AtRef(i) = m_Eigenvalues.At(eigenvalueShiftIndex);
  }
  m_ComputationState = EComputationState::ShiftsSelected;
}

template<typename InputLargeMatrixVectorMultiplication,
        typename Orthogonalization>
void CImplicitlyRestartedArnoldi<InputLargeMatrixVectorMultiplication, Orthogonalization>
::LockConvergedEigenvalues() {
  for (size_t i = 0;
       i < m_ConvergedEigenvalues.GetSize();
       ++i) {
    if (m_EigenvaluePrecision > 0.001) {
      if (!IsEigenvalueConvergedAccordingToErrorEstimate(m_EigenvalueErrorEstimates.At(i), m_Eigenvalues.At(i))) {
        return;
      }
    } else if (!IsEigenvalueConvergedAccordingToErrorEstimate(m_EigenvalueErrorEstimates.At(i) * 0.001,
                                                              m_Eigenvalues.At(i))) {
      return;
    }
  }
  for (size_t i = m_ConvergedEigenvalues.GetSize();
       i < m_Eigenvalues.GetSize()
       && IsEigenvalueConvergedAccordingToErrorEstimate(m_EigenvalueErrorEstimates.At(i), m_Eigenvalues.At(i));
       ++i) {
//      if(m_NumberOfIterations % 5 != 0)
//      {
//        continue;
//      }
//      // remove this?
//      if(m_ResidualVectorNorm > m_TreatAsZero
//         && m_PreviouslyConvergedEndIndex <= i)
//      { // attempting to lock only when would already be locked in some previous iteration
//        m_PreviouslyConvergedEndIndex = i + 1;
//        continue;
//      }
    const auto currentEigenvalue = m_Eigenvalues.At(i);
    if (currentEigenvalue.imag() == 0) {
      for (size_t j = 0; j < i; ++j)
        m_ConvergedEigenvalues.AtRef(j) = m_Eigenvalues.At(j);
      m_ConvergedEigenvalues.Append(currentEigenvalue);
      continue;
    }
    // have complex eigenvalue and next eigenvalue must be the complex conjugate
    if (i + 1 >= m_Eigenvalues.GetSize())
      throw std::logic_error("CImplicitlyRestartedArnoldi->LockConvergedEigenvalues: "
                             "internal error: "
                             "computed Hessenberg eigenvalues were not sorted properly "
                             // should try to solve this error cause somehow?
                             "or some large eigenvalue converged too late: "
                             "complex eigenvalue is not adjacent to its complex conjugate. "
                             "All Hessenberg eigenvalues converged, "
                             "but the last one is complex and has no complex conjugate.");
    const auto nextEigenvalue = m_Eigenvalues.At(i + 1);
    /*if(currentEigenvalue != std::conj(nextEigenvalue))
      throw std::logic_error("CImplicitlyRestartedArnoldi->LockConvergedEigenvalues: "
                             "internal error: "
                             "computed Hessenberg eigenvalues were not sorted properly "
                             // should try to solve this error cause somehow?
                             "or some large eigenvalue converged too late: "
                             "complex eigenvalue is not adjacent to its complex conjugate");*/
    if (IsEigenvalueConvergedAccordingToErrorEstimate(m_EigenvalueErrorEstimates.At(i + 1), nextEigenvalue)) {
      for (size_t j = 0; j < i; ++j)
        m_ConvergedEigenvalues.AtRef(j) = m_Eigenvalues.At(j);
      m_ConvergedEigenvalues.Append(currentEigenvalue);
      m_ConvergedEigenvalues.Append(nextEigenvalue);
      // appended two eigenvalues, so additional ++i;
      ++i;
      continue;
    }
    // the complex eigenvalue at position i converged already but not its complex conjugate
    break;
  }
}

#endif //EIGENVALUECALCULATORSOLVER_CIMPLICITLYRESTARTEDARNOLDI_H
