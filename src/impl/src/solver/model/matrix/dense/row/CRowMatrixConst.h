#ifndef EIGENVALUECALCULATORSOLVER_CROWMATRIXCONST_H
#define EIGENVALUECALCULATORSOLVER_CROWMATRIXCONST_H

#include <memory>

#include "../CDenseMatrixConst.h"
#include "../../../view/matrix/CDense2DBlockView.h"
#include "../../../vector/row/CRowVector.h"

/// Providing const methods for a row-major matrix
class CRowMatrixConst : public CDenseMatrixConst<false> {
  using Scalar = double;
protected:
  using CDenseMatrixConst<false>::AtRef;
  using CDenseMatrixConst<false>::ToIdentity;
  using CDenseMatrixConst<false>::Assign;
public:
  using CDenseMatrixConst<false>::At;
  using CDenseMatrixConst<false>::GetRowsCnt;
  using CDenseMatrixConst<false>::GetColumnsCnt;
  using CDenseMatrixConst<false>::IsSquare;

  explicit CRowMatrixConst(CDense2DBlockView &&blockView);

  /*CRowMatrixConst(
          Scalar * data,
          size_t columnsCnt,
          size_t rowsCnt,
          size_t nextRowShift)
          : m_BlockView(data,
                        // continuousSize
                        columnsCnt,
                        // discontinuousSize
                        rowsCnt,
                        // discontinuousShift
                        nextRowShift)
  {
  }*/

  [[nodiscard]]
  size_t GetMaximalColumnsCnt() const {
    return m_BlockView.GetMaximalContinuousSize();
  }

  [[nodiscard]]
  CRowVectorConst<Scalar> GetRowConstRefView(
          size_t row,
          size_t firstElementColumn,
          size_t lastElementColumn) const {
    return CRowVectorConst<Scalar>(m_BlockView.GetContinuousConstRefView(
            row,
            firstElementColumn,
            lastElementColumn));
  }

  [[nodiscard]]
  CRowMatrixConst GetSubMatrixConstRefView(
          size_t firstRow,
          size_t lastRow,
          size_t firstColumn,
          size_t lastColumn) const {
    return CRowMatrixConst{m_BlockView.GetSubMatrixConstRefView(
            firstColumn,
            lastColumn,
            firstRow,
            lastRow)};
  }

  /*[[nodiscard]] // circular dependency would occur, make a template from this method if needed
  CColumnMatrix TransposedConst() const
  {
    return CColumnMatrix{CDense2DBlockView{m_BlockView}};
  }*/

  virtual
  ~CRowMatrixConst();


  void Multiply(const CColumnVectorConst<double> &columnVector, CColumnVector<double> &result) const;

protected:
};

inline
CRowMatrixConst::CRowMatrixConst(CDense2DBlockView &&blockView)
        : CDenseMatrixConst<false>(std::move(blockView)) {
}

inline
CRowMatrixConst::~CRowMatrixConst() = default;

inline void
CRowMatrixConst::Multiply(const CColumnVectorConst<double> &columnVector, CColumnVector<double> &result) const {
  if (GetColumnsCnt() != columnVector.GetSize())
    throw std::invalid_argument("CRowMatrixConst->Multiply(): "
                                "Matrix columns count != vector size, i.e. rows count,"
                                "so cannot multiply the matrix by the vector from the right");
  // allow <= ? and throw only when > ?
  if (GetRowsCnt() != result.GetSize())
    throw std::invalid_argument("CRowMatrixConst->Multiply(): "
                                "Matrix rows count != result vector size, i.e. rows count,"
                                "so the result of multiplication "
                                "cannot be stored in the result vector");
  for (size_t i = 0; i < GetRowsCnt(); ++i)
    result.AtRef(i) = GetRowConstRefView(i,
                                         0,
                                         GetColumnsCnt() - 1)
            .MultiplyToScalar(columnVector);
}

#endif //EIGENVALUECALCULATORSOLVER_CROWMATRIXCONST_H
