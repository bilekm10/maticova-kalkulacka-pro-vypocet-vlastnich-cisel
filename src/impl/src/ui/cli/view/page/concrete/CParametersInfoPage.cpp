#include "./CParametersInfoPage.h"

#include <utility>

CParametersInfoPage::CParametersInfoPage(CRestartedArnoldiInputArguments inputArguments)
        : m_InputArguments(std::move(inputArguments)) {
}

[[nodiscard]]
std::string CParametersInfoPage::Render() const {
  std::stringstream ss;
  ss << "--------------------------------------------------\n";
  ss << "Parameters information:\n";
  ss << CRestartedArnoldiInputArguments::FirstLetterToUpperCase(m_InputArguments.m_InputMatrixFilenameArgumentName)
     << ": ";
  m_InputArguments.m_InputMatrixFilename
  ? ss << m_InputArguments.m_InputMatrixFilename.value()
  : ss << m_NotSpecifiedMessage;
  ss << "\n";
  ss << CRestartedArnoldiInputArguments::FirstLetterToUpperCase(
          m_InputArguments.m_NumberOfWantedEigenvaluesArgumentName) << ": ";
  m_InputArguments.m_NumberOfWantedEigenvalues
  ? ss << m_InputArguments.m_NumberOfWantedEigenvalues.value()
  : ss << m_NotSpecifiedMessage;
  ss << "\n";
  //ss << "Maximal Arnoldi factorization size: " << (m_InputArguments.m_FactorizationSizeToRestartAt) << "\n";
  ss << CRestartedArnoldiInputArguments::FirstLetterToUpperCase(
          m_InputArguments.m_FactorizationSizeToRestartAtArgumentName) << ": ";
  m_InputArguments.m_FactorizationSizeToRestartAt
  ? ss << m_InputArguments.m_FactorizationSizeToRestartAt.value()
  : ss << m_NotSpecifiedMessage;
  ss << "\n";
  ss << CRestartedArnoldiInputArguments::FirstLetterToUpperCase(m_InputArguments.m_SortingCriterionArgumentName)
     << ": ";
  m_InputArguments.m_SortingCriterion
  ? ss << toVerboseString(m_InputArguments.m_SortingCriterion.value())
  : ss << m_NotSpecifiedMessage;
  ss << "\n";
  ss << CRestartedArnoldiInputArguments::FirstLetterToUpperCase(m_InputArguments.m_PrecisionArgumentName) << ": ";
  m_InputArguments.m_Precision
  ? (ss << std::setprecision(2) << std::scientific << m_InputArguments.m_Precision.value())
  : ss << m_NotSpecifiedMessage;
  ss << "\n";
  ss << CRestartedArnoldiInputArguments::FirstLetterToUpperCase(
          m_InputArguments.m_MaximalNumberOfIterationsArgumentName) << ": ";
  m_InputArguments.m_MaximalNumberOfIterations
  ? ss << m_InputArguments.m_MaximalNumberOfIterations.value()
  : ss << m_NotSpecifiedMessage;
  ss << "\n";
  ss << CRestartedArnoldiInputArguments::FirstLetterToUpperCase(
          m_InputArguments.m_MaximalNumberOfOrthogonalizationIterationsArgumentName) << ": ";
  m_InputArguments.m_MaximalNumberOfOrthogonalizationIterations
  ? ss << int(m_InputArguments.m_MaximalNumberOfOrthogonalizationIterations.value())
  : ss << m_NotSpecifiedMessage;
  ss << "\n";
  ss << CRestartedArnoldiInputArguments::FirstLetterToUpperCase(
          m_InputArguments.m_IterativeOrthogonalizationEtaArgumentName) << ": ";
  m_InputArguments.m_IterativeOrthogonalizationEta
  ? ss << std::setprecision(5) << std::scientific << m_InputArguments.m_IterativeOrthogonalizationEta.value()
  : ss << m_NotSpecifiedMessage;
  ss << "\n";
  return ss.str();
}
