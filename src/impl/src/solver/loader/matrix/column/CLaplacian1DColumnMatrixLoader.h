#ifndef EIGENVALUECALCULATORSOLVER_CLAPLACIAN1DCOLUMNMATRIXLOADER_H
#define EIGENVALUECALCULATORSOLVER_CLAPLACIAN1DCOLUMNMATRIXLOADER_H

#include "./CColumnMatrixLoader.h"
#include "../../../model/matrix/dense/column/CColumnMatrixDynamicDimensionsHolding.h"

#include <vector>

/// Generate a Laplacian 1D matrix
class CLaplacian1DColumnMatrixLoader : public CColumnMatrixLoader {
public:
  explicit CLaplacian1DColumnMatrixLoader(size_t rowsCnt);

  [[nodiscard]]
  CColumnMatrixDynamicDimensionsHolding Load() override;

protected:
};

inline
CLaplacian1DColumnMatrixLoader::CLaplacian1DColumnMatrixLoader(size_t rowsCnt)
        : CColumnMatrixLoader(rowsCnt) {
}

[[nodiscard]]
inline CColumnMatrixDynamicDimensionsHolding
CLaplacian1DColumnMatrixLoader::Load() {
  CColumnMatrixDynamicDimensionsHolding matrix(m_RowsCnt, m_RowsCnt, m_RowsCnt, m_RowsCnt);
// from SLEPc
  for (size_t i = 0; i < m_RowsCnt; ++i) {
    matrix.AtRef(i, i) = 2.0;
    if (i > 0)
      matrix.AtRef(i, i - 1) = -1.0;
    if (i < m_RowsCnt - 1)
      matrix.AtRef(i, i + 1) = -1.0;
  }
  return matrix;
}

#endif //EIGENVALUECALCULATORSOLVER_CLAPLACIAN1DCOLUMNMATRIXLOADER_H
