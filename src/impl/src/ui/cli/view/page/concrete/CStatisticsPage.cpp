#include "./CStatisticsPage.h"

#include <sstream>
#include <iomanip>

CStatisticsPage::CStatisticsPage(std::chrono::steady_clock::time_point::duration loadingDuration,
                                 std::chrono::steady_clock::time_point::duration computationDuration,
                                 size_t numberOfIterations)
        : m_LoadingDuration(loadingDuration),
          m_ComputationDuration(computationDuration),
          m_NumberOfIterations(numberOfIterations) {
}

[[nodiscard]]
std::string CStatisticsPage::Render() const {
  std::stringstream ss;
  ss << "--------------------------------------------------\n";
  ss << "Statistics:\n";
  ss << "Loading time: " << ToString(m_LoadingDuration) << "\n";
  ss << "Computation time: " << ToString(m_ComputationDuration) << "\n";
  ss << "Number of iterations: " << m_NumberOfIterations << "\n";
  return ss.str();
}

std::string CStatisticsPage::ToString(std::chrono::steady_clock::time_point::duration duration) const {
  long milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
  //cout << "dbg ms:" << milliseconds << "\n";
  long seconds = milliseconds / 1000;
  milliseconds %= 1000;
  std::stringstream ss;
  ss << seconds << "." << std::setfill('0') << std::setw(3) << milliseconds << " s";
  return ss.str();
}
