#ifndef EIGENVALUECALCULATORSOLVER_CSHOWHELPOPTIONHANDLER_H
#define EIGENVALUECALCULATORSOLVER_CSHOWHELPOPTIONHANDLER_H

#include "./COptionHandler.h"
#include "../../controller/CCommandLineController.h"

class CHelpWasShownException : public std::exception {
public:
protected:
};

class CShowHelpOptionHandler : public COptionHandler {
public:
  explicit CShowHelpOptionHandler(CCommandLineController &controller);

  // argumentsCnt must be >= 1
  void Handle(int &argumentsCnt, char **&argumentsVector) override;

  static constexpr const char OptionShortName[] = "-h";
  static constexpr const char OptionFullName[] = "--help";
protected:
  CCommandLineController &m_Controller;
};

#endif //EIGENVALUECALCULATORSOLVER_CSHOWHELPOPTIONHANDLER_H
