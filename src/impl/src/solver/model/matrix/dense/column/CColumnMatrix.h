#ifndef EIGENVALUECALCULATORSOLVER_CCOLUMNMATRIX_H
#define EIGENVALUECALCULATORSOLVER_CCOLUMNMATRIX_H

#include "./CColumnMatrixConst.h"

/// Providing methods for a column-major matrix
class CColumnMatrix : public CColumnMatrixConst {
  using Scalar = double;
public:
  using CColumnMatrixConst::At;
  using CColumnMatrixConst::GetRowsCnt;
  using CColumnMatrixConst::GetColumnsCnt;
  using CColumnMatrixConst::IsSquare;
  using CColumnMatrixConst::AtRef;
  using CColumnMatrixConst::ToIdentity;
  using CColumnMatrixConst::Assign;
  using CColumnMatrixConst::GetMaximalRowsCnt;
  using CColumnMatrixConst::GetColumnConstRefView;
  using CColumnMatrixConst::GetSubMatrixConstRefView;
  using CColumnMatrixConst::Transposed;
  using CColumnMatrixConst::MultiplyLeftColumns;
  using CColumnMatrixConst::Multiply;

  explicit CColumnMatrix(CDense2DBlockView &&blockView);

  CColumnMatrix(
          Scalar *data,
          size_t rowsCnt,
          size_t columnsCnt,
          size_t nextColumnShift);

  CColumnMatrix(CColumnMatrix &&other) noexcept;

  CColumnMatrix &operator=(CColumnMatrix &&other) noexcept;

  void Assign(const Scalar *data, size_t rowsCnt, size_t columnsCnt, size_t nextColumnShift) {
    m_BlockView.Assign(data, rowsCnt, columnsCnt, nextColumnShift);
  }

  [[nodiscard]]
  CColumnVector<Scalar> GetColumnRefView(
          size_t column,
          size_t firstElementRow,
          size_t lastElementRow) {
    return CColumnVector<Scalar>(m_BlockView.GetContinuousRefView(
            column,
            firstElementRow,
            lastElementRow));
  }

  [[nodiscard]]
  CColumnVector<Scalar> GetColumnRefView(
          size_t column) {
    return CColumnVector<Scalar>(m_BlockView.GetContinuousRefView(
            column));
  }

  [[nodiscard]]
  CColumnVector<Scalar> GetColumnToDiagonalRefView(size_t column) {
    return CColumnVector<Scalar>(m_BlockView.GetContinuousToDiagonalRefView(
            column));
  }

  CColumnVector<Scalar> GetLastColumnRefView() {
    /*if(GetColumnsCnt() >= m_MaximalColumnsCnt)
    {
      throw std::invalid_argument("CColumnMatrixDynamicDimensionsHolding->GetEndColumnRefView(): "
                             "GetColumnsCnt() >= m_MaximalColumnsCnt");
    }*/
    /*if(GetRowsCnt() <= 0)
    {
      throw std::invalid_argument("CColumnMatrixDynamicDimensionsHolding->GetEndColumnRefView(): "
                             "GetRowsCnt() <= 0");
    }*/
    return GetColumnRefView(
            GetColumnsCnt() - 1,
            0,
            GetRowsCnt() - 1);
  }

  [[nodiscard]]
  CColumnMatrix GetSubMatrixRefView(
          size_t firstRow,
          size_t lastRow,
          size_t firstColumn,
          size_t lastColumn) {
    return CColumnMatrix{m_BlockView.GetSubMatrixRefView(
            firstRow,
            lastRow,
            firstColumn,
            lastColumn)};
  }


  virtual
  ~CColumnMatrix();

protected:
};

inline
CColumnMatrix::CColumnMatrix(CDense2DBlockView &&blockView)
        : CColumnMatrixConst(std::move(blockView)) {
}

inline
CColumnMatrix::CColumnMatrix(
        Scalar *data,
        size_t rowsCnt,
        size_t columnsCnt,
        size_t nextColumnShift)
        : CColumnMatrixConst(data,
// continuousSize
                             rowsCnt,
// discontinuousSize
                             columnsCnt,
// discontinuousShift
                             nextColumnShift) {
}

inline
CColumnMatrix::CColumnMatrix(CColumnMatrix &&other) noexcept
        : CColumnMatrixConst(std::move(other.m_BlockView)) {
}

inline CColumnMatrix &
CColumnMatrix::operator=(CColumnMatrix &&other) noexcept {
  m_BlockView = std::move(other.m_BlockView);
  return *this;
}

inline
CColumnMatrix::~CColumnMatrix() = default;

#endif //EIGENVALUECALCULATORSOLVER_CCOLUMNMATRIX_H
