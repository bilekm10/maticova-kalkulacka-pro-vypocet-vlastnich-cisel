#ifndef EIGENVALUECALCULATORSOLVER_CRANDOMCOLUMNVECTORLOADER_H
#define EIGENVALUECALCULATORSOLVER_CRANDOMCOLUMNVECTORLOADER_H

#include "CColumnVectorLoader.h"
#include "../../../model/vector/column/CColumnVectorHolding.h"

#include <vector>
#include <stdexcept>
#include <random>

/// Generating a CColumnVectorHolding using a pseudorandom algorithm
template<typename Scalar>
class CRandomColumnVectorLoader : public CColumnVectorLoader<Scalar> {
public:
  CRandomColumnVectorLoader(size_t rowsCnt, unsigned long seed);

  [[nodiscard]]
  CColumnVectorHolding<Scalar> Load() override;

protected:
  size_t m_RowsCnt;
  Scalar m_LowerBound;
  Scalar m_UpperBound;
  unsigned long m_Seed;
  std::default_random_engine m_RandomEngine;
};

template<typename Scalar>
CRandomColumnVectorLoader<Scalar>
::CRandomColumnVectorLoader(size_t rowsCnt, unsigned long seed)
        : CColumnVectorLoader<Scalar>(),
          m_RowsCnt(rowsCnt),
          m_LowerBound(-1),
          m_UpperBound(1),
          m_Seed(seed),
          m_RandomEngine(m_Seed) {
}

template<typename Scalar>
[[nodiscard]]
CColumnVectorHolding<Scalar> CRandomColumnVectorLoader<Scalar>::Load() {
  std::uniform_real_distribution<Scalar> uniformDistribution(m_LowerBound, m_UpperBound);
  auto *data = new Scalar[m_RowsCnt];
  for (size_t i = 0; i < m_RowsCnt; ++i) {
    data[i] = uniformDistribution(m_RandomEngine);
  }
  return CColumnVectorHolding<Scalar>(&data, m_RowsCnt);
}

#endif //EIGENVALUECALCULATORSOLVER_CRANDOMCOLUMNVECTORLOADER_H
