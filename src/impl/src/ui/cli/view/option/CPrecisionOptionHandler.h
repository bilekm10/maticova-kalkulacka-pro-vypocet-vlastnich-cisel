#ifndef EIGENVALUECALCULATORSOLVER_CPRECISIONOPTIONHANDLER_H
#define EIGENVALUECALCULATORSOLVER_CPRECISIONOPTIONHANDLER_H

#include "./COptionHandler.h"

class CPrecisionOptionHandler : public COptionHandler {
public:
  explicit CPrecisionOptionHandler(std::optional<double> &precision);

  // argumentsCnt must be >= 1
  void Handle(int &argumentsCnt, char **&argumentsVector) override;

  static constexpr const char OptionShortName[] = "-p";
  static constexpr const char OptionFullName[] = "--precision";
  static constexpr const char OptionVerboseName[] = "Precision";
  static constexpr const char NumberFormat[] = "%lf";
protected:
  std::optional<double> &m_Precision;
  double m_Number;
};

#endif //EIGENVALUECALCULATORSOLVER_CPRECISIONOPTIONHANDLER_H
