#ifndef EIGENVALUECALCULATORSOLVER_CMATRIXVECTORMULTIPLICATION_H
#define EIGENVALUECALCULATORSOLVER_CMATRIXVECTORMULTIPLICATION_H

#include "../../model/vector/column/CColumnVector.h"
#include "../../model/matrix/sparse/CCompressedSparseRowMatrix.h"
#include "../../model/matrix/dense/column/CColumnMatrix.h"
#include "../../model/matrix/dense/row/CRowMatrix.h"

#include <stdexcept>
#include <vector>
#include <type_traits>

// template template https://stackoverflow.com/questions/213761/what-are-some-uses-of-template-template-parameters

// the last argument can be ignored, present only for common implementation for some matrix types
/// Multiplication of a matrix with a vector
template<typename MatrixType, typename = void>
class CMatrixVectorMultiplication {
};

template<>
class CMatrixVectorMultiplication<CCompressedSparseRowMatrix> {
public:
  explicit CMatrixVectorMultiplication(const CCompressedSparseRowMatrix &matrix);

  void Multiply(const CColumnVectorConst<double> &columnVector, CColumnVector<double> &result) {
    m_Matrix.Multiply(columnVector, result);
  }

protected:
  const CCompressedSparseRowMatrix &m_Matrix;
};

inline CMatrixVectorMultiplication<CCompressedSparseRowMatrix>::CMatrixVectorMultiplication(
        const CCompressedSparseRowMatrix &matrix)
        : m_Matrix(matrix) {
}

#endif //EIGENVALUECALCULATORSOLVER_CMATRIXVECTORMULTIPLICATION_H
