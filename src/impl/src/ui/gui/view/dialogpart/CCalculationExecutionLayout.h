#ifndef EIGENVALUECALCULATORGUI_CCALCULATIONEXECUTIONLAYOUT_H
#define EIGENVALUECALCULATORGUI_CCALCULATIONEXECUTIONLAYOUT_H

#include "../page/CComputationStatePage.h"

#include <QAction>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QTextEdit>
#include <QSpinBox>
#include <QDoubleValidator>
#include <QMessageBox>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QElapsedTimer>
#include <QTimer>

QT_BEGIN_NAMESPACE
class QAction;

class QDialogButtonBox;

class QGroupBox;

class QLabel;

class QLineEdit;

class QMenu;

class QMenuBar;

class QPushButton;

class QTextEdit;

class QComboBox;

class QSpinBox;

class QDoubleValidator;

class QMessageBox;

class QElapsedTimer;

QT_END_NAMESPACE

class CCalculationExecutionLayout : public QVBoxLayout {
Q_OBJECT

public:
  CCalculationExecutionLayout();

  /*QPushButton * GetCalculateBtn()
  { return m_CalculateBtn; }
  QPushButton * GetStopBtn()
  { return m_StopBtn; }*/

  QAction *GetCalculateBtnAction() { return m_CalculateBtnAction; }

  QAction *GetStopBtnAction() { return m_StopBtnAction; }

  /*QLabel * GetComputationStateValue()
  { return m_ComputationStateValue; }*/
  void SetComputationStateValueText(const QString &text) { m_ComputationStateValue->setText(text); }

  void SetComputationStateValue(EComputationState computationState) {
    m_ComputationStateValue->setText(CComputationStatePage{computationState}.Render());
  }

  /*void SetNumberOfIterationsValueText(const QString & text)
  { m_NumberOfIterationsValue->setText(text); }*/
  void SetNumberOfIterationsValue(size_t numberOfIterations) {
    m_NumberOfIterationsValue->setText(QString::number(numberOfIterations));
  }

  void SetElapsedTimeValuesToZero();

  void LoadingStart();

  void LoadingEnd();

  void ComputationStart();

  void ComputationEnd();

protected:
  static constexpr int VerticalSpacing = 3;

  void UpdateLoadingTime();

  void UpdateComputationTime();

  void CreateCalculateBtn();

  void CreateStopBtn();

  void CreateCalculateStopBtn();

  void CreateTimeRow(
          QLabel *&timeLabel,
          const QString &timeLabelText,
          QLabel *&timeValue);

  void CreateLoadingRow();

  void CreateComputationRow();

  void CreateStatisticsLayout();

  void CreateActions();

  QHBoxLayout *m_CalculateStopBtnLayout;
  QBoxLayout *m_CalculateBtnBoxLayout;
  QPushButton *m_CalculateBtn;
  QPushButton *m_StopBtn;

  QAction *m_CalculateBtnAction;
  QAction *m_StopBtnAction;

  QFormLayout *m_StatisticsLayout;

  QLabel *m_ComputationStateLabel;
  QLabel *m_ComputationStateValue;

  QTimer m_UpdateLoadingTimer;
  QElapsedTimer m_LoadingTimer;
  QLabel *m_LoadingTimeLabel;
  QLabel *m_LoadingTimeValue;

  QTimer m_UpdateComputationTimer;
  QElapsedTimer m_ComputationTimer;
  QLabel *m_ComputationTimeLabel;
  QLabel *m_ComputationTimeValue;

  QLabel *m_NumberOfIterationsLabel;
  QLabel *m_NumberOfIterationsValue;
  //QAction * m_NumberOfIterationsUpdateAction;
};

#endif //EIGENVALUECALCULATORGUI_CCALCULATIONEXECUTIONLAYOUT_H
