#ifndef EIGENVALUECALCULATORSOLVER_CSCALARCOMPARATOR_H
#define EIGENVALUECALCULATORSOLVER_CSCALARCOMPARATOR_H

#include "ESortingCriterion.h"

#include <complex>
#include <stdexcept>
#include <iostream>

// all specializations should somehow store same eigenvalues next to each other and std::complex conjugates must be adjacent; adjacency of std::complex conjugates can be reached by stable_sort; maybe always also compare real and imaginary parts, so that e.g LargestRealPart stores eigenvalues with only real part after std::complex ones and std::complex ones are also compared with magnitude
/// Compare scalars with the sorting criterion
template<typename Scalar, ESortingCriterion Criterion>
class CScalarComparator {
};

// template specialization for std::complex LargestMagnitude comparison
template<typename Scalar>
class CScalarComparator<std::complex<Scalar>, ESortingCriterion::LargestMagnitude> {
public:
  static
  bool Compare(std::complex<Scalar> a, std::complex<Scalar> b) {
    //return std::abs(a) > std::abs(b);
    if (std::abs(a) == std::abs(b)) {
      // bigger real part comes before
      if (a.real() == b.real())
        // to have positive imaginary part first
        return a.imag() > b.imag();
      return a.real() > b.real();
    }
    return std::abs(a) > std::abs(b);
  }

protected:
};

// template specialization for std::complex SmallestMagnitude comparison
template<typename Scalar>
class CScalarComparator<std::complex<Scalar>, ESortingCriterion::SmallestMagnitude> {
public:
  static
  bool Compare(std::complex<Scalar> a, std::complex<Scalar> b) {
    //return std::abs(a) < std::abs(b);
    if (std::abs(a) == std::abs(b)) {
      // smaller real part comes before
      if (a.real() == b.real())
        // to have positive imaginary part first
        return a.imag() > b.imag();
      return a.real() < b.real();
    }
    return std::abs(a) < std::abs(b);
  }

protected:
};

// template specialization for std::complex LargestRealPart comparison
template<typename Scalar>
class CScalarComparator<std::complex<Scalar>, ESortingCriterion::LargestRealPart> {
public:
  static
  bool Compare(std::complex<Scalar> a, std::complex<Scalar> b) {
    //return a.real() > b.real();
    if (a.real() == b.real())
      // to have positive imaginary part first
      return a.imag() > b.imag();
    return a.real() > b.real();
  }

protected:
};

// template specialization for std::complex SmallestRealPart comparison
template<typename Scalar>
class CScalarComparator<std::complex<Scalar>, ESortingCriterion::SmallestRealPart> {
public:
  static
  bool Compare(std::complex<Scalar> a, std::complex<Scalar> b) {
    //return a.real() < b.real();
    if (a.real() == b.real() && a.imag() == -b.imag())
      // to have positive imaginary part first
      return a.imag() > b.imag();
    return a.real() < b.real();
  }

protected:
};

#endif //EIGENVALUECALCULATORSOLVER_CSCALARCOMPARATOR_H
