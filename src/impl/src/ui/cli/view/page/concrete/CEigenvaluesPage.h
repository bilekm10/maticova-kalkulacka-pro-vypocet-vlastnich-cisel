#ifndef EIGENVALUECALCULATORSOLVER_CEIGENVALUESPAGE_H
#define EIGENVALUECALCULATORSOLVER_CEIGENVALUESPAGE_H

#include <string>
#include <iomanip>

#include "../CTextPage.h"
#include "../../../../../solver/model/vector/column/CColumnVector.h"

//#define DBG_FULL_PRINT_PREC

class CEigenvaluesPage : public CTextPage {
  using Scalar = double;
public:
  explicit CEigenvaluesPage(
          const CColumnVectorConst<std::complex<double>> &eigenvalues,
          size_t numberOfWantedEigenvalues,
          uint8_t precisionManipulator = 10,
          const std::string &separator = "\n",
          bool omitZeroImaginaryPart = true);

  explicit CEigenvaluesPage(
          const CColumnVectorConst<std::complex<double>> &eigenvalues,
          size_t numberOfWantedEigenvalues,
          double precision,
          const std::string &separator = "\n",
          bool omitZeroImaginaryPart = true);

  [[nodiscard]]
  std::string Render() const override;

protected:
  static uint8_t CalculatePrecisionManipulator(double precision);

  static uint8_t CalculatePrecisionManipulatorWithoutLowerBound(double precision);

  const CColumnVectorConst<std::complex<double>> &m_Eigenvalues;
  const size_t m_NumberOfWantedEigenvalues;
  const uint8_t m_PrecisionManipulator;
  const std::string &m_Separator;
  const bool m_OmitZeroImaginaryPart;
};

#endif //EIGENVALUECALCULATORSOLVER_CEIGENVALUESPAGE_H
