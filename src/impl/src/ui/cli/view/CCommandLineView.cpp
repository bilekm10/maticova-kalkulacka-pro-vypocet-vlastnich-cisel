#include "./CCommandLineView.h"

CCommandLineView::CCommandLineView(
        CTextExporter &standardOutputTextExporter,
        CCommandLineController &controller)
        : m_StandardOutputTextExporter(standardOutputTextExporter),
          m_Controller(controller) {
}

void CCommandLineView::Run(int argumentsCnt, char **argumentsVector) {
  --argumentsCnt;
  ++argumentsVector;
  CRestartedArnoldiInputArguments arnoldiArguments;
  CNumberOfWantedEigenvaluesOptionHandler numberOfWantedEigenvaluesOptionHandler{
          arnoldiArguments.m_NumberOfWantedEigenvalues};
  CFactorizationSizeToRestartAtOptionHandler factorizationSizeToRestartAtOptionHandler{
          arnoldiArguments.m_FactorizationSizeToRestartAt};
  CInputMatrixFilenameOptionHandler inputMatrixFilenameOptionHandler{
          arnoldiArguments.m_InputMatrixFilename};
  CSortingCriterionOptionHandler sortingCriterionOptionHandler{
          arnoldiArguments.m_SortingCriterion};
  CPrecisionOptionHandler precisionOptionHandler{
          arnoldiArguments.m_Precision};
  CMaximalNumberOfIterationsOptionHandler maximalNumberOfIterationsOptionHandler{
          arnoldiArguments.m_MaximalNumberOfIterations};
  CMaximalNumberOfOrthogonalizationIterationsOptionHandler maximalNumberOfOrthogonalizationIterationsOptionHandler{
          arnoldiArguments.m_MaximalNumberOfOrthogonalizationIterations};
  CIterativeOrthogonalizationEtaOptionHandler iterativeOrthogonalizationEtaOptionHandler{
          arnoldiArguments.m_IterativeOrthogonalizationEta};
  CShowHelpOptionHandler showHelpOptionHandler{m_Controller};
  CThrowNoOptionMatchedHandler throwNoOptionMatchedHandler;
  showHelpOptionHandler.SetNextOptionHandler(&throwNoOptionMatchedHandler);
  iterativeOrthogonalizationEtaOptionHandler.SetNextOptionHandler(&showHelpOptionHandler);
  maximalNumberOfOrthogonalizationIterationsOptionHandler.SetNextOptionHandler(
          &iterativeOrthogonalizationEtaOptionHandler);
  maximalNumberOfIterationsOptionHandler.SetNextOptionHandler(&maximalNumberOfOrthogonalizationIterationsOptionHandler);
  precisionOptionHandler.SetNextOptionHandler(&maximalNumberOfIterationsOptionHandler);
  sortingCriterionOptionHandler.SetNextOptionHandler(&precisionOptionHandler);
  inputMatrixFilenameOptionHandler.SetNextOptionHandler(&sortingCriterionOptionHandler);
  factorizationSizeToRestartAtOptionHandler.SetNextOptionHandler(&inputMatrixFilenameOptionHandler);
  numberOfWantedEigenvaluesOptionHandler.SetNextOptionHandler(&factorizationSizeToRestartAtOptionHandler);
  COptionHandler &optionHandler = numberOfWantedEigenvaluesOptionHandler;
  while (argumentsCnt > 0) {
    try {
      optionHandler.Handle(argumentsCnt, argumentsVector);
    }
    catch (const CHelpWasShownException &helpWasShownException) {
      // exceptional state when the arguments contained help-showing option
      return;
    }
            #ifdef DBG_OPTIONS_FAIL_PRINT
      catch(const std::invalid_argument & exc)
      {
        //std::cout << std::string(argumentsVector[0] + 1) << " ";
        if(strncmp(exc.what(), "No option matched for |", length("No option matched for |")) == 0)
        {
          --argumentsCnt;
          ++argumentsVector;
        }
        std::cout << exc.what() << ", remaining: " << argumentsCnt << std::endl;
        continue;
      }
      catch(const std::exception & exc)
      {
        --argumentsCnt;
        ++argumentsVector;
        std::cout << "std::exception: " << exc.what() << ", remaining: " << argumentsCnt << std::endl;
        continue;
      }
      std::cout << "matched: k" << numberOfWantedEigenvalues.value_or(1234567890) << " matched: m" << factorizationSizeToRestartAt.value_or(1234567890) << " matched: s" << int(sortingCriterion.value_or(ESortingCriterion::LargestRealPart)) << ", remaining: " << argumentsCnt << std::endl;
            #else
    catch (const std::exception &exc) {
      m_StandardOutputTextExporter.Export(
              "Option parsing error: " +
              std::string(exc.what()) +
              "\nExecution terminated.\n" +
              m_HelpRedirectionMessage);
      return;
    }
    catch (...) {
      m_StandardOutputTextExporter.Export(
              "Unknown exception was thrown.\n"
              "Execution terminated.\n" +
              m_HelpRedirectionMessage);
      return;
    }
    #endif
  }
  #ifdef DBG_PARSED_OPTIONS_PRINT
  std::cout << std::endl;
    if(numberOfWantedEigenvalues)
      std::cout << "numberOfWantedEigenvalues: " << numberOfWantedEigenvalues.value() << std::endl;
    else std::cout << "\n";
    if(factorizationSizeToRestartAt)
      std::cout << "factorizationSizeToRestartAt: " << factorizationSizeToRestartAt.value() << std::endl;
    else std::cout << "\n";
    if(inputMatrixFilename)
      std::cout << "inputMatrixFilename: " << inputMatrixFilename.value() << std::endl;
    else std::cout << "\n";
    if(sortingCriterion)
      std::cout << "sortingCriterion: " << int(sortingCriterion.value()) << std::endl;
    else std::cout << "\n";
    if(precision)
      std::cout << "precision: " << precision.value() << std::endl;
    else std::cout << "\n";
    if(maximalNumberOfIterations)
      std::cout << "maximalNumberOfIterations: " << maximalNumberOfIterations.value() << std::endl;
    else std::cout << "\n";
    if(maximalNumberOfOrthogonalizationIterations)
      std::cout << "maximalNumberOfOrthogonalizationIterations: " << int(maximalNumberOfOrthogonalizationIterations.value()) << std::endl;
    else std::cout << "\n";
    if(iterativeOrthogonalizationEta)
      std::cout << "iterativeOrthogonalizationEta: " << iterativeOrthogonalizationEta.value() << std::endl;
    else std::cout << "\n";
  #endif
  try {
    arnoldiArguments.Validate();
    arnoldiArguments.SetEmptyToDefault();
  }
  catch (const std::exception &exc) {
    m_StandardOutputTextExporter.Export(
            "Options error: " +
            std::string(exc.what()) +
            "\nExecution terminated.\n" +
            m_HelpRedirectionMessage);
    return;
  }
  try {
    m_Controller.ComputeFewEigenvalues(
            arnoldiArguments);
  }
  catch (const std::exception &exc) {
    m_StandardOutputTextExporter.Export(
            "Error: " +
            std::string(exc.what()) +
            "\nExecution terminated.\n" +
            m_HelpRedirectionMessage);
    return;
  }
  catch (...) {
    m_StandardOutputTextExporter.Export(
            "Unknown exception was thrown.\n"
            "Execution terminated.\n" +
            m_HelpRedirectionMessage);
    return;
  }
}
