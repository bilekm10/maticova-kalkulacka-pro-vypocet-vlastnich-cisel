#ifndef EIGENVALUECALCULATORSOLVER_CSTATISTICSPAGE_H
#define EIGENVALUECALCULATORSOLVER_CSTATISTICSPAGE_H

#include "../CTextPage.h"

#include <string>
#include <chrono>

class CStatisticsPage : public CTextPage {
public:
  CStatisticsPage(std::chrono::steady_clock::time_point::duration loadingDuration,
                  std::chrono::steady_clock::time_point::duration computationDuration,
                  size_t numberOfIterations);

  [[nodiscard]]
  std::string Render() const override
  /*{
    std::stringstream ss;
    ss << "--------------------------------------------------\n";
    ss << "Statistics:\n";
    ss << "Loading time: " << ToString(m_LoadingDuration) << "\n";
    ss << "Computation time: " << ToString(m_ComputationDuration) << "\n";
    ss << "Number of iterations: " << m_NumberOfIterations << "\n";
    return ss.str();
  }*/;

protected:
  [[nodiscard]]
  std::string ToString(std::chrono::steady_clock::time_point::duration duration) const;

  std::chrono::steady_clock::time_point::duration m_LoadingDuration;
  std::chrono::steady_clock::time_point::duration m_ComputationDuration;
  size_t m_NumberOfIterations;
};

#endif //EIGENVALUECALCULATORSOLVER_CSTATISTICSPAGE_H
