#ifndef EIGENVALUECALCULATORSOLVER_CCOLUMNVECTORLOADER_H
#define EIGENVALUECALCULATORSOLVER_CCOLUMNVECTORLOADER_H

#include "../../../model/vector/column/CColumnVectorHolding.h"

/// Loading a CColumnVectorHolding
template<typename Scalar>
class CColumnVectorLoader {
public:
  [[nodiscard]] virtual
  CColumnVectorHolding<Scalar> Load() = 0;

protected:
};

#endif //EIGENVALUECALCULATORSOLVER_CCOLUMNVECTORLOADER_H
