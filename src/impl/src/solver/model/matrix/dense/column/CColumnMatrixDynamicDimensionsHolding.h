#ifndef EIGENVALUECALCULATORSOLVER_CCOLUMNMATRIXDYNAMICDIMENSIONSHOLDING_H
#define EIGENVALUECALCULATORSOLVER_CCOLUMNMATRIXDYNAMICDIMENSIONSHOLDING_H

#include "./CColumnMatrix.h"
//#include "../../../view/matrix/CDense2DStorage.h"

//#define DBG_PRINT_DEALLOCATION_TILDE

// delete if new[] for deepCopy

/// Providing methods for and storing a column-major matrix with dynamic dimensions with known upper bound
class CColumnMatrixDynamicDimensionsHolding : public CColumnMatrix {
  using Scalar = double;
public:
  CColumnMatrixDynamicDimensionsHolding(
          size_t rowsCnt,
          size_t columnsCnt,
          size_t maximalRowsCnt,
          size_t maximalColumnsCnt,
          bool setToZero = true);

  CColumnMatrixDynamicDimensionsHolding(
          size_t rowsCnt,
          size_t columnsCnt,
          bool setToZero = true);

  CColumnMatrixDynamicDimensionsHolding(
          size_t rowsCnt,
          bool setToZero = true);

  CColumnMatrixDynamicDimensionsHolding(
          Scalar **dataMaximalDimensionsSized,
          size_t rowsCnt,
          size_t columnsCnt,
          size_t maximalRowsCnt,
          size_t maximalColumnsCnt);

  CColumnMatrixDynamicDimensionsHolding(
          // the data to copy with size rowsCnt * columnsCnt
          Scalar *dataToCopy,
          // rowsCnt is also maximalRowsCnt
          size_t rowsCnt,
          size_t columnsCnt,
          bool setRestToZero,
          size_t maximalColumnsCnt);/*

  CColumnMatrixDynamicDimensionsHolding & operator=(
          CColumnMatrixDynamicDimensionsHolding && other)
  {
    m_BlockView = std::move(other.m_BlockView);
    return *this;
  }*/

  CColumnMatrixDynamicDimensionsHolding(CColumnMatrixDynamicDimensionsHolding &&other) noexcept;

  CColumnMatrixDynamicDimensionsHolding &operator=(CColumnMatrixDynamicDimensionsHolding &&other) noexcept;

  [[nodiscard]]
  CColumnVector<Scalar> GetEndColumnRefView() {
    IncreaseColumnsCnt(1);
    auto ret = CColumnVector<Scalar>(m_BlockView.GetContinuousRefView(
            GetColumnsCnt() - 1));
    DecreaseColumnsCnt(1);
    return ret;
  }

  void ChangeColumnsCnt(size_t newColumnsCnt) {
    if (newColumnsCnt > GetMaximalColumnsCnt())
      throw std::invalid_argument("CColumnMatrixDynamicDimensionsHolding ChangeColumnsCnt(): "
                                  "newColumnsCnt > MaximalColumnsCnt");
    m_BlockView.ChangeDiscontinuousSize(newColumnsCnt);
  }

  void IncreaseColumnsCnt(size_t cnt = 1) {
    ChangeColumnsCnt(GetColumnsCnt() + cnt);
  }

  void DecreaseColumnsCnt(size_t cnt = 1) {
    ChangeColumnsCnt(GetColumnsCnt() - cnt);
  }

  void ChangeRowsCnt(size_t newRowsCnt) {
    if (newRowsCnt > GetMaximalRowsCnt())
      throw std::invalid_argument("CRowMatrixDynamicDimensionsHolding ChangeRowsCnt(): "
                                  "newRowsCnt > MaximalRowsCnt");
    m_BlockView.ChangeContinuousSize(newRowsCnt);
  }

  void IncreaseRowsCnt(size_t cnt = 1) {
    ChangeRowsCnt(GetRowsCnt() + cnt);
  }

  void DecreaseRowsCnt(size_t cnt = 1) {
    ChangeRowsCnt(GetRowsCnt() - cnt);
  }

  void ChangeBothDimensions(size_t newRowsCnt, size_t newColumnsCnt) {
    ChangeRowsCnt(newRowsCnt);
    ChangeColumnsCnt(newColumnsCnt);
  }

  void IncreaseBothDimensions(size_t cnt = 1) {
    ChangeBothDimensions(GetRowsCnt() + cnt, GetColumnsCnt() + cnt);
  }

  void ChangeBothDimensions(size_t newRowsAndColumnsCnt) {
    ChangeBothDimensions(newRowsAndColumnsCnt, newRowsAndColumnsCnt);
  }

  [[nodiscard]]
  size_t GetMaximalColumnsCnt() const {
    return m_MaximalColumnsCnt;
  }

  virtual
  ~CColumnMatrixDynamicDimensionsHolding();

protected:
  size_t m_MaximalColumnsCnt;
};

inline
CColumnMatrixDynamicDimensionsHolding::CColumnMatrixDynamicDimensionsHolding(
        size_t rowsCnt,
        size_t columnsCnt,
        size_t maximalRowsCnt,
        size_t maximalColumnsCnt,
        bool setToZero)
        : CColumnMatrix(
        setToZero
        ? new Scalar[maximalRowsCnt * maximalColumnsCnt]()
        : new Scalar[maximalRowsCnt * maximalColumnsCnt],
        rowsCnt,
        columnsCnt,
        maximalRowsCnt),
          m_MaximalColumnsCnt(maximalColumnsCnt) {
  if (rowsCnt > maximalRowsCnt
      || columnsCnt > maximalColumnsCnt)
    throw std::invalid_argument("CColumnMatrixDynamicDimensionsHolding constructor: "
                                "rowsCnt > maximalRowsCnt "
                                "|| columnsCnt > maximalColumnsCnt");
  if (maximalRowsCnt <= 0
      || maximalColumnsCnt <= 0)
    throw std::invalid_argument("CColumnMatrixDynamicDimensionsHolding constructor: "
                                "maximalRowsCnt <= 0 "
                                "|| maximalColumnsCnt <= 0");
}

inline
CColumnMatrixDynamicDimensionsHolding::CColumnMatrixDynamicDimensionsHolding(
        size_t rowsCnt,
        size_t columnsCnt,
        bool setToZero)
        : CColumnMatrixDynamicDimensionsHolding(
        rowsCnt,
        columnsCnt,
        rowsCnt,
        columnsCnt,
        setToZero) {
}

inline
CColumnMatrixDynamicDimensionsHolding::CColumnMatrixDynamicDimensionsHolding(
        size_t rowsCnt,
        bool setToZero)
        : CColumnMatrixDynamicDimensionsHolding(
        rowsCnt,
        rowsCnt,
        setToZero) {
}

inline
CColumnMatrixDynamicDimensionsHolding::CColumnMatrixDynamicDimensionsHolding(
        Scalar **dataMaximalDimensionsSized,
        size_t rowsCnt,
        size_t columnsCnt,
        size_t maximalRowsCnt,
        size_t maximalColumnsCnt)
        : CColumnMatrix(
        *dataMaximalDimensionsSized,
        rowsCnt,
        columnsCnt,
        maximalRowsCnt),
          m_MaximalColumnsCnt(maximalColumnsCnt) {
  if (rowsCnt > maximalRowsCnt
      || columnsCnt > maximalColumnsCnt)
    throw std::invalid_argument("CColumnMatrixDynamicDimensionsHolding constructor: "
                                "rowsCnt > maximalRowsCnt "
                                "|| columnsCnt > maximalColumnsCnt");
  if (maximalRowsCnt <= 0
      || maximalColumnsCnt <= 0)
    throw std::invalid_argument("CColumnMatrixDynamicDimensionsHolding constructor: "
                                "maximalRowsCnt <= 0 "
                                "|| maximalColumnsCnt <= 0");
}

inline
CColumnMatrixDynamicDimensionsHolding::CColumnMatrixDynamicDimensionsHolding(
        // the data to copy with size rowsCnt * columnsCnt
        Scalar *dataToCopy,
// rowsCnt is also maximalRowsCnt
        size_t rowsCnt,
        size_t columnsCnt,
        bool setRestToZero,
        size_t maximalColumnsCnt)
        : CColumnMatrix(
        setRestToZero
        ? new Scalar[rowsCnt * maximalColumnsCnt]()
        : new Scalar[rowsCnt * maximalColumnsCnt],
        rowsCnt,
        columnsCnt,
        rowsCnt),
          m_MaximalColumnsCnt(maximalColumnsCnt) {
  if (columnsCnt > maximalColumnsCnt)
    throw std::invalid_argument("CColumnMatrixDynamicDimensionsHolding constructor: "
                                "columnsCnt > maximalColumnsCnt");
  if (rowsCnt <= 0 || columnsCnt <= 0
      || maximalColumnsCnt <= 0)
    throw std::invalid_argument("CColumnMatrixDynamicDimensionsHolding constructor: "
                                "rowsCnt <= 0 || columnsCnt <= 0 "
                                "|| maximalColumnsCnt <= 0");
  std::copy(dataToCopy,
            dataToCopy + rowsCnt * columnsCnt,
            &m_BlockView.AtRef(0, 0));
}

inline
CColumnMatrixDynamicDimensionsHolding::CColumnMatrixDynamicDimensionsHolding(
        CColumnMatrixDynamicDimensionsHolding &&other) noexcept
        : CColumnMatrix(std::move(other.m_BlockView)) {
  m_MaximalColumnsCnt = other.m_MaximalColumnsCnt;
}

inline
CColumnMatrixDynamicDimensionsHolding &
CColumnMatrixDynamicDimensionsHolding
::operator=(CColumnMatrixDynamicDimensionsHolding &&other) noexcept {
  m_BlockView = std::move(other.m_BlockView);
  m_MaximalColumnsCnt = other.m_MaximalColumnsCnt;
  return *this;
}

inline
CColumnMatrixDynamicDimensionsHolding::~CColumnMatrixDynamicDimensionsHolding() {
  if (this->m_BlockView.GetData()) {
    #ifdef DBG_PRINT_DEALLOCATION_TILDE
    printf("~");
    #endif
  }
  m_BlockView.DeleteData();
  //printf("~CColumnMatrixDynamicDimensionsHolding\n");
}

#endif //EIGENVALUECALCULATORSOLVER_CCOLUMNMATRIXDYNAMICDIMENSIONSHOLDING_H
