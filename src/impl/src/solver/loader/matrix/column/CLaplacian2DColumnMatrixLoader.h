#ifndef EIGENVALUECALCULATORSOLVER_CLAPLACIAN2DCOLUMNMATRIXLOADER_H
#define EIGENVALUECALCULATORSOLVER_CLAPLACIAN2DCOLUMNMATRIXLOADER_H

#include "./CColumnMatrixLoader.h"
#include "../../../model/matrix/dense/column/CColumnMatrixDynamicDimensionsHolding.h"

#include <vector>

/// Generate a Laplacian 2D matrix
class CLaplacian2DColumnMatrixLoader : public CColumnMatrixLoader {
public:
  CLaplacian2DColumnMatrixLoader(size_t m, size_t n);

  /*// from SLEPc
  static
  vector<vector<double>> GenerateMatrixData(int m, int n)
  {
    vector<vector<double>> matrixData(N, std::vector<double>(N, 0));
    for(long II = 0; II < N; ++II)
    {
      long i = II / n, j = II - i * n;
      if(i > 0)
      {
        matrixData[II][II-n] = -1.0;
      }
      if(i < m - 1)
      {
        matrixData[II][II+n] = -1.0;
      }
      if(j > 0)
      {
        matrixData[II][II-1] = -1.0;
      }
      if(j < n - 1)
      {
        matrixData[II][II+1] = -1.0;
      }
      matrixData[II][II] = 4.0;
    }
    return matrixData;
  }*/

  [[nodiscard]]
  CColumnMatrixDynamicDimensionsHolding Load() override;

protected:
  size_t m_M;
  size_t m_N;
};

inline
CLaplacian2DColumnMatrixLoader::CLaplacian2DColumnMatrixLoader(size_t m, size_t n)
        : CColumnMatrixLoader(m * n),
          m_M(m),
          m_N(n) {
}

[[nodiscard]]
inline CColumnMatrixDynamicDimensionsHolding
CLaplacian2DColumnMatrixLoader::Load() { // assuming m and n are small enough to fit into long
  CColumnMatrixDynamicDimensionsHolding matrix(m_RowsCnt, m_RowsCnt, m_RowsCnt, m_RowsCnt);
// from SLEPc
  for (long II = 0; II < static_cast<long>(m_RowsCnt); ++II) {
    matrix.AtRef(II, II) = 4.0;
    long i = II / m_N, j = II - i * m_N;
    if (i > 0)
      matrix.AtRef(II, II - m_N) = -1.0;
    if (i < static_cast<long>(m_M) - 1)
      matrix.AtRef(II, II + m_N) = -1.0;
    if (j > 0)
      matrix.AtRef(II, II - 1) = -1.0;
    if (j < static_cast<long>(m_N) - 1)
      matrix.AtRef(II, II + 1) = -1.0;
  }
  return matrix;
}

#endif //EIGENVALUECALCULATORSOLVER_CLAPLACIAN2DCOLUMNMATRIXLOADER_H
