#include "./CPrecisionOptionHandler.h"

CPrecisionOptionHandler::CPrecisionOptionHandler(std::optional<double> &precision)
        : m_Precision(precision),
          m_Number() {
}

// argumentsCnt must be >= 1
void CPrecisionOptionHandler::Handle(int &argumentsCnt, char **&argumentsVector) {
  if (ScanNumberArgumentBothNames<
          double,
          NumberFormat,
          OptionFullName,
          OptionShortName>(argumentsCnt,
                           argumentsVector,
                           m_Number)) {
    if (m_Precision)
      ThrowOptionDuplicationError(OptionVerboseName);
    if (m_Number < 0)
      throw std::invalid_argument(std::string(OptionVerboseName) +
                                  " must be a double >= 0, e.g. \"1e-10\"");
    m_Precision = m_Number;
  } else
    return MoveToNextOptionHandler(argumentsCnt, argumentsVector);
}
