#ifndef EIGENVALUECALCULATORGUI_CCOMPUTATIONSIGNALS_H
#define EIGENVALUECALCULATORGUI_CCOMPUTATIONSIGNALS_H

#include <QObject>
#include <string>

#include "../../../solver/arnoldi/restarted/EComputationState.h"
#include "../../../solver/model/vector/column/CColumnVectorHolding.h"

QT_BEGIN_NAMESPACE
class QMessageBox;

QT_END_NAMESPACE

class CComputationSignals : public QObject {
Q_OBJECT

public:
signals:

  void EigenvaluesConverged(const CColumnVectorHolding <std::complex<double>> &convergedEigenvalues);

  void NumberOfIterationsChanged(size_t numberOfIterations);

  void ComputationStarted();

  void ComputationEnded();

  void ComputationStateChanged(EComputationState computationState);

  void ErrorOccurred(const std::string &errorMessage);

protected:
};

#endif //EIGENVALUECALCULATORGUI_CCOMPUTATIONSIGNALS_H
