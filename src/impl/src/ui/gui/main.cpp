#include <QApplication>

#include "view/CRestartedArnoldiDialogView.h"
#include "controller/CRestartedArnoldiController.h"

int main(int argc, char *argv[]) {
  QApplication app(argc, argv);
  CRestartedArnoldiController restartedArnoldiDialogController;
  CRestartedArnoldiDialogView restartedArnoldiDialogView(restartedArnoldiDialogController);
  restartedArnoldiDialogView.show();
  return app.exec();
}
