#ifndef EIGENVALUECALCULATORSOLVER_CTRANSPOSEDMATRIXMATRIXMULTIPLICATION_H
#define EIGENVALUECALCULATORSOLVER_CTRANSPOSEDMATRIXMATRIXMULTIPLICATION_H

#include "../../model/vector/column/CColumnVector.h"
#include "../../model/matrix/dense/column/CColumnMatrix.h"
#include "../../model/matrix/dense/column/CColumnMatrixDynamicDimensionsHolding.h"
#include "../../model/matrix/dense/row/CRowMatrix.h"

#include <stdexcept>
#include <vector>
#include <type_traits>

/// Multiplication of a matrix transposition with a matrix
template<typename ResultMatrixType, typename LeftMatrixType, typename RightMatrixType>
class CTransposedMatrixMatrixMultiplication {
};

template<>
class CTransposedMatrixMatrixMultiplication<CColumnMatrix, CColumnMatrix, CColumnMatrix> {
public:
  explicit CTransposedMatrixMatrixMultiplication(const CColumnMatrixConst &leftMatrix);

  void Multiply(const CColumnMatrixConst &rightMatrix, CColumnMatrix &result);

protected:
  const CRowMatrixConst m_LeftMatrix;
};

inline CTransposedMatrixMatrixMultiplication<CColumnMatrix, CColumnMatrix, CColumnMatrix>::CTransposedMatrixMatrixMultiplication(
        const CColumnMatrixConst &leftMatrix)
        : m_LeftMatrix(leftMatrix.Transposed()) {
}

inline void CTransposedMatrixMatrixMultiplication<CColumnMatrix, CColumnMatrix, CColumnMatrix>::Multiply(
        const CColumnMatrixConst &rightMatrix, CColumnMatrix &result) {
  if (m_LeftMatrix.GetColumnsCnt() != rightMatrix.GetRowsCnt())
    throw std::invalid_argument("CTransposedMatrixMatrixMultiplication: "
                                "m_LeftMatrix.GetColumnsCnt() != rightMatrix.GetRowsCnt()");
  if (m_LeftMatrix.GetRowsCnt() != result.GetRowsCnt())
    throw std::invalid_argument("CTransposedMatrixMatrixMultiplication: "
                                "m_LeftMatrix.GetRowsCnt() != result.GetRowsCnt()");
  if (rightMatrix.GetColumnsCnt() != result.GetColumnsCnt())
    throw std::invalid_argument("CTransposedMatrixMatrixMultiplication: "
                                "rightMatrix.GetColumnsCnt() != result.GetColumnsCnt()");
  for (size_t j = 0; j < rightMatrix.GetColumnsCnt(); ++j) {
    auto resultColumnRef = result.GetColumnRefView(j);
    m_LeftMatrix.Multiply(rightMatrix.GetColumnConstRefView(j), resultColumnRef);
  }
}

#endif //EIGENVALUECALCULATORSOLVER_CTRANSPOSEDMATRIXMATRIXMULTIPLICATION_H
