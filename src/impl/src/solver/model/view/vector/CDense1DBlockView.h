#ifndef EIGENVALUECALCULATORSOLVER_CDENSE1DBLOCKVIEW_H
#define EIGENVALUECALCULATORSOLVER_CDENSE1DBLOCKVIEW_H

#include <stdexcept>
#include <memory>

/// Accessing a 1D memory block. Create subviews.
template<typename Scalar>
class CDense1DBlockView {
public:
  CDense1DBlockView(Scalar *data, size_t dataSize);

  CDense1DBlockView(const CDense1DBlockView<Scalar> &other);

  CDense1DBlockView(CDense1DBlockView<Scalar> &&other);

  CDense1DBlockView<Scalar> &operator=(CDense1DBlockView<Scalar> &&other);

  void Assign(const Scalar *first, const Scalar *const end);

  // copy from the other view into *this view
  void Assign(const CDense1DBlockView<Scalar> &other);

  /*explicit CDense1DBlockView(CDense1DBlockView<Scalar> && view)
          : m_View(std::move(view))
  {
  }*/

  [[nodiscard]]
  Scalar At(size_t i) const {
    return m_Data[i];
  }

  [[nodiscard]]
  Scalar &AtRef(size_t i) {
    return m_Data[i];
  }

  [[nodiscard]]
  size_t GetSize() const {
    return m_Size;
  }

  void SetSize(size_t newSize) {
    m_Size = newSize;
  }

  void SetTo(Scalar value) {
    for (size_t i = 0; i < m_Size; ++i)
      m_Data[i] = value;
  }

  void SetToZero() {
    SetTo(0);
  }

  template<template<typename> class ReturnType>
  [[nodiscard]]
  ReturnType<Scalar> GetConstSubView(size_t firstElementIndex, size_t lastElementIndex) const {
    SubViewBoundsCheck(firstElementIndex, lastElementIndex);
    return ReturnType<Scalar>{
            m_Data + firstElementIndex,
            lastElementIndex - firstElementIndex + 1};
  }

  template<template<typename> class ReturnType>
  [[nodiscard]]
  ReturnType<Scalar> GetSubView(size_t firstElementIndex, size_t lastElementIndex) {
    SubViewBoundsCheck(firstElementIndex, lastElementIndex);
    return ReturnType<Scalar>{
            m_Data + firstElementIndex,
            lastElementIndex - firstElementIndex + 1};
  }

  [[nodiscard]]
  Scalar *GetData() const {
    return m_Data;
  }

  void DeleteData() {
    delete[] m_Data;
  }

  virtual
  ~CDense1DBlockView() = default;

protected:
  void SubViewBoundsCheck(size_t firstElementIndex, size_t lastElementIndex) const {
    if (firstElementIndex >= m_Size || lastElementIndex >= m_Size)
      throw std::invalid_argument("CDense1DBlockView->SubViewBoundsCheck(): "
                                  "firstElementIndex >= m_Size || lastElementIndex >= m_Size");
    if (lastElementIndex < firstElementIndex)
      throw std::invalid_argument("CDense1DBlockView->SubViewBoundsCheck(): "
                                  "lastElementIndex < firstElementIndex");
  }

  size_t m_Size;
  Scalar *m_Data;
};

template<typename Scalar>
CDense1DBlockView<Scalar>::CDense1DBlockView(Scalar *data, size_t dataSize)
        : m_Size(dataSize),
          m_Data(data) {
  if (dataSize <= 0)
    throw std::invalid_argument("CDense1DBlockView constructor: "
                                "dataSize <= 0");
}

template<typename Scalar>
CDense1DBlockView<Scalar>::CDense1DBlockView(const CDense1DBlockView<Scalar> &other)
        : m_Size(other.m_Size),
          m_Data(other.m_Data) {
}

template<typename Scalar>
CDense1DBlockView<Scalar>::CDense1DBlockView(CDense1DBlockView<Scalar> &&other)
        : m_Size(other.m_Size),
          m_Data(other.m_Data) {
  other.m_Data = nullptr;
}

template<typename Scalar>
CDense1DBlockView<Scalar> &CDense1DBlockView<Scalar>::operator=(CDense1DBlockView<Scalar> &&other) {
  m_Size = other.m_Size;
  m_Data = other.m_Data;
  other.m_Data = nullptr;
  return *this;
}

template<typename Scalar>
void CDense1DBlockView<Scalar>::Assign(const Scalar *first, const Scalar *const end) {
  if (static_cast<long int>(GetSize()) != std::distance(first, end))
    throw std::invalid_argument("CDense2DBlockView->Assign(): "
                                "GetSize() != std::distance(first, end)");
  std::copy(first,
            end,
            m_Data);
}

// copy from the other view into *this view
template<typename Scalar>
void CDense1DBlockView<Scalar>::Assign(const CDense1DBlockView<Scalar> &other) {
  Assign(other.m_Data,
         other.m_Data + other.GetSize());
}

#endif //EIGENVALUECALCULATORSOLVER_CDENSE1DBLOCKVIEW_H
