#include "CRestartedArnoldiController.h"

CRestartedArnoldiController::CRestartedArnoldiController()
        : m_ComputationSignals(nullptr),
          m_ConvergedEigenvaluesPageRenderObserver(nullptr),
          m_ComputationStateRenderObserver(nullptr),
          m_NumberOfIterationsRenderObserver(nullptr),
          m_ErrorMessageTextExporter(nullptr),
          m_StopComputing(false) {
}

void CRestartedArnoldiController::ComputeFewEigenvalues(CRestartedArnoldiInputArguments inputArguments) {
  if (!m_ErrorMessageTextExporter)
    throw std::invalid_argument("CRestartedArnoldiController->"
                                "ComputeFewEigenvalues(): "
                                "m_ErrorMessageTextExporter was not set yet");
  try {
    if (!m_ConvergedEigenvaluesPageRenderObserver)
      throw std::invalid_argument("CRestartedArnoldiController->"
                                  "ComputeFewEigenvalues(): "
                                  "m_ConvergedEigenvaluesPageRenderObserver was not set yet");
    if (!m_ComputationStateRenderObserver)
      throw std::invalid_argument("CRestartedArnoldiController->"
                                  "ComputeFewEigenvalues(): "
                                  "m_ComputationStateRenderObserver was not set yet");
    if (!m_ComputationSignals)
      throw std::invalid_argument("CRestartedArnoldiController->"
                                  "ComputeFewEigenvalues(): "
                                  "m_ComputationSignals was not set yet");
    if (!m_NumberOfIterationsRenderObserver)
      throw std::invalid_argument("CRestartedArnoldiController->"
                                  "ComputeFewEigenvalues(): "
                                  "m_NumberOfIterationsRenderObserver was not set yet");
    using Orthogonalization = CIterativeOrthogonalization<CClassicalGramSchmidtOrthogonalization>;
    using RestartedArnoldi = CImplicitlyRestartedArnoldi <CMatrixVectorMultiplication<CCompressedSparseRowMatrix>, Orthogonalization>;
    inputArguments.Validate();
    inputArguments.SetEmptyToDefault();
    std::ifstream inputMatrixIfs(inputArguments.m_InputMatrixFilename.value());
    if (!inputMatrixIfs)
      throw std::invalid_argument("Couldn't open file \"" +
                                  inputArguments.m_InputMatrixFilename.value() +
                                  "\" for reading the input large matrix.");
    auto inputMatrix = CStreamMatrixMarketCompressedSparseRowLoader{inputMatrixIfs}.Load();
    CColumnVectorHolding<double> initialVector = CRandomColumnVectorLoader < double > {
            inputMatrix.GetRowsCnt(),
            inputMatrix.GetRowsCnt()}.Load();
    CMatrixVectorMultiplication <CCompressedSparseRowMatrix> inputMatrixMultiplication{inputMatrix};
    Orthogonalization orthogonalization(
            CClassicalGramSchmidtOrthogonalization{inputMatrix.GetRowsCnt()},
            inputArguments.m_FactorizationSizeToRestartAt.value(),
            inputArguments.m_MaximalNumberOfOrthogonalizationIterations.value(),
            inputArguments.m_IterativeOrthogonalizationEta.value());
    RestartedArnoldi restartedArnoldi(
            inputMatrixMultiplication,
            inputMatrix.GetRowsCnt(),
            initialVector,
            inputArguments.m_NumberOfWantedEigenvalues.value(),
            inputArguments.m_FactorizationSizeToRestartAt.value(),
            inputArguments.m_SortingCriterion.value(),
            inputArguments.m_MaximalNumberOfIterations.value(),
            inputArguments.m_Precision.value(),
            inputArguments.m_Precision.value(),
            orthogonalization);
    CRestartedArnoldiObserversHelper restartedArnoldiHelper(restartedArnoldi);
    restartedArnoldiHelper.AddConvergedEigenvaluesObserver(m_ConvergedEigenvaluesPageRenderObserver);
    restartedArnoldiHelper.AddComputationStateObserver(m_ComputationStateRenderObserver);
    restartedArnoldiHelper.AddNumberOfIterationsObserver(m_NumberOfIterationsRenderObserver);
    m_ComputationSignals->ComputationStarted();
    restartedArnoldiHelper.Compute(m_StopComputing);
  }
  catch (const std::exception &exc) {
    m_StopComputing = false;
    m_ErrorMessageTextExporter->Export(exc.what());
    return;
  }
  m_StopComputing = false;
  m_ComputationSignals->ComputationEnded();
}