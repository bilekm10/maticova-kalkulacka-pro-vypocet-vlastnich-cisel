#ifndef EIGENVALUECALCULATORSOLVER_CMATRIXKTHSUBDIAGONALMATRIXMULTIPLICATION_H
#define EIGENVALUECALCULATORSOLVER_CMATRIXKTHSUBDIAGONALMATRIXMULTIPLICATION_H

#include "../../model/vector/column/CColumnVector.h"
#include "../../model/matrix/dense/column/CColumnMatrix.h"

#include <stdexcept>
#include <vector>
#include <type_traits>

/// Multiplication of a matrix with a k-th subdiagonal matrix
template<typename ResultMatrixType, typename LeftMatrixType, typename RightKthSubdiagonalMatrixType>
class CMatrixKthSubdiagonalMatrixMultiplication {
};

// rather should somehow provide a way to create [const] ref view over a rectangle in square matrix
template<>
class CMatrixKthSubdiagonalMatrixMultiplication<CColumnMatrix, CColumnMatrix, CColumnMatrix> {
public:
  explicit CMatrixKthSubdiagonalMatrixMultiplication(const CColumnMatrixConst &leftMatrix);

  /// right matrix: elements below the k-th subdiagonal are all 0
  /// columnsCnt: columns count of the rightMatrix to be multiplied, and also the columns count of the resultMatrix
  void Multiply(const CColumnMatrixConst &rightKthSubdiagonalMatrix,
                size_t kthSubdiagonal,
                CColumnMatrix &result);

protected:
  const CColumnMatrixConst &m_LeftMatrix;
};

inline CMatrixKthSubdiagonalMatrixMultiplication<CColumnMatrix, CColumnMatrix, CColumnMatrix>::CMatrixKthSubdiagonalMatrixMultiplication(
        const CColumnMatrixConst &leftMatrix)
        : m_LeftMatrix(leftMatrix) {
}

/// right matrix: elements below the k-th subdiagonal are all 0
/// columnsCnt: columns count of the rightMatrix to be multiplied, and also the columns count of the resultMatrix
inline void CMatrixKthSubdiagonalMatrixMultiplication<CColumnMatrix, CColumnMatrix, CColumnMatrix>::Multiply(
        const CColumnMatrixConst &rightKthSubdiagonalMatrix,
        size_t kthSubdiagonal,
        CColumnMatrix &result) {
  if (m_LeftMatrix.GetColumnsCnt() != rightKthSubdiagonalMatrix.GetRowsCnt())
    throw std::invalid_argument("CMatrixKthSubdiagonalMatrixMultiplication: "
                                "m_LeftMatrix columns count != rightMatrix rows count, "
                                "so cannot multiply the m_LeftMatrix by the rightMatrix "
                                "from the right");
  if (m_LeftMatrix.GetRowsCnt() != result.GetRowsCnt())
    throw std::invalid_argument("CMatrixKthSubdiagonalMatrixMultiplication: "
                                "m_LeftMatrix rows count != resultMatrix rows count, "
                                "so the result of multiplication cannot be stored "
                                "in the resultMatrix");
  if (rightKthSubdiagonalMatrix.GetColumnsCnt() != result.GetColumnsCnt())
    throw std::invalid_argument("CMatrixKthSubdiagonalMatrixMultiplication: "
                                "rightMatrix columns count != resultMatrix columns count, "
                                "so the result of multiplication cannot be stored "
                                "in the resultMatrix");
  for (size_t resultColumnIndex = 0;
       resultColumnIndex < rightKthSubdiagonalMatrix.GetColumnsCnt();
       ++resultColumnIndex) {
    auto resultColumnRef = result.GetColumnRefView(
            resultColumnIndex);
    const size_t rightMatrixColumnEnd = std::min(
            resultColumnIndex + kthSubdiagonal + 1,
            m_LeftMatrix.GetColumnsCnt());
    m_LeftMatrix.MultiplyLeftColumns(
            rightKthSubdiagonalMatrix.GetColumnConstRefView(
                    resultColumnIndex,
                    0,
                    rightMatrixColumnEnd - 1),
            resultColumnRef);
  }
}

#endif //EIGENVALUECALCULATORSOLVER_CMATRIXKTHSUBDIAGONALMATRIXMULTIPLICATION_H
