#ifndef EIGENVALUECALCULATORSOLVER_CHELPPAGE_H
#define EIGENVALUECALCULATORSOLVER_CHELPPAGE_H

#include <string>
#include <sstream>

#include "../CTextPage.h"
#include "../../../../../solver/orthogonalization/refined/CIterativeOrthogonalization.h"
#include "../../../../../solver/arnoldi/restarted/CImplicitlyRestartedArnoldi.h"

class CHelpPage : public CTextPage {
public:
  [[nodiscard]]
  std::string Render() const override;

protected:
  static std::string ToString(double number, uint8_t manipPrecision);
};

#endif //EIGENVALUECALCULATORSOLVER_CHELPPAGE_H
