#include "CComputationStatePage.h"

CComputationStatePage::CComputationStatePage(EComputationState computationState)
        : m_ComputationState(computationState) {
}

QString CComputationStatePage::Render() const {
  if (RestartedArnoldi::IsNotInitialized(m_ComputationState))
    return NotInitialized;
  else if (RestartedArnoldi::IsComputing(m_ComputationState))
    return Computing;
  else if (RestartedArnoldi::IsEnd(m_ComputationState)) {
    if (m_ComputationState == EComputationState::AlreadyComputed)
      return Computed;
    else if (m_ComputationState == EComputationState::ExceptionallyConverged)
      return ExceptionallyConverged;
    else if (m_ComputationState == EComputationState::MaximalNumberOfIterationsReached)
      return MaximalNumberOfIterationsReached;
    else if (m_ComputationState == EComputationState::MaximalNumberOfIterationsReachedAndRestarted)
      return MaximalNumberOfIterationsReachedAndRestarted;
    else
      return ComputationEnded;
  } else
    return NotKnown;
}

bool CComputationStatePage::IsEnd(const char *stateText) {
  return strcmp(stateText, ComputationEnded) == 0
         || strcmp(stateText, Computed) == 0
         || strcmp(stateText, ExceptionallyConverged) == 0
         || strcmp(stateText, MaximalNumberOfIterationsReached) == 0
         || strcmp(stateText, MaximalNumberOfIterationsReachedAndRestarted) == 0;
}
