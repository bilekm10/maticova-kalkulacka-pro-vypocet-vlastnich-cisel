#ifndef EIGENVALUECALCULATORSOLVER_CHESSENBERGQRSINGLESHIFTSTEP_H
#define EIGENVALUECALCULATORSOLVER_CHESSENBERGQRSINGLESHIFTSTEP_H

#include <Spectra/LinAlg/UpperHessenbergQR.h>
#include <complex>
#include <cmath>
#include <iostream>

#include "../../model/vector/column/CColumnVector.h"
#include "../../model/matrix/dense/column/CColumnMatrix.h"
#include "CHessenbergQrQuasiTriangularization.h"

/// Using Spectra solution for performing QR single shift step
class CHessenbergQrSingleShiftStep {
public:
  explicit CHessenbergQrSingleShiftStep(size_t rowsCnt);

  void PerformSingleShiftQrStep(
          Eigen::Matrix<
                  double,
                  Eigen::Dynamic,
                  Eigen::Dynamic,
                  Eigen::ColMajor> &hessenbergMatrix,
          Eigen::Matrix<
                  double,
                  Eigen::Dynamic,
                  Eigen::Dynamic,
                  Eigen::ColMajor> &orthogonalMatrix,
          double shift) {   // ideal interface: void PerformSingleShiftQrStep(CColumnMatrix & hessenbergMatrix, CColumnMatrix & orthogonalMatrix, double shift)
    // QR decomposition of H - mu * I, mu is real
    m_SpectraSingleShiftStep.compute(hessenbergMatrix, shift);
    // Q -> Q * Qi
    m_SpectraSingleShiftStep.apply_YQ(orthogonalMatrix);
    // H -> Q'HQ = RQ + mu * I
    //m_fac.compress_H(m_SpectraSingleShiftStep);
    m_SpectraSingleShiftStep.matrix_QtHQ(hessenbergMatrix);
  }

protected:
  Spectra::UpperHessenbergQR<double> m_SpectraSingleShiftStep;
};

inline
CHessenbergQrSingleShiftStep::CHessenbergQrSingleShiftStep(size_t rowsCnt)
        : m_SpectraSingleShiftStep(rowsCnt) {
}

#endif //EIGENVALUECALCULATORSOLVER_CHESSENBERGQRSINGLESHIFTSTEP_H
