#ifndef EIGENVALUECALCULATORSOLVER_CPARAMETERSINFOPAGE_H
#define EIGENVALUECALCULATORSOLVER_CPARAMETERSINFOPAGE_H

#include <string>

#include "../CTextPage.h"
#include "../../../../uihelpers/CRestartedArnoldiInputArguments.h"

class CParametersInfoPage : public CTextPage {
public:
  explicit CParametersInfoPage(CRestartedArnoldiInputArguments inputArguments);

  [[nodiscard]]
  std::string Render() const override;

protected:
  CRestartedArnoldiInputArguments m_InputArguments;
  const char *m_NotSpecifiedMessage = "<not specified>";
};

#endif //EIGENVALUECALCULATORSOLVER_CPARAMETERSINFOPAGE_H
