#ifndef EIGENVALUECALCULATORSOLVER_ESORTINGCRITERION_H
#define EIGENVALUECALCULATORSOLVER_ESORTINGCRITERION_H

#include <unordered_map>
#include <string>
#include <stdexcept>

/// Scalar sorting criterion
enum class ESortingCriterion {
  LargestMagnitude,
  LargestRealPart,
  SmallestMagnitude,
  SmallestRealPart
};

inline constexpr const char *toVerboseString(ESortingCriterion sortingCriterion) {
  switch (sortingCriterion) {
    case ESortingCriterion::LargestMagnitude:
      return "Largest Magnitude";
    case ESortingCriterion::LargestRealPart:
      return "Largest Real Part";
    case ESortingCriterion::SmallestMagnitude:
      return "Smallest Magnitude";
    case ESortingCriterion::SmallestRealPart:
      return "Smallest Real Part";
    default:
      throw std::logic_error("toVerboseString(ESortingCriterion): "
                             "Conversion to string not specified "
                             "for this ESortingCriterion");
  }
}

inline constexpr const char *toFullKeywordString(ESortingCriterion sortingCriterion) {
  switch (sortingCriterion) {
    case ESortingCriterion::LargestMagnitude:
      return "largest-magnitude";
    case ESortingCriterion::LargestRealPart:
      return "largest-real-part";
    case ESortingCriterion::SmallestMagnitude:
      return "smallest-magnitude";
    case ESortingCriterion::SmallestRealPart:
      return "smallest-real-part";
    default:
      throw std::logic_error("toFullKeywordString(ESortingCriterion): "
                             "Conversion to string not specified "
                             "for this ESortingCriterion");
  }
}

static const std::unordered_map<std::string, ESortingCriterion>
        FullKeywordToSortingCriterion = {
        {
                toFullKeywordString(ESortingCriterion::LargestMagnitude),
                ESortingCriterion::LargestMagnitude
        },
        {
                toFullKeywordString(ESortingCriterion::LargestRealPart),
                ESortingCriterion::LargestRealPart
        },
        {
                toFullKeywordString(ESortingCriterion::SmallestMagnitude),
                ESortingCriterion::SmallestMagnitude
        },
        {
                toFullKeywordString(ESortingCriterion::SmallestRealPart),
                ESortingCriterion::SmallestRealPart
        }};

/*auto it = FullKeywordToSortingCriterion.find("largest-magnitude");
if (it != FullKeywordToSortingCriterion.end()) {
return it->second;
} else { error() }*/

#endif //EIGENVALUECALCULATORSOLVER_ESORTINGCRITERION_H
