#ifndef EIGENVALUECALCULATORSOLVER_CRESTARTEDARNOLDIOBSERVERSHELPER_H
#define EIGENVALUECALCULATORSOLVER_CRESTARTEDARNOLDIOBSERVERSHELPER_H

#include "./CImplicitlyRestartedArnoldi.h"
#include "../../observer/CPropertyChangeObserver.h"
#include "EComputationState.h"

#include <omp.h>
#include <iostream>
#include <chrono>
#include <thread>
#include <atomic>
#include <mutex>
#include <string>
#include <set>
#include <complex>

/// Informing observers about changes after each restarted Arnoldi iteration
class CRestartedArnoldiObserversHelper {
public:
  using RestartedArnoldi = CImplicitlyRestartedArnoldi<CMatrixVectorMultiplication<CCompressedSparseRowMatrix>, CIterativeOrthogonalization<CClassicalGramSchmidtOrthogonalization>>;

  explicit CRestartedArnoldiObserversHelper(RestartedArnoldi &restartedArnoldi);

  void Compute(const std::atomic<bool> &stop = false);

  void AddEigenvaluesObserver(CPropertyChangeObserver<CColumnVectorConst<std::complex<double>>> *observer) {
    AddObserver(m_CurrentHessenbergEigenvaluesObservers, observer);
  }

  void RemoveEigenvaluesObserver(CPropertyChangeObserver<CColumnVectorConst<std::complex<double>>> *observer) {
    RemoveObserver(m_CurrentHessenbergEigenvaluesObservers, observer);
  }

  void AddEigenvalueErrorEstimatesObserver(CPropertyChangeObserver<CColumnVectorConst<double>> *observer) {
    AddObserver(m_EigenvalueErrorEstimatesObservers, observer);
  }

  void RemoveEigenvalueErrorEstimatesObserver(CPropertyChangeObserver<CColumnVectorConst<double>> *observer) {
    RemoveObserver(m_EigenvalueErrorEstimatesObservers, observer);
  }

  void AddNumberOfIterationsObserver(CPropertyChangeObserver<size_t> *observer) {
    AddObserver(m_NumberOfIterationsObservers, observer);
  }

  void RemoveNumberOfIterationsObserver(CPropertyChangeObserver<size_t> *observer) {
    RemoveObserver(m_NumberOfIterationsObservers, observer);
  }

  void AddComputationStateObserver(CPropertyChangeObserver<EComputationState> *observer) {
    AddObserver(m_ComputationStateObservers, observer);
  }

  void RemoveComputationStateObserver(CPropertyChangeObserver<EComputationState> *observer) {
    RemoveObserver(m_ComputationStateObservers, observer);
  }

  void AddConvergedEigenvaluesObserver(CPropertyChangeObserver<CColumnVectorConst<std::complex<double>>> *observer) {
    AddObserver(m_ConvergedEigenvaluesObservers, observer);
  }

  void RemoveConvergedEigenvaluesObserver(CPropertyChangeObserver<CColumnVectorConst<std::complex<double>>> *observer) {
    RemoveObserver(m_ConvergedEigenvaluesObservers, observer);
  }

protected:
  void NotifyCurrentHessenbergEigenvaluesObservers(
          const CColumnVectorConst<std::complex<double>> &currentHessenbergEigenvalues) {
    NotifyObservers(m_CurrentHessenbergEigenvaluesObservers, currentHessenbergEigenvalues);
  }

  void NotifyEigenvalueErrorEstimatesObservers(const CColumnVectorConst<double> &eigenvalueErrorEstimates) {
    NotifyObservers(m_EigenvalueErrorEstimatesObservers, eigenvalueErrorEstimates);
  }

  void NotifyNumberOfIterationsObservers(size_t numberOfIterations) {
    NotifyObservers(m_NumberOfIterationsObservers, numberOfIterations);
  }

  void NotifyComputationStateObservers(EComputationState computationState) {
    NotifyObservers(m_ComputationStateObservers, computationState);
  }

  void NotifyConvergedEigenvaluesObservers(const CColumnVectorConst<std::complex<double>> &convergedEigenvalues) {
    NotifyObservers(m_ConvergedEigenvaluesObservers, convergedEigenvalues);
  }

  template<typename T>
  void AddObserver(std::set<CPropertyChangeObserver<T> *> &observers, CPropertyChangeObserver<T> *observerToAdd);

  template<typename T>
  void RemoveObserver(std::set<CPropertyChangeObserver<T> *> &observers, CPropertyChangeObserver<T> *observerToRemove);

  template<typename T>
  void NotifyObservers(std::set<CPropertyChangeObserver<T> *> &observers, const T &newValue);

  RestartedArnoldi &m_RestartedArnoldi;

  std::set<CPropertyChangeObserver<CColumnVectorConst<std::complex<double>>> *> m_CurrentHessenbergEigenvaluesObservers;
  std::set<CPropertyChangeObserver<CColumnVectorConst<double>> *> m_EigenvalueErrorEstimatesObservers;
  std::set<CPropertyChangeObserver<size_t> *> m_NumberOfIterationsObservers;
  std::set<CPropertyChangeObserver<EComputationState> *> m_ComputationStateObservers;
  std::set<CPropertyChangeObserver<CColumnVectorConst<std::complex<double>>> *> m_ConvergedEigenvaluesObservers;
};

inline
CRestartedArnoldiObserversHelper::CRestartedArnoldiObserversHelper(RestartedArnoldi &restartedArnoldi)
        : m_RestartedArnoldi(restartedArnoldi) {
}

inline void
CRestartedArnoldiObserversHelper::Compute(const std::atomic<bool> &stop) {
  NotifyComputationStateObservers(m_RestartedArnoldi.GetComputationState());
  NotifyNumberOfIterationsObservers(m_RestartedArnoldi.GetNumberOfIterations());
  if (RestartedArnoldi::IsNotInitialized(m_RestartedArnoldi.GetComputationState())) {
    m_RestartedArnoldi.Initialize();
    NotifyNumberOfIterationsObservers(m_RestartedArnoldi.GetNumberOfIterations());
    NotifyComputationStateObservers(m_RestartedArnoldi.GetComputationState());
    NotifyCurrentHessenbergEigenvaluesObservers(m_RestartedArnoldi.GetCurrentHessenbergEigenvalues());
    NotifyEigenvalueErrorEstimatesObservers(m_RestartedArnoldi.GetCurrentHessenbergEigenvalueErrorEstimates());
  }
  while (RestartedArnoldi::IsComputing(m_RestartedArnoldi.GetComputationState())
         && !stop) {
    m_RestartedArnoldi.PerformOneIteration();
    NotifyNumberOfIterationsObservers(m_RestartedArnoldi.GetNumberOfIterations());
    NotifyComputationStateObservers(m_RestartedArnoldi.GetComputationState());
    NotifyCurrentHessenbergEigenvaluesObservers(m_RestartedArnoldi.GetCurrentHessenbergEigenvalues());
    NotifyEigenvalueErrorEstimatesObservers(m_RestartedArnoldi.GetCurrentHessenbergEigenvalueErrorEstimates());
  }
  NotifyConvergedEigenvaluesObservers(m_RestartedArnoldi.GetConvergedEigenvalues());
  //m_DurationMilliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
  return;
}

template<typename T>
void CRestartedArnoldiObserversHelper::AddObserver(std::set<CPropertyChangeObserver<T> *> &observers,
                                                   CPropertyChangeObserver<T> *observerToAdd) {
  observers.insert(observerToAdd);
}

template<typename T>
void CRestartedArnoldiObserversHelper::RemoveObserver(std::set<CPropertyChangeObserver<T> *> &observers,
                                                      CPropertyChangeObserver<T> *observerToRemove) {
  observers.erase(observerToRemove);
}

template<typename T>
void CRestartedArnoldiObserversHelper::NotifyObservers(std::set<CPropertyChangeObserver<T> *> &observers,
                                                       const T &newValue) {
  for (auto observer : observers)
    observer->PropertyChange(newValue);
}

#endif //EIGENVALUECALCULATORSOLVER_CRESTARTEDARNOLDIOBSERVERSHELPER_H
