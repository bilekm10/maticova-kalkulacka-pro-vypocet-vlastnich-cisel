#ifndef EIGENVALUECALCULATORSOLVER_CCOLUMNMATRIXLOADER_H
#define EIGENVALUECALCULATORSOLVER_CCOLUMNMATRIXLOADER_H

#include "../../../model/matrix/dense/column/CColumnMatrixDynamicDimensionsHolding.h"

/// Loading a CColumnMatrixDynamicDimensionsHolding
class CColumnMatrixLoader {
public:
  explicit CColumnMatrixLoader(size_t rowsCnt);

  [[nodiscard]] virtual
  CColumnMatrixDynamicDimensionsHolding Load() = 0;

protected:
  size_t m_RowsCnt;
};

inline
CColumnMatrixLoader::CColumnMatrixLoader(size_t rowsCnt)
        : m_RowsCnt(rowsCnt) {
}

#endif //EIGENVALUECALCULATORSOLVER_CCOLUMNMATRIXLOADER_H
