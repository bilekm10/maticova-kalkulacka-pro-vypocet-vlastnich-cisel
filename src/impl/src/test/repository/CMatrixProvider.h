#ifndef EIGENVALUECALCULATORSOLVER_CMATRIXPROVIDER_H
#define EIGENVALUECALCULATORSOLVER_CMATRIXPROVIDER_H

#include "../../debug/check/equality/matrix/CAreMatricesSame.h"
#include "../../debug/print/printFunctions.h"
#include "../../debug/eig/eig.h"
#include "../../solver/decomposition/qr/CHessenbergQrQuasiTriangularization.h"
#include "../../solver/sorting/CVectorSorting.h"
#include "../../solver/model/vector/column/CColumnVectorHolding.h"
#include "../../solver/model/matrix/dense/column/CColumnMatrixDynamicDimensionsHolding.h"
#include "../../solver/model/matrix/sparse/CCompressedSparseRowMatrix.h"
#include "../../solver/operator/matrixvector/CMatrixVectorMultiplication.h"
#include "../../solver/orthogonalization/basic/CClassicalGramSchmidtOrthogonalization.h"
#include "../../solver/arnoldi/basic/CArnoldiFactorization.h"
#include "../../solver/loader/matrix/column/CLaplacian1DColumnMatrixLoader.h"
#include "../../solver/loader/matrix/column/CLaplacian2DColumnMatrixLoader.h"
#include "../../solver/arnoldi/basic/CArnoldiFactorization.h"
#include "../../solver/arnoldi/restarted/CImplicitlyRestartedArnoldi.h"
#include "../../solver/decomposition/qr/CHessenbergQrQuasiTriangularization.h"
#include "../../solver/decomposition/qr/CHessenbergQrDoubleShiftStep.h"
#include "../../solver/decomposition/qr/CHessenbergQrSingleShiftStep.h"
#include "../../solver/decomposition/qr/CHessenbergQrShifts.h"
#include "../../solver/decomposition/eigen/CQuasiTriangularEigenInfo.h"
#include "../../solver/decomposition/eigen/CHessenbergEigen.h"
#include "../../debug/archive/CArnoldiRestartArchiver.h"
#include "../../solver/loader/vector/column/CFileColumnVectorLoader.h"
#include "../../solver/loader/matrix/row/csr/CStreamMatrixMarketCompressedSparseRowLoader.h"
#include "../../solver/loader/matrix/row/csr/CFileMatrixMarketCompressedSparseRowLoader.h"
#include "../../solver/converter/matrix/tocsr/CColumnMatrixConstToCompressedSparseRowMatrixConverter.h"

#include <iostream>
#include <vector>
#include <iomanip>
#include <cmath>
#include <complex>
#include <fstream>
#include <cassert>
#include <functional>
#include <algorithm>
#include <memory>
#include <bitset>
#include <optional>
#include <limits>
#include <cfloat>
#include <string>
#include <deque>
#include <forward_list>
#include <list>
#include <set>
#include <queue>
#include <map>
#include <stack>
#include <array>
#include <execution>
#include <numbers>
#include <numeric>
#include <filesystem>
#include <regex>
#include <tuple>
#include <type_traits>
#include <typeinfo>
#include <initializer_list>

/*// constexpr char std::array concatenation solution from https://stackoverflow.com/questions/28708497/constexpr-to-concatenate-two-or-more-char-strings
// we cannot return a char std::array from a function, therefore we need a wrapper
template <unsigned N>
struct String {
  char m_C[N];
};

template<unsigned ...Len>
constexpr auto cat(const char (&...strings)[Len]) {
  constexpr unsigned N = (... + Len) - sizeof...(Len);
  String<N + 1> result = {};
  result.m_C[N] = '\0';

  char* dst = result.m_C;
  for (const char* src : {strings...}) {
    for (; *src != '\0'; src++, dst++) {
      *dst = *src;
    }
  }
  return result;
}*/

/*template<size_t First, size_t Last>
class CRangeArrayHelper {
public:
  constexpr CRangeArrayHelper()
          : m_Array()
  {
    for(size_t i = 0; i < m_Array.size(); ++i)
      m_Array[i] = First + i;
  }
  std::array<size_t, Last - First + 1> m_Array;
protected:
};*/

// maybe could change this to provide matrixProductVector and eigenvalues
class CMatrixProvider {
public:
  CMatrixProvider();

  static const constexpr size_t Laplacian1DMatricesCnt = 3;
  static const constexpr size_t Laplacian2DMatricesCnt = 3;
  static const constexpr size_t NonSymmetricMatrixMarketMatricesCnt = 7;

  static const constexpr size_t FirstSymmetricMatrixId = 0;
  static const constexpr size_t FirstLaplacian1DId = FirstSymmetricMatrixId;
  static const constexpr size_t LastLaplacian1DId = FirstLaplacian1DId + Laplacian1DMatricesCnt - 1;
  static const constexpr size_t FirstLaplacian2DId = LastLaplacian1DId + 1;
  static const constexpr size_t LastLaplacian2DId = FirstLaplacian2DId + Laplacian2DMatricesCnt - 1;
  static const constexpr size_t LastSymmetricMatrixId = LastLaplacian2DId;

  static const constexpr size_t FirstNonSymmetricMatrixId = LastSymmetricMatrixId + 1;
  static const constexpr size_t FirstNonSymmetricLaplacian1DId = FirstNonSymmetricMatrixId;
  static const constexpr size_t LastNonSymmetricLaplacian1DId =
          FirstNonSymmetricLaplacian1DId + Laplacian1DMatricesCnt - 1;
  static const constexpr size_t FirstNonSymmetricLaplacian2DId = LastNonSymmetricLaplacian1DId + 1;
  static const constexpr size_t LastNonSymmetricLaplacian2DId =
          FirstNonSymmetricLaplacian2DId + Laplacian2DMatricesCnt - 1;

  static const constexpr size_t FirstNonSymmetricMatrixMarketMatrixId = LastNonSymmetricLaplacian2DId + 1;
  static const constexpr size_t LastNonSymmetricMatrixMarketMatrixId =
          FirstNonSymmetricMatrixMarketMatrixId + NonSymmetricMatrixMarketMatricesCnt - 1;
  static const constexpr size_t LastNonSymmetricMatrixId = LastNonSymmetricMatrixMarketMatrixId;

  //static const constexpr auto SymmetricMatrixIds = CRangeArrayHelper<FirstSymmetricMatrixId, LastSymmetricMatrixId>().m_Array;
  //static const constexpr auto NonSymmetricMatrixIds = CRangeArrayHelper<FirstNonSymmetricMatrixId, LastNonSymmetricMatrixId>().m_Array;

  [[nodiscard]]
  std::pair<CCompressedSparseRowMatrix, CColumnVectorHolding<std::complex<double>>>
  GetMatrixWithEigenvalues(size_t id,
                           ESortingCriterion sortingCriterion);

  [[nodiscard]]
  std::pair<CCompressedSparseRowMatrix, CColumnVectorHolding<std::complex<double>>>
  GetSymmetricMatrixWithEigenvalues(size_t id,
                                    ESortingCriterion sortingCriterion);

  [[nodiscard]]
  std::pair<CCompressedSparseRowMatrix, CColumnVectorHolding<std::complex<double>>>
  GetNonSymmetricMatrixWithEigenvalues(size_t id,
                                       ESortingCriterion sortingCriterion);

  [[nodiscard]]
  std::pair<CCompressedSparseRowMatrix, CColumnVectorHolding<std::complex<double>>>
  GetLaplacian1DWithEigenvalues(size_t matrixNumber,
                                ESortingCriterion sortingCriterion);

  [[nodiscard]]
  std::pair<CCompressedSparseRowMatrix, CColumnVectorHolding<std::complex<double>>>
  GetLaplacian2DWithEigenvalues(size_t matrixNumber,
                                ESortingCriterion sortingCriterion);

  [[nodiscard]]
  std::pair<CCompressedSparseRowMatrix, CColumnVectorHolding<std::complex<double>>>
  GetNonSymmetricLaplacian1DWithEigenvalues(size_t matrixNumber,
                                            ESortingCriterion sortingCriterion);

  [[nodiscard]]
  std::pair<CCompressedSparseRowMatrix, CColumnVectorHolding<std::complex<double>>>
  GetNonSymmetricLaplacian2DWithEigenvalues(
          size_t matrixNumber,
          ESortingCriterion sortingCriterion);

  [[nodiscard]]
  std::pair<CCompressedSparseRowMatrix, CColumnVectorHolding<std::complex<double>>>
  GetNonSymmetricMatrixMarketMatrixWithFewEigenvalues(
          size_t matrixNumber,
          ESortingCriterion sortingCriterion);

  void CalculateReferenceEigenvaluesSymmetricLaplacian1D();

  void CalculateReferenceEigenvaluesSymmetricLaplacian2D();

  void CalculateReferenceEigenvaluesNonSymmetricLaplacian1D();

  void CalculateReferenceEigenvaluesNonSymmetricLaplacian2D();

  std::string GetName(size_t matrixId);

protected:
  /*std::pair<CRowSquareMatrix, CColumnVectorHolding<std::complex<double>>> AddEigenvaluesToRowSquareMatrix(CRowSquareMatrix && matrix)
  {
    return {matrix, CColumnVectorHolding<std::complex<double>>{computeEigenvalues(matrix)}};
  }*/

  [[nodiscard]]
  std::pair<CCompressedSparseRowMatrix, CColumnVectorHolding<std::complex<double>>> AddEigenvalues(
          CCompressedSparseRowMatrix &&matrix,
          const std::string &referenceEigenvaluesFilename,
          ESortingCriterion sortingCriterion);

  CColumnVectorHolding<std::complex<double>> GetEigenvalues(const std::string &eigenvaluesFilename,
                                                            ESortingCriterion sortingCriterion);

  CColumnMatrixDynamicDimensionsHolding GetLaplacian1D(size_t n);

  CColumnMatrixDynamicDimensionsHolding GetLaplacian2D(size_t m, size_t n);

  CColumnMatrixDynamicDimensionsHolding GetNonSymmetricLaplacian1D(size_t n);

  CColumnMatrixDynamicDimensionsHolding GetNonSymmetricLaplacian2D(size_t m, size_t n);

  void MakeNonSymmetric(CColumnMatrixDynamicDimensionsHolding &matrix);

  [[nodiscard]]
  CColumnMatrixDynamicDimensionsHolding &&MakeNonSymmetric(CColumnMatrixDynamicDimensionsHolding &&matrix);

  CCompressedSparseRowMatrix GetCompressedSparseRow(const std::string &filename);

  std::vector<std::string>
  PrependAppend(const std::string &prefix, const std::vector<std::string> &vec, const std::string &postfix);

  std::vector<std::string> Prepend(const std::string &prefix, const std::vector<std::string> &vec);

  static const constexpr size_t NonSymmetricElementSize = 2;
  static const constexpr std::array<double, NonSymmetricElementSize> NonSymmetricRowCoefficients = {9.0 / 10, 3.0 / 10};
  static const constexpr std::array<double, NonSymmetricElementSize> NonSymmetricColumnCoefficients = {1.0 / 10,
                                                                                                       0.5 / 10};
  static const constexpr std::array<double, NonSymmetricElementSize> NonSymmetricValues = {5.353, 1.353};


  static const constexpr std::array<size_t, Laplacian1DMatricesCnt> Laplacian1DMatricesNs = {51, 71, 91};

  static const constexpr std::array<size_t, Laplacian2DMatricesCnt> Laplacian2DMatricesMs = {5, 7, 15};
  static const constexpr std::array<size_t, Laplacian2DMatricesCnt> Laplacian2DMatricesNs = {9, 13, 9};

/*


  static const constexpr char RepositoryDirectory[] = "../repository";
  static const constexpr auto EigenvaluesDirectory = cat(RepositoryDirectory, "/eigenvalues");
  static const constexpr auto SymmetricEigenvaluesDirectory = cat(EigenvaluesDirectory.m_C, "/symmetric");
  static const constexpr auto SymmetricLaplacianEigenvaluesDirectory = cat(SymmetricEigenvaluesDirectory.m_C, "/laplacian");
  static const constexpr auto SymmetricLaplacianEigenvaluesFilenames = {"1d-lap-0.txt",
                                                                        "1d-lap-1.txt"};
  static const constexpr auto SymmetricLaplacianEigenvaluesFullFilenames = {"1d-lap-0.txt",
                                                                            "1d-lap-1.txt"};
*/

  const std::vector<std::string> m_Laplacian1DNames;
  const std::vector<std::string> m_Laplacian1DEigenvaluesFilenames;
  const std::vector<std::string> m_Laplacian1DEigenvaluesFullFilenames;
  const std::vector<std::string> m_Laplacian2DNames;
  const std::vector<std::string> m_Laplacian2DEigenvaluesFilenames;
  const std::vector<std::string> m_Laplacian2DEigenvaluesFullFilenames;
  const std::vector<std::string> m_NonSymmetricLaplacian1DNames;
  const std::vector<std::string> m_NonSymmetricLaplacian1DEigenvaluesFilenames;
  const std::vector<std::string> m_NonSymmetricLaplacian1DEigenvaluesFullFilenames;
  const std::vector<std::string> m_NonSymmetricLaplacian2DNames;
  const std::vector<std::string> m_NonSymmetricLaplacian2DEigenvaluesFilenames;
  const std::vector<std::string> m_NonSymmetricLaplacian2DEigenvaluesFullFilenames;

  const std::vector<std::string> m_NonSymmetricMatrixMarketNames;
  /*vector<std::string> m_NonSymmetricMatrixMarketNames = {"cry2500",
                                                        "mhd4800a",
                                                        "olm5000",
                                                        "tols4000"};*/
  //vector<std::string> m_NonSymmetricMatrixMarketNames = {"rw5151"};
  //vector<std::string> m_NonSymmetricMatrixMarketNames = {"cry2500"};
  const std::vector<std::string> m_NonSymmetricMatrixMarketMatricesFullFilenames;
  //const std::vector<std::string> m_NonSymmetricMatrixMarketLargestMagnitudeEigenvaluesFullFilenames = PrependAppend("../repository/eigenvalues/non-symmetric/matrix-market/largest-magnitude/", m_NonSymmetricMatrixMarketNames, ".txt");

  const std::string m_NonSymmetricMatrixMarketEigenvaluesDirectory;
  std::unordered_map<ESortingCriterion, std::vector<std::string>>
          m_SortingCriterionToNonSymmetricMatrixMarketEigenvaluesFullFilenames;

/*auto it = FullKeywordToSortingCriterion.find("largest-magnitude");
if (it != FullKeywordToSortingCriterion.end()) {
return it->second;
} else { error() }*/

  CColumnMatrixConstToCompressedSparseRowMatrixConverter m_RowSquareToCompressedSparseRowMatrixConverter;
};

inline
CMatrixProvider::CMatrixProvider()
        : m_Laplacian1DNames({"1d-lap-0",
                              "1d-lap-1",
                              "1d-lap-2"}),
          m_Laplacian1DEigenvaluesFilenames(PrependAppend("", m_Laplacian1DNames, ".txt")),
          m_Laplacian1DEigenvaluesFullFilenames(
                  Prepend("../repository/eigenvalues/symmetric/laplacian/", m_Laplacian1DEigenvaluesFilenames)),
          m_Laplacian2DNames({"2d-lap-0",
                              "2d-lap-1",
                              "2d-lap-2"}),
          m_Laplacian2DEigenvaluesFilenames(PrependAppend("", m_Laplacian2DNames, ".txt")),
          m_Laplacian2DEigenvaluesFullFilenames(
                  Prepend("../repository/eigenvalues/symmetric/laplacian/", m_Laplacian2DEigenvaluesFilenames)),
          m_NonSymmetricLaplacian1DNames({"mod-1d-lap-0",
                                          "mod-1d-lap-1",
                                          "mod-1d-lap-2"}),
          m_NonSymmetricLaplacian1DEigenvaluesFilenames(PrependAppend("", m_NonSymmetricLaplacian1DNames, ".txt")),
          m_NonSymmetricLaplacian1DEigenvaluesFullFilenames(
                  Prepend("../repository/eigenvalues/non-symmetric/modified-laplacian/",
                          m_NonSymmetricLaplacian1DEigenvaluesFilenames)),
          m_NonSymmetricLaplacian2DNames({"mod-2d-lap-0",
                                          "mod-2d-lap-1",
                                          "mod-2d-lap-2"}),
          m_NonSymmetricLaplacian2DEigenvaluesFilenames(PrependAppend("", m_NonSymmetricLaplacian2DNames, ".txt")),
          m_NonSymmetricLaplacian2DEigenvaluesFullFilenames(
                  Prepend("../repository/eigenvalues/non-symmetric/modified-laplacian/",
                          m_NonSymmetricLaplacian2DEigenvaluesFilenames)),

          m_NonSymmetricMatrixMarketNames({"cry2500",
                                           "mhd4800a",
                                           "tols4000",
                                           "rw5151",
                                           "cry10000",
                                           "af23560",
                                           "olm5000"}),
          m_NonSymmetricMatrixMarketMatricesFullFilenames(
                  PrependAppend("../repository/matrices/non-symmetric/matrix-market/", m_NonSymmetricMatrixMarketNames,
                                ".mtx")),
//const std::vector<std::string> m_NonSymmetricMatrixMarketLargestMagnitudeEigenvaluesFullFilenames = PrependAppend("../repository/eigenvalues/non-symmetric/matrix-market/largest-magnitude/", m_NonSymmetricMatrixMarketNames, ".txt");

          m_NonSymmetricMatrixMarketEigenvaluesDirectory("../repository/eigenvalues/non-symmetric/matrix-market/"),
          m_SortingCriterionToNonSymmetricMatrixMarketEigenvaluesFullFilenames(
                  {
                          {
                                  ESortingCriterion::LargestMagnitude,
                                  PrependAppend(m_NonSymmetricMatrixMarketEigenvaluesDirectory +
                                                std::string(toFullKeywordString(ESortingCriterion::LargestMagnitude)) +
                                                "/",
                                                m_NonSymmetricMatrixMarketNames,
                                                ".txt")
                          },
                          {
                                  ESortingCriterion::LargestRealPart,
                                  PrependAppend(m_NonSymmetricMatrixMarketEigenvaluesDirectory +
                                                std::string(toFullKeywordString(ESortingCriterion::LargestRealPart)) +
                                                "/",
                                                m_NonSymmetricMatrixMarketNames,
                                                ".txt")
                          },
                          {
                                  ESortingCriterion::SmallestMagnitude,
                                  PrependAppend(m_NonSymmetricMatrixMarketEigenvaluesDirectory +
                                                std::string(toFullKeywordString(ESortingCriterion::SmallestMagnitude)) +
                                                "/",
                                                m_NonSymmetricMatrixMarketNames,
                                                ".txt")
                          },
                          {
                                  ESortingCriterion::SmallestRealPart,
                                  PrependAppend(m_NonSymmetricMatrixMarketEigenvaluesDirectory +
                                                std::string(toFullKeywordString(ESortingCriterion::SmallestRealPart)) +
                                                "/",
                                                m_NonSymmetricMatrixMarketNames,
                                                ".txt")
                          }}) {
  assert(m_NonSymmetricMatrixMarketNames.size() == NonSymmetricMatrixMarketMatricesCnt);
}

[[nodiscard]]
inline
std::pair<CCompressedSparseRowMatrix, CColumnVectorHolding<std::complex<double>>>
CMatrixProvider::GetMatrixWithEigenvalues(size_t id,
                                          ESortingCriterion sortingCriterion) {
  if (id >= FirstSymmetricMatrixId && id <= LastSymmetricMatrixId)
    return GetSymmetricMatrixWithEigenvalues(id,
                                             sortingCriterion);
  else if (id >= FirstNonSymmetricMatrixId && id <= LastNonSymmetricMatrixId)
    return GetNonSymmetricMatrixWithEigenvalues(id,
                                                sortingCriterion);
  throw std::invalid_argument("CMatrixProvider->GetMatrixWithEigenvalues(): "
                              "wrong id=" + std::to_string(id));
}

[[nodiscard]]
inline
std::pair<CCompressedSparseRowMatrix, CColumnVectorHolding<std::complex<double>>>
CMatrixProvider::GetSymmetricMatrixWithEigenvalues(size_t id,
                                                   ESortingCriterion sortingCriterion) {
  if (id >= FirstLaplacian1DId && id <= LastLaplacian1DId)
    return GetLaplacian1DWithEigenvalues(id - FirstLaplacian1DId,
                                         sortingCriterion);
  else if (id >= FirstLaplacian2DId && id <= LastLaplacian2DId)
    return GetLaplacian2DWithEigenvalues(id - FirstLaplacian2DId,
                                         sortingCriterion);
  throw std::invalid_argument("CMatrixProvider->GetSymmetricMatrixWithEigenvalues(): "
                              "wrong id=" + std::to_string(id));
}

[[nodiscard]]
inline
std::pair<CCompressedSparseRowMatrix, CColumnVectorHolding<std::complex<double>>>
CMatrixProvider::GetNonSymmetricMatrixWithEigenvalues(size_t id,
                                                      ESortingCriterion sortingCriterion) {
  if (id >= FirstNonSymmetricLaplacian1DId && id <= LastNonSymmetricLaplacian1DId)
    return GetNonSymmetricLaplacian1DWithEigenvalues(
            id - FirstNonSymmetricLaplacian1DId,
            sortingCriterion);
  else if (id >= FirstNonSymmetricLaplacian2DId && id <= LastNonSymmetricLaplacian2DId)
    return GetNonSymmetricLaplacian2DWithEigenvalues(
            id - FirstNonSymmetricLaplacian2DId,
            sortingCriterion);
  else if (id >= FirstNonSymmetricMatrixMarketMatrixId && id <= LastNonSymmetricMatrixMarketMatrixId)
    return GetNonSymmetricMatrixMarketMatrixWithFewEigenvalues(
            id - FirstNonSymmetricMatrixMarketMatrixId,
            sortingCriterion);
  throw std::invalid_argument("CMatrixProvider->GetNonSymmetricMatrixWithEigenvalues(): "
                              "wrong id=" + std::to_string(id));
}

[[nodiscard]]
inline
std::pair<CCompressedSparseRowMatrix, CColumnVectorHolding<std::complex<double>>>
CMatrixProvider::GetLaplacian1DWithEigenvalues(size_t matrixNumber,
                                               ESortingCriterion sortingCriterion) {
  if (matrixNumber >= Laplacian1DMatricesCnt)
    throw std::invalid_argument("CMatrixProvider->GetLaplacian1DWithEigenvalues(): "
                                "matrixNumber >= Laplacian1DMatricesCnt");
  return AddEigenvalues(
          m_RowSquareToCompressedSparseRowMatrixConverter.Convert(GetLaplacian1D(Laplacian1DMatricesNs[matrixNumber])),
          m_Laplacian1DEigenvaluesFullFilenames[matrixNumber],
          sortingCriterion);
}

[[nodiscard]]
inline
std::pair<CCompressedSparseRowMatrix, CColumnVectorHolding<std::complex<double>>>
CMatrixProvider::GetLaplacian2DWithEigenvalues(size_t matrixNumber,
                                               ESortingCriterion sortingCriterion) {
  if (matrixNumber >= Laplacian2DMatricesCnt) {
    throw std::invalid_argument("CMatrixProvider->GetLaplacian2DWithEigenvalues(): "
                                "matrixNumber >= Laplacian2DMatricesCnt");
  }
  return AddEigenvalues(
          m_RowSquareToCompressedSparseRowMatrixConverter.Convert(
                  GetLaplacian2D(Laplacian2DMatricesMs[matrixNumber],
                                 Laplacian2DMatricesNs[matrixNumber])),
          m_Laplacian2DEigenvaluesFullFilenames[matrixNumber],
          sortingCriterion);
}

[[nodiscard]]
inline
std::pair<CCompressedSparseRowMatrix, CColumnVectorHolding<std::complex<double>>>
CMatrixProvider::GetNonSymmetricLaplacian1DWithEigenvalues(size_t matrixNumber,
                                                           ESortingCriterion sortingCriterion) {
  if (matrixNumber >= Laplacian1DMatricesCnt)
    throw std::invalid_argument("CMatrixProvider->GetNonSymmetricLaplacian1DWithEigenvalues(): "
                                "matrixNumber >= Laplacian1DMatricesCnt");
  return AddEigenvalues(m_RowSquareToCompressedSparseRowMatrixConverter.Convert(
          GetNonSymmetricLaplacian1D(Laplacian1DMatricesNs[matrixNumber])),
                        m_NonSymmetricLaplacian1DEigenvaluesFullFilenames[matrixNumber],
                        sortingCriterion);
}

[[nodiscard]]
inline
std::pair<CCompressedSparseRowMatrix, CColumnVectorHolding<std::complex<double>>>
CMatrixProvider::GetNonSymmetricLaplacian2DWithEigenvalues(
        size_t matrixNumber,
        ESortingCriterion sortingCriterion) {
  if (matrixNumber >= Laplacian2DMatricesCnt)
    throw std::invalid_argument("CMatrixProvider->GetNonSymmetricLaplacian2DWithEigenvalues(): "
                                "matrixNumber >= Laplacian2DMatricesCnt");
  return AddEigenvalues(
          m_RowSquareToCompressedSparseRowMatrixConverter.Convert(
                  GetNonSymmetricLaplacian2D(Laplacian2DMatricesMs[matrixNumber],
                                             Laplacian2DMatricesNs[matrixNumber])),
          m_NonSymmetricLaplacian2DEigenvaluesFullFilenames[matrixNumber],
          sortingCriterion);
}

[[nodiscard]]
inline
std::pair<CCompressedSparseRowMatrix, CColumnVectorHolding<std::complex<double>>>
CMatrixProvider::GetNonSymmetricMatrixMarketMatrixWithFewEigenvalues(
        size_t matrixNumber,
        ESortingCriterion sortingCriterion) {
  if (matrixNumber >= m_NonSymmetricMatrixMarketMatricesFullFilenames.size())
    throw std::invalid_argument("CMatrixProvider->GetNonSymmetricMatrixMarketMatrixWithFewEigenvalues(): "
                                "matrixNumber >= m_NonSymmetricMatrixMarketMatricesFullFilenames.size()");
  auto itEigenvaluesFilenames = m_SortingCriterionToNonSymmetricMatrixMarketEigenvaluesFullFilenames.find(
          sortingCriterion);
  if (itEigenvaluesFilenames == m_SortingCriterionToNonSymmetricMatrixMarketEigenvaluesFullFilenames.end())
    throw std::invalid_argument("CMatrixProvider->GetNonSymmetricMatrixMarketMatrixWithFewEigenvalues(): "
                                "Eigenvalues filenames for the sorting criterion not found.");
  return {GetCompressedSparseRow(m_NonSymmetricMatrixMarketMatricesFullFilenames[matrixNumber]),
          GetEigenvalues(itEigenvaluesFilenames->second[matrixNumber],
                         sortingCriterion)};
}

inline
void CMatrixProvider::CalculateReferenceEigenvaluesSymmetricLaplacian1D() {
  for (size_t i = 0; i < Laplacian1DMatricesCnt; ++i) {
    auto matrix = GetLaplacian1D(Laplacian1DMatricesNs[i]);
    std::ofstream ofs(m_Laplacian1DEigenvaluesFullFilenames[i]);
    printEigenvaluesFileTemplate(matrix, ESortingCriterion::LargestMagnitude, ofs);
  }
}

inline
void CMatrixProvider::CalculateReferenceEigenvaluesSymmetricLaplacian2D() {
  for (size_t i = 0; i < Laplacian2DMatricesCnt; ++i) {
    auto matrix = GetLaplacian2D(Laplacian2DMatricesMs[i], Laplacian2DMatricesNs[i]);
    std::ofstream ofs(m_Laplacian2DEigenvaluesFullFilenames[i]);
    printEigenvaluesFileTemplate(matrix, ESortingCriterion::LargestMagnitude, ofs);
  }
}

inline
void CMatrixProvider::CalculateReferenceEigenvaluesNonSymmetricLaplacian1D() {
  for (size_t i = 0; i < Laplacian1DMatricesCnt; ++i) {
    auto matrix = MakeNonSymmetric(GetLaplacian1D(Laplacian1DMatricesNs[i]));
    std::ofstream ofs(m_NonSymmetricLaplacian1DEigenvaluesFullFilenames[i]);
    printEigenvaluesFileTemplate(matrix, ESortingCriterion::LargestMagnitude, ofs);
  }
}

inline
void CMatrixProvider::CalculateReferenceEigenvaluesNonSymmetricLaplacian2D() {
  for (size_t i = 0; i < Laplacian2DMatricesCnt; ++i) {
    auto matrix = MakeNonSymmetric(GetLaplacian2D(Laplacian2DMatricesMs[i], Laplacian2DMatricesNs[i]));
    std::ofstream ofs(m_NonSymmetricLaplacian2DEigenvaluesFullFilenames[i]);
    printEigenvaluesFileTemplate(matrix, ESortingCriterion::LargestMagnitude, ofs);
  }
}

inline
std::string CMatrixProvider::GetName(size_t matrixId) {
  if (matrixId >= FirstLaplacian1DId && matrixId <= LastLaplacian1DId)
    return m_Laplacian1DNames[matrixId - FirstLaplacian1DId];
  else if (matrixId >= FirstLaplacian2DId && matrixId <= LastLaplacian2DId)
    return m_Laplacian2DNames[matrixId - FirstLaplacian2DId];
  if (matrixId >= FirstNonSymmetricLaplacian1DId && matrixId <= LastNonSymmetricLaplacian1DId)
    return m_NonSymmetricLaplacian1DNames[matrixId - FirstNonSymmetricLaplacian1DId];
  else if (matrixId >= FirstNonSymmetricLaplacian2DId && matrixId <= LastNonSymmetricLaplacian2DId)
    return m_NonSymmetricLaplacian2DNames[matrixId - FirstNonSymmetricLaplacian2DId];
  else if (matrixId >= FirstNonSymmetricMatrixMarketMatrixId && matrixId <= LastNonSymmetricMatrixMarketMatrixId)
    return m_NonSymmetricMatrixMarketNames[matrixId - FirstNonSymmetricMatrixMarketMatrixId];
  return "unknownId" + std::to_string(matrixId);
}

[[nodiscard]]
inline
std::pair<CCompressedSparseRowMatrix, CColumnVectorHolding<std::complex<double>>>
CMatrixProvider::AddEigenvalues(
        CCompressedSparseRowMatrix &&matrix,
        const std::string &referenceEigenvaluesFilename,
        ESortingCriterion sortingCriterion) {
  auto referenceEigenvalues = GetEigenvalues(referenceEigenvaluesFilename,
                                             sortingCriterion);
  if (referenceEigenvalues.GetSize() != matrix.GetRowsCnt())
    throw std::invalid_argument("CMatrixProvider->AddEigenvalues("
                                "matrix[" +
                                std::to_string(matrix.GetRowsCnt()) +
                                " x " +
                                std::to_string(matrix.GetColumnsCnt()) +
                                "], filename: \"" +
                                referenceEigenvaluesFilename +
                                "\"): "
                                "loading error: "
                                "referenceEigenvalues.GetSize()[" +
                                std::to_string(referenceEigenvalues.GetSize()) +
                                "] != matrix.GetRowsCnt()[" +
                                std::to_string(matrix.GetRowsCnt()) +
                                "]");
  //CColumnVectorRefSortingHelper<std::complex<double>>::Sort(referenceEigenvalues, sortingCriterion);
  return {matrix, std::move(referenceEigenvalues)};
}

inline
CColumnVectorHolding<std::complex<double>>
CMatrixProvider::GetEigenvalues(const std::string &eigenvaluesFilename,
                                ESortingCriterion sortingCriterion) {
  CFileColumnVectorLoader<std::complex<double>> loader(eigenvaluesFilename);
  auto eigenvalues = loader.Load();
  CColumnVectorRefSortingHelper<std::complex<double>>::Sort(eigenvalues, sortingCriterion);
  return eigenvalues;
}

inline
CColumnMatrixDynamicDimensionsHolding CMatrixProvider::GetLaplacian1D(size_t n) {
  CLaplacian1DColumnMatrixLoader laplacianLoader(n);
  return laplacianLoader.Load();
}

inline
CColumnMatrixDynamicDimensionsHolding CMatrixProvider::GetLaplacian2D(size_t m, size_t n) {
  CLaplacian2DColumnMatrixLoader laplacianLoader(m, n);
  return laplacianLoader.Load();
}

inline
CColumnMatrixDynamicDimensionsHolding
CMatrixProvider::GetNonSymmetricLaplacian1D(size_t n) {
  CLaplacian1DColumnMatrixLoader laplacianLoader(n);
  return MakeNonSymmetric(laplacianLoader.Load());
}

inline
CColumnMatrixDynamicDimensionsHolding
CMatrixProvider::GetNonSymmetricLaplacian2D(size_t m, size_t n) {
  CLaplacian2DColumnMatrixLoader laplacianLoader(m, n);
  return MakeNonSymmetric(laplacianLoader.Load());
}

inline
void CMatrixProvider::MakeNonSymmetric(CColumnMatrixDynamicDimensionsHolding &matrix) {
  size_t rowsCnt = matrix.GetRowsCnt();
  size_t columnsCnt = matrix.GetColumnsCnt();
  for (size_t i = 0; i < NonSymmetricElementSize; ++i)
    matrix.AtRef(static_cast<size_t>(NonSymmetricRowCoefficients[i] * rowsCnt),
                 static_cast<size_t>(NonSymmetricColumnCoefficients[i] * columnsCnt))
            = NonSymmetricValues[i];
}

[[nodiscard]]
inline
CColumnMatrixDynamicDimensionsHolding &&
CMatrixProvider::MakeNonSymmetric(CColumnMatrixDynamicDimensionsHolding &&matrix) {
  MakeNonSymmetric(matrix);
  return std::move(matrix);
}

inline
CCompressedSparseRowMatrix CMatrixProvider::GetCompressedSparseRow(const std::string &filename) {
  CFileMatrixMarketCompressedSparseRowLoader csrLoader(filename);
  return csrLoader.Load();
}

inline
std::vector<std::string> CMatrixProvider::PrependAppend(const std::string &prefix, const std::vector<std::string> &vec,
                                                        const std::string &postfix) {
  std::vector<std::string> newVec(vec.size());
  for (size_t i = 0; i < vec.size(); ++i)
    newVec[i] = prefix + vec[i] + postfix;
  return newVec;
}

inline
std::vector<std::string> CMatrixProvider::Prepend(const std::string &prefix, const std::vector<std::string> &vec) {
  return PrependAppend(prefix, vec, "");
}

#endif //EIGENVALUECALCULATORSOLVER_CMATRIXPROVIDER_H
