#ifndef EIGENVALUECALCULATORSOLVER_CTEXTEXPORTER_H
#define EIGENVALUECALCULATORSOLVER_CTEXTEXPORTER_H

#include <string>

/// Export text somewhere
class CTextExporter {
public:
  virtual
  void Export(const std::string &item) = 0;

protected:
};

#endif //EIGENVALUECALCULATORSOLVER_CTEXTEXPORTER_H
