#include "./CEigenvaluesPage.h"

CEigenvaluesPage::CEigenvaluesPage(
        const CColumnVectorConst<std::complex<double>> &eigenvalues,
        size_t numberOfWantedEigenvalues,
        uint8_t precisionManipulator,
        const std::string &separator,
        bool omitZeroImaginaryPart)
        : m_Eigenvalues(eigenvalues),
          m_NumberOfWantedEigenvalues(numberOfWantedEigenvalues),
          m_PrecisionManipulator(precisionManipulator),
          m_Separator(separator),
          m_OmitZeroImaginaryPart(omitZeroImaginaryPart) {
}

CEigenvaluesPage::CEigenvaluesPage(
        const CColumnVectorConst<std::complex<double>> &eigenvalues,
        size_t numberOfWantedEigenvalues,
        double precision,
        const std::string &separator,
        bool omitZeroImaginaryPart)
        : CEigenvaluesPage(eigenvalues,
                           numberOfWantedEigenvalues,
        //static_cast<uint8_t>(std::max(static_cast<int>(std::max(-std::log10(precision), 0.0)),0) + 2),
                           CalculatePrecisionManipulator(precision),
                           separator,
                           omitZeroImaginaryPart) {
}

[[nodiscard]]
std::string CEigenvaluesPage::Render() const {
  std::stringstream ss;
  ss << "==================================================\n";
  ss << "Converged eigenvalues [" <<
     m_Eigenvalues.GetSize() << " / " <<
     m_NumberOfWantedEigenvalues << "]:" <<
     m_Separator;
  ss << std::setprecision(m_PrecisionManipulator) << std::scientific;
  for (size_t i = 0; i < m_Eigenvalues.GetSize(); ++i) {
    ss << std::setw(5) << std::left << std::to_string(i + 1) + ":";
    auto eigenvalue = m_Eigenvalues.At(i);
    ss << (eigenvalue.real() > 0 ? "+" : "-") << " " << std::abs(eigenvalue.real());
    if (!(eigenvalue.imag() == 0 && m_OmitZeroImaginaryPart)) {
      ss << " " <<
         (eigenvalue.imag() > 0 ? "+" : "-") << " " <<
         std::abs(eigenvalue.imag()) << "i";
    }
    ss << m_Separator;
  }
  return ss.str();
}

uint8_t CEigenvaluesPage::CalculatePrecisionManipulator(double precision) {
  #ifdef DBG_FULL_PRINT_PREC
  return std::numeric_limits<double>::max_digits10;
  #else
  // https://en.cppreference.com/w/cpp/types/numeric_limits/max_digits10
  if(precision < std::numeric_limits<Scalar>::epsilon() * 10)
    //return CalculatePrecisionManipulatorWithoutLowerBound(std::numeric_limits<Scalar>::epsilon());
    return std::numeric_limits<Scalar>::max_digits10;
  return CalculatePrecisionManipulatorWithoutLowerBound(precision);
  #endif
}

uint8_t CEigenvaluesPage::CalculatePrecisionManipulatorWithoutLowerBound(double precision) {
  return static_cast<uint8_t>(std::max(static_cast<int>(std::max(-std::log10(precision), 0.0)),
                                       0) + 2);
}
