#ifndef EIGENVALUECALCULATORSOLVER_CVECTORSORTING_H
#define EIGENVALUECALCULATORSOLVER_CVECTORSORTING_H

#include "../model/vector/column/CColumnVector.h"
#include "ESortingCriterion.h"
#include "CScalarComparator.h"

#include <vector>
#include <algorithm>
#include <stdexcept>

/// Sort vector indices according to a sorting criterion
/**
 * Inspired by Spectra - at least the passing *this to the sort function from <algorithm>
 * @tparam Criterion
 * @tparam Scalar
 * @tparam VectorType
 */
template<ESortingCriterion Criterion, typename Scalar, template<typename> class VectorType>
class CVectorSorting {
public:
  explicit CVectorSorting(const VectorType<Scalar> &columnVector);

  bool operator()(size_t i, size_t j) {
    return CScalarComparator<Scalar, Criterion>::Compare(m_Vector.At(i), m_Vector.At(j));
  }

  void Sort();

  [[nodiscard]]
  std::vector<size_t> GetIndices() const {
    return m_Indices;
  }

protected:
  const VectorType<Scalar> &m_Vector;
  std::vector<size_t> m_Indices;
};

template<ESortingCriterion Criterion, typename Scalar, template<typename> class VectorType>
CVectorSorting<Criterion, Scalar, VectorType>::CVectorSorting(const VectorType<Scalar> &columnVector)
        : m_Vector(columnVector),
          m_Indices(columnVector.GetSize()) {
  for (size_t i = 0; i < m_Vector.GetSize(); ++i)
    m_Indices[i] = i;
}

template<ESortingCriterion Criterion, typename Scalar, template<typename> class VectorType>
void CVectorSorting<Criterion, Scalar, VectorType>::Sort() {
  // stable_sort doc: Sorts the elements in the range [first, last) in non-descending order. The order of equivalent elements is guaranteed to be preserved.
  // this means that when adjacent complex conjugates are compared equal, the first one is still the first one
  std::stable_sort(m_Indices.begin(), m_Indices.end(), *this);
}

/// Sort CColumnVector indices according to a sorting criterion. A helper class to allow to select the sorting criterion in run-time via a standard parameter.
template<typename Scalar>
class CColumnVectorSortingHelper {
public:
  [[nodiscard]]
  static
  std::vector<size_t> Sort(const CColumnVectorConst<Scalar> &columnVector, ESortingCriterion sortingCriterion) {
    switch (sortingCriterion) {
      case ESortingCriterion::LargestMagnitude: {
        CVectorSorting<ESortingCriterion::LargestMagnitude, std::complex<double>, CColumnVectorConst> vectorSorting(
                columnVector);
        vectorSorting.Sort();
        return vectorSorting.GetIndices();
      }
      case ESortingCriterion::LargestRealPart: {
        CVectorSorting<ESortingCriterion::LargestRealPart, std::complex<double>, CColumnVectorConst> vectorSorting(
                columnVector);
        vectorSorting.Sort();
        return vectorSorting.GetIndices();
      }
      case ESortingCriterion::SmallestMagnitude: {
        CVectorSorting<ESortingCriterion::SmallestMagnitude, std::complex<double>, CColumnVectorConst> vectorSorting(
                columnVector);
        vectorSorting.Sort();
        return vectorSorting.GetIndices();
      }
      case ESortingCriterion::SmallestRealPart: {
        CVectorSorting<ESortingCriterion::SmallestRealPart, std::complex<double>, CColumnVectorConst> vectorSorting(
                columnVector);
        vectorSorting.Sort();
        return vectorSorting.GetIndices();
      }
      default: {
        throw std::invalid_argument("CColumnVectorSortingHelper->Sort(): "
                                    "unsupported sorting criterion");
      }
    }
  }

protected:
};

/// Sort CColumnVector according to a sorting criterion. A helper class to avoid the need to handle the sorting indices.
template<typename Scalar>
class CColumnVectorRefSortingHelper {
public:
  static
  void Sort(CColumnVector<Scalar> &columnVector, ESortingCriterion sortingCriterion) {
    auto sortingIndices = CColumnVectorSortingHelper<Scalar>::Sort(columnVector, sortingCriterion);
    CColumnVectorHolding<Scalar> sortedVectorTmp(columnVector.GetSize());
    for (size_t i = 0; i < sortedVectorTmp.GetSize(); ++i)
      sortedVectorTmp.AtRef(i) = columnVector.At(sortingIndices[i]);
    columnVector.Assign(sortedVectorTmp);
  }

protected:
};

#endif //EIGENVALUECALCULATORSOLVER_CVECTORSORTING_H
