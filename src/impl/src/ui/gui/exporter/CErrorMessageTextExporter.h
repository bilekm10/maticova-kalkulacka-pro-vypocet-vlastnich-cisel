#ifndef EIGENVALUECALCULATORGUI_CERRORMESSAGEEXPORTER_H
#define EIGENVALUECALCULATORGUI_CERRORMESSAGEEXPORTER_H

#include "../controller/CComputationSignals.h"
#include "../../../solver/exporter/CTextExporter.h"

class CErrorMessageTextExporter : public CTextExporter {
public:
  explicit CErrorMessageTextExporter(CComputationSignals &computationSignals);

  void Export(const std::string &item) override { m_ComputationSignals->ErrorOccurred(item); }

protected:
  CComputationSignals *m_ComputationSignals;
};

#endif //EIGENVALUECALCULATORGUI_CERRORMESSAGEEXPORTER_H
