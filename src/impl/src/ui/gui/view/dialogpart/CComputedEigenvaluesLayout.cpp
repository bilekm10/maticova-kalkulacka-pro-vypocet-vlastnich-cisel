#include "CComputedEigenvaluesLayout.h"

CComputedEigenvaluesLayout::CComputedEigenvaluesLayout()
        : m_Precision(1e-10) {
  m_ComputedEigenvaluesLabel = new QLabel(QString("Computed eigenvalues"));
  m_ComputedEigenvaluesTextEdit = new QTextEdit;
  m_ComputedEigenvaluesTextEdit->setReadOnly(true);
  addWidget(m_ComputedEigenvaluesLabel);
  addWidget(m_ComputedEigenvaluesTextEdit);
  setSpacing(VerticalSpacing);
}
