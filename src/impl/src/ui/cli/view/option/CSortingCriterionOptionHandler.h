#ifndef EIGENVALUECALCULATORSOLVER_CSORTINGCRITERIONOPTIONHANDLER_H
#define EIGENVALUECALCULATORSOLVER_CSORTINGCRITERIONOPTIONHANDLER_H

#include "./COptionHandler.h"
#include "../../../../solver/sorting/ESortingCriterion.h"

class CSortingCriterionOptionHandler : public COptionHandler {
public:
  explicit CSortingCriterionOptionHandler(std::optional<ESortingCriterion> &sortingCriterion);

  // argumentsCnt must be >= 1
  void Handle(int &argumentsCnt, char **&argumentsVector) override;

  static constexpr const char OptionShortName[] = "-s";
  static constexpr const char OptionFullName[] = "--sorting-criterion";
  static constexpr const char OptionVerboseName[] = "Sorting criterion";
protected:
  std::optional<ESortingCriterion> &m_SortingCriterion;
  std::string m_Text;
};

#endif //EIGENVALUECALCULATORSOLVER_CSORTINGCRITERIONOPTIONHANDLER_H
