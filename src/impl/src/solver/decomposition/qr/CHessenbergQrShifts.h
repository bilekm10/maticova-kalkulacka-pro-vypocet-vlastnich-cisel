#ifndef EIGENVALUECALCULATORSOLVER_CHESSENBERGQRSHIFTS_H
#define EIGENVALUECALCULATORSOLVER_CHESSENBERGQRSHIFTS_H

#include <complex>
#include <cmath>
#include <stdexcept>

#include "../../model/vector/column/CColumnVector.h"
#include "../../model/matrix/dense/column/CColumnMatrixDynamicDimensionsHolding.h"
#include "../../model/matrix/dense/column/CColumnMatrix.h"
#include "./CHessenbergQrSingleShiftStep.h"
#include "./CHessenbergQrDoubleShiftStep.h"
#include "../../eigenlibhelpers/eigenlibmap.h"

//#define DBG_PRINT
#ifdef DBG_PRINT
#include "../../../debug/print/printFunctions.h"
#include "../../../debug/eig/eig.h"
#endif //DBG_PRINT

/// Using Spectra solution to apply QR steps with a collection of shifts, accumulating the orthogonal matrix
class CHessenbergQrShifts {
public:
  explicit CHessenbergQrShifts(size_t rowsCnt);

  // complex conjugate of a complex shift must be right after its complex conjugate
  void PerformQrWithShifts(CColumnMatrix &hessenbergMatrix,
                           const CColumnVectorConst<std::complex<double>> &shifts);

  [[nodiscard]]
  const CColumnMatrixConst &GetOrthogonalMatrix() const {
    return m_OrthogonalMatrix;
  }

protected:
  CHessenbergQrSingleShiftStep m_QrSingleShiftStep;
  CHessenbergQrDoubleShiftStep m_QrDoubleShiftStep;
  CColumnMatrixDynamicDimensionsHolding m_OrthogonalMatrix;
};

inline
CHessenbergQrShifts::CHessenbergQrShifts(size_t rowsCnt)
        : m_QrSingleShiftStep(rowsCnt),
          m_QrDoubleShiftStep(rowsCnt),
          m_OrthogonalMatrix(rowsCnt, false) {
}

// complex conjugate of a complex shift must be right after its complex conjugate
inline void
CHessenbergQrShifts::PerformQrWithShifts(CColumnMatrix &hessenbergMatrix,
                                         const CColumnVectorConst<std::complex<double>> &shifts) {
  if (hessenbergMatrix.GetRowsCnt() != m_OrthogonalMatrix.GetRowsCnt())
    throw std::invalid_argument("CHessenbergQrShifts->PerformQrWithShifts(): "
                                "hessenbergMatrix.GetRowsCnt() != m_OrthogonalMatrix.GetRowsCnt()");
  m_OrthogonalMatrix.ToIdentity();
  auto spectraHessenbergMatrix = mapToEigenMatrix(hessenbergMatrix);
  auto spectraOrthogonalMatrix = mapToEigenMatrix(m_OrthogonalMatrix);
  #ifdef DBG_PRINT
  printf("PerformQrWithShifts start\n");
    printMatrix(hessenbergMatrix, "PerformQrWithShifts start hessenberg");
    printEigenvalues(hessenbergMatrix);
    printVector(shifts, "  ", "PerformQrWithShifts shifts");
  #endif //DBG_PRINT
  for (size_t i = 0; i < shifts.GetSize(); ++i) {
    auto currentShift = shifts.At(i);
    if (currentShift.imag() == 0) // real shift
      m_QrSingleShiftStep.PerformSingleShiftQrStep(
              spectraHessenbergMatrix,
              spectraOrthogonalMatrix,
              currentShift.real());
    else // complex shift
    {
      if (!(i < shifts.GetSize() - 1 && std::conj(currentShift) == shifts.At(i + 1)))
        throw std::invalid_argument("DEBUG: CHessenbergQrShifts->PerformQrWithShifts: "
                                    "!(i < shifts.GetSize() - 1 && std::conj(currentShift) == shifts.At(i + 1)) "
                                    "in the complex shift case");
      double s = 2 * currentShift.real();
      double t = std::norm(currentShift);
      m_QrDoubleShiftStep.PerformDoubleShiftQrStep(
              spectraHessenbergMatrix,
              spectraOrthogonalMatrix,
              s,
              t);
      ++i;
    }
    #ifdef DBG_PRINT
    printMatrix(hessenbergMatrix, "PerformQrWithShifts hessenberg");
    #endif //DBG_PRINT
  }
  fromEigenMatrix(hessenbergMatrix, spectraHessenbergMatrix);
  fromEigenMatrix(m_OrthogonalMatrix, spectraOrthogonalMatrix);
  #ifdef DBG_PRINT
  printEigenvalues(hessenbergMatrix);
    printMatrix(hessenbergMatrix, "PerformQrWithShifts end hessenberg");
    printf("PerformQrWithShifts end\n");
    printMatrix(m_OrthogonalMatrix, "PerformQrWithShifts end orthogonalMatrix");
  #endif //DBG_PRINT
}

#endif //EIGENVALUECALCULATORSOLVER_CHESSENBERGQRSHIFTS_H
