#ifndef EIGENVALUECALCULATORSOLVER_CINPUTMATRIXFILENAMEOPTIONHANDLER_H
#define EIGENVALUECALCULATORSOLVER_CINPUTMATRIXFILENAMEOPTIONHANDLER_H

#include "./COptionHandler.h"

class CInputMatrixFilenameOptionHandler : public COptionHandler {
public:
  explicit CInputMatrixFilenameOptionHandler(std::optional<std::string> &inputMatrixFilename);

  // argumentsCnt must be >= 1
  void Handle(int &argumentsCnt, char **&argumentsVector) override;

  static constexpr const char OptionShortName[] = "-i";
  static constexpr const char OptionFullName[] = "--input-matrix";
  static constexpr const char OptionVerboseName[] = "Input matrix filename";
protected:
  std::optional<std::string> &m_InputMatrixFilename;
  std::string m_Text;
};

#endif //EIGENVALUECALCULATORSOLVER_CINPUTMATRIXFILENAMEOPTIONHANDLER_H
