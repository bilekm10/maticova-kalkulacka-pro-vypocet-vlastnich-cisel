#ifndef EIGENVALUECALCULATORSOLVER_CROWVECTORCONST_H
#define EIGENVALUECALCULATORSOLVER_CROWVECTORCONST_H

#include "../CVector.h"

#include <cmath>
#include <complex>

/// Providing const methods for a row vector
template<typename Scalar>
class CRowVectorConst : protected CVector<Scalar> {
public:
  using CVector<Scalar>::At;
  using CVector<Scalar>::GetSize;
  using CVector<Scalar>::AbsoluteNorm;
  using CVector<Scalar>::EuclideanNorm;
  using CVector<Scalar>::MaximumNorm;
  using CVector<Scalar>::MultiplyToScalar;
//  using CVector<Scalar>::AtRef;
//  using CVector<Scalar>::SetTo;
//  using CVector<Scalar>::SetToZero;

  CRowVectorConst(Scalar *data, size_t dataSize);

  explicit CRowVectorConst(CDense1DBlockView<Scalar> &&view);

  [[nodiscard]]
  CRowVectorConst<Scalar> GetSubVectorConstRefView(size_t firstElementIndex, size_t lastElementIndex) const {
    return this->template GetConstSubView<CRowVectorConst>(firstElementIndex, lastElementIndex);
  }

  virtual
  ~CRowVectorConst();

  /*template<typename VectorType>
  Scalar MultiplyToScalar(const VectorType & multiplicand) const
  {
    return CVector<Scalar>::template MultiplyToScalar(
            multiplicand);
  }*/
protected:
  // copy from the other view into *this view
  void Assign(const CRowVectorConst<Scalar> &other) {
    this->m_View.Assign(other.m_View);
  }

  using CVector<Scalar>::Assign;
};

template<typename Scalar>
CRowVectorConst<Scalar>::CRowVectorConst(Scalar *data, size_t dataSize)
        : CVector<Scalar>(data,
                          dataSize) {
}

template<typename Scalar>
CRowVectorConst<Scalar>::CRowVectorConst(CDense1DBlockView<Scalar> &&view)
        : CVector<Scalar>(std::move(view)) {
}

template<typename Scalar>
CRowVectorConst<Scalar>::~CRowVectorConst() = default;

#endif //EIGENVALUECALCULATORSOLVER_CROWVECTORCONST_H
