#ifndef EIGENVALUECALCULATORSOLVER_CARNOLDIRESTARTARCHIVER_H
#define EIGENVALUECALCULATORSOLVER_CARNOLDIRESTARTARCHIVER_H

#include "../../solver/arnoldi/restarted/CImplicitlyRestartedArnoldi.h"
#include "../../solver/model/vector/column/CColumnVector.h"
#include "../print/printFunctions.h"

#include <list>
#include <complex>
#include <iomanip>
#include <vector>
#include <optional>

/*
using RestartedArnoldi = CImplicitlyRestartedArnoldi<ESortingCriterion::LargestMagnitude,
        CMatrixVectorMultiplication<CRowSquareMatrix>,
        CIterativeOrthogonalization<CClassicalGramSchmidtOrthogonalization<CMatrixVectorMultiplication<CColumnRectangularMatrix>, CTransposedMatrixVectorMultiplication<CColumnRectangularMatrix>>>>;
*/

template<typename /*= void*/RestartedArnoldi>
class CArnoldiRestartArchiver {
public:
  explicit CArnoldiRestartArchiver(RestartedArnoldi &restartedArnoldi,
                                   uint8_t manipWidthComplex = 49,
                                   uint8_t manipWidthReal = 51,
                                   uint8_t manipPrecision = 17);

  void ComputeAndArchive();

  void PrintTogether(bool referenceEigenvalues = true,
                     bool eigenvalues = true,
                     bool eigenvalueErrorEstimates = true,
                     bool shiftsForRestart = true,
                     bool convergedEigenvalues = true,
                     bool cmpWithReference = true,
                     bool absCmpWithReference = true) const;

  [[nodiscard]] static
  CColumnVectorHolding<std::complex<double>>
  complexDifferenceMinSized(const CColumnVectorConst<std::complex<double>> &left,
                            const CColumnVectorConst<std::complex<double>> &right);

  [[nodiscard]] static
  CColumnVectorHolding<double> absOfComplex(const CColumnVectorConst<std::complex<double>> &vec);

  void SetReferenceEigenvalues(const CColumnVectorConst<std::complex<double>> &referenceEigenvalues) {
    m_ReferenceEigenvalues = &referenceEigenvalues;
  }

protected:
  RestartedArnoldi &m_RestartedArnoldi;
  const CColumnVectorConst<std::complex<double>> *m_ReferenceEigenvalues;
  std::vector<std::pair<size_t, size_t>> m_IterationsWhenEigenvalueConvergedAndCnt;
  std::list<CColumnVectorConst<std::complex<double>>> m_EigenvaluesArchive;
  std::list<CColumnVectorConst<double>> m_EigenvalueErrorEstimatesArchive;
  std::list<CColumnVectorConst<std::complex<double>>> m_ShiftsForRestartArchive;
  std::list<CColumnVectorConst<std::complex<double>>> m_ConvergedEigenvaluesArchive;
  uint8_t m_ManipWidthComplex;
  uint8_t m_ManipWidthReal;
  uint8_t m_ManipPrecision;
};

template<typename /*= void*/RestartedArnoldi>
CArnoldiRestartArchiver<RestartedArnoldi>
::CArnoldiRestartArchiver(RestartedArnoldi &restartedArnoldi,
                          uint8_t manipWidthComplex,
                          uint8_t manipWidthReal,
                          uint8_t manipPrecision)
        : m_RestartedArnoldi(restartedArnoldi),
          m_ReferenceEigenvalues(nullptr),
          m_IterationsWhenEigenvalueConvergedAndCnt(),
          m_EigenvaluesArchive(),
          m_EigenvalueErrorEstimatesArchive(),
          m_ShiftsForRestartArchive(),
          m_ConvergedEigenvaluesArchive(),
          m_ManipWidthComplex(manipWidthComplex),
          m_ManipWidthReal(manipWidthReal),
          m_ManipPrecision(manipPrecision) {
}

template<typename /*= void*/RestartedArnoldi>
void CArnoldiRestartArchiver<RestartedArnoldi>
::ComputeAndArchive() {
  if (RestartedArnoldi::IsNotInitialized(m_RestartedArnoldi.m_ComputationState))
    m_RestartedArnoldi.Initialize();
  bool diverge = true;
  size_t oldNumberOfConvergedEigenvalues = 0;
  size_t iteration = 0;
  while (RestartedArnoldi::IsComputing(m_RestartedArnoldi.m_ComputationState)) {
    ++iteration;
    size_t numberOfConvergedEigenvalues = m_RestartedArnoldi.PerformOneIteration();
    if (oldNumberOfConvergedEigenvalues != numberOfConvergedEigenvalues) {
      m_IterationsWhenEigenvalueConvergedAndCnt.emplace_back(iteration, numberOfConvergedEigenvalues -
                                                                        oldNumberOfConvergedEigenvalues);
    }
    m_EigenvaluesArchive.emplace_back(m_RestartedArnoldi.GetCurrentHessenbergEigenvalues());
    m_EigenvalueErrorEstimatesArchive.emplace_back(m_RestartedArnoldi.GetCurrentHessenbergEigenvalueErrorEstimates());
    m_ShiftsForRestartArchive.emplace_back(m_RestartedArnoldi.GetShiftsForRestart());
    m_ConvergedEigenvaluesArchive.emplace_back(m_RestartedArnoldi.GetConvergedEigenvalues());
    if (numberOfConvergedEigenvalues >= m_RestartedArnoldi.m_NumberOfWantedEigenvalues) {
      diverge = false;
      break;
    }
    oldNumberOfConvergedEigenvalues = numberOfConvergedEigenvalues;
  }
}

template<typename /*= void*/RestartedArnoldi>
void CArnoldiRestartArchiver<RestartedArnoldi>
::PrintTogether(bool referenceEigenvalues,
                bool eigenvalues,
                bool eigenvalueErrorEstimates,
                bool shiftsForRestart,
                bool convergedEigenvalues,
                bool cmpWithReference,
                bool absCmpWithReference) const {
  const bool inRow = true;
  printf("CArnoldiRestartArchiver->PrintTogether\n");
  //const char * name = "CArnoldiRestartArchiver->PrintTogether ";
  auto itEigenvalues = m_EigenvaluesArchive.cbegin();
  auto itEigenvalueErrorEstimates = m_EigenvalueErrorEstimatesArchive.cbegin();
  auto itShiftsForRestart = m_ShiftsForRestartArchive.cbegin();
  auto itConvergedEigenvalues = m_ConvergedEigenvaluesArchive.cbegin();
  const double almostZero = 1e-15;
  const char *referenceEigenvaluesName = "referenceEigenvalues    ";
  const char *eigenvaluesName = "eigenvalues             ";
  const char *eigenvalueErrorEstimatesName = "eigenvalueErrorEstimates";
  const char *shiftsForRestartName = "shiftsForRestart        ";
  const char *convergedEigenvaluesName = "convergedEigenvalues    ";
  const char *cmpWithReferenceName = "cmpWithReference        ";
  const char *absCmpWithReferenceName = "absCmpWithReference     ";

  const size_t referenceEigenvaluesSize = m_ReferenceEigenvalues ? m_ReferenceEigenvalues->GetSize() : 0;
  const size_t cmpWithReferenceSize = std::min(referenceEigenvaluesSize, itEigenvalues->GetSize());
  const std::string referenceEigenvaluesNameWithSizeString =
          "[" + std::to_string(referenceEigenvaluesSize) + "]" + referenceEigenvaluesName;
  const std::string eigenvaluesNameWithSizeString =
          "[" + std::to_string(m_EigenvaluesArchive.front().GetSize()) + "]" + eigenvaluesName;
  const std::string eigenvalueErrorEstimatesNameWithSizeString =
          "[" + std::to_string(m_EigenvalueErrorEstimatesArchive.front().GetSize()) + "]" +
          eigenvalueErrorEstimatesName;
  const std::string shiftsForRestartNameWithSizeString =
          "[" + std::to_string(m_ShiftsForRestartArchive.front().GetSize()) + "]" + shiftsForRestartName;
  const std::string convergedEigenvaluesNameWithSizeString =
          "[" + std::to_string(m_ConvergedEigenvaluesArchive.front().GetSize()) + "]" + convergedEigenvaluesName;
  const std::string cmpWithReferenceNameWithSizeString =
          "[" + std::to_string(cmpWithReferenceSize) + "]" + cmpWithReferenceName;
  const std::string absCmpWithReferenceNameWithSizeString =
          "[" + std::to_string(cmpWithReferenceSize) + "]" + absCmpWithReferenceName;

  //const size_t manipWidthName = 30;
  const char *iterationSeparator = "\n\n";
  for (size_t i = 0; i < m_EigenvaluesArchive.size(); ++i) {
    printf("iteration: %d\n", i);
    if (referenceEigenvalues && m_ReferenceEigenvalues) {
      printVector(*m_ReferenceEigenvalues, " ", referenceEigenvaluesNameWithSizeString, m_ManipWidthComplex,
                  m_ManipPrecision, almostZero, false);
      printf("\n");
    }
    if (eigenvalues) {
      printVector(*itEigenvalues, " ", eigenvaluesNameWithSizeString, m_ManipWidthComplex, m_ManipPrecision, almostZero,
                  false);
      printf("\n");
    }
    if (eigenvalueErrorEstimates) {
      printVector(*itEigenvalueErrorEstimates, " ", eigenvalueErrorEstimatesNameWithSizeString, m_ManipWidthReal,
                  m_ManipPrecision, almostZero, false);
      printf("\n");
    }
    if (shiftsForRestart) {
      printVector(*itShiftsForRestart, " ", shiftsForRestartNameWithSizeString, m_ManipWidthComplex, m_ManipPrecision,
                  almostZero, false);
      printf("\n");
    }
    if (convergedEigenvalues) {
      printVector(*itConvergedEigenvalues, " ", convergedEigenvaluesNameWithSizeString, m_ManipWidthComplex,
                  m_ManipPrecision, almostZero, false);
      printf("\n");
    }
    std::optional<CColumnVectorHolding<std::complex<double>>> complexErrors;
    if (m_ReferenceEigenvalues)
      complexErrors = complexDifferenceMinSized(*m_ReferenceEigenvalues, *itEigenvalues);
    if (cmpWithReference && m_ReferenceEigenvalues) {
      printVector(complexErrors.value(), " ", cmpWithReferenceNameWithSizeString, m_ManipWidthComplex, m_ManipPrecision,
                  almostZero, false);
      printf("\n");
    }
    if (absCmpWithReference && m_ReferenceEigenvalues) {
      printVector(absOfComplex(complexErrors.value()), " ", absCmpWithReferenceNameWithSizeString, m_ManipWidthComplex,
                  m_ManipPrecision, almostZero, false);
      printf("\n");
    }
    printf(iterationSeparator);
    ++itEigenvalues;
    ++itEigenvalueErrorEstimates;
    ++itShiftsForRestart;
    ++itConvergedEigenvalues;
  }
}

template<typename /*= void*/RestartedArnoldi>
CColumnVectorHolding<std::complex<double>> CArnoldiRestartArchiver<RestartedArnoldi>
::complexDifferenceMinSized(const CColumnVectorConst<std::complex<double>> &left,
                            const CColumnVectorConst<std::complex<double>> &right) {
  CColumnVectorHolding<std::complex<double>> difference(std::min(left.GetSize(), right.GetSize()));
  if (difference.GetSize() == 0)
    return difference;
  if (left.GetSize() <= right.GetSize()) {
    difference.Assign(left);
    difference -= right.GetSubVectorConstRefView(0, left.GetSize() - 1);
    return difference;
  } else {
    difference.Assign(left.GetSubVectorConstRefView(0, right.GetSize() - 1));
    difference -= right;
    return difference;
  }
}

template<typename /*= void*/RestartedArnoldi>
CColumnVectorHolding<double>
CArnoldiRestartArchiver<RestartedArnoldi>::absOfComplex(const CColumnVectorConst<std::complex<double>> &vec) {
  CColumnVectorHolding<double> result(vec.GetSize());
  for (size_t i = 0; i < vec.GetSize(); ++i)
    result.AtRef(i) = std::abs(vec.At(i));
  return result;
}

#endif //EIGENVALUECALCULATORSOLVER_CARNOLDIRESTARTARCHIVER_H
