# Maticová kalkulačka pro výpočet vlastních čísel

Bakalářská práce  
Fakulta informačních technologií ČVUT v Praze

Student: Matouš Bílek  
Vedoucí: doc. Ing. Ivan Šimeček, Ph.D.  
Studijní program: Informatika  
Obor / specializace: Webové a softwarové inženýrství, zaměření Softwarové inženýrství  
Katedra: Katedra softwarového inženýrství



## Popis

Tato práce implementuje maticovou kalkulačku pro výpočet několika největších vlastních čísel vstupní velké řídké nesymetrické reálné matice uložené v souřadnicovém formátu [MatrixMarket](https://math.nist.gov/MatrixMarket). Pro tento výpočet implementuje implicitně restartovanou Arnoldiho metodu.

K této kalkulačce je poskytnuto rozhraní přes příkazovou řádku, společně s jednoduchým grafickým uživatelským rozhraním.

Kalkulačka používá knihovnu [Eigen](https://eigen.tuxfamily.org) a [Spectra](https://spectralib.org/).



## Instalace

Níže popsaný způsob instalace byl otestován pouze na systému Kali GNU/Linux Rolling, na kterém byla tato práce naimplementována.

Obě rozhraní potřebují pro sestavení knihovnu [Eigen](https://eigen.tuxfamily.org) 3.3.9 a [Spectra](https://spectralib.org) 1.0.0, nebo jiné kompatibilní verze. Nejdříve je proto nutné nastavit a exportovat proměnné prostředí `EIGEN_INCLUDE_DIR` a `SPECTRA_INCLUDE_DIR` tak, aby obsahovaly cestu k příslušným knihovnám. To lze, po stáhnutí těchto open-source knihoven například do složky `/home`, vykonat následujícími příkazy  
`export EIGEN_INCLUDE_DIR=/home/eigen-3.3.9`  
`export SPECTRA_INCLUDE_DIR=/home/spectra-1.0.0/include`

Pro sestavení obou rozhraní je mimo nástroj Make také potřebný nástroj [CMake](https://cmake.org), na který Makefile projektu deleguje všechnu práci se sestavením.

### Rozhraní přes příkazovou řádku
Rozhraní přes příkazovou řádku, se zkratkou CLI, se sestaví tak, že se v kořenovém adresáři tohoto projektu spustí příkaz  
`make compile-cli`  
Tento příkaz vytvoří spustitelný soubor s názvem **fewEigenvaluesCalculatorCli** v adresáři **build/cli/**.

Po sestavení souboru **build/cli/fewEigenvaluesCalculatorCli** se tento soubor spustí například příkazem  
`./build/cli/fewEigenvaluesCalculatorCli --sorting-criterion=largest-magnitude -p 1e-10 -maxit 150 -maxortit 5 -eta 0.7 -i "../repository/matrices/non-symmetric/matrix-market/cry2500.mtx" -k 3 -m10`  
Jednotlivé parametry jsou popsány níže.

### Grafické uživatelské rozhraní
Grafické uživatelské rozhraní, se zkratkou GUI, používá framework **Qt 5**, a tedy je nejprve nutné nainstalovat tento framework.

Pro sestavení GUI se v kořenovém adresáři tohoto projektu zavolá příkaz  
`make compile-gui`

Rozhraní přes příkazovou řádku se potom spustí příkazem  
`make run-gui`  
Jestliže GUI doposud nebylo sestaveno, příkaz `make compile-gui` se spustí automaticky.



## Ovládání CLI

Rozhraní přes příkazovou řádku v souboru **build/cli/fewEigenvaluesCalculatorCli** sestavené příkazem `make compile-cli` lze volat s následujícími parametry:

### Povinné parametry:
- `-k <POČET>`, `--number-of-wanted-eigenvalues=<POČET>`  
  `POČET` je počet požadovaných vlastních čísel.

- `-i <SOUBOR_SE_VSTUPNÍ_MATICÍ>`, `--input-matrix=<SOUBOR_SE_VSTUPNÍ_MATICÍ>`  
	`SOUBOR_SE_VSTUPNÍ_MATICÍ` je jméno souboru se vstupní velkou řídkou maticí, jejíž několik vlastních čísel se má spočítat.
  
### Volitelné parametry:
- `-m <MAXIMÁLNÍ_VELIKOST>`, `--factorization-size-to-restart-at=<MAXIMÁLNÍ_VELIKOST>`  
	`MAXIMÁLNÍ_VELIKOST` je velikost Arnoldiho faktorizace, při které se má restartovat a zkomprimovat faktorizace. Jestliže je tento parametr nespicifikovaný, pak se spočítá výchozí velikost z počtu požadovaných vlastních čísel, a je pak alespoň dvakrát tak větší než počet požadovaných vlastních čísel.

- `-s <SROVNÁVACÍ_KRITÉRIUM>`, `--sorting-criterion=<SROVNÁVACÍ_KRITÉRIUM>`  
	`SROVNÁVACÍ_KRITÉRIUM` udává část spektra vstupní matice, ze kterého se mají spočítat vlastní čísla. Může nabývat těchto hodnot: `largest-magnitude`, `smallest-magnitude`, `largest-real-part`, `smallest-real-part`. Výchozí hodnotou je `largest-magnitude`, pro výpočet vlastních čísel s největší absolutní hodnotou.

- `-p <PŘESNOST>`, `--precision=<PŘESNOST>`  
	`PŘESNOST` je reálné číslo udávající požadovanou přesnost vlastních čísel, mezi 0 a 1e-2. Výchozí přesnost je 1e-10.

- `-maxit <MAXIMÁLNÍ_POČET_ITERACÍ>`, `--maximal-number-of-iterations=<MAXIMÁLNÍ_POČET_ITERACÍ>`  
	`MAXIMÁLNÍ_POČET_ITERACÍ` je maximální povolený počet iterací implicitně restartované Arnoldiho metody. Implicitně je tento počet nastaven na 1000.


### Pokročilé volitelné parametry:
- `-maxortit <MAXIMÁLNÍ_POČET_ORTOGONALIZACÍ>`, `--maximal-number-of-orthogonalization-iterations=<MAXIMÁLNÍ_POČET_ORTOGONALIZACÍ>`  
	`MAXIMÁLNÍ_POČET_ORTOGONALIZACÍ` je maximální povolený počet iterací iterativní ortogonalizace. Totiž jestliže `MAXIMÁLNÍ_POČET_ORTOGONALIZACÍ=1`, pak je pokaždé provedena pouze jedna ortogonalizace. Výchozí hodnotou je 3.

- `-eta <ÉTA_ITERATIVNÍ_ORTOGONALIZACE>`, `--iterative-orthogonalization-eta=<ÉTA_ITERATIVNÍ_ORTOGONALIZACE>`  
	`ÉTA_ITERATIVNÍ_ORTOGONALIZACE` je číslo, které rozhoduje o tom, zda se má provést opětovná iterace algoritmu iterativní ortogonalizace po spočtení eukleidovské normy právě ortogonalizovaného vektoru. Opětovná ortogonalizace je provedena, pokud ještě nebyl dosažen maximální počet iterací iterativní ortogonalizace, a pokud `ÉTA_ITERATIVNÍ_ORTOGONALIZACE` \* eukleidovská norma vektoru po poslední ortogonalizaci &lt;\= eukleidovská norma vektoru před poslední ortogonalizací. Odtud, pokud `ÉTA_ITERATIVNÍ_ORTOGONALIZACE=1`, zmíněné kritérium na novou eukleidovskou normu bude splněno, dokud nebude vektor ortogonální na pracovní přesnost, a pokud `ÉTA_ITERATIVNÍ_ORTOGONALIZACE=0`, žádná opětovná ortogonalizace nebude provedena, což může vést k závažným výpočetním problémům. Výchozí hodnotou je 1/sqrt\(2\), totiž přibližně 7.07e-01, tedy hodnota doporučená v práci Daniel a jiní 1976.
