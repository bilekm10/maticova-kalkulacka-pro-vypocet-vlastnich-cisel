#ifndef EIGENVALUECALCULATORSOLVER_CVECTOR_H
#define EIGENVALUECALCULATORSOLVER_CVECTOR_H

#include "../view/vector/CDense1DBlockView.h"

#include <stdexcept>
#include <complex>

/// Providing methods for a vector. A helper class to avoid code duplication in row and column vectors
template<typename Scalar>
class CVector {
//public:
protected:
  void Assign(const Scalar *first, const Scalar *const end) {
    return m_View.Assign(first, end);
  }

  [[nodiscard]]
  Scalar At(size_t i) const {
    return m_View.At(i);
  }

  [[nodiscard]]
  Scalar &AtRef(size_t i) {
    return m_View.AtRef(i);
  }

  [[nodiscard]]
  size_t GetSize() const {
    return m_View.GetSize();
  }

  void SetTo(Scalar value) {
    m_View.SetTo(value);
  }

  void SetToZero() {
    SetTo(0);
  }

  virtual
  ~CVector();



  // Assign?

  /// sum of the absolute values
  Scalar AbsoluteNorm() const {
    Scalar currSum = 0;
    for (size_t i = 0; i < GetSize(); ++i)
      currSum += std::abs(At(i)); // |.|
    return currSum;
  }

  // for complex Scalars the return type should be Scalar::value_type
  /// two-norm, the square root of the sum of squared absolute values
  Scalar EuclideanNorm() const { // set FAST_MATH?
    Scalar currSum = 0;
    for (size_t i = 0; i < GetSize(); ++i)
      currSum += std::norm(At(i)); // |.|^2
    return sqrt(currSum);
  }

  /// maximum of the absolute values
  Scalar MaximumNorm() const {
    auto currMax = std::abs(Scalar(0));
    for (size_t i = 0; i < GetSize(); ++i)
      currMax = std::max(currMax, std::abs(At(i)));
    return currMax;
  }

  //template<template<typename> class VectorType>
  template<typename VectorType>
  void AddAssignment(const VectorType &addend) {
    for (size_t i = 0; i < GetSize(); ++i)
      AtRef(i) += addend.At(i);
  }

  template<typename VectorType>
  void SubtractAssignment(const VectorType &subtrahend) { // *this is the minuend and the result of the difference
    for (size_t i = 0; i < GetSize(); ++i)
      AtRef(i) -= subtrahend.At(i);
  }

  void MultiplyAssignment(Scalar multiplier) {
    for (size_t i = 0; i < GetSize(); ++i)
      AtRef(i) *= multiplier;
  }

  void DivideAssignment(
          Scalar denominator) { // multiplication is said to be faster, maybe change from scalar division to multiplication
    for (size_t i = 0; i < GetSize(); ++i)
      AtRef(i) /= denominator;
  }

  template<typename VectorType>
  void ScalarMultipleAddScalarMultiple(
          const Scalar firstMultiplier,
          const Scalar secondMultiplier,
          const VectorType &addend) { // *this is the first addend and the result of the scaled addition
    if (GetSize() != addend.GetSize())
      throw std::invalid_argument("CVector->ScalarMultipleAddScalarMultiple(): "
                                  "GetSize() != addend.GetSize(),"
                                  "so cannot add nor subtract");
    for (size_t i = 0; i < addend.GetSize(); ++i)
      AtRef(i) = firstMultiplier * At(i) + secondMultiplier * addend.At(i);
  }

  // maybe could try to stabilize this computation, is used a lot for matrix-vector multiplication
  template<typename VectorType>
  void AddScalarMultiple(
          const Scalar multiplier,
          const VectorType &addend) { // *this is the first addend and the result of the scaled addition
    if (GetSize() != addend.GetSize())
      throw std::invalid_argument("CVector->ScalarMultipleAddScalarMultiple(): "
                                  "GetSize() != addend.GetSize(),"
                                  "so cannot add nor subtract");
    for (size_t i = 0; i < addend.GetSize(); ++i)
      AtRef(i) += multiplier * addend.At(i);
    /*no time improvement const double * const mEnd = m_View.GetData() + GetSize();
    double * mElement = m_View.GetData();
    const double * const addendEnd = addend.AtConstPtr(addend.GetSize());
    for(const double * addendElement = addend.AtConstPtr(0);
        addendElement < addendEnd;
        ++addendElement,
        ++mElement)
      *mElement += multiplier * *addendElement;*/
  }

  template<typename VectorType>
  void AssignScalarMultiple(
          const Scalar multiplier,
          const VectorType &addend) { // *this is the first addend and the result of the scaled addition
    if (GetSize() != addend.GetSize())
      throw std::invalid_argument("CVector->ScalarMultipleAddScalarMultiple(): "
                                  "GetSize() != addend.GetSize(),"
                                  "so cannot add nor subtract");
    for (size_t i = 0; i < addend.GetSize(); ++i)
      AtRef(i) = multiplier * addend.At(i);
    /*no time improvement const double * const mEnd = m_View.GetData() + GetSize();
    double * mElement = m_View.GetData();
    const double * const addendEnd = addend.AtConstPtr(addend.GetSize());
    for(const double * addendElement = addend.AtConstPtr(0);
        addendElement < addendEnd;
        ++addendElement,
                ++mElement)
      *mElement = multiplier * *addendElement;*/
  }

  template<typename VectorType>
  Scalar MultiplyToScalar(const VectorType &multiplicand) const {
    if (GetSize() != multiplicand.GetSize())
      throw std::invalid_argument("CVector->MultiplyToScalar(): "
                                  "GetSize() != multiplicand.GetSize(),"
                                  "so cannot add nor multiply");
    Scalar currSum = 0;
    for (size_t i = 0; i < GetSize(); ++i)
      currSum += At(i) * multiplicand.At(i);
    return currSum;
  }


//protected:
  template<template<typename> class ReturnType>
  [[nodiscard]]
  ReturnType<Scalar> GetConstSubView(size_t firstElementIndex, size_t lastElementIndex) const {
    return m_View.template GetConstSubView<ReturnType>(
            firstElementIndex,
            lastElementIndex);
  }

  template<template<typename> class ReturnType>
  [[nodiscard]]
  ReturnType<Scalar> GetSubView(size_t firstElementIndex, size_t lastElementIndex) {
    return m_View.template GetSubView<ReturnType>(
            firstElementIndex,
            lastElementIndex);
  }

  CVector(Scalar *data, size_t dataSize);

  explicit CVector(CDense1DBlockView<Scalar> &&view);

  explicit CVector(const CDense1DBlockView<Scalar> &view);

  /*CVector(CVector && other)
          : m_View(std::move(other.m_View))
  {
  }

  CVector & operator=(CVector && other)
  {
    m_View = std::move(other.m_View);
    return *this;
  }*/

  CDense1DBlockView<Scalar> m_View;
};

template<typename Scalar>
CVector<Scalar>::CVector(Scalar *data, size_t dataSize)
        : m_View(data, dataSize) {
}

template<typename Scalar>
CVector<Scalar>::CVector(CDense1DBlockView<Scalar> &&view)
        : m_View(std::move(view)) {
}

template<typename Scalar>
CVector<Scalar>::CVector(const CDense1DBlockView<Scalar> &view)
        : m_View(view) {
}

template<typename Scalar>
CVector<Scalar>::~CVector() = default;

#endif //EIGENVALUECALCULATORSOLVER_CVECTOR_H
