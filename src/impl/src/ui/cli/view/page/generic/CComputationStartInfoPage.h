#ifndef EIGENVALUECALCULATORSOLVER_CCOMPUTATIONSTARTINFOPAGE_H
#define EIGENVALUECALCULATORSOLVER_CCOMPUTATIONSTARTINFOPAGE_H

#include "../CTextPage.h"

#include <string>

class CComputationStartInfoPage : public CTextPage {
public:
  explicit CComputationStartInfoPage(const std::string &message = "Computation started\n");

  [[nodiscard]]
  std::string Render() const override;

protected:
  const std::string &m_Message;
};

#endif //EIGENVALUECALCULATORSOLVER_CCOMPUTATIONSTARTINFOPAGE_H
