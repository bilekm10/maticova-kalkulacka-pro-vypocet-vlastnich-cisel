#include "CNumberOfIterationsSenderObserver.h"

CNumberOfIterationsSenderObserver::CNumberOfIterationsSenderObserver(CComputationSignals &computationSignals)
        : m_ComputationSignals(computationSignals) {
}

void CNumberOfIterationsSenderObserver::PropertyChange(const size_t &newValue) {
  m_ComputationSignals.NumberOfIterationsChanged(
          newValue);
}
