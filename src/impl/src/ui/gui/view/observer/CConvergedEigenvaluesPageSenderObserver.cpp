#include "./CConvergedEigenvaluesPageSenderObserver.h"

CConvergedEigenvaluesPageSenderObserver::CConvergedEigenvaluesPageSenderObserver(
        CComputationSignals &computationSignals)
        : m_ComputationSignals(computationSignals) {
}

void
CConvergedEigenvaluesPageSenderObserver::PropertyChange(const CColumnVectorConst <std::complex<double>> &newValue) {
  if (newValue.GetSize() > 0)
    m_ComputationSignals.EigenvaluesConverged(CColumnVectorHolding < std::complex<double>>
  {
    newValue.AtConstPtr(0),
            newValue.GetSize()
  });
}
