#include "CComputationStateSenderObserver.h"

CComputationStateSenderObserver::CComputationStateSenderObserver(
        CComputationSignals &computationSignals)
        : m_ComputationSignals(computationSignals) {
}

void CComputationStateSenderObserver::PropertyChange(const EComputationState &newValue) {
  /*if(m_OldValue == newValue)
    return;*/
  /*if(RestartedArnoldi::IsEnd(newValue))
    m_ComputationSignals.ComputationEnded(newValue);*/
  m_ComputationSignals.ComputationStateChanged(newValue);
}
