#include "./CMaximalNumberOfOrthogonalizationIterationsOptionHandler.h"

CMaximalNumberOfOrthogonalizationIterationsOptionHandler::CMaximalNumberOfOrthogonalizationIterationsOptionHandler(
        std::optional<uint8_t> &maximalNumberOfOrthogonalizationIterations)
        : m_MaximalNumberOfOrthogonalizationIterations(maximalNumberOfOrthogonalizationIterations),
          m_Number() {
}

// argumentsCnt must be >= 1
void CMaximalNumberOfOrthogonalizationIterationsOptionHandler::Handle(int &argumentsCnt, char **&argumentsVector) {
  if (ScanNumberArgumentBothNames<
          uint8_t,
          NumberFormat,
          OptionFullName,
          OptionShortName>(argumentsCnt,
                           argumentsVector,
                           m_Number)) {
    if (m_MaximalNumberOfOrthogonalizationIterations)
      ThrowOptionDuplicationError(OptionVerboseName);
    if (m_Number <= 0)
      throw std::invalid_argument(std::string(OptionVerboseName) +
                                  " must be an integer >= 1");
    m_MaximalNumberOfOrthogonalizationIterations = m_Number;
  } else
    return MoveToNextOptionHandler(argumentsCnt, argumentsVector);
}
