#ifndef EIGENVALUECALCULATORSOLVER_CCOMPRESSEDSPARSEROWMATRIX_H
#define EIGENVALUECALCULATORSOLVER_CCOMPRESSEDSPARSEROWMATRIX_H

#include "../../vector/column/CColumnVector.h"

#include <vector>
#include <stdexcept>
#include <algorithm>

/// Providing const methods for and storing a Compressed Sparse Row matrix
class CCompressedSparseRowMatrix /*: public CSparseMatrix */{
public:
  /*explicit CCompressedSparseRowMatrix(const std::vector<double> & data,
                                      const std::vector<size_t> & indicesOfColumns,
                                      const std::vector<size_t> & indicesOfRowStarts)
          : m_Data(data),
            m_IndicesOfColumns(indicesOfColumns),
            m_IndicesOfRowStarts(indicesOfRowStarts),
            m_RowsCnt(0)
  {
    CheckBasicDataProperties();
  }*/

  explicit CCompressedSparseRowMatrix(std::vector<double> &&data,
                                      std::vector<size_t> &&indicesOfColumns,
                                      std::vector<size_t> &&indicesOfRowStarts);

  [[nodiscard]]
  double At(size_t i, size_t j) const {
    const size_t indexOfRowStart = m_IndicesOfRowStarts[i];
    const size_t indexOfRowEnd = m_IndicesOfRowStarts[i + 1];
    const size_t *const columnsRowStartPtr = m_IndicesOfColumns.data() + indexOfRowStart;
    const size_t *const columnsRowEndPtr = m_IndicesOfColumns.data() + indexOfRowEnd;
    const size_t *const columnsPtr = std::lower_bound(columnsRowStartPtr, columnsRowEndPtr, j);
    if (columnsPtr != columnsRowEndPtr && *columnsPtr == j)
      return m_Data[indexOfRowStart + (columnsPtr - columnsRowStartPtr)];
    return 0;
  }

  /// square, so number of rows = number of columns
  [[nodiscard]]
  size_t GetRowsCnt() const { return m_RowsCnt; }

  /// square, so number of rows = number of columns
  [[nodiscard]]
  size_t GetColumnsCnt() const { return m_RowsCnt; }

  [[nodiscard]]
  size_t GetNumberOfNonZeros() const { return m_Data.size(); }

  [[nodiscard]]
  const std::vector<double> &GetNonZeros() const { return m_Data; }

  [[nodiscard]]
  const std::vector<size_t> &GetIndicesOfColumns() const { return m_IndicesOfColumns; }

  [[nodiscard]]
  const std::vector<size_t> &GetIndicesOfRowStarts() const { return m_IndicesOfRowStarts; }

  void Multiply(const CColumnVectorConst<double> &columnVector, CColumnVector<double> &result) const;

protected:
  void CheckBasicDataProperties() const {
    if (m_IndicesOfRowStarts.size() <= 1)
      throw std::invalid_argument("CCompressedSparseRowMatrix: no rows - empty indicesOfRowStarts, IA");
    if (m_IndicesOfRowStarts[0] != 0)
      throw std::invalid_argument("CCompressedSparseRowMatrix: indexing from 0, "
                                  "but indicesOfRowStarts[0], IA[0], != 0");
    if (m_RowsCnt * m_RowsCnt < m_Data.size())
      throw std::invalid_argument("CCompressedSparseRowMatrix: matrix rows-count * columns-count < data-size");
    if (m_Data.size() != m_IndicesOfColumns.size())
      throw std::invalid_argument("CCompressedSparseRowMatrix: size of data elements, "
                                  "AA, != size of indices of Columns, JA");
    if (m_IndicesOfRowStarts.back() != m_Data.size())
      throw std::invalid_argument("CCompressedSparseRowMatrix: the last element of indices-of-row-starts, IA,"
                                  "does not contain the size of data, NNZ, but it has to, "
                                  "because of indexing from 0");
    // should also check integrity of the data? i.e. in-row sorted JA, consistent with IA?
  }

  const std::vector<double> m_Data; ///< AA
  const std::vector<size_t> m_IndicesOfColumns; ///< JA
  const std::vector<size_t> m_IndicesOfRowStarts; ///< IA
  size_t m_RowsCnt; ///< square, so number of rows = number of columns
};

inline
CCompressedSparseRowMatrix::CCompressedSparseRowMatrix(std::vector<double> &&data,
                                                       std::vector<size_t> &&indicesOfColumns,
                                                       std::vector<size_t> &&indicesOfRowStarts)
        : m_Data(move(data)),
          m_IndicesOfColumns(move(indicesOfColumns)),
          m_IndicesOfRowStarts(move(indicesOfRowStarts)),
          m_RowsCnt(m_IndicesOfRowStarts.size() - 1) {
  CheckBasicDataProperties();
}

/*void CCompressedSparseRowMatrix::Multiply(const CColumnVectorConst<double> &columnVector,
                                          CColumnVector<double> &result) const
{
  if(GetColumnsCnt() != columnVector.GetSize())
    throw std::invalid_argument("CCompressedSparseRowMatrix->Multiply(): "
                                "Matrix columns count != vector size, i.e. rows count,"
                                "so cannot multiply the matrix by the vector from the right");
  if(GetRowsCnt() != result.GetSize())
    throw std::invalid_argument("CCompressedSparseRowMatrix->Multiply(): "
                                "Matrix rows count != result vector size, i.e. rows count,"
                                "so the result of multiplication cannot be stored "
                                "in the result vector, or it would be inappropriate");
  for(size_t i = 0; i < m_RowsCnt; ++i)
  {
    const size_t indexOfRowStart = m_IndicesOfRowStarts[i];
    const size_t indexOfRowEnd = m_IndicesOfRowStarts[i + 1];
    const size_t * const columnsRowEndPtr = m_IndicesOfColumns.data() + indexOfRowEnd;
    const size_t * columnsPtr = m_IndicesOfColumns.data() + indexOfRowStart;
    const double * dataPtr = m_Data.data() + indexOfRowStart;
    double currSum = 0;
    for(; columnsPtr < columnsRowEndPtr; ++columnsPtr, ++dataPtr)
      currSum += *dataPtr * columnVector.At(*columnsPtr);
    result.AtRef(i) = currSum;
  }
}*/

inline void
CCompressedSparseRowMatrix::Multiply(const CColumnVectorConst<double> &columnVector,
                                     CColumnVector<double> &result) const {
  if (GetColumnsCnt() != columnVector.GetSize())
    throw std::invalid_argument("CCompressedSparseRowMatrix->Multiply(): "
                                "Matrix columns count != vector size, i.e. rows count,"
                                "so cannot multiply the matrix by the vector from the right");
  if (GetRowsCnt() != result.GetSize())
    throw std::invalid_argument("CCompressedSparseRowMatrix->Multiply(): "
                                "Matrix rows count != result vector size, i.e. rows count,"
                                "so the result of multiplication cannot be stored "
                                "in the result vector, or it would be inappropriate");
  for (size_t i = 0; i < m_RowsCnt; ++i) {
    const size_t indexOfRowStart = m_IndicesOfRowStarts[i];
    const size_t indexOfRowEnd = m_IndicesOfRowStarts[i + 1];
    const size_t *const columnsRowEndPtr = m_IndicesOfColumns.data() + indexOfRowEnd;
    const size_t *columnsPtr = m_IndicesOfColumns.data() + indexOfRowStart;
    const double *dataPtr = m_Data.data() + indexOfRowStart;
    double currSum = 0;
    for (; columnsPtr < columnsRowEndPtr; ++columnsPtr, ++dataPtr)
      currSum += *dataPtr * columnVector.At(*columnsPtr);
    result.AtRef(i) = currSum;
  }
}

#endif //EIGENVALUECALCULATORSOLVER_CCOMPRESSEDSPARSEROWMATRIX_H
