#ifndef EIGENVALUECALCULATORSOLVER_CCLASSICALGRAMSCHMIDTORTHOGONALIZATION_H
#define EIGENVALUECALCULATORSOLVER_CCLASSICALGRAMSCHMIDTORTHOGONALIZATION_H

#include <memory>

#include "../../model/vector/column/CColumnVector.h"
#include "../../model/vector/column/CColumnVectorHolding.h"

/// Orthogonalize a vector against a matrix storing the orthogonalization coefficients using a version of CGS, BLAS-2 version algorithm from Orthogonalization methods in SLEPc
class CClassicalGramSchmidtOrthogonalization {
  using Scalar = double;
public:
  explicit CClassicalGramSchmidtOrthogonalization(
          size_t vectorToBeOrthogonalizedSize)
          : m_VectorAux(vectorToBeOrthogonalizedSize) {
  }

  template<typename MatrixType>
  [[nodiscard]]
  bool Orthogonalize(const MatrixType &matrixToOrthogonalizeAgainst,
                     CColumnVector<double> &vectorToBeOrthogonalized,
                     CColumnVector<double> &coefficients,
                     double &newNorm) {
    matrixToOrthogonalizeAgainst.Transposed().Multiply(vectorToBeOrthogonalized, coefficients);
    //vectorToBeOrthogonalized -= matrixToOrthogonalizeAgainst.Multiply(coefficients);
    if (coefficients.MaximumNorm() <= std::numeric_limits<Scalar>::epsilon() * newNorm)
      return true;
    matrixToOrthogonalizeAgainst.Multiply(coefficients, m_VectorAux);
    vectorToBeOrthogonalized -= m_VectorAux;
    newNorm = vectorToBeOrthogonalized.EuclideanNorm();
    return newNorm != 0;
  }

protected:
  CColumnVectorHolding<Scalar> m_VectorAux;
};

#endif //EIGENVALUECALCULATORSOLVER_CCLASSICALGRAMSCHMIDTORTHOGONALIZATION_H
