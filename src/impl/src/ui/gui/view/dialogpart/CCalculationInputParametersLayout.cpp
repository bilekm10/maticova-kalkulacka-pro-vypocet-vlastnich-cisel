#include "CCalculationInputParametersLayout.h"

#include <utility>

CCalculationInputParametersLayout::CCalculationInputParametersLayout() {
  CreateEigenvalueParametersFormLayout();
  CreateSortingCriterionGroupBox();
  CreateInputMatrixFilenameLayout();
  CreateMaximalNumberOfOrthogonalizationIterationsRow();
  CreateIterativeOrthogonalizationEtaRow();

  addLayout(m_EigenvalueParametersFormLayout);
  addWidget(m_SortingCriterionGroupBox);
  addLayout(m_InputMatrixFilenameLayout);
  setSpacing(VerticalSpacing);
}

std::optional<QString> CCalculationInputParametersLayout::ReadInputMatrixFilename() {
  QString filename = m_InputMatrixFilenameLineEdit->text();
  if (filename.isEmpty()) {
    QMessageBox msgBox(QMessageBox::Icon::Critical,
                       QString("Invalid parameter: Input matrix filename"),
                       QString("Input matrix filename has to be specified."),
                       QMessageBox::StandardButton::Cancel);
    msgBox.exec();
    return std::nullopt;
  }
  if (filename.endsWith('/')) {
    QMessageBox msgBox(QMessageBox::Icon::Critical,
                       QString("Invalid parameter: Input matrix filename"),
                       QString("Input matrix filename \"%1\" has to be a file, not a directory.")
                               .arg(filename),
                       QMessageBox::StandardButton::Cancel);
    msgBox.exec();
    return std::nullopt;
  }
  QFile file(filename);
  if (!file.exists()) {
    QMessageBox msgBox(QMessageBox::Icon::Critical,
                       QString("Invalid parameter: Input matrix filename"),
                       QString("Input matrix filename \"%1\" is not a name of an existing file.")
                               .arg(filename),
                       QMessageBox::StandardButton::Cancel);
    msgBox.exec();
    return std::nullopt;
  }
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    QMessageBox msgBox(QMessageBox::Icon::Critical,
                       QString("Invalid parameter: Input matrix filename"),
                       QString("Input matrix filename \"%1\" couldn't be opened for reading.")
                               .arg(filename),
                       QMessageBox::StandardButton::Cancel);
    msgBox.exec();
    return std::nullopt;
  }
  file.close();
  return filename;
}

std::optional<ESortingCriterion> CCalculationInputParametersLayout::ReadSortingCriterion() {
  if (m_LargestMagnitude->isChecked())
    return ESortingCriterion::LargestMagnitude;
  else if (m_SmallestMagnitude->isChecked())
    return ESortingCriterion::SmallestMagnitude;
  else if (m_LargestRealPart->isChecked())
    return ESortingCriterion::LargestRealPart;
  else if (m_SmallestRealPart->isChecked())
    return ESortingCriterion::SmallestRealPart;
  else {
    QMessageBox msgBox(QMessageBox::Icon::Critical,
                       QString("Invalid parameter: Sorting criterion"),
                       QString("At least one sorting criterion has to be selected."),
                       QMessageBox::StandardButton::Cancel);
    msgBox.exec();
    return std::nullopt;
  }
}

std::optional<double>
CCalculationInputParametersLayout::ReadDouble(QLineEdit *lineEdit, QDoubleValidator *doubleValidator,
                                              const QString &parameterName, const QString &minValue,
                                              const QString &maxValue) {
  bool okDouble = true;
  int pos = 0;
  QString doubleNumberText = lineEdit->text();
  double doubleNumber = doubleNumberText.toDouble(&okDouble);
  if (doubleValidator->validate(doubleNumberText, pos) != QValidator::Acceptable || !okDouble) {
    QMessageBox msgBox(QMessageBox::Icon::Critical,
                       QString("Invalid parameter: " + parameterName),
                       QString(parameterName +
                               " has to be at least " +
                               minValue +
                               " and at most " +
                               maxValue +
                               "."),
                       QMessageBox::StandardButton::Cancel);
    msgBox.exec();
    return std::nullopt;
  }
  return doubleNumber;
}

std::optional<double> CCalculationInputParametersLayout::ReadPrecision() {
  return ReadDouble(m_PrecisionLineEdit,
                    m_PrecisionValidator,
                    "Precision",
                    "0",
                    "1e-2");
}

std::optional<double> CCalculationInputParametersLayout::ReadIterativeOrthogonalizationEta() {
  return ReadDouble(m_IterativeOrthogonalizationEtaLineEdit,
                    m_IterativeOrthogonalizationEtaValidator,
                    "Iterative orthogonalization eta",
                    "0",
                    "1");
}

bool CCalculationInputParametersLayout::ReadInputParameters(CRestartedArnoldiInputArguments &inputArguments) {
  size_t numberOfWantedEigenvalues = m_NumberOfWantedEigenvaluesSpinBox->value();
  size_t factorizationSizeToRestartAt = m_MaximalArnoldiFactorizationSizeSpinBox->value();
  if (factorizationSizeToRestartAt < numberOfWantedEigenvalues + 2) {
    QMessageBox msgBox(QMessageBox::Icon::Critical,
                       QString("Invalid parameter: Maximal Arnoldi factorization size"),
                       QString("Maximal Arnoldi factorization size has to be at least "
                               "the Number of wanted eigenvalues + 2."),
                       QMessageBox::StandardButton::Cancel);
    msgBox.exec();
    return false;
  }
  size_t maximalNumberOfIterations = m_MaximalNumberOfIterationsSpinBox->value();
  std::optional<double> optionalPrecision = ReadPrecision();
  if (!optionalPrecision)
    return false;
  uint8_t maximalNumberOfOrthogonalizationIterations = m_MaximalNumberOfOrthogonalizationIterationsSpinBox->value();
  std::optional<double> optionalIterativeOrthogonalizationEta = ReadIterativeOrthogonalizationEta();
  if (!optionalIterativeOrthogonalizationEta)
    return false;
  std::optional<ESortingCriterion> optionalSortingCriterion = ReadSortingCriterion();
  if (!optionalSortingCriterion)
    return false;
  std::optional<QString> optionalFilename = ReadInputMatrixFilename();
  if (!optionalFilename)
    return false;
  #ifdef DBG_PRINT_PARAMETERS
  qDebug() << numberOfWantedEigenvalues;
    qDebug() << factorizationSizeToRestartAt;
    qDebug() << maximalNumberOfIterations;
    qDebug() << optionalPrecision.value();
    qDebug() << int(optionalSortingCriterion.value());
    qDebug() << optionalFilename.value();
    qDebug() << int(maximalNumberOfOrthogonalizationIterations);
    qDebug() << optionalIterativeOrthogonalizationEta.value();
    qDebug();
  #endif // DBG_PRINT_PARAMETERS
  inputArguments.m_InputMatrixFilename = optionalFilename.value().toStdString();
  inputArguments.m_NumberOfWantedEigenvalues = numberOfWantedEigenvalues;
  inputArguments.m_FactorizationSizeToRestartAt = factorizationSizeToRestartAt;
  inputArguments.m_MaximalNumberOfIterations = maximalNumberOfIterations;
  inputArguments.m_Precision = optionalPrecision.value();
  inputArguments.m_SortingCriterion = optionalSortingCriterion.value();
  inputArguments.m_MaximalNumberOfOrthogonalizationIterations = maximalNumberOfOrthogonalizationIterations;
  inputArguments.m_IterativeOrthogonalizationEta = optionalIterativeOrthogonalizationEta.value();
  try {
    inputArguments.Validate();
    inputArguments.SetEmptyToDefault();
  }
  catch (const std::exception &exc) {
    QMessageBox msgBox(QMessageBox::Icon::Critical,
                       QString("Invalid parameter"),
                       exc.what(),
                       QMessageBox::StandardButton::Cancel);
    msgBox.exec();
    return false;
  }
  return true;
}

std::string CCalculationInputParametersLayout::ToString(double number, uint8_t manipPrecision) {
  std::stringstream ss;
  ss << std::scientific << std::setprecision(manipPrecision) << number;
  return ss.str();
}

void CCalculationInputParametersLayout::AddLabelWidgetRow(QFormLayout *&formLayout, QLabel *label, QWidget *widget) {
  auto *boxLayout = new QBoxLayout(QBoxLayout::LeftToRight);
  boxLayout->addWidget(
          widget,
          0,
          Qt::AlignVCenter);
  formLayout->addRow(label,
                     boxLayout);
}

void CCalculationInputParametersLayout::CreateLabelDoubleLineEdit(QLabel *&label, const QString &labelText,
                                                                  QDoubleValidator *&doubleValidator,
                                                                  QLineEdit *&lineEdit, double bottomValue,
                                                                  double topValue, QString defaultValueText) {
  label = new QLabel(labelText);
  label->setWordWrap(LabelTextWordWrap);
  lineEdit = new QLineEdit;
  doubleValidator = new QDoubleValidator(lineEdit);
  doubleValidator->setBottom(bottomValue);
  doubleValidator->setTop(topValue);
  int pos = 0;
  if (doubleValidator->validate(defaultValueText, pos) != QValidator::Acceptable)
    throw std::invalid_argument("CRestartedArnoldiDialogView->CreateLabelDoubleLineEdit(): "
                                "Default value text is not acceptable by the double validator");
  lineEdit->setValidator(doubleValidator);
  lineEdit->setText(defaultValueText);
  lineEdit->setMinimumWidth(100);
}

void CCalculationInputParametersLayout::AddLabelDoubleLineEditRow(QFormLayout *&formLayout, QLabel *&label,
                                                                  const QString &labelText,
                                                                  QDoubleValidator *&doubleValidator,
                                                                  QLineEdit *&lineEdit, double bottomValue,
                                                                  double topValue, QString defaultValueText) {
  CreateLabelDoubleLineEdit(label,
                            labelText,
                            doubleValidator,
                            lineEdit,
                            bottomValue,
                            topValue,
                            std::move(defaultValueText));
  AddLabelWidgetRow(formLayout,
                    label,
                    lineEdit);
}

void CCalculationInputParametersLayout::CreateLabelSpinBox(QLabel *&label, const QString &labelText, QSpinBox *&spinBox,
                                                           int minValue, int defaultValue, int maxValue) {
  label = new QLabel(labelText);
  label->setWordWrap(LabelTextWordWrap);
  spinBox = new QSpinBox;
  spinBox->setMinimum(minValue);
  spinBox->setMaximum(maxValue);
  if (defaultValue == INT_MIN)
    defaultValue = minValue;
  spinBox->setValue(defaultValue);
  spinBox->setMinimumWidth(50);
}

void CCalculationInputParametersLayout::AddLabelSpinBoxRow(QFormLayout *&formLayout, QLabel *&label,
                                                           const QString &labelText, QSpinBox *&spinBox, int minValue,
                                                           int defaultValue, int maxValue) {
  CreateLabelSpinBox(label,
                     labelText,
                     spinBox,
                     minValue,
                     defaultValue,
                     maxValue);
  AddLabelWidgetRow(formLayout,
                    label,
                    spinBox);
}

void CCalculationInputParametersLayout::CreateEigenvalueParametersFormLayout() {
  m_EigenvalueParametersFormLayout = new QFormLayout;
  m_EigenvalueParametersFormLayout->setHorizontalSpacing(HorizontalSpacing);
  m_EigenvalueParametersFormLayout->setVerticalSpacing(VerticalSpacing);
  m_EigenvalueParametersFormLayout->setAlignment(Qt::AlignVCenter);

  CreateNumberOfWantedEigenvaluesRow();
  CreateMaximalArnoldiFactorizationSizeRow();
  CreateMaximalNumberOfIterationsRow();
  CreatePrecisionRow();
}

void CCalculationInputParametersLayout::CreateNumberOfWantedEigenvaluesRow() {
  AddLabelSpinBoxRow(m_EigenvalueParametersFormLayout,
                     m_NumberOfWantedEigenvaluesLabel,
                     QString("Number of wanted eigenvalues"),
                     m_NumberOfWantedEigenvaluesSpinBox,
                     1,
                     DefaultNumberOfWantedEigenvalues);
}

void CCalculationInputParametersLayout::CreateMaximalArnoldiFactorizationSizeRow() {
  AddLabelSpinBoxRow(m_EigenvalueParametersFormLayout,
                     m_MaximalArnoldiFactorizationSizeLabel,
                     QString("Maximal Arnoldi factorization size"),
                     m_MaximalArnoldiFactorizationSizeSpinBox,
                     3,
                     DefaultFactorizationSizeToRestartAt);
  m_MaximalArnoldiFactorizationSizeLabel->setToolTip(
          "The size of the Arnoldi factorization to restart at and compress the factorization.\n"
          "Larger value often means faster convergence.\n"
          "\nShould be at least twice as big as the number of wanted eigenvalues, plus one.");
}

void CCalculationInputParametersLayout::CreateMaximalNumberOfIterationsRow() {
  AddLabelSpinBoxRow(m_EigenvalueParametersFormLayout,
                     m_MaximalNumberOfIterationsLabel,
                     QString("Maximal number of iterations"),
                     m_MaximalNumberOfIterationsSpinBox,
                     1,
                     DefaultMaximalNumberOfIterations);
  m_MaximalNumberOfIterationsLabel->setToolTip(QString(
          "The maximal allowed number of iterations, i.e. number of restarts + 1, of the implicitly restarted Arnoldi method.\n"
          "When the wanted eigenvalues don't converge before this value is reached, "
          "try increasing this value next time.\n"
          "\nThe default maximal number of iterations is %1, which many times isn't enough."
  ).arg(DefaultMaximalNumberOfIterations));
}

void CCalculationInputParametersLayout::CreatePrecisionRow() {
  AddLabelDoubleLineEditRow(
          m_EigenvalueParametersFormLayout,
          m_PrecisionLabel,
          QString("Precision"),
          m_PrecisionValidator,
          m_PrecisionLineEdit,
          0,
          1e-2,
          QString(ToString(CRestartedArnoldiInputArguments::DefaultPrecision(),
                           2).c_str()));
  m_PrecisionLabel->setToolTip(QString(
          "A floating point number specifying the wanted precision of the eigenvalues,\n"
          "e.g.: 1e-10, 1e-5.\n"
          "\nThe default precision is %1."
  ).arg(ToString(CRestartedArnoldiInputArguments::DefaultPrecision(),
                 2).c_str()));
}

void CCalculationInputParametersLayout::CreateMaximalNumberOfOrthogonalizationIterationsRow() {
  AddLabelSpinBoxRow(m_EigenvalueParametersFormLayout,
                     m_MaximalNumberOfOrthogonalizationIterationsLabel,
                     QString("Maximal number of\northogonalization iterations"),
                     m_MaximalNumberOfOrthogonalizationIterationsSpinBox,
                     1,
                     CRestartedArnoldiInputArguments::DefaultMaximalNumberOfOrthogonalizationIterations(),
                     std::numeric_limits<uint8_t>::max());
  //m_MaximalNumberOfOrthogonalizationIterationsLabel->setWordWrap(true);
  m_MaximalNumberOfOrthogonalizationIterationsLabel->setToolTip(QString(
          "The maximal allowed number "
          "of [re]orthogonalization iterations for iterative orthogonalization.\n"
          "Only one orthogonalization is performed each time if the input value equals 1,\n"
          "what can lead to loss of orthogonality and finding completely wrong solution.\n"
          "\nThe default maximal number of iterative orthogonalization iterations is %1."
  ).arg(CRestartedArnoldiInputArguments::DefaultMaximalNumberOfOrthogonalizationIterations()));
}

void CCalculationInputParametersLayout::CreateIterativeOrthogonalizationEtaRow() {
  AddLabelDoubleLineEditRow(
          m_EigenvalueParametersFormLayout,
          m_IterativeOrthogonalizationEtaLabel,
          QString("Iterative orthogonalization eta"),
          m_IterativeOrthogonalizationEtaValidator,
          m_IterativeOrthogonalizationEtaLineEdit,
          0,
          1,
          QString(ToString(CRestartedArnoldiInputArguments::DefaultIterativeOrthogonalizationEta(),
                           5).c_str()));
  m_IterativeOrthogonalizationEtaLabel->setToolTip(QString(
          "ITERATIVE_ORTHOGONALIZATION_ETA is a number which\ndetermines whether to perform "
          "another iteration of the iterative orthogonalization\nafter computing "
          "the two-norm of the orthogonalized vector.\nAnother orthogonalization "
          "is performed\nif the maximal number of orthogonalization iterations "
          "wasn't reached yet\nand if "
          "ITERATIVE_ORTHOGONALIZATION_ETA * ||afterLastOrthogVec||_2 "
          "<= ||beforeLastOrthogVec||_2.\nHence, if ITERATIVE_ORTHOGONALIZATION_ETA=1, "
          "this reorthogonalization criterion will be satisfied\nuntil the new vector is "
          "orthogonal to working precision,\nand if ITERATIVE_ORTHOGONALIZATION_ETA=0, "
          "no reorthogonalization is performed,\nwhich can lead to serious computational "
          "problems.\n"
          "\nThe default iterative orthogonalization eta is 1/sqrt(2), rounded into %1,\n"
          "a value recommended by Daniel et al. 1976."
  ).arg(ToString(CRestartedArnoldiInputArguments::DefaultIterativeOrthogonalizationEta(),
                 5).c_str()));
}

//Specifies the side of the spectrum from which to compute the eigenvalues
void CCalculationInputParametersLayout::CreateSortingCriterionGroupBox() {
  m_SortingCriterionGroupBox = new QGroupBox(QString("Sorting criterion"));
  m_SortingCriterionLayout = new QVBoxLayout;
  m_LargestMagnitude = new QRadioButton(QString("Largest magnitude"));
  m_SmallestMagnitude = new QRadioButton(QString("Smallest magnitude"));
  m_LargestRealPart = new QRadioButton(QString("Largest real part"));
  m_SmallestRealPart = new QRadioButton(QString("Smallest real part"));
  m_LargestMagnitude->setChecked(true);
  m_SortingCriterionLayout->addWidget(m_LargestMagnitude);
  m_SortingCriterionLayout->addWidget(m_SmallestMagnitude);
  m_SortingCriterionLayout->addWidget(m_LargestRealPart);
  m_SortingCriterionLayout->addWidget(m_SmallestRealPart);
  m_SortingCriterionGroupBox->setLayout(m_SortingCriterionLayout);
  m_SortingCriterionGroupBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
}

void CCalculationInputParametersLayout::CreateInputMatrixFilenameLayout() {
  m_InputMatrixFilenameLayout = new QHBoxLayout;
  m_InputMatrixFilenameLayout->setSpacing(HorizontalSpacing);
  m_InputMatrixFilenameLabel = new QLabel(QString("Input matrix filename"));
  m_InputMatrixFilenameLabel->setWordWrap(LabelTextWordWrap);
  m_InputMatrixFilenameLineEdit = new QLineEdit;
  m_InputMatrixFilenameFileDialogBtn = new QPushButton(QString("Find"));
  m_InputMatrixFilenameFileDialogBtn->setFixedWidth(40);
  connect(m_InputMatrixFilenameFileDialogBtn, &QPushButton::clicked,
          this, &CCalculationInputParametersLayout::InputMatrixFilenameFileDialog);
  m_InputMatrixFilenameLayout->addWidget(m_InputMatrixFilenameLabel);
  m_InputMatrixFilenameLayout->addWidget(m_InputMatrixFilenameLineEdit);
  m_InputMatrixFilenameLayout->addWidget(m_InputMatrixFilenameFileDialogBtn);
}

void CCalculationInputParametersLayout::InputMatrixFilenameFileDialog() {
  QFileDialog dialog;
  dialog.setWindowTitle(QString("Input matrix filename"));
  dialog.setFileMode(QFileDialog::ExistingFile);
  if (!dialog.exec())
    return;
  QString filename = dialog.selectedFiles().first();
  m_InputMatrixFilenameLineEdit->setText(filename);
}
