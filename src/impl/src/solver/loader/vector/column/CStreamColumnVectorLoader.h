#ifndef EIGENVALUECALCULATORSOLVER_CSTREAMCOLUMNVECTORLOADER_H
#define EIGENVALUECALCULATORSOLVER_CSTREAMCOLUMNVECTORLOADER_H

#include "CColumnVectorLoader.h"
#include "../../../model/vector/column/CColumnVector.h"

#include <vector>
#include <stdexcept>
#include <complex>
#include <istream>

/// Loading a CColumnVectorHolding from a stream
template<typename Scalar>
class CStreamColumnVectorLoader : public CColumnVectorLoader<Scalar> {
public:
  explicit CStreamColumnVectorLoader(std::istream &is);

  [[nodiscard]]
  CColumnVectorHolding<Scalar> Load() override;

protected:
  std::istream &m_Is;
};

template<typename Scalar>
CStreamColumnVectorLoader<Scalar>::CStreamColumnVectorLoader(std::istream &is)
        : CColumnVectorLoader<Scalar>(),
          m_Is(is) {
}

template<typename Scalar>
[[nodiscard]]
CColumnVectorHolding<Scalar> CStreamColumnVectorLoader<Scalar>::Load() {
  Scalar element;
  std::vector<Scalar> vectorData;
  while (m_Is >> element)
    vectorData.push_back(element);
  if (vectorData.empty())
    throw std::invalid_argument("CStreamColumnVectorLoader->Load(): "
                                "bad std::istream, vector is empty");
  return CColumnVectorHolding<Scalar>(vectorData.data(), vectorData.size());
}

#endif //EIGENVALUECALCULATORSOLVER_CSTREAMCOLUMNVECTORLOADER_H
