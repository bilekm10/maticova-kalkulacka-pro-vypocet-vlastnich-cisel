#include "./COptionHandler.h"

COptionHandler::COptionHandler()
        : m_NextOptionHandler(nullptr) {
}

void COptionHandler::MoveToNextOptionHandler(int &argumentsCnt, char **&argumentsVector) {
  if (!m_NextOptionHandler)
    /*throw logic_error("COptionHandler->MoveToNextOptionHandler(): "
                      "called without next option handler");*/
    return;
  return m_NextOptionHandler->Handle(argumentsCnt, argumentsVector);
}

void COptionHandler::ThrowOptionDuplicationError(const char optionVerboseName[]) {
  throw std::invalid_argument(std::string(optionVerboseName) +
                              " was already specified. Option specification duplication "
                              "is prohibited.");
}

void COptionHandler::ThrowRedundantCharactersAfterNumber(const char *optionName) {
  throw std::invalid_argument("Invalid argument for option \"" +
                              std::string(optionName) +
                              "\": there are some characters right after the number.");
}
