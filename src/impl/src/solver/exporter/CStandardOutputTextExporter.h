#ifndef EIGENVALUECALCULATORSOLVER_CSTANDARDOUTPUTTEXTEXPORTER_H
#define EIGENVALUECALCULATORSOLVER_CSTANDARDOUTPUTTEXTEXPORTER_H

#include "./CStreamTextExporter.h"

#include <iostream>

/// Export text to standard output
class CStandardOutputTextExporter : public CStreamTextExporter {
public:
  explicit CStandardOutputTextExporter();

protected:
};

inline
CStandardOutputTextExporter::CStandardOutputTextExporter()
        : CStreamTextExporter(std::cout) {
}

#endif //EIGENVALUECALCULATORSOLVER_CSTANDARDOUTPUTTEXTEXPORTER_H
