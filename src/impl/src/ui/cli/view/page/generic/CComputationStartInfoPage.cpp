#include "./CComputationStartInfoPage.h"

CComputationStartInfoPage::CComputationStartInfoPage(const std::string &message)
        : m_Message(message) {
}

[[nodiscard]]
std::string CComputationStartInfoPage::Render() const {
  return "--------------------------------------------------\n" +
         m_Message;
}
