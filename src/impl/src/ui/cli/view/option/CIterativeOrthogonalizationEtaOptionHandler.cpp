#include "./CIterativeOrthogonalizationEtaOptionHandler.h"

CIterativeOrthogonalizationEtaOptionHandler::CIterativeOrthogonalizationEtaOptionHandler(
        std::optional<double> &iterativeOrthogonalizationEta)
        : m_IterativeOrthogonalizationEta(iterativeOrthogonalizationEta),
          m_Number() {
}

// argumentsCnt must be >= 1
void CIterativeOrthogonalizationEtaOptionHandler::Handle(int &argumentsCnt, char **&argumentsVector) {
  if (ScanNumberArgumentBothNames<
          double,
          NumberFormat,
          OptionFullName,
          OptionShortName>(argumentsCnt,
                           argumentsVector,
                           m_Number)) {
    if (m_IterativeOrthogonalizationEta)
      ThrowOptionDuplicationError(OptionVerboseName);
    if (m_Number < 0 || m_Number > 1)
      throw std::invalid_argument(std::string(OptionVerboseName) +
                                  " must be a double "
                                  ">= 0 and <= 1, e.g. \"7.07e-1\"");
    m_IterativeOrthogonalizationEta = m_Number;
  } else
    return MoveToNextOptionHandler(argumentsCnt, argumentsVector);
}
