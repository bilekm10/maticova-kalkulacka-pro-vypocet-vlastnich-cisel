#ifndef EIGENVALUECALCULATORSOLVER_CCOMMANDLINEVIEW_H
#define EIGENVALUECALCULATORSOLVER_CCOMMANDLINEVIEW_H

#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include <stdexcept>
#include <optional>

#include "../controller/CCommandLineController.h"
#include "./option/COptionHandler.h"
#include "./option/CNumberOfWantedEigenvaluesOptionHandler.h"
#include "./option/CFactorizationSizeToRestartAtOptionHandler.h"
#include "./option/CSortingCriterionOptionHandler.h"
#include "./option/CPrecisionOptionHandler.h"
#include "./option/CMaximalNumberOfIterationsOptionHandler.h"
#include "./option/CInputMatrixFilenameOptionHandler.h"
#include "./option/CMaximalNumberOfOrthogonalizationIterationsOptionHandler.h"
#include "./option/CIterativeOrthogonalizationEtaOptionHandler.h"
#include "./option/CShowHelpOptionHandler.h"
#include "./option/CThrowNoOptionMatchedHandler.h"
#include "../../../solver/exporter/CTextExporter.h"

//#define DBG_PARSED_OPTIONS_PRINT
//#define DBG_OPTIONS_FAIL_PRINT

class CCommandLineView {
public:
  explicit CCommandLineView(
          CTextExporter &standardOutputTextExporter,
          CCommandLineController &controller);

  void Run(int argumentsCnt, char **argumentsVector);

protected:
  CTextExporter &m_StandardOutputTextExporter;
  CCommandLineController &m_Controller;

  const std::string m_HelpRedirectionMessage =
          "Type " +
          std::string(CShowHelpOptionHandler::OptionShortName) +
          " or " +
          std::string(CShowHelpOptionHandler::OptionFullName) +
          " for help.\n";
};

#endif //EIGENVALUECALCULATORSOLVER_CCOMMANDLINEVIEW_H
