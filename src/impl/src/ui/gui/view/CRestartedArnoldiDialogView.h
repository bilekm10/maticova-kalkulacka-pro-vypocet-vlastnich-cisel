#ifndef EIGENVALUECALCULATORGUI_CRESTARTEDARNOLDIDIALOGVIEW_H
#define EIGENVALUECALCULATORGUI_CRESTARTEDARNOLDIDIALOGVIEW_H

#include "../controller/CRestartedArnoldiController.h"
#include "./dialogpart/CComputedEigenvaluesLayout.h"
#include "./dialogpart/CCalculationExecutionLayout.h"
#include "./dialogpart/CCalculationInputParametersLayout.h"
#include "./observer/CNumberOfIterationsSenderObserver.h"
#include "../../../solver/sorting/ESortingCriterion.h"

#include <QDialog>
#include <QtWidgets>
#include <QtConcurrent/QtConcurrent>
#include <stdexcept>
#include <optional>
#include <functional>
#include <atomic>
#include <string>

QT_BEGIN_NAMESPACE
class QAction;

class QDialogButtonBox;

class QGroupBox;

class QLabel;

class QLineEdit;

class QMenu;

class QMenuBar;

class QPushButton;

class QTextEdit;

class QComboBox;

class QSpinBox;

class QDoubleValidator;

class QMessageBox;

QT_END_NAMESPACE

// https://doc.qt.io/qt-5/qtimer.html

// https://doc.qt.io/qt-5/examples-layouts.html
// https://doc.qt.io/qt-5/qtwidgets-layouts-basiclayouts-example.html
// https://doc.qt.io/qt-5/qtwidgets-widgets-groupbox-example.html

// QMessageBox https://doc.qt.io/qt-5/qmessagebox.html
// QDialog https://doc.qt.io/qt-5/qdialog.html
class CRestartedArnoldiDialogView : public QDialog {
Q_OBJECT
public:
  explicit CRestartedArnoldiDialogView(CRestartedArnoldiController &controller);

protected:
  void ShowComputationErrorMessage(const std::string &message);

  static constexpr int HorizontalSpacingMainLayout = 5;

  QFrame *ReturnLine(QFrame::Shape shape);

  QFrame *ReturnHLine() { return ReturnLine(QFrame::HLine); }

  QFrame *ReturnVLine() { return ReturnLine(QFrame::VLine); }

  void StopComputing();

  void ComputationEnded();

  void ReadInputParametersAndCalculate();

  void reject() override;

  CRestartedArnoldiController &m_RestartedArnoldiDialogController;

  std::atomic<bool> m_Running;
  std::atomic<bool> m_Stopped;

  CComputationSignals m_ComputationSignals;
  CConvergedEigenvaluesPageSenderObserver m_ConvergedEigenvaluesPageSenderObserver;
  CComputationStateSenderObserver m_ComputationStateSenderObserver;
  CNumberOfIterationsSenderObserver m_NumberOfIterationsRenderObserver;
  CErrorMessageTextExporter m_ErrorMessageTextExporter;

  QHBoxLayout *m_MainLayout;

  /*QVBoxLayout * m_ComputedEigenvaluesLayout;
  QLabel * m_ComputedEigenvaluesLabel;
  QTextEdit * m_ComputedEigenvaluesTextEdit;*/
  CComputedEigenvaluesLayout *m_ComputedEigenvaluesLayout;
  CCalculationInputParametersLayout *m_CalculationInputParametersLayout;
  CCalculationExecutionLayout *m_CalculationExecutionLayout;
};

#endif //EIGENVALUECALCULATORGUI_CRESTARTEDARNOLDIDIALOGVIEW_H
