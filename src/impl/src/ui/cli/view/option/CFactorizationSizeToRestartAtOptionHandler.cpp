#include "./CFactorizationSizeToRestartAtOptionHandler.h"

CFactorizationSizeToRestartAtOptionHandler::CFactorizationSizeToRestartAtOptionHandler(
        std::optional<size_t> &factorizationSizeToRestartAt)
        : m_FactorizationSizeToRestartAt(factorizationSizeToRestartAt),
          m_Number() {
}

// argumentsCnt must be >= 1
void CFactorizationSizeToRestartAtOptionHandler::Handle(int &argumentsCnt, char **&argumentsVector) {
  if (ScanNumberArgumentBothNames<
          size_t,
          NumberFormat,
          OptionFullName,
          OptionShortName>(argumentsCnt,
                           argumentsVector,
                           m_Number)) {
    if (m_FactorizationSizeToRestartAt)
      ThrowOptionDuplicationError(OptionVerboseName);
    if (m_Number <= 2)
      throw std::invalid_argument(std::string(OptionVerboseName) +
                                  " must be an integer >= 3");
    m_FactorizationSizeToRestartAt = m_Number;
  } else
    return MoveToNextOptionHandler(argumentsCnt, argumentsVector);
}
