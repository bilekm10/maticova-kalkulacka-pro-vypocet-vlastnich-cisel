#include "./CNumberOfWantedEigenvaluesOptionHandler.h"

CNumberOfWantedEigenvaluesOptionHandler::CNumberOfWantedEigenvaluesOptionHandler(
        std::optional<size_t> &numberOfWantedEigenvalues)
        : m_NumberOfWantedEigenvalues(numberOfWantedEigenvalues),
          m_Number() {
}

// argumentsCnt must be >= 1
void CNumberOfWantedEigenvaluesOptionHandler::Handle(int &argumentsCnt, char **&argumentsVector) {
  if (ScanNumberArgumentBothNames<
          size_t,
          NumberFormat,
          OptionFullName,
          OptionShortName>(argumentsCnt,
                           argumentsVector,
                           m_Number)) {
    if (m_NumberOfWantedEigenvalues)
      ThrowOptionDuplicationError(OptionVerboseName);
    if (m_Number <= 0)
      throw std::invalid_argument(std::string(OptionVerboseName) +
                                  " must be an integer > 0");
    m_NumberOfWantedEigenvalues = m_Number;
  } else
    return MoveToNextOptionHandler(argumentsCnt, argumentsVector);
}
