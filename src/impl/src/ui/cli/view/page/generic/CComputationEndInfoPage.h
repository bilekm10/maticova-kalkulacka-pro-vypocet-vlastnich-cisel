#ifndef EIGENVALUECALCULATORSOLVER_CCOMPUTATIONENDINFOPAGE_H
#define EIGENVALUECALCULATORSOLVER_CCOMPUTATIONENDINFOPAGE_H

#include "../CTextPage.h"

#include <string>

class CComputationEndInfoPage : public CTextPage {
public:
  explicit CComputationEndInfoPage(const std::string &message = "Computation ended\n");

  [[nodiscard]]
  std::string Render() const override;

protected:
  const std::string &m_Message;
};

#endif //EIGENVALUECALCULATORSOLVER_CCOMPUTATIONENDINFOPAGE_H
