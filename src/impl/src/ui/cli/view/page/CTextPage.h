#ifndef EIGENVALUECALCULATORSOLVER_CTEXTPAGE_H
#define EIGENVALUECALCULATORSOLVER_CTEXTPAGE_H

#include <string>

class CTextPage {
public:
  [[nodiscard]] virtual
  std::string Render() const = 0;

protected:
};

#endif //EIGENVALUECALCULATORSOLVER_CTEXTPAGE_H
