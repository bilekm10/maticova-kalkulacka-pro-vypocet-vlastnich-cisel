#ifndef EIGENVALUECALCULATORSOLVER_CCOLUMNMATRIXCONSTTOCOMPRESSEDSPARSEROWMATRIXCONVERTER_H
#define EIGENVALUECALCULATORSOLVER_CCOLUMNMATRIXCONSTTOCOMPRESSEDSPARSEROWMATRIXCONVERTER_H

/// A class converting ColumnMatrixConst to CCompressedSparseRowMatrix
class CColumnMatrixConstToCompressedSparseRowMatrixConverter {
public:
  CCompressedSparseRowMatrix Convert(const CColumnMatrixConst &denseMatrix);

protected:
};

inline CCompressedSparseRowMatrix
CColumnMatrixConstToCompressedSparseRowMatrixConverter::Convert(const CColumnMatrixConst &denseMatrix) {
  std::vector<double> data;
  std::vector<size_t> columns;
  std::vector<size_t> rowStarts;
  for (size_t row = 0; row < denseMatrix.GetRowsCnt(); ++row) {
    rowStarts.push_back(data.size());
    for (size_t column = 0; column < denseMatrix.GetColumnsCnt(); ++column) {
      double element = denseMatrix.At(row, column);
      if (element != 0) {
        data.push_back(element);
        columns.push_back(column);
      }
    }
  }
  // number of nonZeros, NNZ
  rowStarts.push_back(data.size());
  return CCompressedSparseRowMatrix{move(data), move(columns), move(rowStarts)};
}

#endif //EIGENVALUECALCULATORSOLVER_CCOLUMNMATRIXCONSTTOCOMPRESSEDSPARSEROWMATRIXCONVERTER_H
