#ifndef EIGENVALUECALCULATORSOLVER_EIG_H
#define EIGENVALUECALCULATORSOLVER_EIG_H

#include "../../solver/operator/matrixvector/CMatrixVectorMultiplication.h"
#include "../print/printFunctions.h"
#include "../../solver/eigenlibhelpers/eigenlibmap.h"
#include "../../solver/sorting/ESortingCriterion.h"
#include "../../solver/model/vector/column/CColumnVectorHolding.h"

#include <Eigen/Eigenvalues>
#include <Eigen/Core>
#include <Spectra/GenEigsBase.h>
#include <Spectra/GenEigsSolver.h>
#include <vector>
#include <complex>
#include <memory>
#include <string>

/*Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> mapToEigenMatrix(const CColumnMatrixConst & columnMatrix)
{
  Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> eigenMatrix(columnMatrix.GetRowsCnt(), columnMatrix.GetColumnsCnt());
  for (size_t i = 0; i < columnMatrix.GetColumnsCnt(); ++i)
    eigenMatrix.col(i) = Eigen::VectorXd::Map(columnMatrix.GetColumnData(i).data(), columnMatrix.GetRowsCnt());
  return eigenMatrix;
}*/

/*void fromEigenMatrix(CColumnSquareMatrix & columnMatrix, const Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> & eigenMatrix)
{
  for (size_t i = 0; i < eigenMatrix.cols(); ++i)
  {
    columnMatrix.GetColumnDataRef(i).assign(eigenMatrix.col(i).data(), eigenMatrix.col(i).data()+eigenMatrix.rows());
  }
}*/

inline Spectra::SortRule toSpectraSortRule(ESortingCriterion sortingCriterion) {
  switch (sortingCriterion) {
    case ESortingCriterion::LargestMagnitude:
      return Spectra::SortRule::LargestMagn;
    case ESortingCriterion::LargestRealPart:
      return Spectra::SortRule::LargestReal;
    case ESortingCriterion::SmallestMagnitude:
      return Spectra::SortRule::SmallestMagn;
    case ESortingCriterion::SmallestRealPart:
      return Spectra::SortRule::SmallestReal;
    default:
      throw std::logic_error("toVerboseString(ESortingCriterion): "
                             "Conversion to string not specified "
                             "for this ESortingCriterion");
  }
}

inline void sortEigenvalues(std::vector<std::complex<double>> &eigenvalues,
                            Spectra::SortRule selection = Spectra::SortRule::LargestMagn) {
  std::vector<Eigen::Index> indices;
  // copy from Spectra
  switch (selection) {
    case Spectra::SortRule::LargestMagn: {
      Spectra::SortEigenvalue<std::complex<double>, Spectra::SortRule::LargestMagn> sorting(eigenvalues.data(),
                                                                                            eigenvalues.size());
      sorting.swap(indices);
      break;
    }
    case Spectra::SortRule::LargestReal: {
      Spectra::SortEigenvalue<std::complex<double>, Spectra::SortRule::LargestReal> sorting(eigenvalues.data(),
                                                                                            eigenvalues.size());
      sorting.swap(indices);
      break;
    }
    case Spectra::SortRule::LargestImag: {
      Spectra::SortEigenvalue<std::complex<double>, Spectra::SortRule::LargestImag> sorting(eigenvalues.data(),
                                                                                            eigenvalues.size());
      sorting.swap(indices);
      break;
    }
    case Spectra::SortRule::SmallestMagn: {
      Spectra::SortEigenvalue<std::complex<double>, Spectra::SortRule::SmallestMagn> sorting(eigenvalues.data(),
                                                                                             eigenvalues.size());
      sorting.swap(indices);
      break;
    }
    case Spectra::SortRule::SmallestReal: {
      Spectra::SortEigenvalue<std::complex<double>, Spectra::SortRule::SmallestReal> sorting(eigenvalues.data(),
                                                                                             eigenvalues.size());
      sorting.swap(indices);
      break;
    }
    case Spectra::SortRule::SmallestImag: {
      Spectra::SortEigenvalue<std::complex<double>, Spectra::SortRule::SmallestImag> sorting(eigenvalues.data(),
                                                                                             eigenvalues.size());
      sorting.swap(indices);
      break;
    }
    default:
      throw std::invalid_argument("Spectra eig: unsupported selection rule");
  }
  std::vector<std::complex<double>> newEigenvalues;
  for (auto i : indices) {
    newEigenvalues.push_back(eigenvalues[i]);
  }
  // own refinement to have first positive imaginary part
  eigenvalues.swap(newEigenvalues);
  for (size_t i = 0; i < eigenvalues.size() - 1; ++i) {
    if (eigenvalues[i].imag() < 0
        && eigenvalues[i] == std::conj(eigenvalues[i + 1])) {
      // to have first positive imaginary part
      std::swap(eigenvalues[i], eigenvalues[i + 1]);
      ++i;
    }
  }
}

inline CColumnVectorHolding<std::complex<double>> computeEigenvalues(
        CColumnMatrix &matrix,
        ESortingCriterion sortingCriterion) {
  Spectra::SortRule spectraSortRule = toSpectraSortRule(sortingCriterion);
  Eigen::MatrixXd eigenMatrix(mapToEigenMatrix(matrix));
  Eigen::EigenSolver<Eigen::MatrixXd> es(eigenMatrix);
  es.setMaxIterations(10000000);
  es.compute(eigenMatrix);
  const auto &eigenEigenvalues = es.eigenvalues();
  std::vector<std::complex<double>> eigenvalues(matrix.GetRowsCnt());
  for (size_t i = 0; i < matrix.GetRowsCnt(); ++i) {
    eigenvalues[i] = eigenEigenvalues.coeff(i);
  }
  sortEigenvalues(eigenvalues, spectraSortRule);
  return CColumnVectorHolding<std::complex<double>>{eigenvalues.data(), eigenvalues.size()};
}

inline void printEigenvalues(CColumnMatrix &matrix,
                             ESortingCriterion sortingCriterion = ESortingCriterion::LargestMagnitude,
                             const char *name = "",
                             const int manipWidth = 10,
                             const int manipPrecision = 17,
                             double almostZero = 1e-15,
                             bool printSize = true,
                             const char *separator = "\n",
                             std::ostream &os = std::cout) {
  const auto &eigenvalues = computeEigenvalues(matrix, sortingCriterion);
  printVector(eigenvalues, separator, name, manipWidth, manipPrecision, almostZero, printSize, os);
}

inline void printEigenvaluesFileTemplate(CColumnMatrix &matrix,
                                         ESortingCriterion sortingCriterion = ESortingCriterion::LargestMagnitude,
                                         std::ostream &os = std::cout) {
  const auto &eigenvalues = computeEigenvalues(matrix, sortingCriterion);
  printVector(eigenvalues,
              "\n",
              "",
              10,
              20,
              -1,
              false,
              os);
}

#endif //EIGENVALUECALCULATORSOLVER_EIG_H
