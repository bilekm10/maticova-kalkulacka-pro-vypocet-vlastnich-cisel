#include "./CMaximalNumberOfIterationsOptionHandler.h"

CMaximalNumberOfIterationsOptionHandler::CMaximalNumberOfIterationsOptionHandler(
        std::optional<size_t> &maximalNumberOfIterations)
        : m_MaximalNumberOfIterations(maximalNumberOfIterations),
          m_Number() {
}

// argumentsCnt must be >= 1
void CMaximalNumberOfIterationsOptionHandler::Handle(int &argumentsCnt, char **&argumentsVector) {
  if (ScanNumberArgumentBothNames<
          size_t,
          NumberFormat,
          OptionFullName,
          OptionShortName>(argumentsCnt,
                           argumentsVector,
                           m_Number)) {
    if (m_MaximalNumberOfIterations)
      ThrowOptionDuplicationError(OptionVerboseName);
    if (m_Number <= 0)
      throw std::invalid_argument(std::string(OptionVerboseName) +
                                  " must be an integer >= 1");
    m_MaximalNumberOfIterations = m_Number;
  } else
    return MoveToNextOptionHandler(argumentsCnt, argumentsVector);
}
