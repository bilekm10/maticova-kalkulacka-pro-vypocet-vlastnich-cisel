#ifndef EIGENVALUECALCULATORSOLVER_CCOLUMNMATRIXCONST_H
#define EIGENVALUECALCULATORSOLVER_CCOLUMNMATRIXCONST_H

#include "../CDenseMatrixConst.h"
#include "../../../view/matrix/CDense2DBlockView.h"
#include "../../../vector/column/CColumnVector.h"
#include "../row/CRowMatrixConst.h"

/// Providing const methods for a column-major matrix
class CColumnMatrixConst : public CDenseMatrixConst<true> {
  using Scalar = double;
protected:
  using CDenseMatrixConst<true>::AtRef;
  using CDenseMatrixConst<true>::ToIdentity;
  using CDenseMatrixConst<true>::Assign;
public:
  using CDenseMatrixConst<true>::At;
  using CDenseMatrixConst<true>::GetRowsCnt;
  using CDenseMatrixConst<true>::GetColumnsCnt;
  using CDenseMatrixConst<true>::IsSquare;

  CColumnMatrixConst(CDense2DBlockView &&blockView);

  CColumnMatrixConst(
          Scalar *data,
          size_t rowsCnt,
          size_t columnsCnt,
          size_t nextColumnShift);

  CColumnMatrixConst(CColumnMatrixConst &&other) noexcept;

  CColumnMatrixConst &operator=(CColumnMatrixConst &&other) noexcept;

  [[nodiscard]]
  size_t GetMaximalRowsCnt() const {
    return m_BlockView.GetMaximalContinuousSize();
  }

  [[nodiscard]]
  CColumnVectorConst<Scalar> GetColumnConstRefView(
          size_t column,
          size_t firstElementRow,
          size_t lastElementRow) const {
    return CColumnVectorConst<Scalar>(m_BlockView.GetContinuousConstRefView(
            column,
            firstElementRow,
            lastElementRow));
  }

  [[nodiscard]]
  CColumnVectorConst<Scalar> GetColumnConstRefView(
          size_t column) const {
    return CColumnVectorConst<Scalar>(m_BlockView.GetContinuousConstRefView(
            column));
  }

  [[nodiscard]]
  CColumnMatrixConst GetSubMatrixConstRefView(
          size_t firstRow,
          size_t lastRow,
          size_t firstColumn,
          size_t lastColumn) const {
    return CColumnMatrixConst{m_BlockView.GetSubMatrixConstRefView(
            firstRow,
            lastRow,
            firstColumn,
            lastColumn)};
  }

  [[nodiscard]]
  CRowMatrixConst Transposed() const {
    return CRowMatrixConst{CDense2DBlockView{m_BlockView}};
  }

  virtual
  ~CColumnMatrixConst();


  void MultiplyLeftColumns(const CColumnVectorConst<double> &columnVector, CColumnVector<double> &result) const;

  void Multiply(const CColumnVectorConst<double> &columnVector, CColumnVector<double> &result) const;

protected:
};

inline
CColumnMatrixConst::CColumnMatrixConst(CDense2DBlockView &&blockView)
        : CDenseMatrixConst<true>(std::move(blockView)) {
}

inline
CColumnMatrixConst::CColumnMatrixConst(
        Scalar *data,
        size_t rowsCnt,
        size_t columnsCnt,
        size_t nextColumnShift)
        : CDenseMatrixConst<true>(
        data,
// continuousSize
        rowsCnt,
// discontinuousSize
        columnsCnt,
// discontinuousShift
        nextColumnShift) {
}

inline
CColumnMatrixConst::CColumnMatrixConst(CColumnMatrixConst &&other) noexcept
        : CDenseMatrixConst<true>(std::move(other.m_BlockView)) {
}

inline CColumnMatrixConst &
CColumnMatrixConst::operator=(CColumnMatrixConst &&other) noexcept {
  m_BlockView = std::move(other.m_BlockView);
  return *this;
}

inline
CColumnMatrixConst::~CColumnMatrixConst() = default;

inline void
CColumnMatrixConst::MultiplyLeftColumns(const CColumnVectorConst<double> &columnVector,
                                        CColumnVector<double> &result) const {
  if (GetColumnsCnt() < columnVector.GetSize())
    throw std::invalid_argument("CColumnMatrixConst->MultiplyLeftColumns(): "
                                "Matrix columns count < vector size, i.e. rows count,"
                                "so cannot multiply the matrix by the vector from the right");
  // allow <= ? and throw only when > ?
  if (GetRowsCnt() != result.GetSize())
    throw std::invalid_argument("CColumnMatrixConst->Multiply(): "
                                "Matrix rows count != result vector size, i.e. rows count,"
                                "so the result of multiplication cannot be stored in the result vector");
//    bool onceAssigned = false;
//    for(size_t j = 0; j < columnVector.GetSize(); ++j)
//    {
//      const double columnVectorCoefficient = columnVector.At(j);
//      if(columnVectorCoefficient == 0)
//        continue;
//      const auto & matrixColumn = GetColumnConstRefView(j, 0, GetRowsCnt() - 1);
//      if(!onceAssigned)
//      {
//        result.AssignScalarMultiple(columnVectorCoefficient, matrixColumn);
//        onceAssigned = true;
//        continue;
//      }
//      result.AddScalarMultiple(columnVectorCoefficient, matrixColumn);
//    }
//    if(!onceAssigned)
//      result.SetToZero();
  const size_t nextColumnShift = m_BlockView.GetMaximalContinuousSize();
  double *resultElement = &result.AtRef(0);
  double *const resultEnd = &result.AtRef(result.GetSize());
  const double *columnVectorElement;
  const double *const columnVectorBegin = columnVector.AtConstPtr(0);
  const double *const columnVectorEnd = columnVector.AtConstPtr(columnVector.GetSize());
  double currSum;
  const double *mRow = m_BlockView.GetData();
  const double *mRowElement;
  for (; resultElement < resultEnd; ++resultElement, ++mRow) {
    currSum = 0;
    mRowElement = mRow;
    for (columnVectorElement = columnVectorBegin;
         columnVectorElement < columnVectorEnd;
         ++columnVectorElement,
                 mRowElement += nextColumnShift) {
      //currSum += At(i, j) * columnVector.At(j);
      currSum += *mRowElement * *columnVectorElement;
    }
    *resultElement = currSum;
  }
}

inline void
CColumnMatrixConst::Multiply(const CColumnVectorConst<double> &columnVector, CColumnVector<double> &result) const {
  if (GetColumnsCnt() != columnVector.GetSize())
    throw std::invalid_argument("CColumnMatrixConst->Multiply(): "
                                "Matrix columns count != vector size, i.e. rows count,"
                                "so cannot multiply the matrix by the vector from the right");
  return MultiplyLeftColumns(columnVector, result);
}

#endif //EIGENVALUECALCULATORSOLVER_CCOLUMNMATRIXCONST_H
