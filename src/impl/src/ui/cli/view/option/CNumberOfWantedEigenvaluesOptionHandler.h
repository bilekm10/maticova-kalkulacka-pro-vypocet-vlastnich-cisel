#ifndef EIGENVALUECALCULATORSOLVER_CNUMBEROFWANTEDEIGENVALUESOPTIONHANDLER_H
#define EIGENVALUECALCULATORSOLVER_CNUMBEROFWANTEDEIGENVALUESOPTIONHANDLER_H

#include "./COptionHandler.h"

class CNumberOfWantedEigenvaluesOptionHandler : public COptionHandler {
public:
  explicit CNumberOfWantedEigenvaluesOptionHandler(std::optional<size_t> &numberOfWantedEigenvalues);

  // argumentsCnt must be >= 1
  void Handle(int &argumentsCnt, char **&argumentsVector) override;

  static constexpr const char OptionShortName[] = "-k";
  static constexpr const char OptionFullName[] = "--number-of-wanted-eigenvalues";
  static constexpr const char OptionVerboseName[] = "Number of wanted eigenvalues";
  static constexpr const char NumberFormat[] = "%lu";
protected:
  std::optional<size_t> &m_NumberOfWantedEigenvalues;
  size_t m_Number;
};

#endif //EIGENVALUECALCULATORSOLVER_CNUMBEROFWANTEDEIGENVALUESOPTIONHANDLER_H
