#include "./CSortingCriterionOptionHandler.h"

CSortingCriterionOptionHandler::CSortingCriterionOptionHandler(std::optional<ESortingCriterion> &sortingCriterion)
        : m_SortingCriterion(sortingCriterion),
          m_Text() {
}

// argumentsCnt must be >= 1
void CSortingCriterionOptionHandler::Handle(int &argumentsCnt, char **&argumentsVector) {
  if (ScanTextArgumentBothNames<
          OptionFullName,
          OptionShortName>(argumentsCnt,
                           argumentsVector,
                           m_Text)) {
    if (m_SortingCriterion)
      ThrowOptionDuplicationError(OptionVerboseName);
    auto it = FullKeywordToSortingCriterion.find(m_Text);
    if (it != FullKeywordToSortingCriterion.end())
      m_SortingCriterion = it->second;
    else
      throw std::invalid_argument("Unsupported " +
                                  std::string(OptionVerboseName) +
                                  " |" + m_Text + "|");
  } else
    return MoveToNextOptionHandler(argumentsCnt, argumentsVector);
}
