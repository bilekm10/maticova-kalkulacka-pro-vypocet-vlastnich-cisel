cmake_minimum_required(VERSION 3.19)
project(fewEigenvaluesCalculatorGui)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_FLAGS_RELEASE "-Ofast")
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

add_executable(
        ${PROJECT_NAME}
        main.cpp
        view/CRestartedArnoldiDialogView.h
        view/page/CEigenvaluesPage.h
        view/page/CQStringPage.h
        controller/CRestartedArnoldiController.h
        controller/CComputationSignals.h
        view/dialogpart/CComputedEigenvaluesLayout.h
        view/dialogpart/CCalculationExecutionLayout.h
        exporter/CErrorMessageTextExporter.h
        view/dialogpart/CCalculationInputParametersLayout.h
        view/observer/CConvergedEigenvaluesPageSenderObserver.h
        view/observer/CComputationStateSenderObserver.h
        view/observer/CNumberOfIterationsSenderObserver.h
        exporter/CErrorMessageTextExporter.cpp
        controller/CRestartedArnoldiController.cpp
        view/dialogpart/CCalculationExecutionLayout.cpp
        view/dialogpart/CCalculationInputParametersLayout.cpp
        view/dialogpart/CComputedEigenvaluesLayout.cpp
        view/page/CComputationStatePage.h
        view/observer/CComputationStateSenderObserver.cpp
        view/observer/CConvergedEigenvaluesPageSenderObserver.cpp
        view/observer/CNumberOfIterationsSenderObserver.cpp
        view/page/CComputationStatePage.cpp
        view/page/CEigenvaluesPage.cpp
        view/CRestartedArnoldiDialogView.cpp)

target_include_directories(${PROJECT_NAME} PRIVATE .)

target_include_directories(${PROJECT_NAME} PRIVATE ../../../src/solver)

target_include_directories(${PROJECT_NAME} PRIVATE ../uihelpers)

# only a warning, not needed if no error occures
if (NOT CMAKE_PREFIX_PATH)
    message(WARNING "CMAKE_PREFIX_PATH is not defined, you may need to set it "
            "(-DCMAKE_PREFIX_PATH=\"path/to/Qt/lib/cmake\" or -DCMAKE_PREFIX_PATH=/usr/include/{host}/qt{version}/ on Ubuntu)")
endif ()

#################################################
### require EIGEN_INCLUDE_DIR
#set(EIGEN_INCLUDE_DIR "/home/eigen-3.3.9")
if (NOT EIGEN_INCLUDE_DIR)
    set(EIGEN_INCLUDE_DIR $ENV{EIGEN_INCLUDE_DIR})
endif ()
if (NOT EIGEN_INCLUDE_DIR)
    message(FATAL_ERROR "EIGEN_INCLUDE_DIR variable is not set yet it needs to be set and exported to specify the absolute path to Eigen library e.g. export EIGEN_INCLUDE_DIR=/home/eigen-3.3.9")
endif ()
if (NOT EXISTS ${EIGEN_INCLUDE_DIR})
    message(FATAL_ERROR "The set Eigen directory EIGEN_INCLUDE_DIR is not valid. It needs to be set and exported to specify the absolute path to Eigen library e.g. export EIGEN_INCLUDE_DIR=/home/eigen-3.3.9")
endif ()
target_include_directories(${PROJECT_NAME} PRIVATE ${EIGEN_INCLUDE_DIR})

#################################################
### require SPECTRA_INCLUDE_DIR
#set(SPECTRA_INCLUDE_DIR "/home/spectra-1.0.0/include")
if (NOT SPECTRA_INCLUDE_DIR)
    set(SPECTRA_INCLUDE_DIR $ENV{SPECTRA_INCLUDE_DIR})
endif ()
if (NOT SPECTRA_INCLUDE_DIR)
    message(FATAL_ERROR "SPECTRA_INCLUDE_DIR variable is not set yet it needs to be set and exported to specify the absolute path to Spectra library e.g. export SPECTRA_INCLUDE_DIR=/home/spectra-1.0.0/include")
endif ()
if (NOT EXISTS ${SPECTRA_INCLUDE_DIR})
    message(FATAL_ERROR "The set Spectra directory SPECTRA_INCLUDE_DIR is not valid. It needs to be set and exported to specify the absolute path to Spectra library e.g. export SPECTRA_INCLUDE_DIR=/home/spectra-1.0.0/include")
endif ()
target_include_directories(${PROJECT_NAME} PRIVATE ${SPECTRA_INCLUDE_DIR})

#################################################
### require Qt5
set(QT_VERSION 5)
set(REQUIRED_LIBS Core Gui Widgets)
set(REQUIRED_LIBS_QUALIFIED Qt5::Core Qt5::Gui Qt5::Widgets)
find_package(Qt${QT_VERSION} COMPONENTS ${REQUIRED_LIBS} REQUIRED)
target_link_libraries(${PROJECT_NAME} ${REQUIRED_LIBS_QUALIFIED})
