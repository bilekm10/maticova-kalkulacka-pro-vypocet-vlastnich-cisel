#include "./CInputMatrixFilenameOptionHandler.h"

CInputMatrixFilenameOptionHandler::CInputMatrixFilenameOptionHandler(std::optional<std::string> &inputMatrixFilename)
        : m_InputMatrixFilename(inputMatrixFilename),
          m_Text() {
}

// argumentsCnt must be >= 1
void CInputMatrixFilenameOptionHandler::Handle(int &argumentsCnt, char **&argumentsVector) {
  if (ScanTextArgumentBothNames<
          OptionFullName,
          OptionShortName>(argumentsCnt,
                           argumentsVector,
                           m_Text)) {
    if (m_InputMatrixFilename)
      ThrowOptionDuplicationError(OptionVerboseName);
    m_InputMatrixFilename = m_Text;
  } else
    return MoveToNextOptionHandler(argumentsCnt, argumentsVector);
}
