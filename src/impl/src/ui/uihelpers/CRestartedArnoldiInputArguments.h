#ifndef EIGENVALUECALCULATORSOLVER_CRESTARTEDARNOLDIINPUTARGUMENTS_H
#define EIGENVALUECALCULATORSOLVER_CRESTARTEDARNOLDIINPUTARGUMENTS_H

#include "../../solver/arnoldi/restarted/CImplicitlyRestartedArnoldi.h"
#include "../../solver/orthogonalization/refined/CIterativeOrthogonalization.h"
#include "../../solver/orthogonalization/basic/CClassicalGramSchmidtOrthogonalization.h"
#include "../../solver/operator/matrixvector/CMatrixVectorMultiplication.h"

#include <string>
#include <optional>
#include <stdexcept>
#include <fstream>

class CRestartedArnoldiInputArguments {
public:
  void Validate();

  // m_InputMatrixFilename and m_NumberOfWantedEigenvalues cannot be empty
  void SetEmptyToDefault();

  std::optional<std::string> m_InputMatrixFilename;
  std::optional<size_t> m_NumberOfWantedEigenvalues;
  std::optional<size_t> m_FactorizationSizeToRestartAt;
  std::optional<ESortingCriterion> m_SortingCriterion;
  std::optional<double> m_Precision;
  std::optional<size_t> m_MaximalNumberOfIterations;
  std::optional<uint8_t> m_MaximalNumberOfOrthogonalizationIterations;
  std::optional<double> m_IterativeOrthogonalizationEta;

  std::string m_InputMatrixFilenameArgumentName = "input matrix filename";
  std::string m_NumberOfWantedEigenvaluesArgumentName = "number of wanted eigenvalues";
  std::string m_FactorizationSizeToRestartAtArgumentName = "factorization size to restart";
  std::string m_SortingCriterionArgumentName = "sorting criterion";
  std::string m_PrecisionArgumentName = "precision";
  std::string m_MaximalNumberOfIterationsArgumentName = "maximal number of iterations";
  std::string m_MaximalNumberOfOrthogonalizationIterationsArgumentName =
          "maximal number of iterative orthogonalization iterations";
  std::string m_IterativeOrthogonalizationEtaArgumentName =
          "iterative orthogonalization eta";

  static std::string FirstLetterToUpperCase(const std::string &text);

  static constexpr double DefaultPrecision() {
    return CImplicitlyRestartedArnoldi<CMatrixVectorMultiplication<CCompressedSparseRowMatrix>, CIterativeOrthogonalization<CClassicalGramSchmidtOrthogonalization>>
    ::DefaultTreatAsZero;
  }

  static constexpr size_t DefaultMaximalNumberOfIterations() {
    return CImplicitlyRestartedArnoldi<CMatrixVectorMultiplication<CCompressedSparseRowMatrix>, CIterativeOrthogonalization<CClassicalGramSchmidtOrthogonalization>>
    ::DefaultMaximalNumberOfIterations;
  }

  static constexpr uint8_t DefaultMaximalNumberOfOrthogonalizationIterations() {
    return CIterativeOrthogonalization<CClassicalGramSchmidtOrthogonalization>
    ::DefaultMaximalNumberOfOrthogonalizationIterations;
  }

  static constexpr double DefaultIterativeOrthogonalizationEta() {
    return CIterativeOrthogonalization<CClassicalGramSchmidtOrthogonalization>
    ::DefaultEta;
  }

protected:
};

inline
void CRestartedArnoldiInputArguments::Validate() {
  if (!m_InputMatrixFilename)
    throw std::invalid_argument(
            FirstLetterToUpperCase(m_InputMatrixFilenameArgumentName) +
            " must be specified");
  if (m_InputMatrixFilename.value().empty())
    throw std::invalid_argument(
            FirstLetterToUpperCase(m_InputMatrixFilenameArgumentName) +
            " cannot be empty");
  if (m_InputMatrixFilename.value().ends_with('/'))
    throw std::invalid_argument(
            FirstLetterToUpperCase(m_InputMatrixFilenameArgumentName) +
            " \"" +
            m_InputMatrixFilename.value() +
            "\" cannot be a directory");
  std::ifstream inputMatrixFilenameIfs(m_InputMatrixFilename.value());
  if (!inputMatrixFilenameIfs)
    throw std::invalid_argument(
            FirstLetterToUpperCase(m_InputMatrixFilenameArgumentName) +
            " \"" +
            m_InputMatrixFilename.value() +
            "\" couldn't be opened for reading");
  if (!m_NumberOfWantedEigenvalues)
    throw std::invalid_argument(
            FirstLetterToUpperCase(m_NumberOfWantedEigenvaluesArgumentName) +
            " must be specified");
  if (m_NumberOfWantedEigenvalues.value() < 1)
    throw std::invalid_argument(
            FirstLetterToUpperCase(m_NumberOfWantedEigenvaluesArgumentName) +
            " must be at least 1");
  if (m_FactorizationSizeToRestartAt
      && m_FactorizationSizeToRestartAt.value() < m_NumberOfWantedEigenvalues.value() + 2)
    throw std::invalid_argument(
            FirstLetterToUpperCase(m_FactorizationSizeToRestartAtArgumentName) +
            " must be at least " +
            m_NumberOfWantedEigenvaluesArgumentName +
            " + 1");
  if (m_Precision
      && (m_Precision.value() < 0 || m_Precision.value() > 1e-2))
    throw std::invalid_argument(
            FirstLetterToUpperCase(m_PrecisionArgumentName) +
            " must be at least 0 and at most 1e-2");
  if (m_MaximalNumberOfIterations
      && m_MaximalNumberOfIterations.value() < 1)
    throw std::invalid_argument(
            FirstLetterToUpperCase(m_MaximalNumberOfIterationsArgumentName) +
            " must be at least 1");
  if (m_MaximalNumberOfOrthogonalizationIterations
      && m_MaximalNumberOfOrthogonalizationIterations.value() < 1)
    throw std::invalid_argument(
            FirstLetterToUpperCase(m_MaximalNumberOfOrthogonalizationIterationsArgumentName) +
            " must be at least 1");
  if (m_IterativeOrthogonalizationEta
      && (m_IterativeOrthogonalizationEta.value() < 0 || m_IterativeOrthogonalizationEta.value() > 1))
    throw std::invalid_argument(
            FirstLetterToUpperCase(m_IterativeOrthogonalizationEtaArgumentName) +
            " must be at least 0 and at most 1");
}

// m_InputMatrixFilename and m_NumberOfWantedEigenvalues cannot be empty
inline
void CRestartedArnoldiInputArguments::SetEmptyToDefault() {
  Validate();
  using Orthogonalization = CIterativeOrthogonalization<CClassicalGramSchmidtOrthogonalization>;
  using RestartedArnoldi = CImplicitlyRestartedArnoldi<CMatrixVectorMultiplication<CCompressedSparseRowMatrix>, Orthogonalization>;
  if (!m_FactorizationSizeToRestartAt)
    m_FactorizationSizeToRestartAt = RestartedArnoldi::DefaultFactorizationSizeToRestartAt(
            m_NumberOfWantedEigenvalues.value());
  if (!m_SortingCriterion)
    m_SortingCriterion = RestartedArnoldi::DefaultSortingCriterion;
  if (!m_Precision)
    m_Precision = DefaultPrecision();
  if (!m_MaximalNumberOfIterations)
    m_MaximalNumberOfIterations = DefaultMaximalNumberOfIterations();
  if (!m_MaximalNumberOfOrthogonalizationIterations)
    m_MaximalNumberOfOrthogonalizationIterations = DefaultMaximalNumberOfOrthogonalizationIterations();
  if (!m_IterativeOrthogonalizationEta)
    m_IterativeOrthogonalizationEta = DefaultIterativeOrthogonalizationEta();
  Validate();
}

inline
std::string CRestartedArnoldiInputArguments::FirstLetterToUpperCase(const std::string &text) {
  if (text.empty())
    return {};
  return char(std::toupper(text[0])) + text.substr(1);
}

#endif //EIGENVALUECALCULATORSOLVER_CRESTARTEDARNOLDIINPUTARGUMENTS_H
