#ifndef EIGENVALUECALCULATORSOLVER_CCOLUMNVECTOR_H
#define EIGENVALUECALCULATORSOLVER_CCOLUMNVECTOR_H

#include "./CColumnVectorConst.h"

#include <cmath>
#include <complex>

/// Providing methods for a column vector
template<typename Scalar>
class CColumnVector : public CColumnVectorConst<Scalar> {
public:
  using CColumnVectorConst<Scalar>::At;
  using CColumnVectorConst<Scalar>::GetSize;
  using CColumnVectorConst<Scalar>::AbsoluteNorm;
  using CColumnVectorConst<Scalar>::EuclideanNorm;
  using CColumnVectorConst<Scalar>::MaximumNorm;
  using CColumnVectorConst<Scalar>::Assign;
  using CColumnVectorConst<Scalar>::AtRef;
  using CColumnVectorConst<Scalar>::SetTo;
  using CColumnVectorConst<Scalar>::SetToZero;
  using CColumnVectorConst<Scalar>::GetSubVectorConstRefView;

  CColumnVector(Scalar *data, size_t dataSize);

  explicit CColumnVector(CDense1DBlockView<Scalar> &&view);

  CColumnVector(const CColumnVector<Scalar> &other);

  CColumnVector(CColumnVector<Scalar> &&other) noexcept;

  CColumnVector<Scalar> &operator=(CColumnVector<Scalar> &&other) noexcept;

  [[nodiscard]]
  CColumnVector<Scalar> GetSubVectorRefView(size_t firstElementIndex, size_t lastElementIndex) {
    return this->template GetSubView<CColumnVector>(firstElementIndex, lastElementIndex);
  }


  CColumnVector<Scalar> &operator+=(const CColumnVectorConst<Scalar> &addend) {
    this->template AddAssignment(addend);
    return *this;
  }

  CColumnVector<Scalar> &
  operator-=(const CColumnVectorConst<Scalar> &subtrahend) { // *this is the minuend and the result of the difference
    this->template SubtractAssignment(subtrahend);
    return *this;
  }

  CColumnVector<Scalar> &operator*=(Scalar multiplier) {
    this->MultiplyAssignment(multiplier);
    return *this;
  }

  CColumnVector<Scalar> &operator/=(
          Scalar denominator) { // multiplication is said to be faster, maybe change from scalar division to multiplication
    this->DivideAssignment(denominator);
    return *this;
  }

  void ScalarMultipleAddScalarMultiple(
          const Scalar firstMultiplier,
          const Scalar secondMultiplier,
          const CColumnVectorConst<Scalar> &addend) { // *this is the first addend and the result of the scaled addition
    return CVector<Scalar>::template ScalarMultipleAddScalarMultiple<CColumnVectorConst<Scalar>>(
            firstMultiplier,
            secondMultiplier,
            addend);
  }

  // maybe could try to stabilize this computation, is used a lot for matrix-vector multiplication
  void AddScalarMultiple(
          const Scalar multiplier,
          const CColumnVectorConst<Scalar> &addend) { // *this is the first addend and the result of the scaled addition
    return CVector<Scalar>::template AddScalarMultiple(
            multiplier,
            addend);
  }

  void AssignScalarMultiple(
          const Scalar multiplier,
          const CColumnVectorConst<Scalar> &addend) { // *this is the first addend and the result of the scaled addition
    return CVector<Scalar>::template AssignScalarMultiple(
            multiplier,
            addend);
  }

  virtual
  ~CColumnVector();

protected:
  //using CColumnVectorConst<Scalar>::m_View;
};

template<typename Scalar>
CColumnVector<Scalar>::CColumnVector(Scalar *data, size_t dataSize)
        : CColumnVectorConst<Scalar>(data,
                                     dataSize) {
}

template<typename Scalar>
CColumnVector<Scalar>::CColumnVector(CDense1DBlockView<Scalar> &&view)
        : CColumnVectorConst<Scalar>(std::move(view)) {
}

template<typename Scalar>
CColumnVector<Scalar>::CColumnVector(const CColumnVector<Scalar> &other)
        : CColumnVectorConst<Scalar>(other.m_View) {
}

template<typename Scalar>
CColumnVector<Scalar>::CColumnVector(CColumnVector<Scalar> &&other) noexcept
        : CColumnVectorConst<Scalar>(std::move(other.m_View)) {
}

template<typename Scalar>
CColumnVector<Scalar> &CColumnVector<Scalar>::operator=(CColumnVector<Scalar> &&other) noexcept {
  this->m_View = std::move(other.m_View);
  return *this;
}

template<typename Scalar>
CColumnVector<Scalar>::~CColumnVector() = default;

#endif //EIGENVALUECALCULATORSOLVER_CCOLUMNVECTOR_H
