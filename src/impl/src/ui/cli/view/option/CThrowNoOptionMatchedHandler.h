#ifndef EIGENVALUECALCULATORSOLVER_CTHROWNOOPTIONMATCHEDHANDLER_H
#define EIGENVALUECALCULATORSOLVER_CTHROWNOOPTIONMATCHEDHANDLER_H

#include "./COptionHandler.h"

class CThrowNoOptionMatchedHandler : public COptionHandler {
public:
  void Handle(int &argumentsCnt, char **&argumentsVector) override;

protected:
};

#endif //EIGENVALUECALCULATORSOLVER_CTHROWNOOPTIONMATCHEDHANDLER_H
