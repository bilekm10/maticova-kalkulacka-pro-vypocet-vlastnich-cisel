#include "./CShowHelpOptionHandler.h"

CShowHelpOptionHandler::CShowHelpOptionHandler(CCommandLineController &controller)
        : m_Controller(controller) {
}

// argumentsCnt must be >= 1
void CShowHelpOptionHandler::Handle(int &argumentsCnt, char **&argumentsVector) {
  if (ScanBothNames<
          OptionFullName,
          OptionShortName>(argumentsCnt,
                           argumentsVector)) {
    m_Controller.ShowHelp();
    throw CHelpWasShownException{};
  } else
    return MoveToNextOptionHandler(argumentsCnt, argumentsVector);
}
