#ifndef EIGENVALUECALCULATORSOLVER_CMATRIXMARKETLOADER_H
#define EIGENVALUECALCULATORSOLVER_CMATRIXMARKETLOADER_H

#include <vector>
#include <stdexcept>
#include <istream>
#include <string>
#include <algorithm>
#include <type_traits>
#include <concepts>

// could use https://eigen.tuxfamily.org/dox/unsupported/MarketIO_8h_source.html instead
// https://stackoverflow.com/questions/57625758/how-to-import-matrix-market-files-with-eigen-library-in-c
//enable_if_t, is_floating_point_v, is_integral_v, integral;
// complex concept https://stackoverflow.com/questions/64287476/c20-concept-for-complex-floating-point-types

/// The state of loading a MatrixMarket object
enum class EMatrixMarketLoadingState {
  NothingWasRead,
  HeaderWasRead,
  DimensionsWereRead,
  NumberOfNonZerosWasRead,
  NumberOfNonZerosElementsWereRead
  // EofReached would be more of a error state
};

enum class EMatrixMarketFormat {
  Coordinate,
  Array,
  Unknown
};

enum class EMatrixMarketElementType {
  Real,
  Complex,
  Integer,
  Unknown,
  Pattern
};

enum class EMatrixMarketSymmetryStructure {
  General,
  Symmetric,
  SkewSymmetric,
  Hermitian,
  Unknown
};

enum class EMatrixMarketObjectType {
  Matrix,
  Vector,
  Unknown
};

inline bool caseInsensitiveEquals(const std::string &left, const std::string &right) {
  if (left.size() != right.size())
    return false;
  for (size_t i = 0; i < left.size(); ++i)
    if (tolower(left[i]) != tolower(right[i]))
      return false;
  return true;
}

inline bool caseInsensitiveEquals(const std::string &left, const std::string_view &right) {
  return caseInsensitiveEquals(left, std::string(right));
}

/// Compute sorting indices of a vector
template<typename Index>
class CCoordinateColumnsSorting {
public:
  CCoordinateColumnsSorting(std::vector<Index> &indices,
                            const std::vector<Index> &columns);

  bool operator()(Index i, Index j) {
    return m_Columns[i] < m_Columns[j];
  }

  void Sort();

protected:
  std::vector<Index> &m_Indices;
  const std::vector<Index> &m_Columns;
};

template<typename Index>
CCoordinateColumnsSorting<Index>
::CCoordinateColumnsSorting(std::vector<Index> &indices,
                            const std::vector<Index> &columns)
        : m_Indices(indices),
          m_Columns(columns) {
  if (indices.size() != columns.size())
    throw std::invalid_argument("CCoordinateColumnsSorting: indices.size() != columns.size()");
}

template<typename Index>
void CCoordinateColumnsSorting<Index>::Sort() {
  for (Index i = 0; i < m_Indices.size(); ++i)
    m_Indices[i] = i;
  std::stable_sort(m_Indices.begin(), m_Indices.end(), *this);
}

/// Stable sort already existing sorting indices of a vector
template<typename Index>
class CCoordinateRowsSorting {
public:
  CCoordinateRowsSorting(std::vector<Index> &indices,
                         const std::vector<Index> &rows);

  bool operator()(Index i, Index j) {
    return m_Rows[i] < m_Rows[j];
  }

  void Sort();

protected:
  std::vector<Index> &m_Indices;
  const std::vector<Index> &m_Rows;
};

template<typename Index>
CCoordinateRowsSorting<Index>::CCoordinateRowsSorting(std::vector<Index> &indices,
                                                      const std::vector<Index> &rows)
        : m_Indices(indices),
          m_Rows(rows) {
  if (indices.size() != rows.size())
    throw std::invalid_argument("CCoordinateRowsSorting: indices.size() != rows.size()");
}

template<typename Index>
void CCoordinateRowsSorting<Index>::Sort() {
  std::stable_sort(m_Indices.begin(), m_Indices.end(), *this);
}

// MatrixMarket: https://math.nist.gov/MatrixMarket/
/// Loading a matrix stored in a MatrixMarket format
class CMatrixMarketLoader {
public:
  explicit CMatrixMarketLoader(std::istream &is, bool throwIfNotSquare = true);

  void ReadHeader();

  [[nodiscard]]
  EMatrixMarketFormat GetFormat() const;

  [[nodiscard]]
  EMatrixMarketElementType GetElementType() const;

  [[nodiscard]]
  EMatrixMarketSymmetryStructure GetSymmetryStructure() const;

  void ReadDimensions();

  [[nodiscard]]
  size_t GetRowsCnt();

  [[nodiscard]]
  size_t GetColumnsCnt();

  void ReadNumberOfNonZeros();

  [[nodiscard]]
  size_t GetNumberOfNonZeros();

  // relying on the caller to call proper Scalar for ElementType
  template<std::floating_point Scalar, typename Index/*, enable_if_t<is_floating_point_v<Scalar> && is_integral_v<Index>>*/>
  std::tuple<std::vector<Index>, std::vector<Index>, std::vector<Scalar>> LoadCoordinates();

  template<std::floating_point Scalar, typename Index/*, enable_if_t<is_floating_point_v<Scalar> && is_integral_v<Index>>*/>
  std::tuple<std::vector<Index>, std::vector<Index>, std::vector<Scalar>, std::vector<Index>>
  LoadCoordinatesWithIndicesOfSortingColumnsThenRows();

  static
  std::vector<std::string> ToLowerCaseWords(const std::string &line);

  static constexpr const std::string_view MatrixMarketHeaderNameText = "%%MatrixMarket";
  static constexpr const std::string_view MatrixMarketHeaderMatrixObjectTypeText = "matrix";
  static constexpr const std::string_view MatrixMarketHeaderVectorObjectTypeText = "vector";
  static constexpr const std::string_view MatrixMarketHeaderCoordinateFormatText = "coordinate";
  static constexpr const std::string_view MatrixMarketHeaderArrayFormatText = "array";
  static constexpr const std::string_view MatrixMarketHeaderRealElementTypeText = "real";
  static constexpr const std::string_view MatrixMarketHeaderComplexElementTypeText = "complex";
  static constexpr const std::string_view MatrixMarketHeaderIntegerElementTypeText = "integer";
  static constexpr const std::string_view MatrixMarketHeaderPatternElementTypeText = "pattern";
  static constexpr const std::string_view MatrixMarketHeaderGeneralSymmetryStructureText = "general";
  static constexpr const std::string_view MatrixMarketHeaderSymmetricSymmetryStructureText = "symmetric";
  static constexpr const std::string_view MatrixMarketHeaderSkewSymmetricSymmetryStructureText = "skew-symmetric";
  static constexpr const std::string_view MatrixMarketHeaderHermitianSymmetryStructureText = "hermitian";
  static const constexpr size_t MatrixMarketHeaderWordsCnt = 5;
  static const constexpr size_t MatrixMarketHeaderNameIndex = 0;
  static const constexpr size_t MatrixMarketHeaderObjectTypeIndex = 1;
  static const constexpr size_t MatrixMarketHeaderFormatIndex = 2;
  static const constexpr size_t MatrixMarketHeaderElementTypeIndex = 3;
  static const constexpr size_t MatrixMarketHeaderSymmetryStructureIndex = 4;
  static const constexpr size_t CoordinatesRowsIndex = 0;
  static const constexpr size_t CoordinatesColumnsIndex = 1;
  static const constexpr size_t CoordinatesDataIndex = 2;
  static const constexpr size_t CoordinatesSortingIndicesIndex = 3;
protected:
  std::istream &m_Is;
  bool m_ThrowIfNotSquare;
  EMatrixMarketLoadingState m_MatrixMarketLoadingState;
  EMatrixMarketFormat m_MatrixMarketFormat;
  EMatrixMarketElementType m_MatrixMarketElementType;
  EMatrixMarketSymmetryStructure m_MatrixMarketSymmetryStructure;
  size_t m_RowsCnt;
  size_t m_ColumnsCnt;
  size_t m_NumberOfNonZeros;

  void SkipComments();

  template<typename Scalar, typename Index>
  void DebugPrintSortingIndices(const std::vector<Index> &vectorRows, const std::vector<Index> &vectorColumns,
                                const std::vector<Scalar> &vectorData, const std::vector<Index> &sortingIndices);
};

inline CMatrixMarketLoader::CMatrixMarketLoader(std::istream &is, bool throwIfNotSquare)
        : m_Is(is),
          m_ThrowIfNotSquare(throwIfNotSquare),
          m_MatrixMarketLoadingState(EMatrixMarketLoadingState::NothingWasRead),
          m_MatrixMarketFormat(EMatrixMarketFormat::Unknown),
          m_MatrixMarketElementType(EMatrixMarketElementType::Unknown),
          m_MatrixMarketSymmetryStructure(EMatrixMarketSymmetryStructure::Unknown),
          m_RowsCnt(0),
          m_ColumnsCnt(0),
          m_NumberOfNonZeros(0) {
}

inline void
CMatrixMarketLoader::ReadHeader() {
  if (m_MatrixMarketLoadingState >= EMatrixMarketLoadingState::HeaderWasRead)
    throw std::invalid_argument("CMatrixMarketLoader->ReadHeader(): "
                                "Header have been already read");
  std::string line;
  if (!(m_Is >> std::ws))
    throw std::invalid_argument("CMatrixMarketLoader: "
                                "Invalid std::istream, couldn't read nothing");
  std::getline(m_Is, line);
  auto headerWords = ToLowerCaseWords(line);
  if (headerWords.size() != MatrixMarketHeaderWordsCnt)
    throw std::invalid_argument("CMatrixMarketLoader: "
                                "The stream didn't contain the header with correct "
                                "count of words, that is " + std::to_string(MatrixMarketHeaderWordsCnt));
  if (!caseInsensitiveEquals(headerWords[MatrixMarketHeaderNameIndex], MatrixMarketHeaderNameText))
    throw std::invalid_argument("CMatrixMarketLoader: "
                                "The stream didn't contain \"" +
                                std::string(MatrixMarketHeaderNameText) +
                                "\" in the header on the first position of "
                                "the first line");
  if (headerWords[MatrixMarketHeaderObjectTypeIndex] != MatrixMarketHeaderMatrixObjectTypeText)
    throw std::invalid_argument("CMatrixMarketLoader: "
                                "The stream didn't contain \"" +
                                std::string(MatrixMarketHeaderMatrixObjectTypeText) +
                                "\" in the header on the second position of "
                                "the first line");
  if (headerWords[MatrixMarketHeaderFormatIndex] == MatrixMarketHeaderCoordinateFormatText)
    m_MatrixMarketFormat = EMatrixMarketFormat::Coordinate;
  else if (headerWords[MatrixMarketHeaderFormatIndex] == MatrixMarketHeaderArrayFormatText)
    m_MatrixMarketFormat = EMatrixMarketFormat::Array;
  else
    throw std::invalid_argument("CMatrixMarketLoader: "
                                "The stream didn't contain known matrix view format "
                                "in the header on the third position of "
                                "the first line");
  if (headerWords[MatrixMarketHeaderElementTypeIndex] == MatrixMarketHeaderRealElementTypeText)
    m_MatrixMarketElementType = EMatrixMarketElementType::Real;
  else if (headerWords[MatrixMarketHeaderElementTypeIndex] == MatrixMarketHeaderComplexElementTypeText)
    m_MatrixMarketElementType = EMatrixMarketElementType::Complex;
  else if (headerWords[MatrixMarketHeaderElementTypeIndex] == MatrixMarketHeaderIntegerElementTypeText)
    m_MatrixMarketElementType = EMatrixMarketElementType::Integer;
  else if (headerWords[MatrixMarketHeaderElementTypeIndex] == MatrixMarketHeaderPatternElementTypeText)
    m_MatrixMarketElementType = EMatrixMarketElementType::Pattern;
  else
    throw std::invalid_argument("CMatrixMarketLoader: "
                                "The stream didn't contain known element type, "
                                "e.g. \"real\", "
                                "in the header on the fourth position of "
                                "the first line");
  if (headerWords[MatrixMarketHeaderSymmetryStructureIndex] == MatrixMarketHeaderGeneralSymmetryStructureText)
    m_MatrixMarketSymmetryStructure = EMatrixMarketSymmetryStructure::General;
  else if (headerWords[MatrixMarketHeaderSymmetryStructureIndex] == MatrixMarketHeaderSymmetricSymmetryStructureText)
    m_MatrixMarketSymmetryStructure = EMatrixMarketSymmetryStructure::Symmetric;
  else if (headerWords[MatrixMarketHeaderSymmetryStructureIndex] ==
           MatrixMarketHeaderSkewSymmetricSymmetryStructureText)
    m_MatrixMarketSymmetryStructure = EMatrixMarketSymmetryStructure::SkewSymmetric;
  else if (headerWords[MatrixMarketHeaderSymmetryStructureIndex] == MatrixMarketHeaderHermitianSymmetryStructureText)
    m_MatrixMarketSymmetryStructure = EMatrixMarketSymmetryStructure::Hermitian;
  else
    throw std::invalid_argument("CMatrixMarketLoader: "
                                "The stream didn't contain known symmetry structure, "
                                "e.g. \"general\" or \"symmetric\", "
                                "in the header on the fifth position of "
                                "the first line");
  m_MatrixMarketLoadingState = EMatrixMarketLoadingState::HeaderWasRead;
}

[[nodiscard]]
inline EMatrixMarketFormat
CMatrixMarketLoader::GetFormat() const {
  if (m_MatrixMarketLoadingState < EMatrixMarketLoadingState::HeaderWasRead)
    throw std::invalid_argument("CMatrixMarketLoader->GetFormat(): "
                                "Header was not read yet");
  return m_MatrixMarketFormat;
}

[[nodiscard]]
inline EMatrixMarketElementType
CMatrixMarketLoader::GetElementType() const {
  if (m_MatrixMarketLoadingState < EMatrixMarketLoadingState::HeaderWasRead)
    throw std::invalid_argument("CMatrixMarketLoader->GetElementType(): "
                                "Header was not read yet");
  return m_MatrixMarketElementType;
}

[[nodiscard]]
inline EMatrixMarketSymmetryStructure
CMatrixMarketLoader::GetSymmetryStructure() const {
  if (m_MatrixMarketLoadingState < EMatrixMarketLoadingState::HeaderWasRead) {
    throw std::invalid_argument("CMatrixMarketLoader->GetSymmetryStructure(): "
                                "Header was not read yet");
  }
  return m_MatrixMarketSymmetryStructure;
}

inline void
CMatrixMarketLoader::ReadDimensions() {
  if (m_MatrixMarketLoadingState >= EMatrixMarketLoadingState::DimensionsWereRead)
    throw std::invalid_argument("CMatrixMarketLoader->ReadDimensions(): "
                                "Dimension have been already read");
  if (m_MatrixMarketLoadingState < EMatrixMarketLoadingState::HeaderWasRead)
    ReadHeader();
  SkipComments();
  if (!(m_Is >> m_RowsCnt >> m_ColumnsCnt))
    throw std::invalid_argument("CMatrixMarketLoader->ReadDimensions(): "
                                "Invalid std::istream: couldn't read the dimensions");
  if (m_RowsCnt <= 0 || m_ColumnsCnt <= 0)
    throw std::invalid_argument("CMatrixMarketLoader->ReadDimensions(): "
                                "One of the matrix dimensions is <= 0");
  if (m_ThrowIfNotSquare && m_RowsCnt != m_ColumnsCnt)
    throw std::invalid_argument("CMatrixMarketLoader->ReadDimensions(): "
                                "Matrix is not square");
  m_MatrixMarketLoadingState = EMatrixMarketLoadingState::DimensionsWereRead;
}

[[nodiscard]]
inline size_t
CMatrixMarketLoader::GetRowsCnt() {
  if (m_MatrixMarketLoadingState < EMatrixMarketLoadingState::DimensionsWereRead)
    ReadDimensions();
  return m_RowsCnt;
}

[[nodiscard]]
inline size_t
CMatrixMarketLoader::GetColumnsCnt() {
  if (m_MatrixMarketLoadingState < EMatrixMarketLoadingState::DimensionsWereRead)
    ReadDimensions();
  return m_ColumnsCnt;
}

inline void
CMatrixMarketLoader::ReadNumberOfNonZeros() {
  if (m_MatrixMarketLoadingState >= EMatrixMarketLoadingState::NumberOfNonZerosWasRead)
    throw std::invalid_argument("CMatrixMarketLoader->ReadNumberOfNonZeros(): "
                                "Number of non-zeros have been already read");
  if (m_MatrixMarketLoadingState < EMatrixMarketLoadingState::DimensionsWereRead)
    ReadDimensions();
  if (!(m_Is >> m_NumberOfNonZeros))
    throw std::invalid_argument("CMatrixMarketLoader->ReadNumberOfNonZeros(): "
                                "Invalid std::istream: couldn't read the number of non-zeros");
  if (m_NumberOfNonZeros <= 0)
    throw std::invalid_argument("CMatrixMarketLoader->ReadNumberOfNonZeros(): "
                                "Number of non-zeros is <= 0");
  if (m_NumberOfNonZeros > m_RowsCnt * m_ColumnsCnt)
    throw std::invalid_argument("CMatrixMarketLoader: "
                                "nnz > rowsCnt * columnsCnt");
  m_MatrixMarketLoadingState = EMatrixMarketLoadingState::NumberOfNonZerosWasRead;
}


[[nodiscard]]
inline size_t
CMatrixMarketLoader::GetNumberOfNonZeros() {
  if (m_MatrixMarketLoadingState < EMatrixMarketLoadingState::NumberOfNonZerosWasRead)
    ReadNumberOfNonZeros();
  return m_NumberOfNonZeros;
}

template<typename Scalar, typename Index>
void CMatrixMarketLoader::DebugPrintSortingIndices(const std::vector<Index> &vectorRows,
                                                   const std::vector<Index> &vectorColumns,
                                                   const std::vector<Scalar> &vectorData,
                                                   const std::vector<Index> &sortingIndices) {
  for (size_t i = 0; i < m_NumberOfNonZeros; ++i) {
    size_t oldIndex = sortingIndices[i];
    std::cout << "  " << std::setw(3) << vectorData[oldIndex];
  }
  std::cout << std::endl;
  for (size_t i = 0; i < m_NumberOfNonZeros; ++i) {
    size_t oldIndex = sortingIndices[i];
    std::cout << "  " << std::setw(3) << vectorRows[oldIndex];
  }
  std::cout << std::endl;
  for (size_t i = 0; i < m_NumberOfNonZeros; ++i) {
    size_t oldIndex = sortingIndices[i];
    std::cout << "  " << std::setw(3) << vectorColumns[oldIndex];
  }
}

inline void
CMatrixMarketLoader::SkipComments() {
  if (m_MatrixMarketLoadingState < EMatrixMarketLoadingState::HeaderWasRead)
    throw std::invalid_argument("CMatrixMarketLoader->SkipComments(): "
                                "Header were not read yet");
  if (m_MatrixMarketLoadingState > EMatrixMarketLoadingState::DimensionsWereRead)
    throw std::invalid_argument("CMatrixMarketLoader->SkipComments(): "
                                "Dimensions were already read so the comments were already skipped");
  std::string commentLine;
  while (m_Is >> std::ws && char(m_Is.peek()) == '%')
    std::getline(m_Is, commentLine);
}

// relying on the caller to call proper Scalar for ElementType
template<std::floating_point Scalar, typename Index/*, enable_if_t<is_floating_point_v<Scalar> && is_integral_v<Index>>*/>
std::tuple<std::vector<Index>, std::vector<Index>, std::vector<Scalar>> CMatrixMarketLoader
::LoadCoordinates() {
  /*if(m_MatrixMarketObjectType != EMatrixMarketObjectType::Matrix)
    throw std::invalid_argument("CMatrixMarketLoader->LoadCoordinates(): "
                           "Not a matrix");*/
  if (m_MatrixMarketSymmetryStructure != EMatrixMarketSymmetryStructure::General)
    throw std::invalid_argument("CMatrixMarketLoader->LoadCoordinates(): "
                                "Warning: symmetry structure != general");
  if (m_MatrixMarketFormat != EMatrixMarketFormat::Coordinate)
    throw std::invalid_argument("CMatrixMarketLoader->LoadCoordinates(): "
                                "Storage format != coordinate");
  if (m_MatrixMarketElementType != EMatrixMarketElementType::Real)
    throw std::invalid_argument("CMatrixMarketLoader->LoadCoordinates(): "
                                "element type != real for floating point Scalar");
//    if(m_MatrixMarketLoadingState < EMatrixMarketLoadingState::HeaderWasRead)
//    {
//      ReadHeader();
//    }
  if (m_MatrixMarketLoadingState < EMatrixMarketLoadingState::HeaderWasRead)
    throw std::invalid_argument("CMatrixMarketLoader->LoadCoordinates(): "
                                "Header was not read yet, should read it "
                                "and call this method with correct "
                                "Scalar type or assert the Scalar is what "
                                "was wanted");
  if (m_MatrixMarketLoadingState < EMatrixMarketLoadingState::DimensionsWereRead)
    ReadDimensions();
  if (m_MatrixMarketLoadingState < EMatrixMarketLoadingState::NumberOfNonZerosWasRead)
    ReadNumberOfNonZeros();
  Index row;
  Index column;
  double element;
  // https://stackoverflow.com/questions/51521031/return-stdtuple-and-move-semantics-copy-elision
  std::tuple<std::vector<Index>, std::vector<Index>, std::vector<Scalar>> coordinates;
  auto &[rows, columns, data] = coordinates;
  //printf("CMatrixMarketLoader LoadCoordinates before resize\n");
  rows.resize(m_NumberOfNonZeros);
  //printf("CMatrixMarketLoader LoadCoordinates resize 1 done\n");
  columns.resize(m_NumberOfNonZeros);
  //printf("CMatrixMarketLoader LoadCoordinates resize 2 done\n");
  data.resize(m_NumberOfNonZeros);
  //printf("CMatrixMarketLoader LoadCoordinates resize 3 done\n");
  //printf("CMatrixMarketLoader LoadCoordinates after resize\n");
  for (size_t i = 0; i < m_NumberOfNonZeros; ++i) {
    if (!(m_Is >> row >> column >> element))
      throw std::invalid_argument("CMatrixMarketLoader->LoadCoordinates(): "
                                  "unable to read " + std::to_string(i + 1) + "-th element");
    if (row > m_RowsCnt || row < 1
        || column > m_ColumnsCnt || column < 1)
      throw std::invalid_argument("CMatrixMarketLoader->LoadCoordinates(): " +
                                  std::to_string(i + 1) +
                                  "-th element: "
                                  "row > m_RowsCnt || row < 1 "
                                  "|| column > m_ColumnsCnt || column < 1 or "
                                  "some value out of representable range");
    if (element == 0) {
      --i;
      --m_NumberOfNonZeros;
      continue;
    }
    rows[i] = row - 1;
    columns[i] = column - 1;
    data[i] = element;
  }
  m_MatrixMarketLoadingState = EMatrixMarketLoadingState::NumberOfNonZerosElementsWereRead;
  //printf("CMatrixMarketLoader LoadCoordinates before additional resize\n");
  data.resize(m_NumberOfNonZeros);
  rows.resize(m_NumberOfNonZeros);
  columns.resize(m_NumberOfNonZeros);
  //printf("CMatrixMarketLoader LoadCoordinates after additional resize\n");
  m_Is >> std::ws;
  if (!m_Is.eof())
    throw std::invalid_argument("CMatrixMarketLoader->LoadCoordinates(): "
                                "end of file not reached after reading NNZ elements");
  //printf("CMatrixMarketLoader LoadCoordinates returning\n");
  return coordinates;
}

template<std::floating_point Scalar, typename Index/*, enable_if_t<is_floating_point_v<Scalar> && is_integral_v<Index>>*/>
std::tuple<std::vector<Index>, std::vector<Index>, std::vector<Scalar>, std::vector<Index>> CMatrixMarketLoader
::LoadCoordinatesWithIndicesOfSortingColumnsThenRows() {
  //printf("CMatrixMarketLoader LoadCoordinatesWithIndicesOfSortingColumnsThenRows before LoadCoordinates\n");
  auto[rows, columns, data] = LoadCoordinates<Scalar, Index>();
  //printf("CMatrixMarketLoader LoadCoordinatesWithIndicesOfSortingColumnsThenRows after LoadCoordinates\n");
  std::tuple<std::vector<Index>, std::vector<Index>, std::vector<Scalar>, std::vector<Index>> coordinatesWithSortingIndices;
  auto &[returnRows, returnColumns, returnData, sortingIndices] = coordinatesWithSortingIndices;
  //printf("CMatrixMarketLoader LoadCoordinatesWithIndicesOfSortingColumnsThenRows before move\n");
  returnRows = move(rows);
  //printf("CMatrixMarketLoader LoadCoordinatesWithIndicesOfSortingColumnsThenRows move 1 done\n");
  returnColumns = move(columns);
  //printf("CMatrixMarketLoader LoadCoordinatesWithIndicesOfSortingColumnsThenRows move 2 done\n");
  returnData = move(data);
  //printf("CMatrixMarketLoader LoadCoordinatesWithIndicesOfSortingColumnsThenRows move 3 done\n");
  //printf("CMatrixMarketLoader LoadCoordinatesWithIndicesOfSortingColumnsThenRows after move\n");
  //printf("CMatrixMarketLoader LoadCoordinatesWithIndicesOfSortingColumnsThenRows before resize\n");
  sortingIndices.resize(m_NumberOfNonZeros);
  //printf("CMatrixMarketLoader LoadCoordinatesWithIndicesOfSortingColumnsThenRows after resize\n");
  //printf("CMatrixMarketLoader LoadCoordinatesWithIndicesOfSortingColumnsThenRows start sorting\n");
  CCoordinateColumnsSorting<Index> columnsSorting(sortingIndices, returnColumns);
  columnsSorting.Sort();
//    DebugPrintSortingIndices(std::get<CoordinatesRowsIndex>(coordinates),
//                             std::get<CoordinatesColumnsIndex>(coordinates),
//                             std::get<CoordinatesDataIndex>(coordinates),
//                             sortingIndices);
//    std::cout << std::endl;
  // stable sort
  //printf("CMatrixMarketLoader LoadCoordinatesWithIndicesOfSortingColumnsThenRows columns sorted\n");
  CCoordinateRowsSorting<Index> rowsSorting(sortingIndices, returnRows);
  rowsSorting.Sort();
//    std::std::cout << std::endl;
//    DebugPrintSortingIndices(std::get<CoordinatesRowsIndex>(coordinates),
//                             std::get<CoordinatesColumnsIndex>(coordinates),
//                             std::get<CoordinatesDataIndex>(coordinates),
//                             sortingIndices);
//    std::cout << std::endl;
  //printf("CMatrixMarketLoader LoadCoordinatesWithIndicesOfSortingColumnsThenRows all sorted\n");
  //printf("CMatrixMarketLoader LoadCoordinatesWithIndicesOfSortingColumnsThenRows returning\n");
  return coordinatesWithSortingIndices;
}

inline std::vector<std::string>
CMatrixMarketLoader::ToLowerCaseWords(const std::string &line) {
  std::vector<std::string> words;
  std::istringstream iss(line);
  std::string word;
  iss >> word;
  for (; iss; iss >> word) {
    std::transform(word.begin(), word.end(), word.begin(),
                   [](unsigned char c) { return std::tolower(c); });
    words.emplace_back(word);
  }
  return words;
}

#endif //EIGENVALUECALCULATORSOLVER_CMATRIXMARKETLOADER_H
