#ifndef EIGENVALUECALCULATORSOLVER_CSTREAMTEXTEXPORTER_H
#define EIGENVALUECALCULATORSOLVER_CSTREAMTEXTEXPORTER_H

#include "./CTextExporter.h"

#include <ostream>

/// Export text to a stream
class CStreamTextExporter : public CTextExporter {
public:
  explicit CStreamTextExporter(std::ostream &outputStream);

  void Export(const std::string &item) override;

protected:
  std::ostream &m_OutputStream;
};

inline
CStreamTextExporter::CStreamTextExporter(std::ostream &outputStream)
        : m_OutputStream(outputStream) {
}

inline void
CStreamTextExporter::Export(const std::string &item) {
  m_OutputStream << item;
}

#endif //EIGENVALUECALCULATORSOLVER_CSTREAMTEXTEXPORTER_H
