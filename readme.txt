Stručný popis obsahu adresáře:

+- readme.txt .......... stručný popis obsahu adresáře
+- src
|  +- impl ............. zdrojové kódy implementace
|  |  +- README.md ..... stručný popis projektu, návod na sestavení a použití
|  +- thesis ........... zdrojová forma práce ve formátu LaTeX
+- text ................ text práce
   +- thesis.pdf ....... text práce ve formátu PDF
