#ifndef EIGENVALUECALCULATORSOLVER_CARNOLDIFACTORIZATION_H
#define EIGENVALUECALCULATORSOLVER_CARNOLDIFACTORIZATION_H

#include <stdexcept>
#include <type_traits>

#include "../../model/vector/column/CColumnVector.h"
#include "../../model/matrix/dense/column/CColumnMatrix.h"
#include "../../model/matrix/dense/column/CColumnMatrixDynamicDimensionsHolding.h"
#include "../../operator/matrixvector/CMatrixVectorMultiplication.h"
#include "../../operator/matrixmatrix/CMatrixKthSubdiagonalMatrixMultiplication.h"

//#define DBG_ORTHOGONALITY_CHECK
#ifdef DBG_ORTHOGONALITY_CHECK
#include "../../../debug/check/orthogonality/COrthogonalityChecker.h"
#endif

/// Expanding and compressing Arnoldi factorization A Vm = Vm Hm + fm emT
template<typename InputLargeMatrixVectorMultiplication,
        typename Orthogonalization>
class CArnoldiFactorization {
public:
  // inputLargeMatrix must be square, so inputLargeMatrixRowsCnt == inputLargeMatrixColumnsCnt
  CArnoldiFactorization(InputLargeMatrixVectorMultiplication &inputLargeMatrixVectorMultiplication,
                        size_t inputLargeMatrixRowsCnt,
                        CColumnVector<double> &initialVector,
                        size_t expectedCompressedFactorizationSize,
                        size_t maximalFactorizationSize,
                        Orthogonalization orthogonalization,
                        double treatAsZero,
                        bool returnIfOrthogonalizationFails = false,
                        bool throwIfOrthogonalizationFails = false);

  size_t InitializeArnoldiFactorization(size_t toSize);

  // toSize could be omitted and deduce it from the columns count of orthonormalVectorsMatrix
  size_t ExpandArnoldiFactorization(size_t fromSize,
                                    size_t toSize);

  [[nodiscard]]
  size_t GetFactorizationSize() const {
    return m_OrthonormalVectorsMatrix.GetColumnsCnt();
  }

  [[nodiscard]]
  double GetResidualVectorNorm() const {
    return m_ResidualVectorNorm;
  }

  [[nodiscard]]
  const CColumnMatrixConst &GetHessenbergMatrix() const {
    return m_HessenbergMatrix;
  }

  CColumnMatrixDynamicDimensionsHolding &GetHessenbergMatrixRef() {
    return m_HessenbergMatrix;
  }

  [[nodiscard]]
  const CColumnMatrixConst &GetOrthonormalVectorsMatrix() const {
    return m_OrthonormalVectorsMatrix;
  }

  [[nodiscard]]
  const CColumnVectorConst<double> &GetResidualVector() const {
    return m_ResidualVector;
  }

  void CompressVectorsWithKthSubdiagonalMatrix(
          const CColumnMatrixConst &kthSubdiagonalMatrix,
          size_t kthSubdiagonal);

  static constexpr const double DefaultAlmostZero =
          std::numeric_limits<double>::epsilon() * std::numeric_limits<double>::epsilon();
protected:
  CColumnMatrixDynamicDimensionsHolding m_OrthonormalVectorsMatrix;
  CColumnMatrixDynamicDimensionsHolding m_HessenbergMatrix;
  CColumnVector<double> m_ResidualVector;
  double m_ResidualVectorNorm;

  //size_t m_InputLargeMatrixRowsCnt;
  InputLargeMatrixVectorMultiplication &m_InputLargeMatrixVectorMultiplication;
  Orthogonalization m_Orthogonalization;
  double m_TreatAsZero;
  double m_AlmostZero;
  bool m_ReturnIfOrthogonalizationFails;
  bool m_ThrowIfOrthogonalizationFails;

  CColumnMatrixDynamicDimensionsHolding m_OrthonormalVectorsMatrixAux;
};

// inputLargeMatrix must be square, so inputLargeMatrixRowsCnt == inputLargeMatrixColumnsCnt
template<typename InputLargeMatrixVectorMultiplication,
        typename Orthogonalization>
CArnoldiFactorization<InputLargeMatrixVectorMultiplication, Orthogonalization>
::CArnoldiFactorization(InputLargeMatrixVectorMultiplication &inputLargeMatrixVectorMultiplication,
                        size_t inputLargeMatrixRowsCnt,
                        CColumnVector<double> &initialVector,
                        size_t expectedCompressedFactorizationSize,
                        size_t maximalFactorizationSize,
                        Orthogonalization orthogonalization,
                        double treatAsZero,
                        bool returnIfOrthogonalizationFails,
                        bool throwIfOrthogonalizationFails)
        : m_OrthonormalVectorsMatrix(
        &initialVector.AtRef(0),
        inputLargeMatrixRowsCnt,
        1,
        false,
        maximalFactorizationSize + 1),
          m_HessenbergMatrix(maximalFactorizationSize, true),
          m_ResidualVector(m_OrthonormalVectorsMatrix.GetColumnRefView(0)),
          m_ResidualVectorNorm(0),
        //m_InputLargeMatrixRowsCnt(inputLargeMatrixRowsCnt),
          m_InputLargeMatrixVectorMultiplication(inputLargeMatrixVectorMultiplication),
          m_Orthogonalization(std::move(orthogonalization)),
          m_TreatAsZero(treatAsZero),
          m_AlmostZero(DefaultAlmostZero),
          m_ReturnIfOrthogonalizationFails(returnIfOrthogonalizationFails),
          m_ThrowIfOrthogonalizationFails(throwIfOrthogonalizationFails),
          m_OrthonormalVectorsMatrixAux(inputLargeMatrixRowsCnt, 0, inputLargeMatrixRowsCnt,
                                        expectedCompressedFactorizationSize + 3, false) {
  if (inputLargeMatrixRowsCnt != m_ResidualVector.GetSize())
    throw std::invalid_argument("CArnoldiFactorization constructor: "
                                "inputLargeMatrixRowsCnt != initialVector size");
  m_OrthonormalVectorsMatrix.DecreaseColumnsCnt();
}

template<typename InputLargeMatrixVectorMultiplication,
        typename Orthogonalization>
size_t CArnoldiFactorization<InputLargeMatrixVectorMultiplication, Orthogonalization>
::InitializeArnoldiFactorization(size_t toSize) {
  if (toSize <= 0)
    throw std::invalid_argument("CArnoldiFactorization->InitializeArnoldiFactorization: toSize <= 0 - "
                                "if 0 size factorization is needed, "
                                "calling this method is redundant and nothing is performed");
  if (m_HessenbergMatrix.GetRowsCnt() < toSize)
    throw std::invalid_argument("CArnoldiFactorization->InitializeArnoldiFactorization: "
                                "m_HessenbergMatrix.GetRowsCnt() < toSize, "
                                "so the factorization could not be expanded to the wanted size");
  if (m_OrthonormalVectorsMatrix.GetMaximalColumnsCnt() < toSize + 1)
    throw std::invalid_argument("CArnoldiFactorization->InitializeArnoldiFactorization: "
                                "m_OrthonormalVectorsMatrix.GetMaximalColumnsCnt() < toSize + 1, "
                                "so the factorization could not be expanded to the wanted size");
  if (m_ResidualVector.GetSize() != m_OrthonormalVectorsMatrix.GetRowsCnt())
    throw std::invalid_argument("CArnoldiFactorization->InitializeArnoldiFactorization: "
                                "m_ResidualVector.GetSize() != m_OrthonormalVectorsMatrix.GetRowsCnt(), "
                                "so it would be impossible or inappropriate to store the residual vector "
                                "in the columns of m_OrthonormalVectorsMatrix");
  if (m_OrthonormalVectorsMatrix.GetColumnsCnt() != 0)
    throw std::invalid_argument("CArnoldiFactorization->InitializeArnoldiFactorization: "
                                "m_OrthonormalVectorsMatrix.GetColumnsCnt() != 0, "
                                "so the factorization has been already initialized");
  m_ResidualVectorNorm = m_ResidualVector.EuclideanNorm();
  if (std::abs(m_ResidualVectorNorm) < m_AlmostZero)
    throw std::invalid_argument("CArnoldiFactorization->InitializeArnoldiFactorization: "
                                "std::abs(m_ResidualVectorNorm) < m_AlmostZero");
  m_ResidualVector /= m_ResidualVectorNorm;
  //m_OrthonormalVectorsMatrix.AddColumn(m_ResidualVector);
  m_OrthonormalVectorsMatrix.IncreaseColumnsCnt();
  auto newResidualVector = m_OrthonormalVectorsMatrix.GetEndColumnRefView();
  m_InputLargeMatrixVectorMultiplication.Multiply(m_ResidualVector, newResidualVector);
  std::swap(m_ResidualVector, newResidualVector);
  auto hessenbergColumnRefView = m_HessenbergMatrix.GetColumnToDiagonalRefView(0);
  bool wasOrthogonalizationSuccessful = m_Orthogonalization.Orthogonalize(
          m_OrthonormalVectorsMatrix,
          m_ResidualVector,
          hessenbergColumnRefView,
          m_ResidualVectorNorm);
  if (!wasOrthogonalizationSuccessful) {
    if (m_ReturnIfOrthogonalizationFails)
      return 1;
    if (m_ThrowIfOrthogonalizationFails)
      throw std::invalid_argument("CArnoldiFactorization->ExpandArnoldiFactorization: "
                                  "Orthogonalization failed");
  }
  return ExpandArnoldiFactorization(
          1,
          toSize);
}

// toSize could be omitted and deduce it from the columns count of orthonormalVectorsMatrix
template<typename InputLargeMatrixVectorMultiplication,
        typename Orthogonalization>
size_t CArnoldiFactorization<InputLargeMatrixVectorMultiplication, Orthogonalization>
::ExpandArnoldiFactorization(size_t fromSize,
                             size_t toSize) {
  if (fromSize <= 0) // rather should call the initialization method
    throw std::invalid_argument(
            "CArnoldiFactorization->ExpandArnoldiFactorization: fromSize <= 0 - need to initialize it before");
  if (toSize <= fromSize)
    throw std::invalid_argument("CArnoldiFactorization->ExpandArnoldiFactorization: toSize <= fromSize - "
                                "if no expansion of the factorization is needed, "
                                "calling this method is redundant and nothing is performed");
  // know that m_HessenbergMatrix is a square matrix from its type, CRowSquareMatrix
//    if(m_OrthonormalVectorsMatrix.GetColumnsCnt() != m_HessenbergMatrix.GetRowsCnt())
//      throw std::invalid_argument("CArnoldiFactorization->ExpandArnoldiFactorization: "
//                             "m_OrthonormalVectorsMatrix.GetColumnsCnt() != m_HessenbergMatrix.GetRowsCnt(), "
//                             "so the input Arnoldi factorization is invalid, as cannot multiply "
//                             "m_OrthonormalVectorsMatrix by m_HessenbergMatrix from the right");
  if (m_OrthonormalVectorsMatrix.GetMaximalColumnsCnt() <= toSize)
    throw std::invalid_argument("CArnoldiFactorization->ExpandArnoldiFactorization: "
                                "m_OrthonormalVectorsMatrix.GetMaximalColumnsCnt() <= toSize, "
                                "so the factorization could not be expanded to the wanted size");
  if (m_OrthonormalVectorsMatrix.GetColumnsCnt() !=
      fromSize) // could possibly allow > and decrease columnsCnt to fromSize in the beginning
    throw std::invalid_argument("CArnoldiFactorization->ExpandArnoldiFactorization: "
                                "m_OrthonormalVectorsMatrix.GetColumnsCnt() != fromSize");
  if (m_HessenbergMatrix.GetRowsCnt() < toSize)
    throw std::invalid_argument("CArnoldiFactorization->ExpandArnoldiFactorization: "
                                "m_HessenbergMatrix.GetRowsCnt() < toSize, "
                                "so the factorization could not be expanded to the wanted size");
  if (m_ResidualVector.GetSize() != m_OrthonormalVectorsMatrix.GetRowsCnt())
    throw std::invalid_argument("CArnoldiFactorization->ExpandArnoldiFactorization: "
                                "m_ResidualVector.GetSize() != m_OrthonormalVectorsMatrix.GetRowsCnt(), "
                                "so it would be impossible or inappropriate to store the residual vector "
                                "in the columns of m_OrthonormalVectorsMatrix");
  m_ResidualVectorNorm = m_ResidualVector.EuclideanNorm();
  for (size_t j = fromSize - 1; j < toSize - 1; ++j) {
    if (std::abs(m_ResidualVectorNorm) <
        m_AlmostZero) { // linear dependency of m_ResidualVector on m_OrthonormalVectorsMatrix detected, eigenvalues of j x j subMatrix of m_HessenbergMatrix are exact approximations
      return j + 1;
    }
    m_ResidualVector /= m_ResidualVectorNorm;
    m_HessenbergMatrix.AtRef(j + 1, j) = m_ResidualVectorNorm;
    //m_OrthonormalVectorsMatrix.AddColumn(m_ResidualVector);
    m_OrthonormalVectorsMatrix.IncreaseColumnsCnt();
    auto newResidualVector = m_OrthonormalVectorsMatrix.GetEndColumnRefView();
    m_InputLargeMatrixVectorMultiplication.Multiply(m_ResidualVector, newResidualVector);
    std::swap(m_ResidualVector, newResidualVector);
    auto hessenbergColumnRefView = m_HessenbergMatrix.GetColumnToDiagonalRefView(j + 1);
    bool wasOrthogonalizationSuccessful = m_Orthogonalization.Orthogonalize(
            m_OrthonormalVectorsMatrix,
            m_ResidualVector,
            hessenbergColumnRefView,
            m_ResidualVectorNorm);
    if (!wasOrthogonalizationSuccessful) {
      if (m_ReturnIfOrthogonalizationFails)
        return j + 2;
      if (m_ThrowIfOrthogonalizationFails)
        throw std::invalid_argument("CArnoldiFactorization->ExpandArnoldiFactorization: "
                                    "Orthogonalization failed");
    }
  }
  #ifdef DBG_ORTHOGONALITY_CHECK
  {
      double orthogonalityCheckMultiplier; //150 or std::sqrt(m_ResidualVector.GetSize());
      double checkTreatAsZero = 1e-15;
      if(m_ResidualVector.GetSize() < 100)
        orthogonalityCheckMultiplier = 75;
      else if(m_ResidualVector.GetSize() < 3500)
        orthogonalityCheckMultiplier = 100;
      else if(m_ResidualVector.GetSize() == 5000)
        orthogonalityCheckMultiplier = 2000;
      else if(m_ResidualVector.GetSize() < 10000)
        orthogonalityCheckMultiplier = 201;
      else
        orthogonalityCheckMultiplier = 1000200;
      COrthogonalityChecker orthogonalityChecker(checkTreatAsZero * orthogonalityCheckMultiplier, false, true, false, "ExpandArnoldiFactorizationWithNormalizedResidualVector");
      orthogonalityChecker.CheckOrthogonality(m_OrthonormalVectorsMatrix);
    }
  #endif
  return toSize;
}

template<typename InputLargeMatrixVectorMultiplication,
        typename Orthogonalization>
void CArnoldiFactorization<InputLargeMatrixVectorMultiplication, Orthogonalization>
::CompressVectorsWithKthSubdiagonalMatrix(
        const CColumnMatrixConst &kthSubdiagonalMatrix,
        size_t kthSubdiagonal) {
  const size_t oldSize = m_OrthonormalVectorsMatrix.GetColumnsCnt();
  const size_t newSize = kthSubdiagonalMatrix.GetColumnsCnt() - 1;
  if (m_OrthonormalVectorsMatrixAux.GetMaximalColumnsCnt() < kthSubdiagonalMatrix.GetColumnsCnt())
    // the expected space is not sufficient so need to allocate more space for more columns
    m_OrthonormalVectorsMatrixAux = std::move(CColumnMatrixDynamicDimensionsHolding(
            m_OrthonormalVectorsMatrixAux.GetRowsCnt(),
            kthSubdiagonalMatrix.GetColumnsCnt(),
            m_OrthonormalVectorsMatrixAux.GetRowsCnt(),
            kthSubdiagonalMatrix.GetColumnsCnt() + 3,
            false));
  else
    m_OrthonormalVectorsMatrixAux.ChangeColumnsCnt(kthSubdiagonalMatrix.GetColumnsCnt());
  CMatrixKthSubdiagonalMatrixMultiplication<CColumnMatrix, CColumnMatrix, CColumnMatrix>{
          m_OrthonormalVectorsMatrix}.Multiply(
          kthSubdiagonalMatrix,
          kthSubdiagonal,
          m_OrthonormalVectorsMatrixAux);
  //m_OrthonormalVectorsMatrixAux.DecreaseColumnsCnt();
  m_OrthonormalVectorsMatrix.ChangeColumnsCnt(m_OrthonormalVectorsMatrixAux.GetColumnsCnt());
  m_OrthonormalVectorsMatrix.Assign(m_OrthonormalVectorsMatrixAux);
  // update residualVector
  auto oldResidualVector(m_ResidualVector);
  m_ResidualVector = m_OrthonormalVectorsMatrix.GetColumnRefView(newSize);
  m_ResidualVector.ScalarMultipleAddScalarMultiple(
          m_HessenbergMatrix.At(newSize, newSize - 1),
          kthSubdiagonalMatrix.At(oldSize - 1, newSize - 1),
          oldResidualVector);
  m_OrthonormalVectorsMatrix.ChangeColumnsCnt(newSize);
}

#endif //EIGENVALUECALCULATORSOLVER_CARNOLDIFACTORIZATION_H
