#include "./CThrowNoOptionMatchedHandler.h"

void CThrowNoOptionMatchedHandler::Handle(int &, char **&argumentsVector) {
  throw std::invalid_argument("No option matched for |" + std::string(argumentsVector[0]) + "|");
}
