#ifndef EIGENVALUECALCULATORSOLVER_CCOMMANDLINECONTROLLER_H
#define EIGENVALUECALCULATORSOLVER_CCOMMANDLINECONTROLLER_H

#include <iostream>
#include <vector>
#include <iomanip>
#include <cmath>
#include <complex>
#include <fstream>
#include <cassert>
#include <functional>
#include <algorithm>
#include <memory>
#include <bitset>
#include <optional>
#include <limits>
#include <cfloat>
#include <ctime>
#include <string>
#include <deque>
#include <forward_list>
#include <list>
#include <set>
#include <queue>
#include <map>
#include <stack>
#include <array>
#include <execution>
#include <numbers>
#include <numeric>
#include <filesystem>
#include <regex>
#include <tuple>
#include <type_traits>
#include <typeinfo>
#include <variant>

#include "../../../debug/check/equality/matrix/CAreMatricesSame.h"
#include "../../../debug/print/printFunctions.h"
#include "../../../debug/eig/eig.h"
#include "../../../solver/decomposition/qr/CHessenbergQrQuasiTriangularization.h"
#include "../../../solver/sorting/CVectorSorting.h"
#include "../../../solver/model/vector/row/CRowVector.h"
#include "../../../solver/operator/matrixvector/CMatrixVectorMultiplication.h"
#include "../../../solver/orthogonalization/basic/CClassicalGramSchmidtOrthogonalization.h"
#include "../../../solver/arnoldi/basic/CArnoldiFactorization.h"
#include "../../../solver/loader/matrix/column/CLaplacian1DColumnMatrixLoader.h"
#include "../../../solver/loader/matrix/column/CLaplacian2DColumnMatrixLoader.h"
#include "../../../solver/loader/helpers/text/CMatrixMarketLoader.h"
#include "../../uihelpers/CRestartedArnoldiInputArguments.h"
#include "../../../solver/arnoldi/restarted/CImplicitlyRestartedArnoldi.h"
#include "../../../solver/arnoldi/restarted/CRestartedArnoldiObserversHelper.h"
#include "../../../solver/decomposition/qr/CHessenbergQrQuasiTriangularization.h"
#include "../../../solver/decomposition/qr/CHessenbergQrDoubleShiftStep.h"
#include "../../../solver/decomposition/qr/CHessenbergQrSingleShiftStep.h"
#include "../../../solver/decomposition/qr/CHessenbergQrShifts.h"
#include "../../../solver/decomposition/eigen/CQuasiTriangularEigenInfo.h"
#include "../../../solver/decomposition/eigen/CHessenbergEigen.h"
#include "../../../solver/loader/matrix/row/csr/CStreamMatrixMarketCompressedSparseRowLoader.h"
#include "../../../debug/archive/CArnoldiRestartArchiver.h"
#include "../../../test/system/CCalculatingFewEigenvaluesTest.h"
#include "../view/page/CTextPage.h"
#include "../view/page/concrete/CHelpPage.h"
#include "../view/page/concrete/CEigenvaluesPage.h"
#include "../view/page/concrete/CStatisticsPage.h"
#include "../view/page/concrete/CParametersInfoPage.h"
#include "../view/page/generic/CComputationStartInfoPage.h"
#include "../view/page/generic/CComputationEndInfoPage.h"
#include "../view/page/concrete/CSimpleEigenvaluesPage.h"
#include "../../../solver/exporter/CTextExporter.h"

class CCommandLineController {
public:
  explicit CCommandLineController(CTextExporter &standardOutputTextExporter);

  void ComputeFewEigenvalues(CRestartedArnoldiInputArguments inputArguments);

  /// render help page into the standard output exporter from constructor
  void ShowHelp();

protected:
  /// render page into the standard output exporter from constructor
  void Render(const CTextPage &textPage);

  CTextExporter &m_StandardOutputTextExporter;
};

#endif //EIGENVALUECALCULATORSOLVER_CCOMMANDLINECONTROLLER_H
