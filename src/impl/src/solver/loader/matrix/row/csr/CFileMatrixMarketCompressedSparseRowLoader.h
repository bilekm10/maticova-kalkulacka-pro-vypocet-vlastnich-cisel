#ifndef EIGENVALUECALCULATORSOLVER_CFILEMATRIXMARKETCOMPRESSEDSPARSEROWLOADER_H
#define EIGENVALUECALCULATORSOLVER_CFILEMATRIXMARKETCOMPRESSEDSPARSEROWLOADER_H

#include "CStreamMatrixMarketCompressedSparseRowLoader.h"

#include <vector>
#include <stdexcept>
#include <complex>
#include <fstream>

//class CFileMatrixMarketCompressedSparseRowLoader : public CStreamMatrixMarketCompressedSparseRowLoader {
/// Loading a CCompressedSparseRowMatrix from a file
class CFileMatrixMarketCompressedSparseRowLoader : public CCompressedSparseRowLoader {
public:
  //explicit CFileMatrixMarketCompressedSparseRowLoader(std::ifstream & ifs)
  explicit CFileMatrixMarketCompressedSparseRowLoader(const std::string &filename);

  [[nodiscard]]
  CCompressedSparseRowMatrix Load() override;

protected:
  std::ifstream m_InputFileStream;
  CStreamMatrixMarketCompressedSparseRowLoader m_StreamMatrixMarketCompressedSparseRowLoader;
};

//explicit CFileMatrixMarketCompressedSparseRowLoader(std::ifstream & ifs)
inline
CFileMatrixMarketCompressedSparseRowLoader::CFileMatrixMarketCompressedSparseRowLoader(const std::string &filename)
        : CCompressedSparseRowLoader(),
          m_InputFileStream(filename),
          m_StreamMatrixMarketCompressedSparseRowLoader(m_InputFileStream) {
  if (!m_InputFileStream)
    throw std::invalid_argument("CFileMatrixMarketCompressedSparseRowLoader constructor: "
                                "Invalid input file stream for filename "
                                "\"" + filename + "\"");
}

[[nodiscard]]
inline CCompressedSparseRowMatrix
CFileMatrixMarketCompressedSparseRowLoader::Load() {
  return m_StreamMatrixMarketCompressedSparseRowLoader.Load();
}

#endif //EIGENVALUECALCULATORSOLVER_CFILEMATRIXMARKETCOMPRESSEDSPARSEROWLOADER_H
