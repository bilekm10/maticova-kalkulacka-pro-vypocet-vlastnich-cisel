#include "CEigenvaluesPage.h"

CEigenvaluesPage::CEigenvaluesPage(
        const CColumnVectorConst <std::complex<CEigenvaluesPage::Scalar>> &eigenvalues,
        uint8_t precisionManipulator,
        const std::string &separator,
        bool omitZeroImaginaryPart)
        : m_Eigenvalues(eigenvalues),
          m_PrecisionManipulator(precisionManipulator),
          m_Separator(separator),
          m_OmitZeroImaginaryPart(omitZeroImaginaryPart) {
}

CEigenvaluesPage::CEigenvaluesPage(
        const CColumnVectorConst <std::complex<CEigenvaluesPage::Scalar>> &eigenvalues,
        double precision,
        const std::string &separator,
        bool omitZeroImaginaryPart)
        : CEigenvaluesPage(eigenvalues,
                           CalculatePrecisionManipulator(precision),
                           separator,
                           omitZeroImaginaryPart) {
}

QString CEigenvaluesPage::Render() const {
  std::stringstream ss;
  ss << std::setprecision(m_PrecisionManipulator) << std::scientific;
  for (size_t i = 0; i < m_Eigenvalues.GetSize(); ++i) {
    //ss << std::setw(5) << std::left << std::to_string(i + 1) + ":";
    auto eigenvalue = m_Eigenvalues.At(i);
    ss << "( " << (eigenvalue.real() > 0 ? "+" : "-") << " " << std::abs(eigenvalue.real());
    if (!(eigenvalue.imag() == 0 && m_OmitZeroImaginaryPart)) {
      ss << " " <<
         (eigenvalue.imag() > 0 ? "+" : "-") << " " <<
         std::abs(eigenvalue.imag()) << " i";
    }
    ss << " )";
    if (i < m_Eigenvalues.GetSize() - 1)
      ss << m_Separator;
  }
  QString res = {ss.str().c_str()};
  //res.replace("-", "–");
  return res;
}

uint8_t CEigenvaluesPage::CalculatePrecisionManipulator(double precision) {
  // https://en.cppreference.com/w/cpp/types/numeric_limits/max_digits10
  if (precision < std::numeric_limits<CEigenvaluesPage::Scalar>::epsilon() * 10)
    //return CalculatePrecisionManipulatorWithoutLowerBound(std::numeric_limits<CEigenvaluesPage::Scalar>::epsilon());
    return std::numeric_limits<CEigenvaluesPage::Scalar>::max_digits10;
  return CalculatePrecisionManipulatorWithoutLowerBound(precision);
}

uint8_t CEigenvaluesPage::CalculatePrecisionManipulatorWithoutLowerBound(double precision) {
  return static_cast<uint8_t>(std::max(static_cast<int>(std::max(-std::log10(precision), 0.0)),
                                       0) + 2);
}
