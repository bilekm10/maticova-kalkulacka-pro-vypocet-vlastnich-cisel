#ifndef EIGENVALUECALCULATORGUI_CCOMPUTEDEIGENVALUESLAYOUT_H
#define EIGENVALUECALCULATORGUI_CCOMPUTEDEIGENVALUESLAYOUT_H

#include "../../../../solver/model/vector/column/CColumnVectorConst.h"
#include "../page/CEigenvaluesPage.h"

#include <QAction>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QTextEdit>
#include <QSpinBox>
#include <QDoubleValidator>
#include <QMessageBox>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QElapsedTimer>
#include <QTimer>
#include <complex>

QT_BEGIN_NAMESPACE
class QAction;

class QDialogButtonBox;

class QGroupBox;

class QLabel;

class QLineEdit;

class QMenu;

class QMenuBar;

class QPushButton;

class QTextEdit;

class QComboBox;

class QSpinBox;

class QDoubleValidator;

class QMessageBox;

QT_END_NAMESPACE

class CComputedEigenvaluesLayout : public QVBoxLayout {
Q_OBJECT

public:
  CComputedEigenvaluesLayout();

  /*QTextEdit * GetTextEdit()
  { return m_ComputedEigenvaluesTextEdit; }*/
  void SetComputedEigenvaluesText(const QString &text) { m_ComputedEigenvaluesTextEdit->setText(text); }

  void SetComputedEigenvalues(const CColumnVectorConst <std::complex<double>> &computedEigenvalues) {
    m_ComputedEigenvaluesTextEdit->setText(CEigenvaluesPage{
            computedEigenvalues,
            m_Precision,
            "\n",
            true}.Render());
  }

  void SetPrecision(double precision) { m_Precision = precision; }

protected:
  static constexpr int VerticalSpacing = 3;

  QLabel *m_ComputedEigenvaluesLabel;
  QTextEdit *m_ComputedEigenvaluesTextEdit;

  double m_Precision;
};

#endif //EIGENVALUECALCULATORGUI_CCOMPUTEDEIGENVALUESLAYOUT_H
