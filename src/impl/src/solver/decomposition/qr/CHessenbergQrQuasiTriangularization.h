#ifndef EIGENVALUECALCULATORSOLVER_CHESSENBERGQRQUASITRIANGULARIZATION_H
#define EIGENVALUECALCULATORSOLVER_CHESSENBERGQRQUASITRIANGULARIZATION_H

#include "../../model/matrix/dense/column/CColumnMatrix.h"
#include "../../eigenlibhelpers/eigenlibmap.h"

#include <Eigen/Eigenvalues>
#include <vector>

//#define DBG_PRINT

/// Using Eigen solution to factorize a matrix into Real Schur decomposition: the quasi-triangular matrix with the same eigenvalues and the corresponding orthogonal matrix
class CHessenbergQrQuasiTriangularization {
public:
  void Compute(CColumnMatrix &hessenbergMatrix, CColumnMatrix &orthogonalMatrix);

protected:
};

inline void
CHessenbergQrQuasiTriangularization::Compute(CColumnMatrix &hessenbergMatrix, CColumnMatrix &orthogonalMatrix) {
  #ifdef DBG_PRINT
  printMatrix(hessenbergMatrix, "CHessenbergQrQuasiTriangularization before computation hessenbergMatrix");
    printMatrix(orthogonalMatrix, "CHessenbergQrQuasiTriangularization before computation orthogonalMatrix");
  #endif //DBG_PRINT

  const auto &eigenHessenbergMatrix = mapToEigenMatrix(hessenbergMatrix);
  const auto &eigenOrthogonalMatrix = mapToEigenMatrix(orthogonalMatrix);
  // or Spectra::UpperHessenbergSchur?
  Eigen::RealSchur<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor>> realSchur;
  realSchur.computeFromHessenberg(eigenHessenbergMatrix, eigenOrthogonalMatrix, true);
  fromEigenMatrix(hessenbergMatrix, realSchur.matrixT());
  fromEigenMatrix(orthogonalMatrix, realSchur.matrixU());

  #ifdef DBG_PRINT
  printMatrix(hessenbergMatrix, "CHessenbergQrQuasiTriangularization right after computation realSchur.matrixT()");
    printMatrix(orthogonalMatrix, "CHessenbergQrQuasiTriangularization right after computation realSchur.matrixU()");
  #endif //DBG_PRINT
}

#endif //EIGENVALUECALCULATORSOLVER_CHESSENBERGQRQUASITRIANGULARIZATION_H
