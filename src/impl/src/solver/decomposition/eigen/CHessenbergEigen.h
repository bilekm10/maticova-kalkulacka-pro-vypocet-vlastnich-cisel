#ifndef EIGENVALUECALCULATORSOLVER_CHESSENBERGEIGEN_H
#define EIGENVALUECALCULATORSOLVER_CHESSENBERGEIGEN_H

#include <vector>
#include <stdexcept>

#include "CQuasiTriangularEigenInfo.h"
#include "../qr/CHessenbergQrQuasiTriangularization.h"
#include "../../model/vector/column/CColumnVector.h"
#include "../../model/vector/column/CColumnVectorHolding.h"
#include "../../sorting/CVectorSorting.h"
#include "../../sorting/ESortingCriterion.h"
#include "../../../debug/eig/eig.h"
#include "../../../debug/print/printFunctions.h"
#include "../../model/matrix/dense/column/CColumnMatrixDynamicDimensionsHolding.h"

//#define DBG_PRINT

/// Computing all eigenvalues of a matrix and their error estimates
class CHessenbergEigen {
public:
  CHessenbergEigen(size_t rowsCnt, double treatAsZero);

  void SetHessenbergMatrix(const CColumnMatrixConst &hessenbergMatrix);

  // should this class compute errorEstimates for Arnoldi method?
  void GetEigenvaluesWithErrorEstimates(CColumnVector<std::complex<double>> &eigenvalues,
                                        CColumnVector<double> &eigenvalueErrorEstimates, double residualNorm,
                                        ESortingCriterion sortingCriterion);

  void GetEigenvalues(CColumnVector<std::complex<double>> &eigenvalues, ESortingCriterion sortingCriterion);

protected:
  void Compute();

  CColumnMatrixDynamicDimensionsHolding m_HessenbergMatrix;
  CColumnMatrixDynamicDimensionsHolding m_OrthogonalMatrix;
  CColumnVectorHolding<std::complex<double>> m_Eigenvalues;
  CHessenbergQrQuasiTriangularization m_QuasiTriangularization;
  CQuasiTriangularEigenInfo m_QuasiTriangularEigenInfo;
  double m_TreatAsZero;
  bool m_Computed;
};

inline
CHessenbergEigen::CHessenbergEigen(size_t rowsCnt, double treatAsZero)
        : m_HessenbergMatrix(rowsCnt, rowsCnt, rowsCnt, rowsCnt, false),
          m_OrthogonalMatrix(rowsCnt, rowsCnt, rowsCnt, rowsCnt, false),
          m_Eigenvalues(rowsCnt/*, {0, 0}*/),
          m_QuasiTriangularization(),
          m_QuasiTriangularEigenInfo(treatAsZero),
          m_TreatAsZero(treatAsZero),
          m_Computed(false) {
  //Compute();
}

inline void
CHessenbergEigen::SetHessenbergMatrix(const CColumnMatrixConst &hessenbergMatrix) {
  if (!hessenbergMatrix.IsSquare())
    throw std::invalid_argument("CHessenbergEigen->SetHessenbergMatrix(): "
                                "The matrix is not square");
  if (m_HessenbergMatrix.GetRowsCnt() != hessenbergMatrix.GetRowsCnt())
    throw std::invalid_argument("CHessenbergEigen->SetHessenbergMatrix(): "
                                "m_HessenbergMatrix.GetRowsCnt() != hessenbergMatrix.GetRowsCnt(): "
                                "The matrix dimensions are not what was "
                                "constructed to be prepared for");
  m_HessenbergMatrix.Assign(hessenbergMatrix);
  //if(m_OrthogonalMatrix.GetRowsCnt() != m_HessenbergMatrix.GetRowsCnt())
  m_OrthogonalMatrix.ToIdentity();
  m_Eigenvalues.SetToZero();
  m_Computed = false;
  Compute();
}

// should this class compute errorEstimates for Arnoldi method?
inline void
CHessenbergEigen::GetEigenvaluesWithErrorEstimates(CColumnVector<std::complex<double>> &eigenvalues,
                                                   CColumnVector<double> &eigenvalueErrorEstimates, double residualNorm,
                                                   ESortingCriterion sortingCriterion) {
  if (m_Eigenvalues.GetSize() != eigenvalues.GetSize()
      || m_Eigenvalues.GetSize() != eigenvalueErrorEstimates.GetSize())
    throw std::invalid_argument("CHessenbergEigen->GetEigenvaluesWithErrorEstimates(): "
                                "m_Eigenvalues.GetSize() != eigenvalues.GetSize() "
                                "|| m_Eigenvalues.GetSize() != eigenvalueErrorEstimates.GetSize(): "
                                "The eigenvalues vector or its error estimates "
                                "size where to write computed eigenvalues "
                                "is not what was constructed to be prepared for");
  if (!m_Computed)
    Compute();
  auto sortIndices = CColumnVectorSortingHelper<std::complex<double>>::Sort(m_Eigenvalues, sortingCriterion);
  if (m_Eigenvalues.GetSize() != sortIndices.size())
    throw std::logic_error("CHessenbergEigen->GetEigenvaluesWithErrorEstimates(): "
                           "m_Eigenvalues.GetSize() != sortIndices.size(): "
                           "The eigenvalues sorting indices size is not "
                           "appropriate for the computed eigenvalues vector size");
  for (size_t i = 0; i < sortIndices.size(); ++i) {
    eigenvalues.AtRef(i) = m_Eigenvalues.At(sortIndices[i]);
    // not sure with the indices for m_OrthogonalMatrix.At() - last row or column? - the last component of the eigenvector of the hessenbergMatrix
    eigenvalueErrorEstimates.AtRef(i) =
            residualNorm * m_OrthogonalMatrix.At(m_OrthogonalMatrix.GetRowsCnt() - 1, sortIndices[i]);
  }
  #ifdef DBG_PRINT
  printMatrix(m_OrthogonalMatrix, "GetEigenvaluesWithErrorEstimates Q matrix");
    //printMatrix({{residualNorm}}, "residualNorm");
    printf("residualNorm: %e\n", residualNorm);
  #endif
}

inline void
CHessenbergEigen::GetEigenvalues(CColumnVector<std::complex<double>> &eigenvalues, ESortingCriterion sortingCriterion) {
  if (m_Eigenvalues.GetSize() != eigenvalues.GetSize())
    throw std::invalid_argument("CHessenbergEigen->GetEigenvaluesWithErrorEstimates(): "
                                "m_Eigenvalues.GetSize() != eigenvalues.GetSize(): "
                                "The eigenvalues vector "
                                "size where to write computed eigenvalues "
                                "is not what was constructed to be prepared for");
  if (!m_Computed)
    Compute();
  auto sortIndices = CColumnVectorSortingHelper<std::complex<double>>::Sort(m_Eigenvalues, sortingCriterion);
  if (m_Eigenvalues.GetSize() != sortIndices.size())
    throw std::logic_error("CHessenbergEigen->GetEigenvaluesWithErrorEstimates(): "
                           "m_Eigenvalues.GetSize() != sortIndices.size(): "
                           "The eigenvalues sorting indices size is not "
                           "appropriate for the computed eigenvalues vector size");
  for (size_t i = 0; i < sortIndices.size(); ++i)
    eigenvalues.AtRef(i) = m_Eigenvalues.At(sortIndices[i]);
  #ifdef DBG_PRINT
  printMatrix(m_HessenbergMatrix, "GetEigenvalues hessenberg matrix");
    printMatrix(m_OrthogonalMatrix, "GetEigenvalues Q matrix");
  #endif
}

inline void
CHessenbergEigen::Compute() {
  if (m_Computed)
    return;
  #ifdef DBG_PRINT
  printMatrix(m_HessenbergMatrix, "GetEigenvalues hessenberg matrix");
    printEigenvalues(m_HessenbergMatrix, "CHessenbergEigen before Compute");
  #endif

  m_QuasiTriangularization.Compute(m_HessenbergMatrix, m_OrthogonalMatrix);
  // m_HessenbergMatrix is now quasiTriangular, orthogonalMatrix is the change os basis m_HessenbergMatrix
  m_QuasiTriangularEigenInfo.SetQuasiTriangularMatrix(m_HessenbergMatrix);
  m_QuasiTriangularEigenInfo.ComputeEigenvalues(m_Eigenvalues);
  m_Computed = true;

  #ifdef DBG_PRINT
  printEigenvalues(m_HessenbergMatrix, "CHessenbergEigen after Compute");
  #endif
}

#endif //EIGENVALUECALCULATORSOLVER_CHESSENBERGEIGEN_H
