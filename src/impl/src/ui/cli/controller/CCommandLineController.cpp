#include "./CCommandLineController.h"

CCommandLineController::CCommandLineController(CTextExporter &standardOutputTextExporter)
        : m_StandardOutputTextExporter(standardOutputTextExporter) {
}

void CCommandLineController::ComputeFewEigenvalues(CRestartedArnoldiInputArguments inputArguments) {
  // also here to make sure the arguments are valid
  inputArguments.Validate();
  // also here to make sure the arguments are valid and present
  inputArguments.SetEmptyToDefault();
  using Orthogonalization = CIterativeOrthogonalization<CClassicalGramSchmidtOrthogonalization>;
  using RestartedArnoldi = CImplicitlyRestartedArnoldi<CMatrixVectorMultiplication<CCompressedSparseRowMatrix>, Orthogonalization>;
  Render(CParametersInfoPage{
          inputArguments});
  std::chrono::steady_clock::time_point beginLoading = std::chrono::steady_clock::now();
  std::ifstream inputMatrixIfs(inputArguments.m_InputMatrixFilename.value());
  auto inputMatrix = CStreamMatrixMarketCompressedSparseRowLoader{inputMatrixIfs}.Load();
  CColumnVectorHolding<double> initialVector = CRandomColumnVectorLoader<double>{
          inputMatrix.GetRowsCnt(),
          inputMatrix.GetRowsCnt()}.Load();
  std::chrono::steady_clock::time_point endLoading = std::chrono::steady_clock::now();
  CMatrixVectorMultiplication<CCompressedSparseRowMatrix> inputMatrixMultiplication{inputMatrix};
  Orthogonalization orthogonalization(
          CClassicalGramSchmidtOrthogonalization{inputMatrix.GetRowsCnt()},
          inputArguments.m_FactorizationSizeToRestartAt.value(),
          inputArguments.m_MaximalNumberOfOrthogonalizationIterations.value(),
          inputArguments.m_IterativeOrthogonalizationEta.value());
  RestartedArnoldi restartedArnoldi(
          inputMatrixMultiplication,
          inputMatrix.GetRowsCnt(),
          initialVector,
          inputArguments.m_NumberOfWantedEigenvalues.value(),
          inputArguments.m_FactorizationSizeToRestartAt.value(),
          inputArguments.m_SortingCriterion.value(),
          inputArguments.m_MaximalNumberOfIterations.value(),
          inputArguments.m_Precision.value(),
          inputArguments.m_Precision.value(),
          orthogonalization);
  CRestartedArnoldiObserversHelper restartedArnoldiHelper(restartedArnoldi);
  Render(CComputationStartInfoPage{});
  std::chrono::steady_clock::time_point beginComputation = std::chrono::steady_clock::now();
  restartedArnoldiHelper.Compute();
  std::chrono::steady_clock::time_point endComputation = std::chrono::steady_clock::now();
  Render(CComputationEndInfoPage{});
  const auto &convergedEigenvalues = restartedArnoldi.GetConvergedEigenvalues();
  Render(CStatisticsPage{
          endLoading - beginLoading,
          endComputation - beginComputation,
          restartedArnoldi.GetNumberOfIterations()});
  Render(CEigenvaluesPage{
          convergedEigenvalues,
          inputArguments.m_NumberOfWantedEigenvalues.value(),
          inputArguments.m_Precision.value(),
          "\n",
          true});
  /*std::cout << CSimpleEigenvaluesPage{
          convergedEigenvalues,
          inputArguments.m_NumberOfWantedEigenvalues.value(),
          inputArguments.m_Precision.value(),
          "\n",
          false}.Render();*/
}

void CCommandLineController::ShowHelp() {
  Render(CHelpPage{});
}

// render page into the standard output exporter from constructor
void CCommandLineController::Render(const CTextPage &textPage) {
  m_StandardOutputTextExporter.Export(textPage.Render());
}
