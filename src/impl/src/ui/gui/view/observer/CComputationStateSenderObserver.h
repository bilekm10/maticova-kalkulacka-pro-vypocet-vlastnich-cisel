#ifndef EIGENVALUECALCULATORGUI_CCOMPUTATIONSTATESENDEROBSERVER_H
#define EIGENVALUECALCULATORGUI_CCOMPUTATIONSTATESENDEROBSERVER_H

#include "../../controller/CComputationSignals.h"
#include "../../../../solver/observer/CPropertyChangeObserver.h"
#include "../../../../solver/arnoldi/restarted/CImplicitlyRestartedArnoldi.h"
#include "../../../../solver/orthogonalization/basic/CClassicalGramSchmidtOrthogonalization.h"
#include "../../../../solver/orthogonalization/refined/CIterativeOrthogonalization.h"

class CComputationStateSenderObserver : public CPropertyChangeObserver<EComputationState> {
public:
  explicit CComputationStateSenderObserver(
          CComputationSignals &computationSignals);

  void PropertyChange(const EComputationState &newValue) override;

protected:
  CComputationSignals &m_ComputationSignals;
  // probably don't update to
  /*EComputationState m_OldValue = EComputationState::FactorizationExpanded;*/
};

#endif //EIGENVALUECALCULATORGUI_CCOMPUTATIONSTATESENDEROBSERVER_H
