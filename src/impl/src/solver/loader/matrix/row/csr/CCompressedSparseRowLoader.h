#ifndef EIGENVALUECALCULATORSOLVER_CCOMPRESSEDSPARSEROWLOADER_H
#define EIGENVALUECALCULATORSOLVER_CCOMPRESSEDSPARSEROWLOADER_H

#include "../../../../model/matrix/sparse/CCompressedSparseRowMatrix.h"

/// Loading a CCompressedSparseRowMatrix
class CCompressedSparseRowLoader {
public:
  [[nodiscard]] virtual
  CCompressedSparseRowMatrix Load() = 0;

protected:
};

#endif //EIGENVALUECALCULATORSOLVER_CCOMPRESSEDSPARSEROWLOADER_H
