#ifndef EIGENVALUECALCULATORSOLVER_CCOLUMNVECTORCONST_H
#define EIGENVALUECALCULATORSOLVER_CCOLUMNVECTORCONST_H

#include "../CVector.h"

#include <cmath>
#include <complex>

/// Providing const methods for a column vector
template<typename Scalar>
class CColumnVectorConst : protected CVector<Scalar> {
public:
  using CVector<Scalar>::At;
  using CVector<Scalar>::GetSize;
  using CVector<Scalar>::AbsoluteNorm;
  using CVector<Scalar>::EuclideanNorm;
  using CVector<Scalar>::MaximumNorm;
  using CVector<Scalar>::MultiplyToScalar;

  CColumnVectorConst(Scalar *data, size_t dataSize);

  explicit CColumnVectorConst(const CDense1DBlockView<Scalar> &view);

  explicit CColumnVectorConst(CDense1DBlockView<Scalar> &&view);

  CColumnVectorConst(const CColumnVectorConst<Scalar> &other);

  CColumnVectorConst(CColumnVectorConst<Scalar> &&other) noexcept;

  CColumnVectorConst<Scalar> &operator=(CColumnVectorConst<Scalar> &&other) noexcept;

  const Scalar *AtConstPtr(size_t i) const {
    return this->m_View.GetData() + i;
  }

  [[nodiscard]]
  CColumnVectorConst<Scalar> GetSubVectorConstRefView(size_t firstElementIndex, size_t lastElementIndex) const {
    return this->template GetConstSubView<CColumnVectorConst>(firstElementIndex, lastElementIndex);
  }

  virtual
  ~CColumnVectorConst();

protected:
  using CVector<Scalar>::m_View;

  // copy from the other view into *this view
  void Assign(const CColumnVectorConst<Scalar> &other) {
    this->m_View.Assign(other.m_View);
  }

  using CVector<Scalar>::Assign;
//  using CVector<Scalar>::AtRef;
//  using CVector<Scalar>::SetTo;
//  using CVector<Scalar>::SetToZero;
};

template<typename Scalar>
CColumnVectorConst<Scalar>::CColumnVectorConst(Scalar *data, size_t dataSize)
        : CVector<Scalar>(data,
                          dataSize) {
}

template<typename Scalar>
CColumnVectorConst<Scalar>::CColumnVectorConst(const CDense1DBlockView<Scalar> &view)
        : CVector<Scalar>(view) {
}

template<typename Scalar>
CColumnVectorConst<Scalar>::CColumnVectorConst(CDense1DBlockView<Scalar> &&view)
        : CVector<Scalar>(std::move(view)) {
}

template<typename Scalar>
CColumnVectorConst<Scalar>::CColumnVectorConst(const CColumnVectorConst<Scalar> &other)
        : CVector<Scalar>(other.m_View) {
}

template<typename Scalar>
CColumnVectorConst<Scalar>::CColumnVectorConst(CColumnVectorConst<Scalar> &&other) noexcept
        : CVector<Scalar>(std::move(other.m_View)) {
}

template<typename Scalar>
CColumnVectorConst<Scalar> &CColumnVectorConst<Scalar>::operator=(CColumnVectorConst<Scalar> &&other) noexcept {
  this->m_View = std::move(other.m_View);
  return *this;
}

template<typename Scalar>
inline std::ostream &operator<<(std::ostream &os, const CColumnVectorConst<Scalar> &vec) {
  for (size_t i = 0; i < vec.GetSize(); ++i)
    os << vec.At(i) << "\n";
  os.flush();
  return os;
}

template<typename Scalar>
CColumnVectorConst<Scalar>::~CColumnVectorConst() = default;

#endif //EIGENVALUECALCULATORSOLVER_CCOLUMNVECTORCONST_H
