#ifndef EIGENVALUECALCULATORSOLVER_CFILECOLUMNVECTORLOADER_H
#define EIGENVALUECALCULATORSOLVER_CFILECOLUMNVECTORLOADER_H

#include "CStreamColumnVectorLoader.h"
#include "../../../model/vector/column/CColumnVectorHolding.h"

#include <vector>
#include <stdexcept>
#include <complex>
#include <fstream>

template<typename Scalar>
//class CFileColumnVectorLoader : public CStreamColumnVectorLoader<Scalar> {
/// Loading a CColumnVectorHolding from a file
class CFileColumnVectorLoader : public CColumnVectorLoader<Scalar> {
public:
  explicit CFileColumnVectorLoader(const std::string &filename);

  [[nodiscard]]
  CColumnVectorHolding<Scalar> Load() override;

protected:
  std::ifstream m_InputFileStream;
  CStreamColumnVectorLoader<Scalar> m_StreamColumnVectorLoader;
};

template<typename Scalar>
CFileColumnVectorLoader<Scalar>::CFileColumnVectorLoader(const std::string &filename)
        : CColumnVectorLoader<Scalar>(),
          m_InputFileStream(filename),
          m_StreamColumnVectorLoader(m_InputFileStream) {
  if (!m_InputFileStream)
    throw std::invalid_argument("CFileColumnVectorLoader constructor: "
                                "Invalid input file stream for filename "
                                "\"" + filename + "\"");
}

template<typename Scalar>
[[nodiscard]]
CColumnVectorHolding<Scalar> CFileColumnVectorLoader<Scalar>::Load() {
  return m_StreamColumnVectorLoader.Load();
}

#endif //EIGENVALUECALCULATORSOLVER_CFILECOLUMNVECTORLOADER_H
