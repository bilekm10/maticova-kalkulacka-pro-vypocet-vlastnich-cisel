#ifndef EIGENVALUECALCULATORSOLVER_CITERATIVEORTHOGONALIZATIONETAOPTIONHANDLER_H
#define EIGENVALUECALCULATORSOLVER_CITERATIVEORTHOGONALIZATIONETAOPTIONHANDLER_H

#include "./COptionHandler.h"

class CIterativeOrthogonalizationEtaOptionHandler : public COptionHandler {
public:
  explicit CIterativeOrthogonalizationEtaOptionHandler(std::optional<double> &iterativeOrthogonalizationEta);

  // argumentsCnt must be >= 1
  void Handle(int &argumentsCnt, char **&argumentsVector) override;

  static constexpr const char OptionShortName[] = "-eta";
  static constexpr const char OptionFullName[] = "--iterative-orthogonalization-eta";
  static constexpr const char OptionVerboseName[] = "Iterative orthogonalization eta";
  static constexpr const char NumberFormat[] = "%lf";
protected:
  std::optional<double> &m_IterativeOrthogonalizationEta;
  double m_Number;
};

#endif //EIGENVALUECALCULATORSOLVER_CITERATIVEORTHOGONALIZATIONETAOPTIONHANDLER_H
