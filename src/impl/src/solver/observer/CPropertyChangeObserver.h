#ifndef EIGENVALUECALCULATORSOLVER_CPROPERTYCHANGEOBSERVER_H
#define EIGENVALUECALCULATORSOLVER_CPROPERTYCHANGEOBSERVER_H

/// Process a change of some property
template<typename T>
class CPropertyChangeObserver {
public:
  virtual
  void PropertyChange(const T &newValue) = 0;
};

#endif //EIGENVALUECALCULATORSOLVER_CPROPERTYCHANGEOBSERVER_H
