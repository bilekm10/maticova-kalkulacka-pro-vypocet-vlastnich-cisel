#ifndef EIGENVALUECALCULATORSOLVER_CMAXIMALNUMBEROFORTHOGONALIZATIONITERATIONSOPTIONHANDLER_H
#define EIGENVALUECALCULATORSOLVER_CMAXIMALNUMBEROFORTHOGONALIZATIONITERATIONSOPTIONHANDLER_H

#include "./COptionHandler.h"

class CMaximalNumberOfOrthogonalizationIterationsOptionHandler : public COptionHandler {
public:
  explicit CMaximalNumberOfOrthogonalizationIterationsOptionHandler(
          std::optional<uint8_t> &maximalNumberOfOrthogonalizationIterations);

  // argumentsCnt must be >= 1
  void Handle(int &argumentsCnt, char **&argumentsVector) override;

  static constexpr const char OptionShortName[] = "-maxortit";
  static constexpr const char OptionFullName[] = "--maximal-number-of-orthogonalization-iterations";
  static constexpr const char OptionVerboseName[] = "Maximal number of iterative orthogonalization iterations";
  // unsigned char format
  static constexpr const char NumberFormat[] = "%hhu";
protected:
  std::optional<uint8_t> &m_MaximalNumberOfOrthogonalizationIterations;
  uint8_t m_Number;
};

#endif //EIGENVALUECALCULATORSOLVER_CMAXIMALNUMBEROFORTHOGONALIZATIONITERATIONSOPTIONHANDLER_H
