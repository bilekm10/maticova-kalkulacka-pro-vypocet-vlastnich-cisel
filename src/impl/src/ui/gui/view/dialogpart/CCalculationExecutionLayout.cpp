#include "CCalculationExecutionLayout.h"

CCalculationExecutionLayout::CCalculationExecutionLayout() {
  CreateCalculateStopBtn();
  CreateStatisticsLayout();
  CreateActions();
  setSpacing(VerticalSpacing);
  addLayout(m_CalculateStopBtnLayout);
  addLayout(m_StatisticsLayout);
}

void CCalculationExecutionLayout::SetElapsedTimeValuesToZero() {
  m_LoadingTimeValue->setText("0.000 s");
  m_ComputationTimeValue->setText("0.000 s");
}

void CCalculationExecutionLayout::LoadingStart() {
  m_LoadingTimer.restart();
  SetNumberOfIterationsValue(0);
  UpdateLoadingTime();
  m_UpdateLoadingTimer.start(127);
}

void CCalculationExecutionLayout::LoadingEnd() {
  UpdateLoadingTime();
  m_UpdateLoadingTimer.stop();
}

void CCalculationExecutionLayout::ComputationStart() {
  m_ComputationTimer.restart();
  SetNumberOfIterationsValue(0);
  UpdateComputationTime();
  m_UpdateComputationTimer.start(127);
}

void CCalculationExecutionLayout::ComputationEnd() {
  UpdateComputationTime();
  m_UpdateComputationTimer.stop();
}

void CCalculationExecutionLayout::UpdateLoadingTime() {
  auto milliseconds = m_LoadingTimer.elapsed();
  m_LoadingTimeValue->setText(
          QString::number(milliseconds / 1000) +
          "." +
          QStringLiteral("%1").arg(milliseconds % 1000,
                                   3,
                                   10,
                                   QLatin1Char('0')) +
          " s");
}

void CCalculationExecutionLayout::UpdateComputationTime() {
  auto milliseconds = m_ComputationTimer.elapsed();
  m_ComputationTimeValue->setText(
          QString::number(milliseconds / 1000) +
          "." +
          QStringLiteral("%1").arg(milliseconds % 1000,
                                   3,
                                   10,
                                   QLatin1Char('0')) +
          " s");
}

void CCalculationExecutionLayout::CreateCalculateBtn() {
  m_CalculateBtn = new QPushButton(QString("Calculate"));
  m_CalculateBtn->setDefault(true);
  m_CalculateBtnBoxLayout = new QBoxLayout(QBoxLayout::LeftToRight);
  m_CalculateBtnBoxLayout->addWidget(
          m_CalculateBtn,
          0,
          Qt::AlignHCenter);
  m_CalculateBtn->setMinimumWidth(200);
}

void CCalculationExecutionLayout::CreateStopBtn() {
  m_StopBtn = new QPushButton(QString("Stop"));
}

void CCalculationExecutionLayout::CreateCalculateStopBtn() {
  CreateCalculateBtn();
  CreateStopBtn();
  m_CalculateStopBtnLayout = new QHBoxLayout;
  m_CalculateStopBtnLayout->addItem(m_CalculateBtnBoxLayout);
  m_CalculateStopBtnLayout->addWidget(m_StopBtn);
}

void CCalculationExecutionLayout::CreateTimeRow(QLabel *&timeLabel, const QString &timeLabelText, QLabel *&timeValue) {
  timeLabel = new QLabel(QString(timeLabelText));
  timeValue = new QLabel(QString("0.000 s"));
  timeValue->setFrameStyle(QFrame::Panel | QFrame::Sunken);
  timeValue->setWordWrap(true);
  timeValue->setAlignment(Qt::AlignRight);
}

void CCalculationExecutionLayout::CreateLoadingRow() {
  CreateTimeRow(m_LoadingTimeLabel,
                "Loading time",
                m_LoadingTimeValue);
  /*connect(&m_UpdateTimer, &QTimer::timeout,
          this, &CCalculationExecutionLayout::UpdateElapsedTime);*/
  m_UpdateLoadingTimer.callOnTimeout(
          this, &CCalculationExecutionLayout::UpdateLoadingTime);
}

void CCalculationExecutionLayout::CreateComputationRow() {
  CreateTimeRow(m_ComputationTimeLabel,
                "Computation time",
                m_ComputationTimeValue);
  m_UpdateComputationTimer.callOnTimeout(
          this, &CCalculationExecutionLayout::UpdateComputationTime);
}

void CCalculationExecutionLayout::CreateStatisticsLayout() {
  m_ComputationStateLabel = new QLabel(QString("Computation state"));
  m_ComputationStateValue = new QLabel(QString("Not loaded"));
  m_ComputationStateValue->setFrameStyle(QFrame::Panel | QFrame::Sunken);
  m_ComputationStateValue->setWordWrap(true);

  m_NumberOfIterationsLabel = new QLabel(QString("Number of iterations"));
  m_NumberOfIterationsValue = new QLabel(QString::number(0));
  m_NumberOfIterationsValue->setFrameStyle(QFrame::Panel | QFrame::Sunken);
  m_NumberOfIterationsValue->setWordWrap(true);

  CreateLoadingRow();
  CreateComputationRow();

  m_StatisticsLayout = new QFormLayout;
  m_StatisticsLayout->setSpacing(VerticalSpacing);
  m_StatisticsLayout->addRow(m_ComputationStateLabel,
                             m_ComputationStateValue);
  m_StatisticsLayout->addRow(m_NumberOfIterationsLabel,
                             m_NumberOfIterationsValue);
  m_StatisticsLayout->addRow(m_LoadingTimeLabel,
                             m_LoadingTimeValue);
  m_StatisticsLayout->addRow(m_ComputationTimeLabel,
                             m_ComputationTimeValue);
}

void CCalculationExecutionLayout::CreateActions() {
  m_CalculateBtnAction = new QAction(m_CalculateBtn);
  m_CalculateBtn->addAction(m_CalculateBtnAction);
  QObject::connect(m_CalculateBtn, &QPushButton::clicked,
                   m_CalculateBtnAction, &QAction::triggered);

  m_StopBtnAction = new QAction(m_StopBtn);
  m_StopBtn->addAction(m_StopBtnAction);
  QObject::connect(m_StopBtn, &QPushButton::clicked,
                   m_StopBtnAction, &QAction::triggered);

  /*m_NumberOfIterationsUpdateAction = new QAction(this);
  QObject::connect(m_NumberOfIterationsUpdateAction, &QAction::triggered,
                   )*/
}
