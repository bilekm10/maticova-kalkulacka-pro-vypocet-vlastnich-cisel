#ifndef EIGENVALUECALCULATORSOLVER_CAREMATRICESSAME_H
#define EIGENVALUECALCULATORSOLVER_CAREMATRICESSAME_H

#include <cmath>

template<typename LeftMatrixType, typename RightMatrixType>
class CAreMatricesSame {
public:
  explicit CAreMatricesSame(double treatAsZero);

  bool AreSame(const LeftMatrixType &leftMatrix, const RightMatrixType &rightMatrix);

  [[nodiscard]]
  double GetMaxAbsElementError() const {
    return m_MaxAbsElementError;
  }

protected:
  // this function should take absError, not error, so complex<double> can be compared
  void TreatError(double error);

  double m_TreatAsZero;

  bool m_Same;
  double m_SumOfAbsErrors;
  double m_SumOfSquaredErrors;
  double m_MaxAbsElementError;
  size_t m_DifferentElementsCnt;
  //double m_DifferentElementsFraction;
  size_t m_ElementsCnt;
};

template<typename LeftMatrixType, typename RightMatrixType>
CAreMatricesSame<LeftMatrixType, RightMatrixType>
::CAreMatricesSame(double treatAsZero)
        : m_TreatAsZero(treatAsZero),
          m_Same(false),
          m_SumOfAbsErrors(0),
          m_SumOfSquaredErrors(0),
          m_MaxAbsElementError(0),
          m_DifferentElementsCnt(0),
          m_ElementsCnt(0) {
}

template<typename LeftMatrixType, typename RightMatrixType>
bool CAreMatricesSame<LeftMatrixType, RightMatrixType>
::AreSame(const LeftMatrixType &leftMatrix, const RightMatrixType &rightMatrix) {
  m_Same = true;
  m_ElementsCnt = leftMatrix.GetRowsCnt() * leftMatrix.GetColumnsCnt();
  m_SumOfAbsErrors = 0;
  m_SumOfSquaredErrors = 0;
  m_MaxAbsElementError = 0;
  m_DifferentElementsCnt = 0;
  double error = 0;
  if (leftMatrix.GetRowsCnt() != rightMatrix.GetRowsCnt()) {
    m_Same = false;
  } else if (leftMatrix.GetColumnsCnt() != rightMatrix.GetColumnsCnt()) {
    m_Same = false;
  } else {
    for (unsigned int j = 0; j < leftMatrix.GetColumnsCnt(); ++j) {
      for (unsigned int i = 0; i < leftMatrix.GetRowsCnt(); ++i) {
        error = leftMatrix.At(i, j) - rightMatrix.At(i, j);
        TreatError(error);
      }
    }
  }
  //m_DifferentElementsFraction = m_DifferentElementsCnt / static_cast<double>(leftMatrix.GetRowsCnt() * leftMatrix.GetColumnsCnt());
  return m_Same;
}

// this function should take absError, not error, so complex<double> can be compared
template<typename LeftMatrixType, typename RightMatrixType>
void CAreMatricesSame<LeftMatrixType, RightMatrixType>
::TreatError(double error) {
  double absError = std::abs(error);
  if (m_MaxAbsElementError < absError) {
    m_MaxAbsElementError = absError;
  }
  m_SumOfAbsErrors += absError;
  m_SumOfSquaredErrors += absError * absError;
  if (absError > m_TreatAsZero) {
    m_Same = false;
    ++m_DifferentElementsCnt;
  }
}

#endif //EIGENVALUECALCULATORSOLVER_CAREMATRICESSAME_H
