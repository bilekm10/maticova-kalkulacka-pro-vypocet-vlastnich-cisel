#ifndef EIGENVALUECALCULATORSOLVER_CHESSENBERGQRDOUBLESHIFTSTEP_H
#define EIGENVALUECALCULATORSOLVER_CHESSENBERGQRDOUBLESHIFTSTEP_H

#include <Spectra/LinAlg/DoubleShiftQR.h>
#include <complex>
#include <cmath>

#include "../../model/vector/column/CColumnVector.h"
#include "CHessenbergQrQuasiTriangularization.h"

/// Using Spectra solution for performing QR double shift step
class CHessenbergQrDoubleShiftStep {
public:
  explicit CHessenbergQrDoubleShiftStep(size_t rowsCnt);

  void PerformDoubleShiftQrStep(
          Eigen::Matrix<
                  double,
                  Eigen::Dynamic,
                  Eigen::Dynamic,
                  Eigen::ColMajor> &hessenbergMatrix,
          Eigen::Matrix<
                  double,
                  Eigen::Dynamic,
                  Eigen::Dynamic,
                  Eigen::ColMajor> &orthogonalMatrix,
          double s,
          double t) {  // ideal interface: void PerformDoubleShiftQrStep(CColumnSquareMatrix & hessenbergMatrix, CColumnSquareMatrix & orthogonalMatrix, double s, double t)
    m_QrDoubleShiftStep.compute(hessenbergMatrix, s, t);
    // Q -> Q * Qi
    m_QrDoubleShiftStep.apply_YQ(orthogonalMatrix);
    // H -> Q'HQ
    // Matrix Q = Matrix::Identity(m_ncv, m_ncv);
    // m_QrDoubleShiftStep.apply_YQ(Q);
    // m_fac_H = Q.transpose() * m_fac_H * Q;
    //m_fac.compress_H(m_QrDoubleShiftStep);
    m_QrDoubleShiftStep.matrix_QtHQ(hessenbergMatrix);
  }

protected:
  Spectra::DoubleShiftQR<double> m_QrDoubleShiftStep;
};

inline
CHessenbergQrDoubleShiftStep::CHessenbergQrDoubleShiftStep(size_t rowsCnt)
        : m_QrDoubleShiftStep(rowsCnt) {
}

#endif //EIGENVALUECALCULATORSOLVER_CHESSENBERGQRDOUBLESHIFTSTEP_H
