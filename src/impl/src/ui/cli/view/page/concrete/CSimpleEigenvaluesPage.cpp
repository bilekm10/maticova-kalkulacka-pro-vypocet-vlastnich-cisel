#include "./CSimpleEigenvaluesPage.h"

CSimpleEigenvaluesPage::CSimpleEigenvaluesPage(
        const CColumnVectorConst<std::complex<CSimpleEigenvaluesPage::Scalar>> &eigenvalues,
        size_t numberOfWantedEigenvalues,
        uint8_t precisionManipulator,
        const std::string &separator,
        bool omitZeroImaginaryPart)
        : m_Eigenvalues(eigenvalues),
          m_NumberOfWantedEigenvalues(numberOfWantedEigenvalues),
          m_PrecisionManipulator(precisionManipulator),
          m_Separator(separator),
          m_OmitZeroImaginaryPart(omitZeroImaginaryPart) {
}

CSimpleEigenvaluesPage::CSimpleEigenvaluesPage(
        const CColumnVectorConst<std::complex<CSimpleEigenvaluesPage::Scalar>> &eigenvalues,
        size_t numberOfWantedEigenvalues,
        double precision,
        const std::string &separator,
        bool omitZeroImaginaryPart)
        : CSimpleEigenvaluesPage(eigenvalues,
                                 numberOfWantedEigenvalues,
        //static_cast<uint8_t>(std::max(static_cast<int>(std::max(-std::log10(precision), 0.0)),0) + 2),
                                 CalculatePrecisionManipulator(precision),
                                 separator,
                                 omitZeroImaginaryPart) {
}

[[nodiscard]]
std::string CSimpleEigenvaluesPage::Render() const {
  std::stringstream ss;
  ss << "==================================================\n";
  ss << "Converged eigenvalues in simple format [" <<
     m_Eigenvalues.GetSize() << " / " <<
     m_NumberOfWantedEigenvalues << "]:" <<
     m_Separator;
  ss << std::setprecision(m_PrecisionManipulator) << std::scientific;
  for (size_t i = 0; i < m_Eigenvalues.GetSize(); ++i) {
    auto eigenvalue = m_Eigenvalues.At(i);
    if (eigenvalue.imag() == 0 && m_OmitZeroImaginaryPart)
      ss << eigenvalue.real();
    else
      ss << eigenvalue;
    ss << m_Separator;
  }
  return ss.str();
}

uint8_t CSimpleEigenvaluesPage::CalculatePrecisionManipulator(double precision) {
  #ifdef DBG_FULL_PRINT_PREC
  return std::numeric_limits<CSimpleEigenvaluesPage::Scalar>::max_digits10;
  #else
  // https://en.cppreference.com/w/cpp/types/numeric_limits/max_digits10
    if(precision < std::numeric_limits<Scalar>::epsilon() * 10)
      //return CalculatePrecisionManipulatorWithoutLowerBound(std::numeric_limits<Scalar>::epsilon());
      return std::numeric_limits<Scalar>::max_digits10;
    return CalculatePrecisionManipulatorWithoutLowerBound(precision);
  #endif
}

uint8_t CSimpleEigenvaluesPage::CalculatePrecisionManipulatorWithoutLowerBound(double precision) {
  return static_cast<uint8_t>(std::max(static_cast<int>(std::max(-std::log10(precision), 0.0)),
                                       0) + 2);
}
