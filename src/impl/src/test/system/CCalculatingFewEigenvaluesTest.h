#ifndef EIGENVALUECALCULATORSOLVER_CCALCULATINGFEWEIGENVALUESTEST_H
#define EIGENVALUECALCULATORSOLVER_CCALCULATINGFEWEIGENVALUESTEST_H

#include "../../debug/check/equality/matrix/CAreMatricesSame.h"
#include "../../debug/print/printFunctions.h"
#include "../../debug/eig/eig.h"
#include "../../solver/decomposition/qr/CHessenbergQrQuasiTriangularization.h"
#include "../../solver/sorting/CVectorSorting.h"
#include "../../solver/model/vector/row/CRowVector.h"
#include "../../solver/model/vector/column/CColumnVectorHolding.h"
#include "../../solver/model/matrix/dense/column/CColumnMatrixDynamicDimensionsHolding.h"
#include "../../solver/model/matrix/sparse/CCompressedSparseRowMatrix.h"
#include "../../solver/operator/matrixvector/CMatrixVectorMultiplication.h"
#include "../../solver/operator/matrixmatrix/CMatrixKthSubdiagonalMatrixMultiplication.h"
#include "../../solver/orthogonalization/basic/CClassicalGramSchmidtOrthogonalization.h"
#include "../../solver/orthogonalization/refined/CIterativeOrthogonalization.h"
#include "../../solver/arnoldi/basic/CArnoldiFactorization.h"
#include "../../solver/loader/matrix/column/CLaplacian1DColumnMatrixLoader.h"
#include "../../solver/loader/matrix/column/CLaplacian2DColumnMatrixLoader.h"
#include "../../solver/loader/vector/column/CRandomColumnVectorLoader.h"
#include "../../solver/arnoldi/basic/CArnoldiFactorization.h"
#include "../../solver/arnoldi/restarted/CImplicitlyRestartedArnoldi.h"
#include "../../solver/decomposition/qr/CHessenbergQrQuasiTriangularization.h"
#include "../../solver/decomposition/qr/CHessenbergQrDoubleShiftStep.h"
#include "../../solver/decomposition/qr/CHessenbergQrSingleShiftStep.h"
#include "../../solver/decomposition/qr/CHessenbergQrShifts.h"
#include "../../solver/decomposition/eigen/CQuasiTriangularEigenInfo.h"
#include "../../solver/decomposition/eigen/CHessenbergEigen.h"
#include "../../debug/archive/CArnoldiRestartArchiver.h"
#include "../repository/CMatrixProvider.h"

#include <iostream>
#include <vector>
#include <iomanip>
#include <cmath>
#include <complex>
#include <fstream>
#include <cassert>
#include <functional>
#include <algorithm>
#include <memory>
#include <bitset>
#include <optional>
#include <limits>
#include <cfloat>
#include <string>
#include <deque>
#include <forward_list>
#include <list>
#include <set>
#include <queue>
#include <map>
#include <stack>
#include <array>
#include <execution>
#include <numbers>
#include <numeric>
#include <filesystem>
#include <regex>
#include <tuple>
#include <type_traits>
#include <typeinfo>
#include <limits>

//#define DBG_ARCHIVE
#define DBG_PRECISION_INFO

#define RESET   "\033[0m"
#define BLACK   "\033[30m"      /* Black */
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */
#define BLUE    "\033[34m"      /* Blue */
#define MAGENTA "\033[35m"      /* Magenta */
#define CYAN    "\033[36m"      /* Cyan */
#define WHITE   "\033[37m"      /* White */
#define BOLDBLACK   "\033[1m\033[30m"      /* Bold Black */
#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define BOLDYELLOW  "\033[1m\033[33m"      /* Bold Yellow */
#define BOLDBLUE    "\033[1m\033[34m"      /* Bold Blue */
#define BOLDMAGENTA "\033[1m\033[35m"      /* Bold Magenta */
#define BOLDCYAN    "\033[1m\033[36m"      /* Bold Cyan */
#define BOLDWHITE   "\033[1m\033[37m"      /* Bold White */
#define MORE_THAN_CORRECT_COLOR BOLDGREEN
#define CORRECT_COLOR GREEN
#define ALMOST_CORRECT_COLOR BOLDMAGENTA
#define FAIL_COLOR BOLDRED
#define INFO_ERROR_COLOR BOLDCYAN
#define INFO_COLOR BOLDBLUE
#define MATRIX_MARKET_COLOR BLUE

//#define RESULT_ARCHIVE
#ifdef RESULT_ARCHIVE
std::ofstream g_ResultArchive("../test-results/toFullKeywordString(ESortingCriterion::LargestMagnitude)/FILENAME.txt");
size_t g_MatrixId;
std::string g_NumberOfWantedEigenvaluesName = "k";
size_t g_FactorizationSizeToRestartAt;
std::string g_FactorizationSizeToRestartAtName = "m";
size_t g_MaximalNumberOfIterations;
std::string g_MaximalNumberOfIterationsName = "maxit";
#endif

// todo: use CAreMatricesSame after passing a Scalar template parameter to CMatrixConst and deriving CVector from CMatrixConst
class CAreVectorsSame {
public:
  explicit CAreVectorsSame(double treatAsZero);

  bool AreSame(const CColumnVectorConst<std::complex<double>> &leftVector,
               const CColumnVectorConst<std::complex<double>> &rightVector, size_t elementsCntToCompare);

  [[nodiscard]]
  double GetMaxAbsElementError() const {
    return m_MaxAbsElementError;
  }

  [[nodiscard]]
  const std::vector<std::complex<double>> &GetErrors() const {
    return m_Errors;
  }

protected:
  void TreatError(std::complex<double> error);

  double m_TreatAsZero;

  bool m_Same;
  double m_SumOfAbsErrors;
  double m_SumOfSquaredErrors;
  double m_MaxAbsElementError;
  size_t m_DifferentElementsCnt;
  //double m_DifferentElementsFraction;
  size_t m_ElementsCnt;
  std::vector<std::complex<double>> m_Errors;
};

inline
CAreVectorsSame::CAreVectorsSame(double treatAsZero)
        : m_TreatAsZero(treatAsZero),
          m_Same(false),
          m_SumOfAbsErrors(0),
          m_SumOfSquaredErrors(0),
          m_MaxAbsElementError(0),
          m_DifferentElementsCnt(0),
          m_ElementsCnt(0),
          m_Errors() {
}

inline
bool CAreVectorsSame::AreSame(const CColumnVectorConst<std::complex<double>> &leftVector,
                              const CColumnVectorConst<std::complex<double>> &rightVector,
                              size_t elementsCntToCompare) {
  m_Same = true;
  m_ElementsCnt = elementsCntToCompare;
  m_SumOfAbsErrors = 0;
  m_SumOfSquaredErrors = 0;
  m_MaxAbsElementError = 0;
  m_DifferentElementsCnt = 0;
  std::complex<double> error = 0;
  if (elementsCntToCompare > leftVector.GetSize() || elementsCntToCompare > rightVector.GetSize()) {
    m_Same = false;
  } else {
    for (unsigned int i = 0; i < leftVector.GetSize(); ++i) {
      error = leftVector.At(i) - rightVector.At(i);
      TreatError(error);
    }
  }
  //m_DifferentElementsFraction = m_DifferentElementsCnt / static_cast<double>(leftVector.GetSize() * leftVector.GetSize());
  return m_Same;
}

inline
void CAreVectorsSame::TreatError(std::complex<double> error) {
  double absError = std::abs(error);
  if (m_MaxAbsElementError < absError) {
    m_MaxAbsElementError = absError;
  }
  m_SumOfAbsErrors += absError;
  m_SumOfSquaredErrors += absError * absError;
  m_Errors.push_back(error);
  if (absError > m_TreatAsZero) {
    m_Same = false;
    ++m_DifferentElementsCnt;
  }
}

class CCalculatingFewEigenvaluesTest {
public:
  static constexpr const size_t DefaultNumberOfWantedEigenvalues = 3;
  static constexpr const size_t DefaultFactorizationSizeToRestartAt = 10;
  static constexpr const size_t DefaultNumberOfIterationToExtractMatricesAndReinitialize = 150;
  static constexpr const size_t DefaultMaximalNumberOfIterations = 150;
  static constexpr const size_t DefaultMaximalNumberOfOrthogonalizationIterations = 3;
  static constexpr const double DefaultTreatAsZero = 1e-16;
  //static constexpr const double DefaultEigenvaluePrecision = 1e-15;
  static constexpr const double DefaultAssertEigenvaluePrecision = 1e-13;
  static constexpr const size_t DefaultRandomVectorLoaderSeed = 111;

  void AssertEigenvalueCorrectness(const CColumnVectorConst<std::complex<double>> &computedEigenvalues,
                                   const CColumnVectorConst<std::complex<double>> &referenceEigenvalues,
                                   const double precision,
                                   const size_t numberOfWantedEigenvalues,
                                   const bool verboseOnError = true);

  CIterativeOrthogonalization<CClassicalGramSchmidtOrthogonalization>
  GetIterativeCgsOrthogonalization(CClassicalGramSchmidtOrthogonalization basicOrthogonalization,
                                   const size_t factorizationSizeToRestartAt,
                                   const uint8_t maximalNumberOfIterations);

  template<typename Orthogonalization>
  void ComputeAndAssertEigenvalues(const CCompressedSparseRowMatrix &inputLargeMatrix,
                                   ESortingCriterion sortingCriterion,
                                   Orthogonalization &orthogonalization,
                                   const CColumnVectorConst<std::complex<double>> &referenceEigenvalues,
                                   size_t numberOfWantedEigenvalues,
                                   size_t factorizationSizeToRestartAt,
                                   size_t maximalNumberOfIterations,
                                   double treatAsZero,
                                   double assertEigenvaluePrecision);

  void OneSymmetricTest(size_t matrixId = CMatrixProvider::FirstSymmetricMatrixId,
                        ESortingCriterion sortingCriterion = ESortingCriterion::LargestMagnitude,
                        const size_t numberOfWantedEigenvalues = DefaultNumberOfWantedEigenvalues,
                        const size_t factorizationSizeToRestartAt = DefaultFactorizationSizeToRestartAt,
                        const size_t maximalNumberOfIterations = DefaultMaximalNumberOfIterations,
                        const size_t maximalNumberOfOrthogonalizationIterations = DefaultMaximalNumberOfOrthogonalizationIterations,
                        const double treatAsZero = DefaultTreatAsZero,
                        const double assertEigenvaluePrecision = DefaultAssertEigenvaluePrecision);

  void OneNonSymmetricTest(size_t matrixId = CMatrixProvider::FirstNonSymmetricMatrixId,
                           ESortingCriterion sortingCriterion = ESortingCriterion::LargestMagnitude,
                           const size_t numberOfWantedEigenvalues = DefaultNumberOfWantedEigenvalues,
                           const size_t factorizationSizeToRestartAt = DefaultFactorizationSizeToRestartAt,
                           const size_t maximalNumberOfIterations = DefaultMaximalNumberOfIterations,
                           const size_t maximalNumberOfOrthogonalizationIterations = DefaultMaximalNumberOfOrthogonalizationIterations,
                           const double treatAsZero = DefaultTreatAsZero,
                           const double assertEigenvaluePrecision = DefaultAssertEigenvaluePrecision);

  void AllSymmetricTestsWithSameParameters(
          ESortingCriterion sortingCriterion = ESortingCriterion::LargestMagnitude,
          const size_t numberOfWantedEigenvalues = DefaultNumberOfWantedEigenvalues,
          size_t factorizationSizeToRestartAt = DefaultFactorizationSizeToRestartAt,
          const size_t maximalNumberOfIterations = DefaultMaximalNumberOfIterations,
          const size_t maximalNumberOfOrthogonalizationIterations = DefaultMaximalNumberOfOrthogonalizationIterations,
          const double treatAsZero = DefaultTreatAsZero,
          const double assertEigenvaluePrecision = DefaultAssertEigenvaluePrecision);

  // orthogonalization with normalization will probably be deleted
  void AllNonSymmetricTestsWithSameParameters(
          ESortingCriterion sortingCriterion = ESortingCriterion::LargestMagnitude,
          const size_t numberOfWantedEigenvalues = DefaultNumberOfWantedEigenvalues,
          const size_t factorizationSizeToRestartAt = DefaultFactorizationSizeToRestartAt,
          const size_t maximalNumberOfIterations = DefaultMaximalNumberOfIterations,
          const size_t maximalNumberOfOrthogonalizationIterations = DefaultMaximalNumberOfOrthogonalizationIterations,
          const double treatAsZero = DefaultTreatAsZero,
          double assertEigenvaluePrecision = DefaultAssertEigenvaluePrecision);

  void AllSymmetricTestsWithSameSortingCriterion(
          ESortingCriterion sortingCriterion = ESortingCriterion::LargestMagnitude,
          const size_t firstNumberOfWantedEigenvalues = 1,
          const size_t lastNumberOfWantedEigenvalues = 23);

  void AllNonSymmetricTestsWithSameSortingCriterion(
          ESortingCriterion sortingCriterion = ESortingCriterion::LargestMagnitude,
          const size_t firstNumberOfWantedEigenvalues = 1,
          const size_t lastNumberOfWantedEigenvalues = 23,
          size_t numberOfFirstInnerFactorizationSizeToRestartAtLoopSkips = 0);

  void AllTestsWithSameSortingCriterion(
          ESortingCriterion sortingCriterion = ESortingCriterion::LargestMagnitude);

protected:
  CMatrixProvider m_MatrixProvider;
};

inline
void CCalculatingFewEigenvaluesTest::AssertEigenvalueCorrectness(
        const CColumnVectorConst<std::complex<double>> &computedEigenvalues,
        const CColumnVectorConst<std::complex<double>> &referenceEigenvalues,
        const double precision,
        const size_t numberOfWantedEigenvalues,
        const bool verboseOnError) {
  if (referenceEigenvalues.GetSize() == 1 && referenceEigenvalues.At(0) == 0.0)
    std::cout << INFO_COLOR << "referenceEigenvalues.GetSize() == 1 && referenceEigenvalues.At(0) == 0.0,"
                               " so the reference eigenvalues were not computed correctly."
              << RESET << std::endl;
  CAreVectorsSame areVectorsSame(precision * std::abs(referenceEigenvalues.MaximumNorm()));
  bool areSame = areVectorsSame.AreSame(computedEigenvalues, referenceEigenvalues,
                                        std::min(computedEigenvalues.GetSize(), referenceEigenvalues.GetSize()));
  #ifdef RESULT_ARCHIVE
  g_ResultArchive << m_MatrixProvider.GetName(g_MatrixId)
                    << ": " << computedEigenvalues.GetSize()
                    << "/" << numberOfWantedEigenvalues
                    << " " << g_FactorizationSizeToRestartAtName
                    << "=" << g_FactorizationSizeToRestartAt
                    << " " << g_MaximalNumberOfIterationsName
                    << "=" << g_MaximalNumberOfIterations
                    << " max|E|=" << std::scientific << std::setprecision(2)
                    << areVectorsSame.GetMaxAbsElementError() << std::endl;
  #endif
  if (!areSame && verboseOnError) {
    std::cout << FAIL_COLOR;
    for (auto error : areVectorsSame.GetErrors())
      std::cout << error << "  ";
    std::cout << std::endl;
    std::cout << "computed:\n" << computedEigenvalues;
    std::cout << "reference:\n" << referenceEigenvalues;
    std::cout << "!areSame: MaxAbsElementError=" << std::scientific << std::setprecision(2)
              << areVectorsSame.GetMaxAbsElementError() << "  but maximalAllowed="
              << precision * std::abs(referenceEigenvalues.At(0));
    std::cout << RESET << std::endl;
  }
  assert(areSame);
  #ifdef DBG_PRECISION_INFO
  std::cout << INFO_ERROR_COLOR;
  for (auto error : areVectorsSame.GetErrors())
    std::cout << std::scientific << std::setprecision(2) << error << "  ";
  std::cout << RESET;
  areVectorsSame.GetErrors().empty() ? std::cout << "" : std::cout << std::endl;
  #endif
  if (computedEigenvalues.GetSize() > numberOfWantedEigenvalues)
    std::cout << MORE_THAN_CORRECT_COLOR;
  else if (computedEigenvalues.GetSize() == numberOfWantedEigenvalues)
    std::cout << CORRECT_COLOR;
  else if (computedEigenvalues.GetSize() > 0)
    std::cout << ALMOST_CORRECT_COLOR;
  else
    std::cout << FAIL_COLOR;
  std::cout << computedEigenvalues.GetSize() << " eigenvalues are same with MaxAbsElementError="
            << areVectorsSame.GetMaxAbsElementError();
  std::cout << RESET << std::endl;
}

inline
CIterativeOrthogonalization<CClassicalGramSchmidtOrthogonalization>
CCalculatingFewEigenvaluesTest::GetIterativeCgsOrthogonalization(
        CClassicalGramSchmidtOrthogonalization basicOrthogonalization,
        const size_t factorizationSizeToRestartAt,
        const uint8_t maximalNumberOfIterations) {
  return CIterativeOrthogonalization<CClassicalGramSchmidtOrthogonalization>(
          std::move(basicOrthogonalization),
          factorizationSizeToRestartAt + 100,
          maximalNumberOfIterations,
          CIterativeOrthogonalization<CClassicalGramSchmidtOrthogonalization>::DgksEta);
}

template<typename Orthogonalization>
void CCalculatingFewEigenvaluesTest::ComputeAndAssertEigenvalues(const CCompressedSparseRowMatrix &inputLargeMatrix,
                                                                 ESortingCriterion sortingCriterion,
                                                                 Orthogonalization &orthogonalization,
                                                                 const CColumnVectorConst<std::complex<double>> &referenceEigenvalues,
                                                                 size_t numberOfWantedEigenvalues,
                                                                 size_t factorizationSizeToRestartAt,
                                                                 size_t maximalNumberOfIterations,
                                                                 double treatAsZero,
                                                                 double assertEigenvaluePrecision) {
  const size_t n = inputLargeMatrix.GetRowsCnt();
  if (factorizationSizeToRestartAt + 10 >= n) {
    std::cout << INFO_COLOR;
    std::cout << "factorizationSizeToRestartAt + 10 >= n so no test is performed and nothing is computed";
    std::cout << RESET << std::endl;
    return;
  }
  CMatrixVectorMultiplication<CCompressedSparseRowMatrix> inputLargeMatrixVectorMultiplication(inputLargeMatrix);
  //CTwoSectionsColumnVectorLoader initialVectorLoader(n, 4, 10);
  CRandomColumnVectorLoader<double> initialVectorLoader(n, DefaultRandomVectorLoaderSeed);
  CColumnVectorHolding<double> initialVector(initialVectorLoader.Load());
  CImplicitlyRestartedArnoldi<CMatrixVectorMultiplication<CCompressedSparseRowMatrix>,
          Orthogonalization>
          implicitlyRestartedArnoldi1(inputLargeMatrixVectorMultiplication,
                                      n,
                                      initialVector,
                                      numberOfWantedEigenvalues,
                                      factorizationSizeToRestartAt,
                                      sortingCriterion,
                                      maximalNumberOfIterations, //DefaultNumberOfIterationToExtractMatricesAndReinitialize,
                                      treatAsZero,
                                      treatAsZero,
                                      orthogonalization);
  #ifdef DBG_ARCHIVE
  CArnoldiRestartArchiver<CImplicitlyRestartedArnoldi<
            CMatrixVectorMultiplication<CCompressedSparseRowMatrix>,
            Orthogonalization>> archiver(implicitlyRestartedArnoldi1);
    archiver.SetReferenceEigenvalues(referenceEigenvalues);
    archiver.ComputeAndArchive();
    archiver.PrintTogether();
  #else
  implicitlyRestartedArnoldi1.Compute();
  #endif
  const auto &convergedEigenvalues1 = implicitlyRestartedArnoldi1.GetConvergedEigenvalues();
  #ifdef RESULT_ARCHIVE
  g_FactorizationSizeToRestartAt = factorizationSizeToRestartAt;
    g_MaximalNumberOfIterations = maximalNumberOfIterations;
  #endif
  if (convergedEigenvalues1.GetSize() >= numberOfWantedEigenvalues)
    return AssertEigenvalueCorrectness(convergedEigenvalues1, referenceEigenvalues, assertEigenvaluePrecision,
                                       numberOfWantedEigenvalues);
  return AssertEigenvalueCorrectness(convergedEigenvalues1, referenceEigenvalues, assertEigenvaluePrecision,
                                     numberOfWantedEigenvalues);
  /*implicitlyRestartedArnoldi1.LastRestart();
  auto orthonormalVectorsMatrix = implicitlyRestartedArnoldi1.GetOrthonormalVectorsMatrix();
  auto hessenbergMatrix = implicitlyRestartedArnoldi1.GetHessenbergMatrix();
  auto residualVector = implicitlyRestartedArnoldi1.GetResidualVector();
  CImplicitlyRestartedArnoldi<CMatrixVectorMultiplication<CCompressedSparseRow>,
          Orthogonalization>
          implicitlyRestartedArnoldi2(inputLargeMatrixVectorMultiplication,
                                      n,
                                      move(orthonormalVectorsMatrix),
                                      move(hessenbergMatrix),
                                      move(residualVector),
                                      numberOfWantedEigenvalues,
                                      factorizationSizeToRestartAt,
                                      ESortingCriterion::LargestMagnitude,
                                      DefaultMaximalNumberOfIterations,
                                      treatAsZero,
                                      treatAsZero,
                                      orthogonalization);
  implicitlyRestartedArnoldi2.Compute();
  const auto & convergedEigenvalues2 = implicitlyRestartedArnoldi2.GetConvergedEigenvalues();
  AssertEigenvalueCorrectness(convergedEigenvalues2, referenceEigenvalues, assertEigenvaluePrecision, numberOfWantedEigenvalues);*/
}

// orthogonalization with normalization will probably be deleted
inline
void CCalculatingFewEigenvaluesTest::OneSymmetricTest(
        size_t matrixId,
        ESortingCriterion sortingCriterion,
        const size_t numberOfWantedEigenvalues,
        const size_t factorizationSizeToRestartAt,
        const size_t maximalNumberOfIterations,
        const size_t maximalNumberOfOrthogonalizationIterations,
        const double treatAsZero,
        const double assertEigenvaluePrecision) {
  auto matrixWithReferenceEigenvalues = m_MatrixProvider.GetSymmetricMatrixWithEigenvalues(
          matrixId,
          sortingCriterion);
  auto inputLargeMatrix = matrixWithReferenceEigenvalues.first;
  auto referenceEigenvalues = matrixWithReferenceEigenvalues.second;
  if (inputLargeMatrix.GetRowsCnt() != inputLargeMatrix.GetColumnsCnt())
    throw std::invalid_argument("inputLargeMatrix.GetRowsCnt() != inputLargeMatrix.GetColumnsCnt()");
  if (referenceEigenvalues.GetSize() > 1000)
    throw std::invalid_argument("referenceEigenvalues.GetSize() > 1000");
  auto basicOrthogonalization = CClassicalGramSchmidtOrthogonalization{inputLargeMatrix.GetRowsCnt()};
  auto orthogonalization = GetIterativeCgsOrthogonalization(
          basicOrthogonalization,
          factorizationSizeToRestartAt,
          maximalNumberOfOrthogonalizationIterations);
  #ifdef RESULT_ARCHIVE
  g_MatrixId = matrixId;
  #endif
  ComputeAndAssertEigenvalues(
          inputLargeMatrix,
          sortingCriterion,
          orthogonalization,
          referenceEigenvalues,
          numberOfWantedEigenvalues,
          factorizationSizeToRestartAt,
          maximalNumberOfIterations,
          treatAsZero,
          assertEigenvaluePrecision);
}

inline
void CCalculatingFewEigenvaluesTest::OneNonSymmetricTest(
        size_t matrixId,
        ESortingCriterion sortingCriterion,
        const size_t numberOfWantedEigenvalues,
        const size_t factorizationSizeToRestartAt,
        const size_t maximalNumberOfIterations,
        const size_t maximalNumberOfOrthogonalizationIterations,
        const double treatAsZero,
        const double assertEigenvaluePrecision) {
  auto matrixWithReferenceEigenvalues = m_MatrixProvider.GetNonSymmetricMatrixWithEigenvalues(
          matrixId,
          sortingCriterion);
  auto inputLargeMatrix = matrixWithReferenceEigenvalues.first;
  auto referenceEigenvalues = matrixWithReferenceEigenvalues.second;
  auto basicOrthogonalization = CClassicalGramSchmidtOrthogonalization{inputLargeMatrix.GetRowsCnt()};
  auto orthogonalization = GetIterativeCgsOrthogonalization(
          basicOrthogonalization,
          factorizationSizeToRestartAt,
          maximalNumberOfOrthogonalizationIterations);
  #ifdef RESULT_ARCHIVE
  g_MatrixId = matrixId;
  #endif
  ComputeAndAssertEigenvalues(
          inputLargeMatrix,
          sortingCriterion,
          orthogonalization,
          referenceEigenvalues,
          numberOfWantedEigenvalues,
          factorizationSizeToRestartAt,
          maximalNumberOfIterations,
          treatAsZero,
          assertEigenvaluePrecision);
}

inline
void CCalculatingFewEigenvaluesTest::AllSymmetricTestsWithSameParameters(
        ESortingCriterion sortingCriterion,
        const size_t numberOfWantedEigenvalues,
        size_t factorizationSizeToRestartAt,
        const size_t maximalNumberOfIterations,
        const size_t maximalNumberOfOrthogonalizationIterations,
        const double treatAsZero,
        const double assertEigenvaluePrecision) {
  std::cout << std::left;
  for (size_t matrixId = CMatrixProvider::FirstSymmetricMatrixId;
       matrixId <= CMatrixProvider::LastSymmetricMatrixId;
       ++matrixId) {
    std::cout << "id=" << std::setw(3) << matrixId << " ";
    std::cout.flush();
    OneSymmetricTest(matrixId,
                     sortingCriterion,
                     numberOfWantedEigenvalues,
                     factorizationSizeToRestartAt,
                     maximalNumberOfIterations,
                     maximalNumberOfOrthogonalizationIterations,
                     treatAsZero,
                     assertEigenvaluePrecision);
  }
}

// orthogonalization with normalization will probably be deleted
inline
void CCalculatingFewEigenvaluesTest::AllNonSymmetricTestsWithSameParameters(
        ESortingCriterion sortingCriterion,
        const size_t numberOfWantedEigenvalues,
        const size_t factorizationSizeToRestartAt,
        const size_t maximalNumberOfIterations,
        const size_t maximalNumberOfOrthogonalizationIterations,
        const double treatAsZero,
        double assertEigenvaluePrecision) {
  for (size_t matrixId = CMatrixProvider::FirstNonSymmetricMatrixId;
       matrixId <= CMatrixProvider::LastNonSymmetricMatrixId;
       ++matrixId) {
    if (matrixId >= CMatrixProvider::FirstNonSymmetricMatrixMarketMatrixId)
      std::cout << MATRIX_MARKET_COLOR;
    std::cout << "id";
    if (matrixId >= CMatrixProvider::FirstNonSymmetricMatrixMarketMatrixId)
      std::cout << RESET;
    std::cout << "=" << std::setw(3) << matrixId << " ";
    std::cout.flush();
    // to not fail on these errors
    double modifiedAssertEigenvaluePrecision;
    if (matrixId < CMatrixProvider::FirstNonSymmetricMatrixMarketMatrixId) {
      if (sortingCriterion == ESortingCriterion::SmallestRealPart)
        assertEigenvaluePrecision = assertEigenvaluePrecision * 10;
      modifiedAssertEigenvaluePrecision = assertEigenvaluePrecision;
      if (matrixId == 10) {
        if (numberOfWantedEigenvalues <= 8) {}
        else if (numberOfWantedEigenvalues <= 9)
          modifiedAssertEigenvaluePrecision = assertEigenvaluePrecision * 5.7;
        else
          modifiedAssertEigenvaluePrecision = assertEigenvaluePrecision * 10;
      }
    } else if (matrixId == 13 && (numberOfWantedEigenvalues == 9 || numberOfWantedEigenvalues == 10) &&
               sortingCriterion == ESortingCriterion::SmallestRealPart) {
      std::cout << "SKIPPED" << std::endl;
      /*OneNonSymmetricTest(matrixId,
                          sortingCriterion,
                          numberOfWantedEigenvalues,
                          factorizationSizeToRestartAt -7,
                          maximalNumberOfIterations,
                          maximalNumberOfOrthogonalizationIterations,
                          treatAsZero,
                          modifiedAssertEigenvaluePrecision);*/
      continue;
    } else if (matrixId == 14) {
      if (numberOfWantedEigenvalues <= 8)
        modifiedAssertEigenvaluePrecision = assertEigenvaluePrecision * 5500;
      else if (numberOfWantedEigenvalues <= 9)
        modifiedAssertEigenvaluePrecision = assertEigenvaluePrecision * 7000;
      else
        modifiedAssertEigenvaluePrecision = assertEigenvaluePrecision * 13000;
    } else if (matrixId == 15) {
//        if(sortingCriterion == ESortingCriterion::SmallestRealPart)
//        {
//          std::cout << "Skipping id " << matrixId << std::endl;
//          continue;
//        }
      if (numberOfWantedEigenvalues <= 3) {}
      else if (numberOfWantedEigenvalues <= 4)
        modifiedAssertEigenvaluePrecision = assertEigenvaluePrecision * 20000;
      else if (numberOfWantedEigenvalues <= 5)
        modifiedAssertEigenvaluePrecision = assertEigenvaluePrecision * 70000;
      else if (numberOfWantedEigenvalues <= 6)
        modifiedAssertEigenvaluePrecision = assertEigenvaluePrecision * 200000;
      else if (numberOfWantedEigenvalues <= 7)
        modifiedAssertEigenvaluePrecision = assertEigenvaluePrecision * 790000;
      else if (numberOfWantedEigenvalues <= 11)
        modifiedAssertEigenvaluePrecision = assertEigenvaluePrecision * 900000;
      else if (numberOfWantedEigenvalues <= 13)
        modifiedAssertEigenvaluePrecision = assertEigenvaluePrecision * 1000000;
      else if (numberOfWantedEigenvalues <= 15)
        modifiedAssertEigenvaluePrecision = assertEigenvaluePrecision * 2000000;
      else if (numberOfWantedEigenvalues <= 17)
        modifiedAssertEigenvaluePrecision = assertEigenvaluePrecision * 4100000;
      else if (numberOfWantedEigenvalues <= 19)
        modifiedAssertEigenvaluePrecision = assertEigenvaluePrecision * 5000000;
      else
        modifiedAssertEigenvaluePrecision = assertEigenvaluePrecision * 7100000;
    } else {
      modifiedAssertEigenvaluePrecision = assertEigenvaluePrecision * 500;
    }
    OneNonSymmetricTest(matrixId,
                        sortingCriterion,
                        numberOfWantedEigenvalues,
                        factorizationSizeToRestartAt,
                        maximalNumberOfIterations,
                        maximalNumberOfOrthogonalizationIterations,
                        treatAsZero,
                        modifiedAssertEigenvaluePrecision);
  }
}

inline
void CCalculatingFewEigenvaluesTest::AllSymmetricTestsWithSameSortingCriterion(
        ESortingCriterion sortingCriterion,
        const size_t firstNumberOfWantedEigenvalues,
        const size_t lastNumberOfWantedEigenvalues) {
  for (size_t numberOfWantedEigenvalues = firstNumberOfWantedEigenvalues;
       numberOfWantedEigenvalues <= lastNumberOfWantedEigenvalues;
       numberOfWantedEigenvalues < 10 ? ++numberOfWantedEigenvalues : numberOfWantedEigenvalues += 2) {
    for (size_t factorizationSizeToRestartAt = 2 * numberOfWantedEigenvalues + 1;
         factorizationSizeToRestartAt < 4 * numberOfWantedEigenvalues;
         factorizationSizeToRestartAt += numberOfWantedEigenvalues + 1) {
      AllSymmetricTestsWithSameParameters(
              sortingCriterion,
              numberOfWantedEigenvalues,
              factorizationSizeToRestartAt,
              DefaultMaximalNumberOfIterations * (numberOfWantedEigenvalues == 1 ? 11 : 1),
              DefaultMaximalNumberOfOrthogonalizationIterations,
              DefaultTreatAsZero,
              DefaultAssertEigenvaluePrecision);
    }
  }
}

inline
void CCalculatingFewEigenvaluesTest::AllNonSymmetricTestsWithSameSortingCriterion(
        ESortingCriterion sortingCriterion,
        const size_t firstNumberOfWantedEigenvalues,
        const size_t lastNumberOfWantedEigenvalues,
        size_t numberOfFirstInnerFactorizationSizeToRestartAtLoopSkips) {
  for (size_t numberOfWantedEigenvalues = firstNumberOfWantedEigenvalues;
       numberOfWantedEigenvalues <= lastNumberOfWantedEigenvalues;
       numberOfWantedEigenvalues < 10 ? ++numberOfWantedEigenvalues : numberOfWantedEigenvalues += 2) {
    for (size_t factorizationSizeToRestartAt = 2 * numberOfWantedEigenvalues + 1;
         factorizationSizeToRestartAt < 4 * numberOfWantedEigenvalues + 2;
         factorizationSizeToRestartAt += numberOfWantedEigenvalues + 1) {
      if (numberOfFirstInnerFactorizationSizeToRestartAtLoopSkips) {
        --numberOfFirstInnerFactorizationSizeToRestartAtLoopSkips;
        continue;
      }
      const size_t maximalNumberOfIterations =
              DefaultMaximalNumberOfIterations * (numberOfWantedEigenvalues == 1 ? 11 : 1);
      std::cout
              << "==============================================================================================================="
                 "\nnumberOfWantedEigenvalues=" << numberOfWantedEigenvalues << "  factorizationSizeToRestartAt="
              << factorizationSizeToRestartAt << "  maximalNumberOfIterations=" << maximalNumberOfIterations
              << std::endl;
      AllNonSymmetricTestsWithSameParameters(
              sortingCriterion,
              numberOfWantedEigenvalues,
              factorizationSizeToRestartAt,
              maximalNumberOfIterations,
              DefaultMaximalNumberOfOrthogonalizationIterations,
              DefaultTreatAsZero,
              DefaultAssertEigenvaluePrecision);
    }
  }
}

inline
void CCalculatingFewEigenvaluesTest::AllTestsWithSameSortingCriterion(
        ESortingCriterion sortingCriterion) {
  AllSymmetricTestsWithSameSortingCriterion(sortingCriterion);
  AllNonSymmetricTestsWithSameSortingCriterion(sortingCriterion);
  //m_MatrixProvider.CalculateReferenceMatrixMarketEigenvalues(25, ESortingCriterion::SmallestMagnitude);
  //m_MatrixProvider.CalculateReferenceMatrixMarketEigenvalues(25, ESortingCriterion::LargestRealPart);
  //m_MatrixProvider.CalculateReferenceMatrixMarketEigenvalues(25, ESortingCriterion::SmallestRealPart);

  //AllNonSymmetricTests(1, 23, 0);
  /*OneNonSymmetricTest(14,
                      DefaultNumberOfWantedEigenvalues,
                      DefaultFactorizationSizeToRestartAt,
                      DefaultMaximalNumberOfIterations,
                      DefaultMaximalNumberOfOrthogonalizationIterations,
                      DefaultTreatAsZero * 1000);*/
  // big eigenvalues, should properly set or update? treatAsZero
  /*OneNonSymmetricTest(18,
                      DefaultNumberOfWantedEigenvalues * 2,
                      DefaultFactorizationSizeToRestartAt * 2,
                      DefaultMaximalNumberOfIterations,
                      DefaultMaximalNumberOfOrthogonalizationIterations,
                      DefaultTreatAsZero * 1000000000);*/
  //m_MatrixProvider.CalculateReferenceMatrixMarketEigenvalues(25);
  /*OneSymmetricTest(0,
                   1,
                   3,
                   DefaultMaximalNumberOfIterations * 3,
                   DefaultMaximalNumberOfOrthogonalizationIterations,
                   DefaultTreatAsZero,
                   DefaultAssertEigenvaluePrecision);*/
  /*OneSymmetricTest(3,
                   16,
                   2 * 16 + 1 + 16 + 1,
                   DefaultMaximalNumberOfIterations,
                   DefaultMaximalNumberOfOrthogonalizationIterations,
                   DefaultTreatAsZero,
                   DefaultAssertEigenvaluePrecision);*/
  /*OneNonSymmetricTest(18,
                      1,
                      3,
                      DefaultMaximalNumberOfIterations * 11,
                      DefaultMaximalNumberOfOrthogonalizationIterations,
                      DefaultTreatAsZero,
                      DefaultAssertEigenvaluePrecision * 500);*/
  /*OneNonSymmetricTest(14,
                      1,
                      5,
                      DefaultMaximalNumberOfIterations * 11,
                      DefaultMaximalNumberOfOrthogonalizationIterations,
                      DefaultTreatAsZero,
                      DefaultAssertEigenvaluePrecision * 5000);*/
  /*OneNonSymmetricTest(9,
                      7,
                      15,
                      DefaultMaximalNumberOfIterations,
                      DefaultMaximalNumberOfOrthogonalizationIterations,
                      DefaultTreatAsZero);*/
  /*OneNonSymmetricTest(18,
                      3,
                      7,
                      DefaultMaximalNumberOfIterations,
                      DefaultMaximalNumberOfOrthogonalizationIterations,
                      DefaultTreatAsZero,
                      DefaultAssertEigenvaluePrecision * 500);*/
}

#endif //EIGENVALUECALCULATORSOLVER_CCALCULATINGFEWEIGENVALUESTEST_H
