#include "CRestartedArnoldiDialogView.h"

CRestartedArnoldiDialogView::CRestartedArnoldiDialogView(CRestartedArnoldiController &controller)
        : m_RestartedArnoldiDialogController(controller),

          m_ComputationSignals(),
          m_ConvergedEigenvaluesPageSenderObserver(m_ComputationSignals),
          m_ComputationStateSenderObserver(m_ComputationSignals),
          m_NumberOfIterationsRenderObserver(m_ComputationSignals),
          m_ErrorMessageTextExporter(m_ComputationSignals),
          m_ComputedEigenvaluesLayout(new CComputedEigenvaluesLayout),
          m_CalculationInputParametersLayout(new CCalculationInputParametersLayout),
          m_CalculationExecutionLayout(new CCalculationExecutionLayout) {
  m_MainLayout = new QHBoxLayout;

  m_MainLayout->addItem(m_ComputedEigenvaluesLayout);
  m_MainLayout->addWidget(ReturnVLine());
  m_MainLayout->addItem(m_CalculationInputParametersLayout);
  m_MainLayout->addWidget(ReturnVLine());
  m_MainLayout->addItem(m_CalculationExecutionLayout);
  m_MainLayout->setSpacing(HorizontalSpacingMainLayout);

  setLayout(m_MainLayout);
  setWindowTitle(tr("Few Eigenvalues Calculator"));

  // move to m_ComputedEigenvaluesView constructor
  //m_ComputedEigenvaluesView.Export(tr("No Eigenvalues computed yet"));
  m_ComputedEigenvaluesLayout->SetComputedEigenvaluesText("No eigenvalues computed yet");

  m_RestartedArnoldiDialogController.SetConvergedEigenvaluesPageRenderObserver(
          m_ConvergedEigenvaluesPageSenderObserver);
  m_RestartedArnoldiDialogController.SetComputationStateSenderObserver(
          m_ComputationStateSenderObserver);
  m_RestartedArnoldiDialogController.SetNumberOfIterationsRenderObserver(
          m_NumberOfIterationsRenderObserver);
  m_RestartedArnoldiDialogController.SetErrorMessageTextExporter(
          m_ErrorMessageTextExporter);
  m_RestartedArnoldiDialogController.SetComputationSignals(
          m_ComputationSignals);

  qRegisterMetaType<size_t>("size_t");
  qRegisterMetaType<std::string>("std::string");
  qRegisterMetaType<EComputationState>("EComputationState");
  qRegisterMetaType<CColumnVectorHolding < std::complex<double>> > ("CColumnVectorHolding<std::complex<double>>");

  connect(&m_ComputationSignals, &CComputationSignals::EigenvaluesConverged,
          m_ComputedEigenvaluesLayout, &CComputedEigenvaluesLayout::SetComputedEigenvalues);
  connect(&m_ComputationSignals, &CComputationSignals::ComputationStateChanged,
          m_CalculationExecutionLayout, &CCalculationExecutionLayout::SetComputationStateValue);
  /*connect(&m_ComputationSignals, &CComputationSignals::ComputationStateChanged,
          this, &CRestartedArnoldiDialogView::SetComputationStateValueOrStopText);*/
  connect(&m_ComputationSignals, &CComputationSignals::NumberOfIterationsChanged,
          m_CalculationExecutionLayout, &CCalculationExecutionLayout::SetNumberOfIterationsValue);
  connect(&m_ComputationSignals, &CComputationSignals::ErrorOccurred,
          this, &CRestartedArnoldiDialogView::ShowComputationErrorMessage);
  connect(&m_ComputationSignals, &CComputationSignals::ComputationStarted,
          m_CalculationExecutionLayout, &CCalculationExecutionLayout::LoadingEnd);
  connect(&m_ComputationSignals, &CComputationSignals::ComputationStarted,
          m_CalculationExecutionLayout, &CCalculationExecutionLayout::ComputationStart);
  connect(&m_ComputationSignals, &CComputationSignals::ComputationEnded,
          this, &CRestartedArnoldiDialogView::ComputationEnded);

  connect(m_CalculationExecutionLayout->GetCalculateBtnAction(), &QAction::triggered,
          this, &CRestartedArnoldiDialogView::ReadInputParametersAndCalculate);
  connect(m_CalculationExecutionLayout->GetStopBtnAction(), &QAction::triggered,
          this, &CRestartedArnoldiDialogView::StopComputing);
}

void CRestartedArnoldiDialogView::ShowComputationErrorMessage(const std::string &message) {
  m_CalculationExecutionLayout->SetComputationStateValueText("Computation error");
  QMessageBox msgBox(QMessageBox::Icon::Critical,
                     tr("Computation error"),
                     tr(message.c_str()),
                     QMessageBox::StandardButton::Cancel);
  msgBox.exec();
}

QFrame *CRestartedArnoldiDialogView::ReturnLine(QFrame::Shape shape) {
  auto *line = new QFrame(/*w*/);
  //m_HorizontalLine->setObjectName(QString::fromUtf8("m_HorizontalLine"));
  //m_HorizontalLine->setGeometry(QRect(320, 150, 118, 3));
  line->setFrameShape(shape);
  line->setFrameShadow(QFrame::Sunken);
  return line;
}

void CRestartedArnoldiDialogView::StopComputing() {
  if (m_Running) {
    m_CalculationExecutionLayout->SetComputationStateValueText("Stopping");
    m_RestartedArnoldiDialogController.StopComputing();
    m_Stopped = true;
    //m_Running = false;
    return;
  }
    // todo should rather hide the stop button when running
  else if (!m_Stopped) {
    QMessageBox msgBox(QMessageBox::Icon::Warning,
                       tr("Not running"),
                       tr("Cannot stop computation as the computation is not running "),
                       QMessageBox::StandardButton::Cancel);
    msgBox.exec();
  }
}

void CRestartedArnoldiDialogView::ComputationEnded() {
  m_Running = false;
  m_CalculationExecutionLayout->ComputationEnd();
  if (m_Stopped)
    m_CalculationExecutionLayout->SetComputationStateValueText("Stopped");
  m_Stopped = false;
}

void CRestartedArnoldiDialogView::ReadInputParametersAndCalculate() {
  if (m_Running) {
    QMessageBox msgBox(QMessageBox::Icon::Critical,
                       tr("Already running"),
                       tr("Cannot start computing as the computation is currently running"),
                       QMessageBox::StandardButton::Cancel);
    msgBox.exec();
    return;
  }
  m_CalculationExecutionLayout->SetElapsedTimeValuesToZero();
  m_Stopped = false;
  m_ComputedEigenvaluesLayout->SetComputedEigenvaluesText(tr("No eigenvalues computed yet"));
  m_CalculationExecutionLayout->SetComputationStateValueText("Not loaded");
  //QCoreApplication::processEvents();
  CRestartedArnoldiInputArguments inputArguments;
  bool areArgumentsValid = m_CalculationInputParametersLayout->ReadInputParameters(inputArguments);
  if (!areArgumentsValid) {
    m_CalculationExecutionLayout->SetComputationStateValueText("Invalid arguments");
    return;
  }
  m_ComputedEigenvaluesLayout->SetPrecision(inputArguments.m_Precision.value());
  m_Running = true;
  m_CalculationExecutionLayout->LoadingStart();
  QtConcurrent::run(
          &m_RestartedArnoldiDialogController,
          &CRestartedArnoldiController::ComputeFewEigenvalues,
          inputArguments);
  //qDebug() << "back in view";
}

void CRestartedArnoldiDialogView::reject() {
  m_RestartedArnoldiDialogController.StopComputing();
  QDialog::reject();
}
