#ifndef EIGENVALUECALCULATORSOLVER_CDENSEMATRIXCONST_H
#define EIGENVALUECALCULATORSOLVER_CDENSEMATRIXCONST_H

#include <iomanip>

#include "../../view/matrix/CDense2DBlockView.h"
#include "../../vector/column/CColumnVector.h"
//#include "../row/CRowMatrix.h"

/// Providing methods for a dense matrix. A helper class to avoid code duplication in row- and column-major matrices
template<bool IsColumnMajor = true>
class CDenseMatrixConst {
  using Scalar = double;
protected:
  explicit CDenseMatrixConst(CDense2DBlockView &&blockView);

  CDenseMatrixConst(
          Scalar *data,
          size_t rowsCnt,
          size_t columnsCnt,
          // nextColumnShift for column major matrix
          size_t discontinuousShift);

  CDenseMatrixConst(CDenseMatrixConst<IsColumnMajor> &&other) noexcept;

  CDenseMatrixConst<IsColumnMajor> &operator=(CDenseMatrixConst<IsColumnMajor> &&other) noexcept;

  // copy from the other block view into *this block view
  void Assign(const CDenseMatrixConst<IsColumnMajor> &other) {
    m_BlockView.Assign(other.m_BlockView);
  }

public:

  [[nodiscard]]
  Scalar At(size_t row, size_t column) const {
    if constexpr(IsColumnMajor)
      return m_BlockView.At(row, column);
    else
      return m_BlockView.At(column, row);
  }

  [[nodiscard]]
  Scalar &AtRef(size_t row, size_t column) {
    if constexpr(IsColumnMajor)
      return m_BlockView.AtRef(row, column);
    else
      return m_BlockView.AtRef(column, row);
  }

  [[nodiscard]]
  size_t GetRowsCnt() const {
    if constexpr(IsColumnMajor)
      return m_BlockView.GetContinuousSize();
    else
      return m_BlockView.GetDiscontinuousSize();
  }

  [[nodiscard]]
  size_t GetColumnsCnt() const {
    if constexpr(IsColumnMajor)
      return m_BlockView.GetDiscontinuousSize();
    else
      return m_BlockView.GetContinuousSize();
  }

  [[nodiscard]]
  bool IsSquare() const {
    return m_BlockView.IsSquare();
  }

  void ToIdentity() {
    m_BlockView.ToIdentity();
  }

  virtual
  ~CDenseMatrixConst();

protected:
  CDense2DBlockView m_BlockView;
};

template<bool IsColumnMajor>
CDenseMatrixConst<IsColumnMajor>::CDenseMatrixConst(CDense2DBlockView &&blockView)
        : m_BlockView(std::move(blockView)) {
}

template<bool IsColumnMajor>
CDenseMatrixConst<IsColumnMajor>::CDenseMatrixConst(
        Scalar *data,
        size_t rowsCnt,
        size_t columnsCnt,
// nextColumnShift for column major matrix
        size_t discontinuousShift)
        : m_BlockView(data,
// continuousSize
                      IsColumnMajor ? rowsCnt
                                    : columnsCnt,
// discontinuousSize
                      IsColumnMajor ? columnsCnt
                                    : rowsCnt,
                      discontinuousShift) {
}

template<bool IsColumnMajor>
CDenseMatrixConst<IsColumnMajor>::CDenseMatrixConst(CDenseMatrixConst<IsColumnMajor> &&other) noexcept
        : m_BlockView(std::move(other.m_BlockView)) {
}

template<bool IsColumnMajor>
CDenseMatrixConst<IsColumnMajor> &
CDenseMatrixConst<IsColumnMajor>::operator=(CDenseMatrixConst<IsColumnMajor> &&other) noexcept {
  m_BlockView = std::move(other.m_BlockView);
  return *this;
}

template<bool IsColumnMajor>
CDenseMatrixConst<IsColumnMajor>::~CDenseMatrixConst() = default;

template<bool IsColumnMajor>
inline std::ostream &operator<<(std::ostream &os, const CDenseMatrixConst<IsColumnMajor> &matrix) {
  std::ios state(NULL);
  state.copyfmt(os);
  std::streamsize prec = os.precision();
  std::streamsize width = os.width();
  os << std::setprecision(2);
  for (size_t i = 0; i < matrix.GetRowsCnt(); ++i) {
    for (size_t j = 0; j < matrix.GetColumnsCnt(); ++j)
      os << std::setw(10) << matrix.At(i, j);
    os << "\n";
  }
  os.copyfmt(state);
  os.precision(prec);
  os.width(width);
  os.flush();
  return os;
}

#endif //EIGENVALUECALCULATORSOLVER_CDENSEMATRIXCONST_H
