#ifndef EIGENVALUECALCULATORSOLVER_CSTREAMMATRIXMARKETCOMPRESSEDSPARSEROWLOADER_H
#define EIGENVALUECALCULATORSOLVER_CSTREAMMATRIXMARKETCOMPRESSEDSPARSEROWLOADER_H

#include "CCompressedSparseRowLoader.h"
#include "../../../helpers/text/CMatrixMarketLoader.h"

#include <vector>
#include <stdexcept>
#include <istream>
#include <string>
#include <algorithm>
#include <tuple>

/// Loading a CCompressedSparseRowMatrix from a stream
class CStreamMatrixMarketCompressedSparseRowLoader : public CCompressedSparseRowLoader {
public:
  explicit CStreamMatrixMarketCompressedSparseRowLoader(std::istream &is);

  [[nodiscard]]
  CCompressedSparseRowMatrix Load() override;

  //static const constexpr auto FirstMatrixMarketLine = "%%MatrixMarket matrix coordinate real general";
protected:
  std::istream &m_Is;
};

inline
CStreamMatrixMarketCompressedSparseRowLoader
::CStreamMatrixMarketCompressedSparseRowLoader(std::istream &is)
        : CCompressedSparseRowLoader(),
          m_Is(is) {
}

[[nodiscard]]
inline CCompressedSparseRowMatrix
CStreamMatrixMarketCompressedSparseRowLoader
::Load() {
  CMatrixMarketLoader matrixMarketLoader(m_Is, true);
  matrixMarketLoader.ReadHeader();
  if (matrixMarketLoader.GetElementType() != EMatrixMarketElementType::Real)
    throw std::invalid_argument("CStreamMatrixMarketCompressedSparseRowLoader: "
                                "The matrix element type is not \"real\"");
  if (matrixMarketLoader.GetFormat() != EMatrixMarketFormat::Coordinate)
    throw std::invalid_argument("CStreamMatrixMarketCompressedSparseRowLoader: "
                                "The matrix view format is not \"coordinate\"");
  if (matrixMarketLoader.GetSymmetryStructure() != EMatrixMarketSymmetryStructure::General)
    throw std::invalid_argument("CStreamMatrixMarketCompressedSparseRowLoader: "
                                "The matrix symmetry structure is not \"general\"");
  //printf("CStreamMatrixMarketCompressedSparseRowLoader before matrixMarketLoader.LoadCoordinatesWithIndicesOfSortingColumnsThenRows\n");
  auto[oldRows, oldColumns, oldData, sortingIndices] = matrixMarketLoader.LoadCoordinatesWithIndicesOfSortingColumnsThenRows<double, size_t>();
  //printf("CStreamMatrixMarketCompressedSparseRowLoader after matrixMarketLoader.LoadCoordinatesWithIndicesOfSortingColumnsThenRows\n");
  size_t nnz = oldData.size();
  size_t rowsCnt = matrixMarketLoader.GetRowsCnt();
  std::vector<double> newData(nnz);
/*std::vector<size_t> newVectorRows(nnz);*/
  std::vector<size_t> newColumns(nnz);
  size_t lastRow = 0;
  std::vector<size_t> indicesOfRowStarts(rowsCnt + 1);
  indicesOfRowStarts[0] = 0;
  for (size_t i = 0; i < nnz; ++i) {
    size_t oldIndex = sortingIndices[i];
    newData[i] = oldData[oldIndex];
    size_t currentRow/* = newVectorRows[i]*/ = oldRows[oldIndex];
    newColumns[i] = oldColumns[oldIndex];
    if (currentRow != lastRow) {
      if (currentRow < lastRow)
        throw std::logic_error("CStreamMatrixMarketCompressedSparseRowLoader: "
                               "rows sorted inappropriately");
      if (currentRow >= rowsCnt)
        throw std::logic_error("CStreamMatrixMarketCompressedSparseRowLoader: "
                               "some rowIndex >= rowsCnt, indexing from 0");
      for (; lastRow < currentRow; ++lastRow)
        indicesOfRowStarts[lastRow + 1] = i;
    }
  }
  for (; lastRow < rowsCnt; ++lastRow)
    indicesOfRowStarts[lastRow + 1] = nnz;
  return CCompressedSparseRowMatrix(move(newData), move(newColumns), move(indicesOfRowStarts));
}

#endif //EIGENVALUECALCULATORSOLVER_CSTREAMMATRIXMARKETCOMPRESSEDSPARSEROWLOADER_H
