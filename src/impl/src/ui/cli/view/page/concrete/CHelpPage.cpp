#include "./CHelpPage.h"
#include "../../../../../solver/orthogonalization/basic/CClassicalGramSchmidtOrthogonalization.h"
#include "../../../../../solver/arnoldi/restarted/CImplicitlyRestartedArnoldi.h"

[[nodiscard]]
std::string CHelpPage::Render() const {
  using RestartedArnoldi = CImplicitlyRestartedArnoldi<CCompressedSparseRowMatrix, CIterativeOrthogonalization<CClassicalGramSchmidtOrthogonalization>>;
  using IterativeOrthogonalization = CIterativeOrthogonalization<CClassicalGramSchmidtOrthogonalization>;
  std::string defaultPrecision =
          ToString(RestartedArnoldi
                   ::DefaultTreatAsZero,
                   2);
  std::string defaultEta =
          ToString(IterativeOrthogonalization
                   ::DefaultEta,
                   2);
  std::string defaultMaximalNumberOfIterations =
          std::to_string(RestartedArnoldi
                         ::DefaultMaximalNumberOfIterations);
  std::string defaultMaximalNumberOfOrthogonalizationIterations =
          std::to_string(IterativeOrthogonalization
                         ::DefaultMaximalNumberOfOrthogonalizationIterations);
  std::string defaultSortingCriterion =
          toFullKeywordString(RestartedArnoldi
                              ::DefaultSortingCriterion);
  std::string sortingCriterion1 =
          toFullKeywordString(ESortingCriterion
                              ::LargestMagnitude);
  std::string sortingCriterion2 =
          toFullKeywordString(ESortingCriterion
                              ::SmallestMagnitude);
  std::string sortingCriterion3 =
          toFullKeywordString(ESortingCriterion
                              ::LargestRealPart);
  std::string sortingCriterion4 =
          toFullKeywordString(ESortingCriterion
                              ::SmallestRealPart);
  return
          "Parameters to execute this program with:\n"
          "-k <NUMBER>, --number-of-wanted-eigenvalues=<NUMBER>\n"
          "\tNUMBER is the number of wanted eigenvalues.\n"
          "\n"
          "-i <INPUT_MATRIX_FILENAME>, --input-matrix=<INPUT_MATRIX_FILENAME>\n"
          "\tINPUT_MATRIX_FILENAME is the filename of the input large matrix "
          "to compute few eigenvalues of.\n"
          "\n"
          "\n"
          "Optional parameters:\n"
          "-m <MAX_SIZE>, --factorization-size-to-restart-at=<MAX_SIZE>\n"
          "\tMAX_SIZE is the size of the Arnoldi factorization to restart at "
          "and compress the factorization. The value is computed from "
          "the number of wanted eigenvalues by default, and is then "
          "at least twice as big as the number of wanted eigenvalues.\n"
          "\n"
          "-s <SORTING_CRITERION>, --sorting-criterion=<SORTING_CRITERION>\n"
          "\tSORTING_CRITERION specifies the side of the spectrum "
          "from which to compute the eigenvalues, and can be: \"" +
          sortingCriterion1 +
          "\", \"" +
          sortingCriterion2 +
          "\", \"" +
          sortingCriterion3 +
          "\", or \"" +
          sortingCriterion4 +
          "\". Default is \"" +
          defaultSortingCriterion +
          "\".\n"
          "\n"
          "-p <PRECISION>, --precision=<PRECISION>\n"
          "\tPRECISION is a floating point number specifying the wanted precision "
          "of the eigenvalues, e.g.: 1e-10, 1e-5. Default is " +
          defaultPrecision +
          ".\n"
          "\n"
          "-maxit <MAX_ITERATIONS>, --maximal-number-of-iterations=<MAX_ITERATIONS>\n"
          "\tMAX_ITERATIONS is the maximal allowed number of iterations "
          "of the algorithm. Default is " +
          defaultMaximalNumberOfIterations +
          ".\n"
          "\n"
          "\n"
          "Advanced optional parameters:\n"
          "-maxortit <MAX_ORTHOGONALIZATION_ITERATIONS>, "
          "--maximal-number-of-orthogonalization-iterations=<MAX_ORTHOGONALIZATION_ITERATIONS>\n"
          "\tMAX_ORTHOGONALIZATION_ITERATIONS is the maximal allowed number "
          "of [re]orthogonalization iterations for iterative orthogonalization. "
          "Only one orthogonalization is performed each time "
          "if MAX_ORTHOGONALIZATION_ITERATIONS=1. Default is " +
          defaultMaximalNumberOfOrthogonalizationIterations +
          ".\n"
          "\n"
          // Daniel et al. 1976, reorthogonalize while (1/sqrt(2)) * ||afterOrthogVec||_2 <= ||beforeOrthogVec||_2
          "-eta <ITERATIVE_ORTHOGONALIZATION_ETA>,"
          "--iterative-orthogonalization-eta=<ITERATIVE_ORTHOGONALIZATION_ETA>\n"
          "\tITERATIVE_ORTHOGONALIZATION_ETA is a number which determines whether to perform "
          "another iteration of the iterative orthogonalization after computing "
          "the two-norm of the orthogonalized vector. Another orthogonalization "
          "is performed if the maximal number of orthogonalization iterations "
          "wasn't reached yet and if "
          "ITERATIVE_ORTHOGONALIZATION_ETA * ||afterLastOrthogVec||_2 "
          "<= ||beforeLastOrthogVec||_2. Hence, if ITERATIVE_ORTHOGONALIZATION_ETA=1, "
          "this reorthogonalization criterion will be satisfied until the new vector is "
          "orthogonal to working precision, and if ITERATIVE_ORTHOGONALIZATION_ETA=0, "
          "no reorthogonalization is performed, which can lead to serious computational "
          "problems. Default is 1/sqrt(2), i.e. " +
          defaultEta +
          ", a value recommended by Daniel et al. 1976.\n";
}

std::string CHelpPage::ToString(double number, uint8_t manipPrecision) {
  std::stringstream ss;
  ss << std::scientific << std::setprecision(manipPrecision) << number;
  return ss.str();
}
