#ifndef EIGENVALUECALCULATORGUI_CRESTARTEDARNOLDICONTROLLER_H
#define EIGENVALUECALCULATORGUI_CRESTARTEDARNOLDICONTROLLER_H

#include <QtWidgets>
#include <optional>
#include <fstream>
#include <stdexcept>
#include <atomic>

#include "../view/observer/CComputationStateSenderObserver.h"
#include "../view/observer/CConvergedEigenvaluesPageSenderObserver.h"
#include "../view/observer/CNumberOfIterationsSenderObserver.h"
#include "../view/page/CEigenvaluesPage.h"
#include "./CComputationSignals.h"
#include "../exporter/CErrorMessageTextExporter.h"
#include "../../../solver/sorting/ESortingCriterion.h"
#include "../../../solver/orthogonalization/basic/CClassicalGramSchmidtOrthogonalization.h"
#include "../../../solver/orthogonalization/refined/CIterativeOrthogonalization.h"
#include "../../../solver/model/matrix/sparse/CCompressedSparseRowMatrix.h"
#include "../../../solver/operator/matrixvector/CMatrixVectorMultiplication.h"
#include "../../../solver/arnoldi/restarted/CImplicitlyRestartedArnoldi.h"
#include "../../../solver/arnoldi/restarted/CRestartedArnoldiObserversHelper.h"
#include "../../../solver/loader/matrix/row/csr/CStreamMatrixMarketCompressedSparseRowLoader.h"
#include "../../../solver/observer/CPropertyChangeObserver.h"
#include "../../../ui/uihelpers/CRestartedArnoldiInputArguments.h"

class CRestartedArnoldiController {
public:
  CRestartedArnoldiController();

  void ComputeFewEigenvalues(
          CRestartedArnoldiInputArguments inputArguments);

  /*void SetComputedEigenvaluesExporter(CQStringExporter & computedEigenvaluesExporter)
  {
    m_ComputedEigenvaluesExporter = &computedEigenvaluesExporter;
  }

  void EigenvaluesConverged(const QString & eigenvaluesString)
  {
    m_ComputationSignals->EigenvaluesConverged(eigenvaluesString);
  }*/

  void SetComputationSignals(CComputationSignals &computationSignals) {
    m_ComputationSignals = &computationSignals;
  }

  void SetConvergedEigenvaluesPageRenderObserver(
          CConvergedEigenvaluesPageSenderObserver &convergedEigenvaluesPageRenderObserver) { m_ConvergedEigenvaluesPageRenderObserver = &convergedEigenvaluesPageRenderObserver; }

  void SetComputationStateSenderObserver(
          CComputationStateSenderObserver &computationStateSenderObserver) { m_ComputationStateRenderObserver = &computationStateSenderObserver; }

  void SetNumberOfIterationsRenderObserver(
          CNumberOfIterationsSenderObserver &numberOfIterationsRenderObserver) { m_NumberOfIterationsRenderObserver = &numberOfIterationsRenderObserver; }

  void SetErrorMessageTextExporter(
          CTextExporter &errorMessageTextExporter) { m_ErrorMessageTextExporter = &errorMessageTextExporter; }

  void StopComputing() { m_StopComputing = true; }

  /*void ShowHelp()
  {
    Render(CHelpPage{});
  }*/
protected:
  CComputationSignals *m_ComputationSignals;
  CConvergedEigenvaluesPageSenderObserver *m_ConvergedEigenvaluesPageRenderObserver;
  CComputationStateSenderObserver *m_ComputationStateRenderObserver;
  CNumberOfIterationsSenderObserver *m_NumberOfIterationsRenderObserver;
  CTextExporter *m_ErrorMessageTextExporter;
  std::atomic<bool> m_StopComputing;
/*// render page into the standard output exporter from constructor
  void Render(const CTextPage & textPage)
  {
    m_StandardOutputTextExporter.Export(textPage.Render());
  }*/
};

#endif //EIGENVALUECALCULATORGUI_CRESTARTEDARNOLDICONTROLLER_H
