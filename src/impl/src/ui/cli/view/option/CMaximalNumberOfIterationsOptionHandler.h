#ifndef EIGENVALUECALCULATORSOLVER_CMAXIMALNUMBEROFITERATIONSOPTIONHANDLER_H
#define EIGENVALUECALCULATORSOLVER_CMAXIMALNUMBEROFITERATIONSOPTIONHANDLER_H

#include "./COptionHandler.h"

class CMaximalNumberOfIterationsOptionHandler : public COptionHandler {
public:
  explicit CMaximalNumberOfIterationsOptionHandler(std::optional<size_t> &maximalNumberOfIterations);

  // argumentsCnt must be >= 1
  void Handle(int &argumentsCnt, char **&argumentsVector) override;

  static constexpr const char OptionShortName[] = "-maxit";
  static constexpr const char OptionFullName[] = "--maximal-number-of-iterations";
  static constexpr const char OptionVerboseName[] = "Maximal number of iterations";
  static constexpr const char NumberFormat[] = "%lu";
protected:
  std::optional<size_t> &m_MaximalNumberOfIterations;
  size_t m_Number;
};

#endif //EIGENVALUECALCULATORSOLVER_CMAXIMALNUMBEROFITERATIONSOPTIONHANDLER_H
