#ifndef EIGENVALUECALCULATORGUI_CCONVERGEDEIGENVALUESPAGESENDEROBSERVER_H
#define EIGENVALUECALCULATORGUI_CCONVERGEDEIGENVALUESPAGESENDEROBSERVER_H

#include "../../../../solver/observer/CPropertyChangeObserver.h"
#include "../../../../solver/model/vector/column/CColumnVectorHolding.h"
#include "../../controller/CComputationSignals.h"

class CConvergedEigenvaluesPageSenderObserver : public CPropertyChangeObserver<
        CColumnVectorConst < std::complex<double>>

> {
public:
explicit CConvergedEigenvaluesPageSenderObserver(
        CComputationSignals
& computationSignals);

void PropertyChange(const CColumnVectorConst <std::complex<double>> &newValue)

override;
protected:
CComputationSignals &m_ComputationSignals;
};

#endif //EIGENVALUECALCULATORGUI_CCONVERGEDEIGENVALUESPAGESENDEROBSERVER_H
