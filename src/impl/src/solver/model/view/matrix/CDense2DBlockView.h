#ifndef EIGENVALUECALCULATORSOLVER_CDENSE2DBLOCKVIEW_H
#define EIGENVALUECALCULATORSOLVER_CDENSE2DBLOCKVIEW_H

//#include <cstddef>
#include <stdexcept>
//#include <>
#include "../vector/CDense1DBlockView.h"

/// Accessing a 1D as a 2D memory block. Create subviews.
class CDense2DBlockView {
  using Scalar = double;
public:
  CDense2DBlockView(
          Scalar *data,
          // columnmajor: rowsCnt
          size_t continuousSize,
          // columnmajor: columnsCnt
          size_t discontinuousSize,
          // columnmajor: nextColumnShift, rowsCnt of the original matrix; // discontinuousShift
          size_t maximalContinuousSize);

  CDense2DBlockView(const CDense2DBlockView &other);

  CDense2DBlockView(CDense2DBlockView &&other);

  CDense2DBlockView &operator=(CDense2DBlockView &&other);

  // copy from the other block view into *this block view
  void Assign(const CDense2DBlockView &other);

  void Assign(const Scalar *data, size_t continuousSize, size_t discontinuousSize, size_t maximalContinuousSize);

  // in columnmajor: continuousIndex=row, discontinuousIndex=column
  [[nodiscard]]
  Scalar At(size_t continuousIndex, size_t discontinuousIndex) const { // use AtRef()?
    return m_Data[discontinuousIndex * m_MaximalContinuousSize + continuousIndex];
  }

  [[nodiscard]]
  Scalar &AtRef(size_t continuousIndex, size_t discontinuousIndex) {
    return m_Data[discontinuousIndex * m_MaximalContinuousSize + continuousIndex];
  }

  [[nodiscard]]
  Scalar &AtRef(size_t continuousIndex, size_t discontinuousIndex) const {
    return m_Data[discontinuousIndex * m_MaximalContinuousSize + continuousIndex];
  }

  [[nodiscard]]
  size_t GetContinuousSize() const {
    return m_ContinuousSize;
  }

  [[nodiscard]]
  size_t GetDiscontinuousSize() const {
    return m_DiscontinuousSize;
  }

  [[nodiscard]]
  size_t GetMaximalContinuousSize() const {
    return m_MaximalContinuousSize;
  }

  [[nodiscard]]
  bool IsSquare() const {
    return m_ContinuousSize == m_DiscontinuousSize;
  }

  void ToIdentity();

  void SetToZero();

  /*[[nodiscard]]
  CRowMatrixConstRefView TransposedConst() const
  {
    return CRowMatrixConstRefView();
  }*/

  //template<template<typename> class ReturnType>

  [[nodiscard]]
  CDense1DBlockView<Scalar> GetContinuousConstRefView(
          size_t discontinuousIndex,
          size_t firstContinuousElementIndex,
          size_t lastContinuousElementIndex) const {
    ContinuousViewBoundsCheck(
            discontinuousIndex,
            firstContinuousElementIndex,
            lastContinuousElementIndex);
    return CDense1DBlockView<Scalar>(
            m_Data
            + discontinuousIndex * m_MaximalContinuousSize
            + firstContinuousElementIndex,
            lastContinuousElementIndex - firstContinuousElementIndex + 1);
  }

  [[nodiscard]]
  CDense1DBlockView<Scalar> GetContinuousConstRefView(
          size_t discontinuousIndex) const {
    return GetContinuousConstRefView(
            discontinuousIndex,
            0,
            m_ContinuousSize - 1);
  }

  [[nodiscard]]
  CDense1DBlockView<Scalar> GetContinuousRefView(
          size_t discontinuousIndex,
          size_t firstContinuousElementIndex,
          size_t lastContinuousElementIndex) {
    ContinuousViewBoundsCheck(
            discontinuousIndex,
            firstContinuousElementIndex,
            lastContinuousElementIndex);
    return CDense1DBlockView<Scalar>(
            m_Data
            + discontinuousIndex * m_MaximalContinuousSize
            + firstContinuousElementIndex,
            lastContinuousElementIndex - firstContinuousElementIndex + 1);
  }

  [[nodiscard]]
  CDense1DBlockView<Scalar> GetContinuousRefView(
          size_t discontinuousIndex) {
    return GetContinuousRefView(
            discontinuousIndex,
            0,
            m_ContinuousSize - 1);
  }

  // including the diagonal element
  [[nodiscard]]
  CDense1DBlockView<Scalar> GetContinuousToDiagonalRefView(size_t discontinuousIndex) {
    return GetContinuousRefView(
            discontinuousIndex,
            0,
            discontinuousIndex);
  }

  [[nodiscard]]
  CDense2DBlockView GetSubMatrixRefView(
          size_t firstContinuousIndex,
          size_t lastContinuousIndex,
          size_t firstDiscontinuousIndex,
          size_t lastDiscontinuousIndex) {
    SubMatrixViewBoundsCheck(
            firstContinuousIndex,
            lastContinuousIndex,
            firstDiscontinuousIndex,
            lastDiscontinuousIndex);
    return CDense2DBlockView(
            m_Data
            + firstDiscontinuousIndex * m_MaximalContinuousSize
            + firstContinuousIndex,
            lastContinuousIndex - firstContinuousIndex + 1,
            lastDiscontinuousIndex - firstDiscontinuousIndex + 1,
            m_MaximalContinuousSize);
  }

  //template<template<typename> class ReturnType>
  [[nodiscard]]
  CDense2DBlockView GetSubMatrixConstRefView(
          size_t firstContinuousIndex,
          size_t lastContinuousIndex,
          size_t firstDiscontinuousIndex,
          size_t lastDiscontinuousIndex) const {
    SubMatrixViewBoundsCheck(
            firstContinuousIndex,
            lastContinuousIndex,
            firstDiscontinuousIndex,
            lastDiscontinuousIndex);
    return CDense2DBlockView(
            m_Data
            + firstDiscontinuousIndex * m_MaximalContinuousSize
            + firstContinuousIndex,
            lastContinuousIndex - firstContinuousIndex + 1,
            lastDiscontinuousIndex - firstDiscontinuousIndex + 1,
            m_MaximalContinuousSize);
  }

  void ChangeContinuousSize(size_t newContinuousSize) {
    if (newContinuousSize > m_MaximalContinuousSize)
      throw std::invalid_argument("CDense2DBlockView->ChangeContinuousSize(): "
                                  "newContinuousSize > m_MaximalContinuousSize");
    m_ContinuousSize = newContinuousSize;
  }

  void ChangeDiscontinuousSize(size_t newDiscontinuousSize) {
    m_DiscontinuousSize = newDiscontinuousSize;
  }

  void ChangeBothSizes(size_t newContinuousSize, size_t newDiscontinuousSize) {
    ChangeContinuousSize(newContinuousSize);
    ChangeDiscontinuousSize(newDiscontinuousSize);
  }

  [[nodiscard]]
  Scalar *GetData() const {
    return m_Data;
  }

  void DeleteData() {
    delete[] m_Data;
  }

  virtual
  ~CDense2DBlockView();

protected:
  void ContinuousViewBoundsCheck(
          size_t discontinuousIndex,
          size_t firstContinuousElementIndex,
          size_t lastContinuousElementIndex) const {
    if (lastContinuousElementIndex >= m_ContinuousSize)
      throw std::invalid_argument("CDense2DBlockView->ContinuousViewBoundsCheck(): "
                                  "lastContinuousElementIndex >= m_ContinuousSize");
    if (lastContinuousElementIndex < firstContinuousElementIndex)
      throw std::invalid_argument("CDense2DBlockView->ContinuousViewBoundsCheck(): "
                                  "lastContinuousElementIndex < firstContinuousElementIndex");
    if (discontinuousIndex >= m_DiscontinuousSize)
      throw std::invalid_argument("CDense2DBlockView->ContinuousViewBoundsCheck(): "
                                  "discontinuousIndex >= m_DiscontinuousSize");
  }

  void SubMatrixViewBoundsCheck(
          size_t firstContinuousIndex,
          size_t lastContinuousIndex,
          size_t firstDiscontinuousIndex,
          size_t lastDiscontinuousIndex) const {
    if (lastContinuousIndex >= m_ContinuousSize)
      throw std::invalid_argument("CDense2DBlockView->SubMatrixViewBoundsCheck(): "
                                  "lastContinuousIndex >= m_ContinuousSize");
    if (lastContinuousIndex < firstContinuousIndex)
      throw std::invalid_argument("CDense2DBlockView->SubMatrixViewBoundsCheck(): "
                                  "lastContinuousIndex < firstContinuousIndex");
    if (lastDiscontinuousIndex >= m_DiscontinuousSize)
      throw std::invalid_argument("CDense2DBlockView->SubMatrixViewBoundsCheck(): "
                                  "lastDiscontinuousIndex >= m_DiscontinuousSize");
    if (lastDiscontinuousIndex < firstDiscontinuousIndex)
      throw std::invalid_argument("CDense2DBlockView->SubMatrixViewBoundsCheck(): "
                                  "lastDiscontinuousIndex < firstDiscontinuousIndex");
  }

  // columnmajor: rowsCnt
  size_t m_ContinuousSize;
  // columnmajor: columnsCnt
  size_t m_DiscontinuousSize;
  // columnmajor: nextColumnShift, rowsCnt of the original matrix
  size_t m_MaximalContinuousSize;
  Scalar *m_Data;
};

inline
CDense2DBlockView::CDense2DBlockView(
        Scalar *data,
// columnmajor: rowsCnt
        size_t continuousSize,
// columnmajor: columnsCnt
        size_t discontinuousSize,
// columnmajor: nextColumnShift, rowsCnt of the original matrix; // discontinuousShift
        size_t maximalContinuousSize)
        : m_ContinuousSize(continuousSize),
          m_DiscontinuousSize(discontinuousSize),
          m_MaximalContinuousSize(maximalContinuousSize),
          m_Data(data) {
  if (continuousSize > maximalContinuousSize)
    throw std::invalid_argument("CDense2DBlockView constructor: "
                                "Warning: continuousSize > maximalContinuousSize");
}

inline
CDense2DBlockView::CDense2DBlockView(const CDense2DBlockView &other)
        : m_ContinuousSize(other.m_ContinuousSize),
          m_DiscontinuousSize(other.m_DiscontinuousSize),
          m_MaximalContinuousSize(other.m_MaximalContinuousSize),
          m_Data(other.m_Data) {
}

inline
CDense2DBlockView::CDense2DBlockView(CDense2DBlockView &&other)
        : m_ContinuousSize(other.m_ContinuousSize),
          m_DiscontinuousSize(other.m_DiscontinuousSize),
          m_MaximalContinuousSize(other.m_MaximalContinuousSize),
          m_Data(other.m_Data) {
  other.m_Data = nullptr;
}

inline
CDense2DBlockView &
CDense2DBlockView::operator=(CDense2DBlockView &&other) {
  m_ContinuousSize = other.m_ContinuousSize;
  m_DiscontinuousSize = other.m_DiscontinuousSize;
  m_MaximalContinuousSize = other.m_MaximalContinuousSize;
  m_Data = other.m_Data;
  other.m_Data = nullptr;
  return *this;
}

// copy from the other block view into *this block view
inline void
CDense2DBlockView::Assign(const CDense2DBlockView &other) {
  if (GetContinuousSize() != other.GetContinuousSize())
    throw std::invalid_argument("CDense2DBlockView->Assign(): "
                                "GetContinuousSize() != other.GetContinuousSize()");
  if (GetDiscontinuousSize() != other.GetDiscontinuousSize())
    throw std::invalid_argument("CDense2DBlockView->Assign(): "
                                "GetDiscontinuousSize() != other.GetDiscontinuousSize()");
  /*for(size_t j = 0; j < m_DiscontinuousSize; ++j)
    for(size_t i = 0; i < m_ContinuousSize; ++i)
      AtRef(i, j) = other.At(i, j);*/
  for (size_t j = 0; j < m_DiscontinuousSize; ++j)
    /*copy(&other.AtRef(0, j),
         &other.AtRef(m_ContinuousSize, j),
         &AtRef(0, j));*/
    // this optimization is maybe useless, the compilator could optimize it
    std::copy(other.m_Data + j * other.m_MaximalContinuousSize,
              other.m_Data + j * other.m_MaximalContinuousSize + m_ContinuousSize,
              m_Data + j * m_MaximalContinuousSize);
}

inline void
CDense2DBlockView::Assign(const Scalar *data, size_t continuousSize, size_t discontinuousSize,
                          size_t maximalContinuousSize) {
  if (continuousSize != maximalContinuousSize
      || m_ContinuousSize != m_MaximalContinuousSize)
    throw std::invalid_argument("CDense2DBlockView->Assign(const Scalar *, ...): "
                                "continuousSize != maximalContinuousSize "
                                "|| m_ContinuousSize != m_MaximalContinuousSize");
  if (GetContinuousSize() != continuousSize)
    throw std::invalid_argument("CDense2DBlockView->Assign(): "
                                "GetContinuousSize() != continuousSize");
  if (GetDiscontinuousSize() != discontinuousSize)
    throw std::invalid_argument("CDense2DBlockView->Assign(): "
                                "GetDiscontinuousSize() != discontinuousSize");
  std::copy(data,
            data + maximalContinuousSize * m_ContinuousSize,
            m_Data);
}

inline void
CDense2DBlockView::ToIdentity() {
  for (size_t iD = 0; iD < m_DiscontinuousSize; ++iD)
    for (size_t iC = 0; iC < m_ContinuousSize; ++iC) {
      if (iC == iD)
        AtRef(iC, iD) = 1;
      else
        AtRef(iC, iD) = 0;
    }
}

inline void
CDense2DBlockView::SetToZero() {
  for (size_t iD = 0; iD < m_DiscontinuousSize; ++iD)
    for (size_t iC = 0; iC < m_ContinuousSize; ++iC)
      AtRef(iC, iD) = 0;
}

inline
CDense2DBlockView::~CDense2DBlockView() = default;

#endif //EIGENVALUECALCULATORSOLVER_CDENSE2DBLOCKVIEW_H
