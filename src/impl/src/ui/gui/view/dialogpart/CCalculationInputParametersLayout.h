#ifndef EIGENVALUECALCULATORGUI_CCALCULATIONINPUTPARAMETERSLAYOUT_H
#define EIGENVALUECALCULATORGUI_CCALCULATIONINPUTPARAMETERSLAYOUT_H

#include "../../../../ui/uihelpers/CRestartedArnoldiInputArguments.h"

#include <tuple>
#include <optional>
#include <QAction>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QTextEdit>
#include <QSpinBox>
#include <QDoubleValidator>
#include <QMessageBox>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QElapsedTimer>
#include <QTimer>
#include <QFileDialog>
#include <QRadioButton>
#include <QDebug>

//#define DBG_PRINT_PARAMETERS

QT_BEGIN_NAMESPACE
class QAction;

class QDialogButtonBox;

class QGroupBox;

class QLabel;

class QLineEdit;

class QMenu;

class QMenuBar;

class QPushButton;

class QTextEdit;

class QComboBox;

class QSpinBox;

class QDoubleValidator;

class QMessageBox;

QT_END_NAMESPACE

class CCalculationInputParametersLayout : public QVBoxLayout {
Q_OBJECT

public:
  static constexpr size_t DefaultNumberOfWantedEigenvalues = 3;
  static constexpr size_t DefaultFactorizationSizeToRestartAt = 10;
  static constexpr size_t DefaultMaximalNumberOfIterations = 1000;

  CCalculationInputParametersLayout();

  std::optional<QString> ReadInputMatrixFilename();

  std::optional<ESortingCriterion> ReadSortingCriterion();

  std::optional<double> ReadDouble(
          QLineEdit *lineEdit,
          QDoubleValidator *doubleValidator,
          const QString &parameterName,
          const QString &minValue,
          const QString &maxValue);

  std::optional<double> ReadPrecision();

  std::optional<double> ReadIterativeOrthogonalizationEta();

  /*
          const std::string & inputMatrixFilename,
          size_t numberOfWantedEigenvalues,
          std::optional<size_t> factorizationSizeToRestartAt,
          std::optional<ESortingCriterion> sortingCriterion,
          std::optional<double> precision,
          std::optional<size_t> maximalNumberOfIterations,
          std::optional<uint8_t> maximalNumberOfOrthogonalizationIterations,
          std::optional<double> iterativeOrthogonalizationEta
   */
  // attention for the QMessageBox and return, maybe return bool and take the inputData struct as a parameter
  [[nodiscard]]
  bool ReadInputParameters(CRestartedArnoldiInputArguments &inputArguments);

protected:
  static constexpr int HorizontalSpacing = 3;
  static constexpr int VerticalSpacing = 5;
  static constexpr bool LabelTextWordWrap = false;

  std::string ToString(double number, uint8_t manipPrecision);

  void AddLabelWidgetRow(
          QFormLayout *&formLayout,
          QLabel *label,
          QWidget *widget);

  void CreateLabelDoubleLineEdit(
          QLabel *&label,
          const QString &labelText,
          QDoubleValidator *&doubleValidator,
          QLineEdit *&lineEdit,
          double bottomValue,
          double topValue,
          QString defaultValueText);

  void AddLabelDoubleLineEditRow(
          QFormLayout *&formLayout,
          QLabel *&label,
          const QString &labelText,
          QDoubleValidator *&doubleValidator,
          QLineEdit *&lineEdit,
          double bottomValue,
          double topValue,
          QString defaultValueText);

  void CreateLabelSpinBox(
          QLabel *&label,
          const QString &labelText,
          QSpinBox *&spinBox,
          int minValue,
          int defaultValue = INT_MIN,
          int maxValue = INT_MAX);

  void AddLabelSpinBoxRow(
          QFormLayout *&formLayout,
          QLabel *&label,
          const QString &labelText,
          QSpinBox *&spinBox,
          int minValue,
          int defaultValue = INT_MIN,
          int maxValue = INT_MAX);

  void CreateEigenvalueParametersFormLayout();

  void CreateNumberOfWantedEigenvaluesRow();

  void CreateMaximalArnoldiFactorizationSizeRow();

  void CreateMaximalNumberOfIterationsRow();

  void CreatePrecisionRow();

  void CreateMaximalNumberOfOrthogonalizationIterationsRow();

  void CreateIterativeOrthogonalizationEtaRow();

  void CreateSortingCriterionGroupBox();

  void CreateInputMatrixFilenameLayout();

  void InputMatrixFilenameFileDialog();

  QFormLayout *m_EigenvalueParametersFormLayout;

  QLabel *m_NumberOfWantedEigenvaluesLabel;
  QSpinBox *m_NumberOfWantedEigenvaluesSpinBox;

  QLabel *m_MaximalArnoldiFactorizationSizeLabel;
  QSpinBox *m_MaximalArnoldiFactorizationSizeSpinBox;

  QLabel *m_PrecisionLabel;
  QDoubleValidator *m_PrecisionValidator;
  QLineEdit *m_PrecisionLineEdit;

  QLabel *m_MaximalNumberOfIterationsLabel;
  QSpinBox *m_MaximalNumberOfIterationsSpinBox;

  QLabel *m_MaximalNumberOfOrthogonalizationIterationsLabel;
  QSpinBox *m_MaximalNumberOfOrthogonalizationIterationsSpinBox;
  QLabel *m_IterativeOrthogonalizationEtaLabel;
  QDoubleValidator *m_IterativeOrthogonalizationEtaValidator;
  QLineEdit *m_IterativeOrthogonalizationEtaLineEdit;

  QGroupBox *m_SortingCriterionGroupBox;
  QVBoxLayout *m_SortingCriterionLayout;
  QRadioButton *m_LargestMagnitude;
  QRadioButton *m_SmallestMagnitude;
  QRadioButton *m_LargestRealPart;
  QRadioButton *m_SmallestRealPart;

  QHBoxLayout *m_InputMatrixFilenameLayout;
  QLabel *m_InputMatrixFilenameLabel;
  QLineEdit *m_InputMatrixFilenameLineEdit;
  QPushButton *m_InputMatrixFilenameFileDialogBtn;
};

#endif //EIGENVALUECALCULATORGUI_CCALCULATIONINPUTPARAMETERSLAYOUT_H
