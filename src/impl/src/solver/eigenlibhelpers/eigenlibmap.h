#ifndef EIGENVALUECALCULATORSOLVER_EIGENLIBMAP_H
#define EIGENVALUECALCULATORSOLVER_EIGENLIBMAP_H

#include <Eigen/Core>

/// Map CColumnMatrix to Eigen Matrix
inline Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor>
mapToEigenMatrix(CColumnMatrix &columnMatrix) {
  if (columnMatrix.GetMaximalRowsCnt() == columnMatrix.GetRowsCnt())
    return Eigen::Map<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor>>(
            &columnMatrix.AtRef(0, 0),
            columnMatrix.GetRowsCnt(),
            columnMatrix.GetColumnsCnt());
  return Eigen::Map<Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor>, Eigen::Unaligned, Eigen::OuterStride<>>(
          &columnMatrix.AtRef(0, 0),
          columnMatrix.GetRowsCnt(),
          columnMatrix.GetColumnsCnt(),
          columnMatrix.GetMaximalRowsCnt());
}

/// Assign Eigen Matrix into CColumnMatrix
inline void fromEigenMatrix(CColumnMatrix &columnMatrix,
                            const Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::ColMajor> &eigenMatrix) {
  columnMatrix.Assign(eigenMatrix.data(), eigenMatrix.rows(), eigenMatrix.cols(), eigenMatrix.colStride());
}

#endif //EIGENVALUECALCULATORSOLVER_EIGENLIBMAP_H
