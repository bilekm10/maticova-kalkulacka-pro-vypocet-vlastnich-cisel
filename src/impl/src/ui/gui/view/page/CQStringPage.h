#ifndef EIGENVALUECALCULATORGUI_CQSTRINGPAGE_H
#define EIGENVALUECALCULATORGUI_CQSTRINGPAGE_H

#include <QObject>

QT_BEGIN_NAMESPACE
class QString;

QT_END_NAMESPACE

class CQStringPage {
public:
  [[nodiscard]] virtual
  QString Render() const = 0;

protected:
};

#endif //EIGENVALUECALCULATORGUI_CQSTRINGPAGE_H
