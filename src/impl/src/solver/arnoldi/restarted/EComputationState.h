#ifndef EIGENVALUECALCULATORSOLVER_ECOMPUTATIONSTATE_H
#define EIGENVALUECALCULATORSOLVER_ECOMPUTATIONSTATE_H

/// State of the restarted Arnoldi computation
enum class EComputationState {
  NotInitialized,
  NotInitializedCompressedFactorization,

  // Computing
  FactorizationExpanded,
  ShiftsSelected,
  Restarted,

  // End
  AlreadyComputed,
  ExceptionallyConverged,
  MaximalNumberOfIterationsReached,
  MaximalNumberOfIterationsReachedAndRestarted
};

#endif //EIGENVALUECALCULATORSOLVER_ECOMPUTATIONSTATE_H
