#ifndef EIGENVALUECALCULATORSOLVER_CROWVECTOR_H
#define EIGENVALUECALCULATORSOLVER_CROWVECTOR_H

#include "./CRowVectorConst.h"

/// Providing methods for a row vector
template<typename Scalar>
class CRowVector : public CRowVectorConst<Scalar> {
public:
  using CRowVectorConst<Scalar>::At;
  using CRowVectorConst<Scalar>::GetSize;
  using CRowVectorConst<Scalar>::AbsoluteNorm;
  using CRowVectorConst<Scalar>::EuclideanNorm;
  using CRowVectorConst<Scalar>::MaximumNorm;
  using CRowVectorConst<Scalar>::MultiplyToScalar;
  using CRowVectorConst<Scalar>::AtRef;
  using CRowVectorConst<Scalar>::SetTo;
  using CRowVectorConst<Scalar>::SetToZero;
  using CRowVectorConst<Scalar>::Assign;
  using CRowVectorConst<Scalar>::GetSubVectorConstRefView;

  CRowVector(Scalar *data, size_t dataSize);

  explicit CRowVector(CDense1DBlockView<Scalar> &&view);

  [[nodiscard]]
  CRowVector<Scalar> GetSubVectorRefView(size_t firstElementIndex, size_t lastElementIndex) {
    return this->template GetSubView<CRowVector>(firstElementIndex, lastElementIndex);
  }

  virtual
  ~CRowVector();


  CColumnVector<Scalar> &operator+=(const CColumnVectorConst<Scalar> &addend) {
    this->template AddAssignment(addend);
    return *this;
  }

  CColumnVector<Scalar> &
  operator-=(const CColumnVectorConst<Scalar> &subtrahend) { // *this is the minuend and the result of the difference
    this->template SubtractAssignment(subtrahend);
    return *this;
  }

  CColumnVector<Scalar> &operator*=(Scalar multiplier) {
    this->MultiplyAssignment(multiplier);
    return *this;
  }

  CColumnVector<Scalar> &operator/=(
          Scalar denominator) { // multiplication is said to be faster, maybe change from scalar division to multiplication
    this->DivideAssignment(denominator);
    return *this;
  }

  void ScalarMultipleAddScalarMultiple(
          const Scalar firstMultiplier,
          const Scalar secondMultiplier,
          const CColumnVectorConst<Scalar> &addend) { // *this is the first addend and the result of the scaled addition
    return CRowVectorConst<Scalar>::template ScalarMultipleAddScalarMultiple<CColumnVector<Scalar>>(
            firstMultiplier,
            secondMultiplier,
            addend);
  }

  // maybe could try to stabilize this computation, is used a lot for matrix-vector multiplication
  void AddScalarMultiple(
          const Scalar multiplier,
          const CColumnVectorConst<Scalar> &addend) { // *this is the first addend and the result of the scaled addition
    return CRowVectorConst<Scalar>::template AddScalarMultiple(
            multiplier,
            addend);
  }

  void AssignScalarMultiple(
          const Scalar multiplier,
          const CColumnVectorConst<Scalar> &addend) { // *this is the first addend and the result of the scaled addition
    return CRowVectorConst<Scalar>::template AssignScalarMultiple(
            multiplier,
            addend);
  }

  /*template<typename VectorType>
  Scalar MultiplyToScalar(const VectorType & multiplicand) const
  {
    return CRowVectorConst<Scalar>::template MultiplyToScalar(
            multiplicand);
  }*/
protected:
};

template<typename Scalar>
CRowVector<Scalar>::CRowVector(Scalar *data, size_t dataSize)
        : CRowVectorConst<Scalar>(data,
                                  dataSize) {
}

template<typename Scalar>
CRowVector<Scalar>::CRowVector(CDense1DBlockView<Scalar> &&view)
        : CRowVectorConst<Scalar>(std::move(view)) {
}

template<typename Scalar>
CRowVector<Scalar>::~CRowVector() = default;

#endif //EIGENVALUECALCULATORSOLVER_CROWVECTOR_H
