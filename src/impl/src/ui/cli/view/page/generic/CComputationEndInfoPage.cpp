#include "./CComputationEndInfoPage.h"

CComputationEndInfoPage::CComputationEndInfoPage(const std::string &message)
        : m_Message(message) {
}

[[nodiscard]]
std::string CComputationEndInfoPage::Render() const {
  return "--------------------------------------------------\n" +
         m_Message;
}
