#ifndef EIGENVALUECALCULATORSOLVER_CITERATIVEORTHOGONALIZATION_H
#define EIGENVALUECALCULATORSOLVER_CITERATIVEORTHOGONALIZATION_H

#include <numbers>
#include <stdexcept>

#include "../../model/vector/column/CColumnVectorHolding.h"

/// Orthogonalize a vector against a matrix storing the orthogonalization coefficients using an iterative orthogonalization
template<typename BasicOrthogonalization>
class CIterativeOrthogonalization {
public:
  explicit CIterativeOrthogonalization(
          BasicOrthogonalization basicOrthogonalization,
          size_t maximalCoefficientsSize,
          uint8_t maximalNumberOfIterations = DefaultMaximalNumberOfOrthogonalizationIterations,
          double eta = DefaultEta);

  template<typename MatrixType>
  [[nodiscard]]
  bool Orthogonalize(const MatrixType &matrixToOrthogonalizeAgainst,
                     CColumnVector<double> &vectorToBeOrthogonalized,
                     CColumnVector<double> &coefficients,
                     double &newNorm);

  static constexpr const uint8_t DefaultMaximalNumberOfOrthogonalizationIterations = 3;
  /// Daniel et al. 1976, reorthogonalize while (1/sqrt(2)) * ||afterOrthogVec||_2 <= ||beforeOrthogVec||_2
  static const constexpr double DgksEta = 1 / std::numbers::sqrt2;
  static const constexpr double DefaultEta = DgksEta;
protected:
  BasicOrthogonalization m_BasicOrthogonalization;
  uint8_t m_MaximalNumberOfIterations;
  double m_Eta;
  double m_TwoNormToTreatAsZero;
  CColumnVectorHolding<double> m_CoefficientsAux;
};

template<typename BasicOrthogonalization>
CIterativeOrthogonalization<BasicOrthogonalization>::CIterativeOrthogonalization(
        BasicOrthogonalization basicOrthogonalization,
        size_t maximalCoefficientsSize,
        uint8_t maximalNumberOfIterations,
        double eta)
        : m_BasicOrthogonalization(std::move(basicOrthogonalization)),
          m_MaximalNumberOfIterations(maximalNumberOfIterations),
          m_Eta(eta),
          m_TwoNormToTreatAsZero(0),
          m_CoefficientsAux(maximalCoefficientsSize) {
  if (maximalNumberOfIterations <= 0)
    throw std::invalid_argument("CIterativeOrthogonalization: maximalNumberOfIterations <= 0, "
                                "so no orthogonalization would be performed");
  if (eta > 1)
    throw std::invalid_argument("CIterativeOrthogonalization: eta > 1, "
                                "so redundant orthogonalization steps would be performed. "
                                "If orthogonality to working precision is needed, pass eta=1.");
  if (eta < 0)
    throw std::invalid_argument("CIterativeOrthogonalization: eta < 0, "
                                "if no reorthogonalization is wanted, let eta be = 0.");
}

template<typename BasicOrthogonalization>
template<typename MatrixType>
[[nodiscard]]
bool CIterativeOrthogonalization<BasicOrthogonalization>::Orthogonalize(const MatrixType &matrixToOrthogonalizeAgainst,
                                                                        CColumnVector<double> &vectorToBeOrthogonalized,
                                                                        CColumnVector<double> &coefficients,
                                                                        double &newNorm) {
  coefficients.SetToZero();
  double oldNorm = vectorToBeOrthogonalized.EuclideanNorm();
  if (oldNorm <= m_TwoNormToTreatAsZero) {
    newNorm = oldNorm;
    return false;
  }
  CColumnVector<double> currentCoefficients(m_CoefficientsAux.GetSubVectorRefView(0, coefficients.GetSize() - 1));
  for (unsigned int i = 0; i < m_MaximalNumberOfIterations; ++i) {
    if (!m_BasicOrthogonalization.Orthogonalize(
            matrixToOrthogonalizeAgainst,
            vectorToBeOrthogonalized,
            currentCoefficients,
            newNorm)
        || newNorm <= m_TwoNormToTreatAsZero) {
      return false; // linear dependency, newNorm = 0
    }
    coefficients += currentCoefficients;
    if (m_Eta * oldNorm <= newNorm) {
      return true; // successfully orthogonalized
    }
    oldNorm = newNorm;
  }
  return false; // according to the chosen eta and maximalNumberOfIterations, this vector is linearly dependent on the others, so could not be orthogonalized
}

#endif //EIGENVALUECALCULATORSOLVER_CITERATIVEORTHOGONALIZATION_H
